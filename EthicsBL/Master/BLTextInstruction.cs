﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using OnlineTrainingDL.Master;
using OnlineTrainingPL.Master;
using OnlineTrainingPL;

namespace OnlineTrainingBL
{

    public class BLTextInstruction
    {
        DLTextInstruction _objDlTextInstruction = new DLTextInstruction();

        public DataSet GetTextInstructions(PLTextInstruction objTextInstruction)
        {
            try
            {
                return _objDlTextInstruction.GetTextInstructions(objTextInstruction);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetNoofAttempts(PLTextInstruction objTextInstruction)
        {
            try
            {
                return _objDlTextInstruction.GetNoofAttempts(objTextInstruction);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] SaveUpdateTextInstructions(PLTextInstruction objTextInstruction)
        {
            try
            {
                return _objDlTextInstruction.SaveUpdateTextInstructions(objTextInstruction);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
