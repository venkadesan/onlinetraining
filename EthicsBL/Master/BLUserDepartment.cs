﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using OnlineTrainingDL.Master;
using OnlineTrainingPL.Master;
using OnlineTrainingPL;


namespace OnlineTrainingBL
{
    public class BLUserDepartment
    {
        DLUserDepartment _objDlUserDepartment = new DLUserDepartment();

        public DataSet GetDepartmentDetails(PLUserDepartment objUserDepartment)
        {
            try
            {
                return _objDlUserDepartment.GetDepartmentDetails(objUserDepartment);
            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }

        public string[] InsertUpdateUserDepartment(PLUserDepartment objUserDepartment)
        {
            try
            {
                return _objDlUserDepartment.InsertUpdateUserDepartment(objUserDepartment);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataSet GetUserBind(PLUserDepartment objUserDepartment)
        {
            try
            {
                return _objDlUserDepartment.GetUserBind(objUserDepartment);
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
