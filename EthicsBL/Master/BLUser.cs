﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OnlineTrainingPL.Master;
using System.Data;
using OnlineTrainingDL.Master;

namespace OnlineTrainingBL
{
   public class BLUser
    {
       DLUser objUser = new DLUser();

       public DataSet UserLoginGet(PLUser User)
       {
           return objUser.UserLoginGet(User);
       }

       public DataSet GetUserBind(PLUser User)
       {
           return objUser.GetUserBind(User);
       }

      

    }
}
