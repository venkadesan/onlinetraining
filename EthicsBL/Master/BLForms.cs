﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using OnlineTrainingDL.Master;
using OnlineTrainingPL;

namespace OnlineTraining.Master
{
    public class BLForms
    {
        private DLForms objDlForms = new DLForms();
        DataSet dsmainmenu = new DataSet();

        public DataSet GetMainMenus(PLForms forms)
        {
            dsmainmenu = objDlForms.GetMainMenus(forms);
            return dsmainmenu;
        }

        public void InsertMainMenus(PLForms forms)
        {
            objDlForms.InsertMainMenus(forms);
        }

        public DataSet GetSubMenus(PLForms forms)
        {
            dsmainmenu = objDlForms.GetSubMenus(forms);
            return dsmainmenu;
        }

        public void InsertSubMenus(PLForms forms)
        {
            objDlForms.InsertSubMenus(forms);
        }

        public DataSet GetMainMenusDet(PLForms forms)
        {
            dsmainmenu = objDlForms.GetMainMenusDet(forms);
            return dsmainmenu;
        }

        public DataSet GetSubMenusDet(PLForms forms)
        {
            dsmainmenu = objDlForms.GetSubMenusDet(forms);
            return dsmainmenu;
        }

        public void DeleteMainMenus(PLForms forms)
        {
            objDlForms.DeleteMainMenus(forms);
        }

        public void DeleteSubMenus(PLForms forms)
        {
            objDlForms.DeleteSubMenus(forms);
        }
    }
}
