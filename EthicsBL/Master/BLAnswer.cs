﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OnlineTrainingPL;
using OnlineTrainingDL;
using System.Data;

namespace OnlineTrainingBL
{
    public class BLAnswer
    {
        DLAnswer objDL = new DLAnswer();

        public string InsertUpdateAnswer(PLAnswer Answer)
        {
            string Value = objDL.InsertUpdateAnswer(Answer);
            return Value;
        }

        public string DeleteAnswerDetails(PLAnswer Answer)
        {
            string Value = objDL.DeleteAnswerDetails(Answer);
            return Value;
        }

        public DataSet GetAnswerDetails(PLAnswer Answer)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetAnswerDetails(Answer);
            return Ds;
        }

        public string[] InsertAnswerKey(PLAnswer Answer)
        {
            string[] Values = objDL.InsertAnswerKey(Answer);
            return Values;
        }

        public DataSet GetAnswerKeyDetails(PLAnswer Answer)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetAnswerKeyDetails(Answer);
            return Ds;
        }

        public string ImportAnswer(PLAnswer Answer)
        {
            if (objDL.ImportAnswer(Answer) > 0)
            {
                return "Save successfully";
            }
            else
            {
                return "";
            }
        }
    }
}
