﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OnlineTrainingDL.Master;
using OnlineTrainingPL.Master;
using System.Data;

namespace OnlineTrainingBL
{
    public class BLTest
    {
        DLTest objDL = new DLTest();

        public string[] InsertUpdateTest(PLTest Testr)
        {
            string[] Values = objDL.InsertUpdateTest(Testr);
            return Values;
        }

        public string[] InsertUpdateTestSpec(PLTest Testr)
        {
            string[] Values = objDL.InsertUpdateTestSpec(Testr);
            return Values;
        }

        public DataSet CheckResults(PLTest Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.CheckResults(Topic);
            return Ds;
        }

        public DataSet GetAnswerResults(PLTest Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetAnswerResults(Topic);
            return Ds;
        }

        public void UpdatePercentage(PLTest Topic)
        {
          objDL.UpdatePercentage(Topic);          
        }

        public int InsertMainLogin(PLTest Topic)
        {
           int value = objDL.InsertMainLogin(Topic);
           return value;
        }

        public int InsertDeclaration(PLTest Topic)
        {
            int value = objDL.InsertDeclaration(Topic);
            return value;
        }

        public DataSet GetMainLogin(PLTest Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetMainLogin(Topic);
            return Ds;
        }
    }
}
