﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingDL;
using OnlineTrainingPL;

namespace OnlineTrainingBL
{
    public class BLQuestion
    {
        DLQuestion objDL = new DLQuestion();

        public string ImportQuestion(PLQuestion Question)
        {
            if (objDL.ImportQuestion(Question) > 0)
            {
                return "Save successfully";
            }
            else
            {
                return "Questions already exist";
            }
        }

        public int InsertUpdateQuestion(PLQuestion Question)
        {
            int Value = objDL.InsertUpdateQuestion(Question);
            return Value;
        }
      
        public DataSet GetProductDetails(PLQuestion Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetProductDetails(Question);
            return Ds;
        }
      
        public string DeleteQuestionDetails(PLQuestion Question)
        {
            string Value = objDL.DeleteQuestionDetails(Question);
            return Value;
        }

        public DataSet GetQuestionDetails(PLQuestion Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetQuestionDetails(Question);
            return Ds;
        }

        public DataSet GetQuestionAnswerReports(PLQuestion Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetQuestionAnswerReports(Question);
            return Ds;
        }

        public DataSet GetQuestionAnswerReport(PLQuestion Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetQuestionAnswerReport(Question);
            return Ds;
        }

        public DataSet GetALLQuestionDetails(PLQuestion Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetALLQuestionDetails(Question);
            return Ds;
        }

        public DataSet GetALLTopicQuestionDetails(PLQuestion Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetALLTopicQuestionDetails(Question);
            return Ds;
        }
    }
}
