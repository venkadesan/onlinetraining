﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using OnlineTrainingDL.Master;
using OnlineTrainingPL;

namespace OnlineTrainingBL
{
   public class BLDepartment
    {
        DLDepartment _objDlDepartment = new DLDepartment();

        public DataSet GetDepartmentDetails(PLDepartment objDepartment)
        {
            try
            {
                return _objDlDepartment.GetDepartmentDetails(objDepartment);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] InsertUpdateDepartment(PLDepartment objDepartment)
        {
            try
            {
                return _objDlDepartment.InsertUpdateDepartment(objDepartment);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DeleteDepartmentDetails(PLDepartment objDepartment)
        {
            try
            {
                return _objDlDepartment.DeleteDepartmentDetails(objDepartment);
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
