﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using OnlineTrainingDL.Master;
using EthicsPL.Master;
using OnlineTrainingPL;

namespace OnlineTrainingBL.Master
{
    public class BLDashboard
    {
        private DLDashboard objDlDashboard = new DLDashboard();

        public DataSet GetDepartmentTopics(int grouID)
        {
            try
            {
                return objDlDashboard.GetDepartmentTopics(grouID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetDepartmentDetails(PLDashboard objPlDashboard)
        {
            try
            {
                return objDlDashboard.GetDashboardDetails(objPlDashboard);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataSet GetDashboardRawDetails(PLDashboard objPlDashboard)
        {
            try
            {
                return objDlDashboard.GetDashboardRawDetails(objPlDashboard);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
