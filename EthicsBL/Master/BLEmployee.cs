﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OnlineTrainingDL.Master;
using OnlineTrainingPL.Master;
using System.Data;
using OnlineTrainingPL;

namespace OnlineTraining.Master
{
    public class BLEmployee
    {
        DLEmployee objDL = new DLEmployee();

        public string[] InsertUpdateEmployee(PLEmployee Employeer)
        {
            string[] Values = objDL.InsertUpdateEmployee(Employeer);
            return Values;
        }

        public DataSet GetPassReports(PLEmployee Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetPassReports(Question);
            return Ds;
        }

        public DataSet GetSummaryReports(PLEmployee Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetSummaryReports(Question);
            return Ds;
        }

        public DataSet GetParticaptesDetails(PLEmployee Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetParticaptesDetails(Question);
            return Ds;
        }

        public DataSet GetParticaptesPassDetails(PLEmployee Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetParticaptesPassDetails(Question);
            return Ds;
        }

        public DataSet GetParticipantsList(PLEmployee Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetParticipantsList(Question);
            return Ds;
        }

        public DataSet GetParticaptesFailDetails(PLEmployee Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetParticaptesFailDetails(Question);
            return Ds;
        }

        public DataSet MainLoginUserID(PLEmployee Question)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.MainLoginUserID(Question);
            return Ds;
        }
    }
}
