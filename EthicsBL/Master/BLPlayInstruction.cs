﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using OnlineTrainingDL.Master;
using OnlineTrainingPL.Master;
using OnlineTrainingPL;


namespace OnlineTrainingBL
{
    public class BLPlayInstruction
    {
        DLPlayInstruction _objDlPlayInstruction = new DLPlayInstruction();

        public DataSet GetInstructions(PLPlayInstruction objPlayInstruction)
        {
            try
            {
                return _objDlPlayInstruction.GetInstructions(objPlayInstruction);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] SaveUpdateInstructions(PLPlayInstruction objPlayInstruction)
        {
            try
            {
                return _objDlPlayInstruction.SaveUpdateInstructions(objPlayInstruction);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
