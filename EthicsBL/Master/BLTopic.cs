﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingDL;
using OnlineTrainingPL;

namespace OnlineTrainingBL
{
    public class BLTopic
    {
        DLTopic objDL = new DLTopic();

        public int InsertUpdateTopic(PLTopic Topic)
        {
            int Value = objDL.InsertUpdateTopic(Topic);
            return Value;
        }

        public string DeleteTopicDetails(PLTopic Topic)
        {
            string Value = objDL.DeleteTopicDetails(Topic);
            return Value;
        }

        public DataSet GetTopicDetails(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetTopicDetails(Topic);
            return Ds;
        }

        public DataSet GetCurrentTopicQAReports(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetCurrentTopicQAReports(Topic);
            return Ds;
        }

        public DataSet GetTopicByActiveStatus(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetTopicByActiveStatus(Topic);
            return Ds;
        }

        public DataSet GetUserByUserId(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetUserByUserId(Topic);
            return Ds;
        }

        public DataSet GetActiveTopicDetails(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetActiveTopicDetails(Topic);
            return Ds;
        }

        public int UpdateTopicStatus(PLTopic Topic)
        {
            int Value = objDL.UpdateTopicStatus(Topic);
            return Value;
        }

        public string CheckActiveTopic(int mode, PLTopic Topic)
        {
            string Value = objDL.CheckActiveTopic(mode, Topic);
            return Value;
        }

        public DataSet CheckPassedstatus(String EmailID, PLTopic Topic)
        {
            return objDL.CheckPassedstatus(EmailID, Topic);
        }

        public int GetUser(string emailID)
        {
            return objDL.Getuser(emailID);
        }

        public DataSet GetCompany(int groupID)
        {
            return objDL.GetCompany(groupID);
        }

        public DataSet GetLocation(int companyID)
        {
            return objDL.GetLocation(companyID);
        }

        public DataSet GetAllScore(int userID, int topicID)
        {
            return objDL.GetAllScore(userID, topicID);
        }

        public int GettotalAttempt(string emailID)
        {
            return objDL.GettotalAttempt(emailID);
        }

        #region CommonTopic

        public int InsertUpdateCommonTopic(PLTopic Topic)
        {
            int Value = objDL.InsertUpdateCommonTopic(Topic);
            return Value;
        }

        public DataSet GetCommonTopicDetails(PLTopic Topic)
        {
            return objDL.GetCommonTopicDetails(Topic);
        }

        public string DeleteCommonTopicDetails(PLTopic Topic)
        {
            string Value = objDL.DeleteCommonTopicDetails(Topic);
            return Value;
        }

        public int InsertUpdateTopicFromCommon(PLTopic Topic)
        {
            int Value = objDL.InsertUpdateTopicFromCommon(Topic);
            return Value;
        }

        public DataSet GetAssignedTopics(PLTopic Topic)
        {
            return objDL.GetAssignedTopics(Topic);
        }



        public int AddCompanyTopicAssign(PLTopic Topic)
        {
            int Value = objDL.AddCompanyTopicAssign(Topic);
            return Value;
        }

        public DataSet GetTopicByCompanyID(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetTopicByCompanyID(Topic);
            return Ds;
        }

        public DataSet GetTopicNameByUserID(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetTopicNameByUserID(Topic);
            return Ds;
        }

        public DataSet getTopicByDepartment(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.getTopicByDepartment(Topic);
            return Ds;
        }

        public DataSet GetActiveTopicDetailsWithGroup(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetActiveTopicDetailsWithGroup(Topic);
            return Ds;
        }

        public DataSet GetCurrentTopicQAReportsWithGroup(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetCurrentTopicQAReportsWithGroup(Topic);
            return Ds;
        }

        public void InsertTrainingDetails(PLTopic Topic)
        {
            objDL.InsertTrainingDetails(Topic);
        }

        #endregion


        #region "Check Maximum Attempts"

        public DataSet CheckMaximumAttempts(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.CheckMaximumAttempts(Topic);
            return Ds;
        }

        #endregion

        public DataSet GetAnswerDetails(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetAnswerDetails(Topic);
            return Ds;
        }

        public DataSet GetRoleName(PLTopic Topic)
        {
            DataSet Ds = new DataSet();
            Ds = objDL.GetRoleName(Topic);
            return Ds;
        }

    }
}
