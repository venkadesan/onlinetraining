﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" 
    CodeBehind="OnlineTraining.aspx.cs" Inherits="OnlineTraining.OnlineTraining" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="Stylesheet" />
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>
    <link href="App_Themes/js/compiled/flipclock.css" rel="stylesheet" type="text/css" />
    <%--   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>--%>
    <script src="App_Themes/js/compiled/flipclock.js" type="text/javascript"></script>
    <script type="text/javascript">    
          
        function Confirm() {
            var x = confirm("You have not cleared the test successfully.Please try again");
            if (x)
                return true;
            else
                return false;
        }

        //var url = "http://www.codemiles.com";
        var seconds = 0;
        function redirect() {

            seconds = $('input[id$=totalmin]').val();
            redirect1();
        }
        function redirect1() {
            if (seconds <= 0) {

                document.getElementById('<%= btnautosave.ClientID%>').click();

                //            window.location = "default.aspx"                

            } else {
                seconds--;

                $('input[id$=totalmin]').val(seconds);
                // multiply by 1000 because Date() requires miliseconds
                var date = new Date(seconds * 1000);
                var hh = date.getUTCHours();
                var mm = date.getUTCMinutes();
                var ss = date.getSeconds();
                // If you were building a timestamp instead of a duration, you would uncomment the following line to get 12-hour (not 24) time
                // if (hh > 12) {hh = hh % 12;}
                // These lines ensure you have two-digits
                if (hh < 10) { hh = "0" + hh; }
                if (mm < 10) { mm = "0" + mm; }
                if (ss < 10) { ss = "0" + ss; }
                // This formats your string to HH:MM:SS
                var t = hh + ":" + mm + ":" + ss;

                document.getElementById("pageInfo").innerHTML = t
                setTimeout("redirect1()", 1000)
            }
        }      

    </script>
    <style type="text/css">
        br
        {
            clear: both;
        }
        .cntSeparator
        {
            font-size: 54px;
            margin: 10px 7px;
            color: #000;
        }
        
        .desc
        {
            margin: 7px 3px;
        }
        .desc div
        {
            float: left;
            font-family: Arial;
            width: 70px;
            margin-right: 65px;
            font-size: 13px;
            font-weight: bold;
            color: #000;
        }
    </style>
    <style>
        pre p
        {
            float: left;
            text-align: justify;
            margin-left: 50px;
            margin-right: 50px;
            width: 90%;
        }
        pre span
        {
            text-align: center;
            font-weight: bold;
            margin-left: 300px;
            font-size: 14px;
            text-decoration: underline;
        }
        .span1
        {
            text-align: center;
            font-weight: bold;
            margin-left: 200px;
            font-size: 14px;
            text-decoration: underline;
        }
        .test
        {
            margin: 0;
            padding: 10px 0;
            font-size: 18px;
            font-weight: 300;
            text-transform: uppercase;
            display: inline-block;
            vertical-align: middle;
            line-height: 28px;
            font-family: 'Roboto' , 'Helvetica' , Arial, sans-serif;
            color: #625748;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div runat="server" id="divFirstPart" >
        <div class="submenu">
            <span class="test">
                <asp:Label ID="lblTopicName" runat="server" Font-Bold="true" />
                 <label id="pageInfo" style="font-size: 38px;"></label>
                <asp:HiddenField runat="server" ID="totalmin" />
            </span>
            <%--<div id="pageInfo" >--%>
        </div>
        <!-- /submenu -->
        <!-- content main container -->

        <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
            runat="server" id="divmsg" visible="false">
            <asp:Label runat="server" ID="lblError"></asp:Label></div>
        <div class="main">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <asp:Repeater ID="rptrAnswer" runat="server" DataSourceID="" OnItemDataBound="rptrAnswer_ItemDataBound">
                            <HeaderTemplate>
                                <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                    <tr style="background-color: #808080; color: White;">
                                        <th width="5%">
                                            <asp:Label Text="Q.No" runat="server" ID="lSNo" />
                                        </th>
                                        <th>
                                            <asp:Label Text="Question" runat="server" ID="Label6" />
                                        </th>
                                        <%-- <th>
                                            <asp:Label Text="Result" runat="server" ID="Label1" />
                                        </th>--%>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr  class="new-color">
                                    <td>
                                        <asp:Label ID="lblNo" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("QuestionName") %>' />
                                    </td>
                                    <asp:ImageButton runat="server" ID="imgResult" Height="20" Width="20" ImageUrl="App_Themes/images/add.png"
                                        Visible="false" />
                                </tr>
                                <tr>
                                    <td colspan="3" style="padding-left: 60px;">
                                        <asp:Repeater ID="rptrAnswer1" runat="server" DataSourceID="">
                                            <ItemTemplate>
                                                <div style="float: left; width: 100%; line-height: 30px;">
                                                    <asp:CheckBox runat="server" ID="chkAnswer" />
                                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("AnswerName") %>' /><br />
                                                    <asp:Label ID="totalAws" CssClass="total" Text='<%# Eval("totalanswer") %>' Style="display: none"
                                                        runat="server"></asp:Label>
                                                    <asp:HiddenField runat="server" ID="hAnswerID" Value='<%# Eval("AnswerID")%>' />
                                                    <div>
                                            </ItemTemplate>
                                            <%--<FooterTemplate>
                                                </table>
                                            </FooterTemplate>--%>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                                <asp:HiddenField runat="server" ID="hQuestionID" Value='<%# Eval("QuestionID")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:HiddenField runat="server" ID="hResolutionTime" Value='<%# Eval("ResolutionTime")%>' />
                        <asp:HiddenField runat="server" ID="hTopicID" Value='<%# Eval("TopicID")%>' />
                        <asp:HiddenField runat="server" ID="hPassPercentage" Value='<%# Eval("PassPercentage")%>' />
                    </div>
                    <div class="table-responsive">
                        <div class="table-responsive">
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="Div1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                                    <div class="form-group form-footer">
                                        <div class="col-sm-offset-5 col-sm-6">
                                            <asp:Button ID="btnautosave" Text="Submit" Style="display: none" runat="server" class="btn btn-primary"
                                                OnClick="btnautosave_Click" />
                                            <asp:Button ID="Button2" Text="Submit Test" runat="server" class="btn btn-primary" OnClick="bAnswerKeySave_Click" />
                                           <%-- <asp:Button ID="Button3" Text="Cancel" runat="server" class="btn btn-default" OnClick="CancelAnswerKey_Click" />--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--<div runat="server" visible="false" id="divSecondPart">
        <center>
            <img src="App_Themes/images/You-Did-It-Congratulations-Banner-Picture.png" width="100%" />
            <br />
            <h1>
                Please click the below button to show your score !
            </h1>
            <asp:HiddenField runat="server" ID="HiddenField1" />
            <asp:Button ID="Button4" runat="server" Text="Show Score" class="btn btn-primary"
                OnClick="Unnamed1_Click" />
        </center>
        <%--        <asp:PlaceHolder runat="server" ID="divPlaceHolders">
            <pre style="background-color: White; border: 0px; font-family: Verdana; color: Black;">
                 <span>Compliance with the Code of Business Ethics</span>

       <p>
           JLL is committed to a corporate culture that embraces and promotes strong principles
           of business and professional ethics at every level. Ethical practices are inherent
           in our values, mission and strategy, and guide all of our interactions with clients,
           customers, vendors and employees. Ethical behaviour is a core responsibility of
           each and every employee of JLL. Our Code of Business Ethics contains ethics policies
           and basic principles to guide conduct and is required to be adhered to in both its
           spirit and letter.
           <br />
           “JLL” means any or more of (i) Jones Lang LaSalle Property Consultants (India) Private
           Limited, (ii) Jones Lang LaSalle Building Operations (India) Private Limited, (iii)
           Jones Lang LaSalle group of companies, and (iv) Homebay Residential Private Limited.
           <br />
           We request you to certify compliance with Code of Business Ethics as per your terms
           of employment by signing the following undertaking/declaration.
       </p>
                      <span>Employee Declaration </span>

<p>
    To, Chief Ethics Officer Jones Lang LaSalle Property Consultants (India) Private
    Limited Level 16, Tower C, Epitome, Building No. 5, DLF Cyber City, DLF Phase III,
    Gurgaon – 122 002, Haryana</p>

<span class="span1">DECLARATION PART 1: UNDERSTANDING OF JLL’S CODE OF BUSINESS ETHICS</span>

<p>
    I,
    <label runat="server" id="lblNames" clientidmode="Static">
    </label>
    EMP.Code :
    <label runat="server" id="lblEmpCode" clientidmode="Static">
    </label>
    , an employee of Jones Lang LaSalle Property Consultants (India) Private Limited,
    currently working as
    <label runat="server" id="lblDesignations" clientidmode="Static">
    </label>
    at
    <label runat="server" id="lblClinetNames" clientidmode="Static">
    </label>
    declare that I have read the JLL’s Code of Business Ethics and have undergone the
    Ethics assessment.
    <br />
    I have understood what is laid down in the Code of Business Ethics and how it relates
    to various aspects of facility and property management services and my duties of
    employment.
</p>
<span class="span1">DECLARATION PART 2: ASSURANCE OF ETHICAL CONDUCT IN THE PRESENT
    AND PAST</span>
<p>
    Since the time I have been employed with JLL: • I have not at any time violated
    any of the terms of the Code of Business Ethics. • I have reported all possible
    violations of the Code of Business Ethics to the Ethics Officer or via Ethics Hotline
    or Ethics website or to the HR Business Partner or to my supervisor. • I have used
    all communication systems (telephones, mobile phones, computers internet access
    etc.) provided by JLL or its clients as per Company/Client’s policy(s) and have
    not viewed, received or sent inappropriate materials which I was not supposed to.
    • I have taken all necessary steps to protect confidential information and intellectual
    property of JLL and/or its clients. • I have not indulged in any abusive, harassing
    or offensive conduct (verbal, physical or visual) or any acts of violence or physical
    intimidation or retaliatory treatment in response to any complaint of harassment
    made against me. • I have not requested or accepted any improper gifts (whether
    in cash, entertainment or other inducements) or any bribe from JLL’s clients, vendors
    or subcontractors or their employees, officers or directors, except as a token gift,
    business meal or entertainment of small or nominal in value i.e. (INR 2500 or less).
    • In case I have made any purchasing decisions, they have been based solely in the
    best interests of JLL or its clients, and consistent with JLL’s/client’s procurement
    policies including obtaining at least three quotations from unrelated vendors and
    fair value evaluation. I have made full disclosures and obtained appropriate approvals
    before making purchasing commitments. • I have dealt fairly with JLL’s customers
    and suppliers. I have not taken unfair advantage of anyone through manipulation,
    concealment, misrepresentation of material facts or any other unfair dealing practice
    and have not entered into any improper or illegitimate transactions. • I have not
    offered, made or promised to make any illegal, improper or questionable payment
    or commitment of personal or JLL’s funds or other valuable consideration to vendors,
    government officials, clients or anyone else for purpose of procuring goods or services,
    or obtaining or retaining business or securing any improper advantage, directly
    or indirectly.
</p>

<span class="span1">DECLARATION PART 3: ASSURANCE OF ETHICAL CONDUCT IN THE FUTURE</span>
<p>
    Going forward, I will not breach any of the terms of the Code of Business Ethics
    at long as I am in employment with JLL (as mentioned but not limited to the examples
    above).
    <br />
    I also understand that any failure to report any violations of the Code of Business
    Ethics at any point of time will be held against me and may have consequences that
    will adversely affect my career, may invite penalties or even cost me my employment
    <br></br> I clearly also understand that any violations of the Code of Business Ethics
    by me at any point of time will have severe repercussions on my terms of employment
    and may even result in cessation of my employment and dismissal with disgrace from
    JLL. <br></br> I acknowledge by my signature below that I have read, understood and
    agree to all terms of this declaration/ letter.
</p>
<asp:HiddenField runat="server" ID="hID" />
<div style="margin-left: 110px;">
    <asp:Button ID="Button1" runat="server" Text="Acknowledge" class="btn btn-primary"
        OnClick="Unnamed1_Click" />
</div>
    </pre>
        </asp:PlaceHolder>--%>
   <%-- </div>--%>
    <script type="text/javascript">    

        $('input:checkbox').click(function () {
            //        if ($(this).is(':checked')) {

            var tr = $(this).closest('tr');
            var acount = $(tr).find('span[id*="totalAws"]').first().text();


            var checkboxes = $(this).closest('td').find(':checkbox');
            var count = 0;
            $(checkboxes).each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
                if (sThisVal == 'on') {
                    count++;
                }
            });
            if (acount == count) {
                $(checkboxes).each(function () {
                    var sThisVal = (this.checked ? $(this).val() : "");
                    if (sThisVal == 'on') {
                        $(this).attr("disabled", false);
                    }
                    else {
                        $(this).attr("disabled", true);
                    }
                });

            }
            else {
                $(checkboxes).each(function () {
                    $(this).attr("disabled", false);

                });
            }
            //}

        });
    </script>
  
--%></asp:Content>
