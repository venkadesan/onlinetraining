﻿<%@ Page Title="" Language="C#" MasterPageFile="~/test.Master" AutoEventWireup="true" CodeBehind="dep1.aspx.cs" Inherits="Ethics.dep1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lidepartment').addClass("active");
            //$("#lidepartment").parent().parent().removeClass("dropdown a1");
            $("#lidepartment").parent().parent().addClass("dropdown a1 open");
        });

        function ConfirmDelete() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div class="submenu">
         <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="AMC Type"></asp:Literal>
        </h1>

        <span style="padding-top: 3px;">
            <asp:Button ID="bNew" class="btn btn-info margin-bottom-20" Text="New" runat="server"
                ToolTip="Click button to add record"  /></span>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class="col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pAdd" runat="server">
                    <section class="tile cornered">
                            <div class="tile-body tile cornered">
                                
                                <div class="row">
                                    <!--col-6-->
                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="basicvalidations" runat="server" class="form-horizontal">
                                              <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Department Name" runat="server" ID="lDepartmentName" />:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="tDepartmentName" runat="server" TabIndex="1" ToolTip="Please enter the department name" class="textonly form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                                     <div id="Div3" runat="server" class="form-horizontal">
                                              <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Short Name" runat="server" ID="Label3" />:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="tShortName" runat="server" TabIndex="2" MaxLength="4" ToolTip="Please enter the short name" class="textonly form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                
                                    <!-- end col 6 -->
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="Form1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                                            <div class="form-group form-footer">
                                                <div class="col-sm-offset-5 col-sm-6">
                                                    <asp:Button ID="bSave" Text="Save" runat="server" ToolTip="Click button to save record" TabIndex="3" class="btn btn-primary"   />
                                                    <asp:Button ID="bCancel" Text="Cancel" runat="server" ToolTip="Click button to cancel record" TabIndex="4" class="btn btn-default" 
                                                         />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                </asp:Panel>
                <br />
                <div class="table-responsive">
                    <asp:Repeater ID="rptrDepartment" runat="server" >
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr class="submenu new-color">
                                    <th width="5%">
                                        <asp:Label Text="S.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Department Name" runat="server" ID="lDepartmentName" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Short Name" runat="server" ID="Label4" />
                                    </th>
                                    <th width="25%" style="text-align: center;">
                                        <asp:Label Text="Options" runat="server" ID="Label1" />
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("DepartmentName") %>' ToolTip='<%# Eval("DepartmentName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("DepartmentShortName") %>' ToolTip='<%# Eval("DepartmentShortName") %>' />
                                </td>
                                <td class="actions text-center">
                                    <asp:LinkButton class="btn btn-primary" ID="lnkEdit" runat="server" ToolTip="Click button to edit record"
                                        Text="Edit" CommandName="edit" />
                                    &nbsp;
                                    <asp:LinkButton class="btn btn-danger" ID="lnkDelete" runat="server" ToolTip="Click button to delete record"
                                        Text="Delete" OnClientClick="return ConfirmDelete();" CommandName="delete" />
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hDepartmentID" Value='<%# Eval("DepartmentID")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
