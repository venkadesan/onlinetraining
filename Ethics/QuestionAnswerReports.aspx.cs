﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using System.Data;
using OnlineTraining.Code;
using System.Web.UI.HtmlControls;
using OnlineTrainingPL;
using OnlineTrainingBL;
using OnlineTrainingPL.Master;
using System.IO;

namespace OnlineTraining
{
    public partial class QuestionAnswerReports : PageBase
    {
        PLAnswer objAnswerPL = new PLAnswer();
        BLAnswer objAnswerBL = new BLAnswer();
        PLQuestion objQuestionPL = new PLQuestion();
        BLQuestion objQuestionBL = new BLQuestion();
        PLTopic objTopicPL = new PLTopic();
        BLTopic objTopicBL = new BLTopic();
        PLTest objTestPL = new PLTest();
        BLTest objTestBL = new BLTest();
        static int PersentageResultCounts = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                if (UserSession.CompanyIDCurrent > 0 && UserSession.LocationIDCurrent > 0)
                {
                    LoadUser();
                    LoadTopic();
                    LoadDetails();                    
                }
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void LoadUser()
        {
            BLTopic objBLTopic = new BLTopic();
            PLTopic objPLTopic = new PLTopic();          
           
            DataSet Ds = new DataSet();
            objPLTopic.LocationID = UserSession.LocationIDUser;
            Ds = objBLTopic.GetUserByUserId(objPLTopic);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.dUser.DataSource = Ds;
                this.dUser.DataTextField = "FLName";
                this.dUser.DataValueField = "UserID";
                this.dUser.DataBind();
            }
            else
            {
                this.dUser.Items.Clear();
            }
        }

        private void LoadTopic()
        {
            BLTopic objBLTopic = new BLTopic();
            PLTopic objPLTopic = new PLTopic();

            objPLTopic.CompanyID = UserSession.CompanyIDUser;
            objPLTopic.Groupid = UserSession.GroupIDCurrent;
            DataSet Ds = new DataSet();
            Ds = objBLTopic.GetCurrentTopicQAReportsWithGroup(objPLTopic);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.dTopic.DataSource = Ds;
                this.dTopic.DataTextField = "TopicName";
                this.dTopic.DataValueField = "TopicID";
                this.dTopic.DataBind();
            }
            else
            {
                this.dTopic.Items.Clear();
            }
        }
     
        public void LoadDetails()
        {
            DataSet Ds = new DataSet();
            int TopicID = 0;
            Common.DDVal(this.dTopic, out TopicID);
            objQuestionPL.CompanyID = UserSession.CompanyIDUser;
            objQuestionPL.LocationID = UserSession.LocationIDCurrent;
            objQuestionPL.CUID = UserSession.UserID;
            objQuestionPL.TopicID = TopicID;
            Ds = objQuestionBL.GetQuestionAnswerReport(objQuestionPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                PersentageResultCounts = 0;
                rptrAnswer.DataSource = Ds;
                rptrAnswer.DataBind();
            }
            else
            {
                rptrAnswer.DataSource = null;
                rptrAnswer.DataBind();
            }
        }

        private void EmptyGrid()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("QuestionName");
            dt.Columns.Add("TopicName");
            dt.Columns.Add("Description");
            dt.Columns.Add("PassPercentage");
            dt.Columns.Add("TopicID");
            dt.Columns.Add("QuestionID");
            DataRow dr;
            dr = dt.NewRow();
            dr["QuestionName"] = "";
            dr["TopicName"] = "";
            dr["Description"] = "";
            dr["PassPercentage"] = "";
            dr["TopicID"] = 0;
            dr["QuestionID"] = 0;
            dt.Rows.Add(dr);
            rptrAnswer.DataSource = dt;
            rptrAnswer.DataBind();

            foreach (RepeaterItem rpt in rptrAnswer.Items)
            {
                Label Label2 = (Label)rpt.FindControl("SLNo");
                Label2.Visible = false;
            }
        }

        protected void rptrTopic_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void rptrTopic_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    HtmlContainerControl dtPer = (HtmlContainerControl)e.Item.FindControl("dtPer");
                    HtmlContainerControl dPPer = (HtmlContainerControl)e.Item.FindControl("dPPer");
                    HiddenField hTopicID = (HiddenField)e.Item.FindControl("hTopicID");
                    HiddenField hPassPercentage = (HiddenField)e.Item.FindControl("hPassPercentage");
                    Label lblPassPercentage = (Label)e.Item.FindControl("lblPassPercentage");
                    Label lblTestPersentage = (Label)e.Item.FindControl("lblTestPersentage");
                    Repeater rptrAnswer = (Repeater)e.Item.FindControl("rptrAnswer");
                    DataSet Ds = new DataSet();
                    objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
                    objQuestionPL.LocationID = UserSession.LocationIDCurrent;
                    objQuestionPL.TopicID = Convert.ToInt32(hTopicID.Value);
                    Ds = objQuestionBL.GetQuestionDetails(objQuestionPL);
                    if (Ds.Tables[0].Rows.Count > 0)
                    {
                        rptrAnswer.DataSource = Ds;
                        rptrAnswer.DataBind();
                    }
                    else
                    {
                        rptrAnswer.DataSource = null;
                        rptrAnswer.DataBind();
                    }

                    for (int items = 0; items < rptrAnswer.Items.Count; items++)
                    {
                        Repeater rptrAnswer1 = (Repeater)rptrAnswer.Items[items].FindControl("rptrAnswer");
                        HiddenField hQuestionID = rptrAnswer.Items[items].FindControl("hQuestionID") as HiddenField;
                        ImageButton imgResultYes = rptrAnswer.Items[items].FindControl("imgResultYes") as ImageButton;
                        ImageButton imgResultNO = rptrAnswer.Items[items].FindControl("imgResultNo") as ImageButton;
                        ImageButton ImgResultNA = rptrAnswer.Items[items].FindControl("ImgResultNA") as ImageButton;
                        DataSet Ds2 = new DataSet();
                        objAnswerPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                        Ds2 = objAnswerBL.GetAnswerKeyDetails(objAnswerPL);
                        if (Ds2.Tables[0].Rows.Count > 0)
                        {
                            string[] answerCount = new string[100];
                            string[] testAnswer = new string[100];
                            if (Ds2.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < Ds2.Tables[0].Rows.Count; i++)
                                {
                                    answerCount[i] = Ds2.Tables[0].Rows[i]["AnswerID"].ToString();
                                }
                            }
                            DataSet Ds1 = new DataSet();
                            objTestPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                            objTestPL.CompanyID = UserSession.CompanyIDCurrent1;
                            objTestPL.LocationID = UserSession.LocationIDCurrent;
                            objTestPL.CUID = UserSession.UserID;
                            Ds1 = objTestBL.CheckResults(objTestPL);
                            if (Ds1.Tables[0].Rows.Count > 0)
                            {
                                for (int j = 0; j < Ds1.Tables[0].Rows.Count; j++)
                                {
                                    testAnswer[j] = Ds1.Tables[0].Rows[j]["AnswerID"].ToString();
                                }
                            }
                            if (Ds2.Tables[0].Rows.Count == 0 || Ds1.Tables[0].Rows.Count == 0)
                            {
                                imgResultYes.Visible = false;
                                imgResultNO.Visible = false;
                                ImgResultNA.Visible = true;
                            }
                            else
                            {
                                bool areEqual = answerCount.SequenceEqual(testAnswer);

                                if (areEqual)
                                {
                                    PersentageResultCounts++;
                                    imgResultYes.Visible = true;
                                    imgResultNO.Visible = false;
                                    ImgResultNA.Visible = false;
                                }
                                else
                                {
                                    imgResultYes.Visible = false;
                                    imgResultNO.Visible = true;
                                    ImgResultNA.Visible = false;
                                }

                                try
                                {
                                    objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
                                    objQuestionPL.LocationID = UserSession.LocationIDCurrent;
                                    objQuestionPL.QuestionID = 0;
                                    int TotalQuestionCount = 0;
                                    DataSet DsQuestion = new DataSet();
                                    DsQuestion = objQuestionBL.GetALLQuestionDetails(objQuestionPL);
                                    if (Ds.Tables[0].Rows.Count > 0)
                                        TotalQuestionCount = DsQuestion.Tables[0].Rows.Count;
                                    else
                                        TotalQuestionCount = DsQuestion.Tables[0].Rows.Count;
                                    double Persentage = (Convert.ToDouble(PersentageResultCounts) / TotalQuestionCount) * 100;
                                    string[] Value = Persentage.ToString().Split('.');
                                    int TestPersentage = 0;
                                    if (Convert.ToInt32(Value[0].ToString()) > 0)
                                    {
                                        TestPersentage = Convert.ToInt32(Value[0].ToString());
                                        bool isDecimal = Persentage.ToString().Contains('.');
                                        if (isDecimal)
                                        {
                                            if (Value[1].ToString() == null)
                                            {
                                                lblTestPersentage.Text = Value[0].ToString() + " %";
                                            }
                                            else if (Value[1].ToString().Length == 1)
                                            {
                                                lblTestPersentage.Text = Persentage.ToString() + " %";
                                            }
                                            else if (Value[1].ToString().Length > 1)
                                            {
                                                int Ditits = Convert.ToInt32(Value[1].ToString().Substring(0, 2));
                                                if (Ditits > 50)
                                                {
                                                    TestPersentage = TestPersentage + 1;
                                                    lblTestPersentage.Text = TestPersentage.ToString() + "%";
                                                }
                                                else
                                                {
                                                    lblTestPersentage.Text = TestPersentage.ToString() + "." + Ditits + "%";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            lblTestPersentage.Text = TestPersentage.ToString() + ".00 %";
                                        }
                                    }
                                }
                                catch { }
                            }
                        }
                    }
                }
                catch { }
            }
        }

        protected void rptrAnswer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void rptrAnswer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hQuestionID = (HiddenField)e.Item.FindControl("hQuestionID") as HiddenField;               
                Label lbltext = (Label)e.Item.FindControl("lbltext") as Label;              
                DataSet Ds2 = new DataSet();
                objAnswerPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                Ds2 = objAnswerBL.GetAnswerKeyDetails(objAnswerPL);
                if (Ds2.Tables[0].Rows.Count > 0)
                {
                    string[] answerCount = new string[100];
                    string[] testAnswer = new string[100];
                    if (Ds2.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < Ds2.Tables[0].Rows.Count; i++)
                        {
                            answerCount[i] = Ds2.Tables[0].Rows[i]["AnswerID"].ToString();
                        }
                    }
                    DataSet Ds1 = new DataSet();
                    objTestPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                    objTestPL.CompanyID = UserSession.CompanyIDCurrent;
                    objTestPL.LocationID = UserSession.LocationIDCurrent;
                    objTestPL.CUID = UserSession.UserID;
                    Ds1 = objTestBL.GetAnswerResults(objTestPL);
                    if (Ds1.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < Ds1.Tables[0].Rows.Count; j++)
                        {
                            testAnswer[j] = Ds1.Tables[0].Rows[j]["AnswerID"].ToString();
                        }
                    }
                    if (Ds2.Tables[0].Rows.Count == 0 || Ds1.Tables[0].Rows.Count == 0)
                    {
                        lbltext.Text = "Not Attend";
                        //imgResultYes.Visible = false;
                        //imgResultNO.Visible = false;
                        //ImgResultNA.Visible = true;
                    }
                    else
                    {
                        bool areEqual = answerCount.SequenceEqual(testAnswer);

                        if (areEqual)
                        {
                            PersentageResultCounts++;
                            lbltext.Text = "Correct";
                            //imgResultYes.Visible = true;
                            //imgResultNO.Visible = false;
                            //ImgResultNA.Visible = false;
                        }
                        else
                        {
                            lbltext.Text = "Wrong";
                            //imgResultYes.Visible = false;
                            //imgResultNO.Visible = true;
                            //ImgResultNA.Visible = false;
                        }

                        try
                        {
                           
                        }
                        catch { }
                    }
                }
            }
        }

        protected void dUser_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void dTopic_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDetails();
        }

        protected void imgExcel_Click(object sender, ImageClickEventArgs e)
        {
            Response.Clear();
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("content-disposition",
                    "attachment;filename=QuestionAnswerReports.xls");

            StringWriter swriter = new StringWriter();
            HtmlTextWriter hwriter = new HtmlTextWriter(swriter);

            HtmlForm frm = new HtmlForm();
            this.rptrAnswer.Parent.Controls.Add(frm);
            frm.Attributes["runat"] = "server";
            frm.Controls.Add(this.rptrAnswer);
            frm.RenderControl(hwriter);
            string style = @"<style> TABLE { border: -1px solid black; } TD { border: -1px solid black; } </style> ";

            Response.Write(style);
            Response.Output.Write(swriter.ToString());
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }      
    }
}