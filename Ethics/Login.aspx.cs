﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using KB = Kowni.BusinessLogic;
using OnlineTrainingPL;
using MCB = Kowni.Common.BusinessLogic;
using Kowni.Helper;
using System.Drawing;
using System.Data;
using System.Web.Security;
using OnlineTraining.Code;
using System.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;
using OnlineTrainingPL.Master;
using OnlineTrainingBL.Master;
using OnlineTrainingBL;

namespace OnlineTraining
{
    public partial class Login : System.Web.UI.Page
    {
        MCB.BLSingleSignOn objSingleSignOn = new MCB.BLSingleSignOn();
        MCB.BLAnnouncement objAnnouncement = new MCB.BLAnnouncement();

        KB.BLRoles objroles = new KB.BLRoles();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Clear();
            }
        }

        public void NotifyMessages(string Message)
        {
            divmsg.Style.Clear();
            divmsg.Attributes.Add("class", Message.ToString());
            divmsg.Attributes.Add("class", "Error");
            divmsg.InnerHtml = Message.Trim();
            divmsg.Visible = true;
        }

        //private void GetUserLogin()
        //{
        //    PLUser objuserPL = new PLUser();
        //    BLUser objuserBL = new BLUser();

        //    objuserPL.UserName = this.txtUserName.Text.Trim();
        //    objuserPL.Password = this.txtPassword.Text.Trim();
        //    DataSet ds = new DataSet();

        //    ds = objuserBL.UserLoginGet(objuserPL);
        //    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //    {
        //        if (objuserPL.UserName == ds.Tables[0].Rows[0]["EMailID"].ToString() && objuserPL.Password == ds.Tables[0].Rows[0]["Password"].ToString())
        //        {
        //            UserSession.UserID = Convert.ToInt32(ds.Tables[0].Rows[0]["userid"].ToString());
        //            UserSession.UserFirstName = ds.Tables[0].Rows[0]["UserName"].ToString();
        //            UserSession.UserLastName = ds.Tables[0].Rows[0]["rolename"].ToString();
        //            UserSession.RoleID = Convert.ToInt32(ds.Tables[0].Rows[0]["roleid"].ToString());
        //            //UserSession.CompanyIDCurrent1 = Convert.ToInt32(ds.Tables[0].Rows[0]["companyID"].ToString());
        //            UserSession.UserCompanyID = Convert.ToInt32(ds.Tables[0].Rows[0]["companyID"].ToString());
        //            UserSession.CompanyIDCurrent = 1;
        //            UserSession.CompanyIDCurrent1 = 1;

        //            UserSession.CompanyName = ds.Tables[0].Rows[0]["companyname"].ToString();
        //            //UserSession.UserImageName = ds.Tables[0].Rows[0]["imagename"].ToString();                    

        //            Response.Redirect("Question.aspx");
        //        }
        //        else
        //        {
        //            this.divmsg.InnerHtml = "Invalid UserName / Password";
        //        }
        //    }
        //    else
        //    {
        //        this.divmsg.InnerHtml = "Invalid UserName / Password";
        //    }
        //}

        private string GetRoleName(int RoleID)
        {
            try
            {
                BLTopic objTopicBL = new BLTopic();
                PLTopic objTopicPL = new PLTopic();

                objTopicPL.RoleID = RoleID;
                DataSet Ds = new DataSet();
                Ds = objTopicBL.GetRoleName(objTopicPL);
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = Ds.Tables[0].Rows[0];
                    string rolename = dr["RoleName"].ToString();
                    return rolename;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void Submit1_Click(object sender, EventArgs e)
        {
            int RoleID = 0;
            int UserID = 0;
            int CompanyID = 0;
            string CompanyName = string.Empty;
            int LocationID = 0;
            string LocationName = string.Empty;
            int GroupID = 0;
            int LanguageID = 0;
            string UserFName = string.Empty;
            string UserLName = string.Empty;
            string UserMailID = string.Empty;
            string ThemeFolderPath = string.Empty;
            string RoleName = string.Empty;
            if (!objSingleSignOn.Login(this.txtUserName.Text, this.txtPassword.Text, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), out RoleID, out UserID, out CompanyID, out CompanyName, out LocationID, out LocationName, out GroupID, out LanguageID, out UserFName, out UserLName, out UserMailID, out ThemeFolderPath))
            {
                NotifyMessages(objSingleSignOn.Message);
            }
            else
            {
                UserSession.FromFramework = false;
                UserSession.CountryID = 1;
                UserSession.CountryIDCurrent = 1;
                UserSession.RoleID = RoleID;
                UserSession.UserID = UserID;
                UserSession.UserIDs = UserID;

                UserSession.CompanyIDUser = CompanyID;
                UserSession.LocationIDUser = LocationID;
                UserSession.CompanyName = CompanyName;
                UserSession.CompanyIDCurrent = 1;
                //UserSession.CompanyIDCurrent1 = 1;
                UserSession.UserCompanyID = CompanyID;
                UserSession.LocationName = LocationName;
                UserSession.LocationIDCurrent = LocationID;
                UserSession.GroupID = GroupID;
                UserSession.GroupIDCurrent = GroupID;
                UserSession.LanguageID = LanguageID;
                UserSession.UserFirstName = UserFName;
                UserSession.UserLastName = UserLName;
                UserSession.ThemeFolderPath = ThemeFolderPath;
                UserSession.RoleName = GetRoleName(RoleID);
                UserSession.FromFramework = false;

                //DataSet dsRoleDet = objroles.GetRolesByID(RoleID);

                DataSet dsAnn = new DataSet();
                dsAnn = objAnnouncement.GetAnnouncement(UserSession.CompanyIDCurrent, Convert.ToInt32(Kowni.Common.BusinessLogic.BLMenu.ToolID.MasterFramework));
                if (dsAnn.Tables.Count > 0 && dsAnn.Tables[0].Rows.Count > 0)
                {
                    if (dsAnn.Tables[0].Rows[0]["AnnouncementType"].ToString() == "1")
                    {
                        UserSession.Announcement = dsAnn.Tables[0].Rows[0]["Announcement"].ToString();
                    }
                }
                else
                {
                    UserSession.Announcement = string.Empty;
                }
                Response.Redirect("dashboard.aspx", true);
            }
        }

        Ethics.JLLLDAP.Profile1 objLdap = new Ethics.JLLLDAP.Profile1();

        //protected void Submit1_Click(object sender, EventArgs e)
        //{
        //    GetUserLogin();

        //    //if (txtUserName.Text == "Demo" && txtPassword.Text == "Demo1234$$")
        //    //{
        //    //    UserSession.UserFirstName = "ananth.b@ap.jll.com";
        //    //    UserSession.UserLastName = "ananth.b@ap.jll.com";               
        //    //    Response.Redirect("Welcome.aspx");               
        //    //}
        //    //else
        //    //{
        //    //    bool loginStatus = false;
        //    //    try
        //    //    {
        //    //        UserSession.UserFirstName = txtUserName.Text;
        //    //        UserSession.UserLastName = txtUserName.Text;
        //    //        string[] txtDomainName = txtUserName.Text.Split('@');
        //    //        string[] txtDomainName1 = txtDomainName[1].Split('.');
        //    //        loginStatus = objLdap.Authenticate(txtDomainName1[0].ToString(), txtDomainName[0].ToString(), txtPassword.Text.Trim());
        //    //        if (loginStatus)
        //    //        {
        //    //            //UserSession.UserFirstName = "ananth.b@ap.jll.com";
        //    //            UserSession.UserLastName = "ananth.b@ap.jll.com";
        //    //            Response.Redirect("Welcome.aspx", true);
        //    //        }
        //    //        else
        //    //        {
        //    //            NotifyMessages("Invalid username/password");
        //    //        }
        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        //string[] txtDomainName = txtUserName.Text.Split('@');
        //    //        //string[] Val = { "ap.jll.com" };
        //    //        //if (txtDomainName[1] != null && txtDomainName[1].ToString() != string.Empty)
        //    //        //{
        //    //        //    for (int i = 0; i < Val.Length; i++)
        //    //        //    {
        //    //        //        if (Val[i].ToString() == txtDomainName[1].ToString())
        //    //        //        {
        //    //        //            Response.Redirect("Welcome.aspx", true);
        //    //        //            break;
        //    //        //        }
        //    //        //    }
        //    //        //}
        //    //        //else
        //    //        //{
        //    //            NotifyMessages("Invalid username/password");
        //    //        //}
        //    //    }
        //    //}
        //}

        protected void ResetCancel_Click(object sender, EventArgs e)
        {

        }

        protected void ResetPassword_Click(object sender, EventArgs e)
        {

        }
    }
}