﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OnlineTraining.Code;
using OnlineTraining.Master;
using OnlineTrainingPL;

namespace OnlineTraining
{
    public partial class mainforms : PageBase
    {
        private BLForms objBlForms = new BLForms();
        private PLForms objPlForms = new PLForms();

        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ButtonStatus(Common.ButtonStatus.Cancel);
                BindMainMenuDetails();
            }
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            //this.txtInspection.Text = this.txtDescription.Text = string.Empty;
            this.txtOrder.Text = this.txtpagename.Text = this.txtpagetext.Text = string.Empty;
            this.txtCss.Text = string.Empty;
            chkisvisible.Checked = false;
            this.divmsg.Visible = false;
            this.bNew.Visible = false;
            this.pDet.Visible = true;

            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;

                this.bNew.Visible = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.pAdd.Visible = true;

                this.bNew.Visible = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;

                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;

                this.ID = 0;
            }
            else if (status == Common.ButtonStatus.Views)
            {
                this.pAdd.Visible = false;
                this.pDet.Visible = false;
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void rptrInspection_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindCtrls();
            }
            else if (e.CommandName == "delete")
            {
                this.ID = Convert.ToInt32(e.CommandArgument);
                objPlForms.MainFormid = Convert.ToInt32(ID);
                objBlForms.DeleteMainMenus(objPlForms);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages("Deleted Sucessfully", Common.ErrorType.Information);
                BindMainMenuDetails();
            }
        }

        private void BindCtrls()
        {
            try
            {
                objPlForms.MainFormid = Convert.ToInt32(ID);
                DataTable dtMainForm = new DataTable();
                dtMainForm = objBlForms.GetMainMenusDet(objPlForms).Tables[0];
                if (dtMainForm != null && dtMainForm.Rows.Count > 0)
                {
                    txtpagetext.Text = dtMainForm.Rows[0]["MainFormsPageText"].ToString();
                    txtpagename.Text = dtMainForm.Rows[0]["MainFormsPageName"].ToString();
                    txtOrder.Text = dtMainForm.Rows[0]["OrderIs"].ToString();
                    chkisvisible.Checked = false;
                    txtCss.Text = dtMainForm.Rows[0]["cssclass"].ToString(); ;
                    if (dtMainForm.Rows[0]["IsVisible"].ToString() != "1" || dtMainForm.Rows[0]["IsVisible"].ToString().ToLower() != "true")
                    {
                        chkisvisible.Checked = true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        private void BindMainMenuDetails()
        {
            try
            {
                objPlForms.ToolId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("toolid"));
                DataTable dtmailTable = new DataTable();
                dtmailTable = objBlForms.GetMainMenus(objPlForms).Tables[0];
                rptrmainforms.DataSource = dtmailTable;
                rptrmainforms.DataBind();
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            try
            {
                objPlForms.ToolId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("toolid"));
                objPlForms.MenFormName = txtpagename.Text;
                objPlForms.MenFormText = txtpagetext.Text;
                objPlForms.Order = txtOrder.Text.Trim() == string.Empty ? 0 : Convert.ToInt32(txtOrder.Text);
                objPlForms.IsVisible = chkisvisible.Checked;
                objPlForms.MainFormid = Convert.ToInt32(ID);
                objPlForms.Css = txtCss.Text;
                objBlForms.InsertMainMenus(objPlForms);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages("Sucessfully Saved", Common.ErrorType.Information);
                BindMainMenuDetails();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}