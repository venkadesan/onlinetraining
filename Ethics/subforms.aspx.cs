﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTraining.Code;
using OnlineTraining.Master;
using OnlineTrainingPL;

namespace OnlineTraining
{
    public partial class subforms : PageBase
    {
        private BLForms objBlForms = new BLForms();
        private PLForms objPlForms = new PLForms();

        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
                BindMainMenuDetails();
                BindMainMenu();
                BindSubMenuDetails();
            }
        }

        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                lmainheader.Text = mainmenu;
                lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        protected void dMainMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            divmsg.Visible = false;
            BindSubMenuDetails();
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            //this.txtInspection.Text = this.txtDescription.Text = string.Empty;
            this.txtOrder.Text = this.txtpagename.Text = this.txtpagetext.Text = string.Empty;
            this.txtCss.Text = string.Empty;
            chkisvisible.Checked = false;
            ddlmainform.SelectedIndex = -1;
            this.divmsg.Visible = false;
            this.bNew.Visible = false;
            this.pDet.Visible = true;

            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;

                this.bNew.Visible = false;
                this.bSave.Enabled = true;
                this.dMainMenu.Visible = false;
                this.lblmainmenu.Visible = false;

            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.pAdd.Visible = true;

                this.bNew.Visible = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;

                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                this.dMainMenu.Visible = true;
                this.lblmainmenu.Visible = true;

                this.ID = 0;
            }
            else if (status == Common.ButtonStatus.Views)
            {
                this.pAdd.Visible = false;
                this.pDet.Visible = false;
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void rptrInspection_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindCtrls();
            }
            else if (e.CommandName == "delete")
            {
                this.ID = Convert.ToInt32(e.CommandArgument);
                objPlForms.SubFormid = Convert.ToInt32(ID);
                objBlForms.DeleteSubMenus(objPlForms);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages("Sucessfully Deleted", Common.ErrorType.Information);
                BindMainMenuDetails();
                BindSubMenuDetails();
            }
        }

        private void BindCtrls()
        {
            try
            {
                objPlForms.SubFormid = Convert.ToInt32(ID);
                DataTable dtMainForm = new DataTable();
                dtMainForm = objBlForms.GetSubMenusDet(objPlForms).Tables[0];
                if (dtMainForm != null && dtMainForm.Rows.Count > 0)
                {
                    txtpagetext.Text = dtMainForm.Rows[0]["SubFormsPageText"].ToString();
                    txtpagename.Text = dtMainForm.Rows[0]["SubFormsPageName"].ToString();
                    ddlmainform.SelectedValue = dtMainForm.Rows[0]["MainFormsID"].ToString();
                    txtOrder.Text = dtMainForm.Rows[0]["OrderIs"].ToString();
                    txtCss.Text = dtMainForm.Rows[0]["cssclass"].ToString(); ;
                    chkisvisible.Checked = false;
                    if (dtMainForm.Rows[0]["IsVisible"].ToString() != "1" || dtMainForm.Rows[0]["IsVisible"].ToString().ToLower() != "true")
                    {
                        chkisvisible.Checked = true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        private void BindMainMenuDetails()
        {
            try
            {
                objPlForms.ToolId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("toolid"));
                DataTable dtmailTable = new DataTable();
                dtmailTable = objBlForms.GetMainMenus(objPlForms).Tables[0];
                this.ddlmainform.DataSource = dtmailTable;
                this.ddlmainform.DataTextField = "MainFormsPageText";
                this.ddlmainform.DataValueField = "MainFormsID";
                this.ddlmainform.DataBind();

            }
            catch (Exception)
            {
                throw;
            }
        }

        private void BindMainMenu()
        {
            try
            {
                objPlForms.ToolId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("toolid"));
                DataTable dtmailTable = new DataTable();
                dtmailTable = objBlForms.GetMainMenus(objPlForms).Tables[0];

                this.dMainMenu.DataSource = dtmailTable;
                this.dMainMenu.DataTextField = "MainFormsPageText";
                this.dMainMenu.DataValueField = "MainFormsID";
                this.dMainMenu.DataBind();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void BindSubMenuDetails()
        {
            try
            {
                objPlForms.ToolId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("toolid"));
                int mainformid = 0;
                Common.DDVal(this.dMainMenu, out mainformid);
                objPlForms.MainFormid = mainformid;
                DataTable dtmailTable = new DataTable();
                dtmailTable = objBlForms.GetSubMenus(objPlForms).Tables[0];
                rptrmainforms.DataSource = dtmailTable;
                rptrmainforms.DataBind();
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtpagetext.Text == String.Empty)
                {
                    NotifyMessages("Enter Page Text ", Common.ErrorType.Error);
                    return;
                }
                if (txtpagename.Text == String.Empty)
                {
                    NotifyMessages("Enter Page Name ", Common.ErrorType.Error);
                    return;
                }
                if (txtOrder.Text == String.Empty)
                {
                    NotifyMessages("Enter Order ", Common.ErrorType.Error);
                    return;
                }

                objPlForms.ToolId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("toolid"));
                objPlForms.SubFormName = txtpagename.Text;
                objPlForms.SubFormText = txtpagetext.Text;
                objPlForms.Order = txtOrder.Text.Trim() == string.Empty ? 0 : Convert.ToInt32(txtOrder.Text);
                objPlForms.IsVisible = chkisvisible.Checked;
                int mainformid = 0;
                Common.DDVal(ddlmainform, out mainformid);
                objPlForms.MainFormid = Convert.ToInt32(mainformid);
                objPlForms.SubFormid = Convert.ToInt32(ID);
                objPlForms.Css = txtCss.Text.Trim();
                objBlForms.InsertSubMenus(objPlForms);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages("Sucessfully Saved", Common.ErrorType.Information);
                BindMainMenuDetails();
                BindSubMenuDetails();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}