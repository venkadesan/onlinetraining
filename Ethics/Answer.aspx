﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="Answer.aspx.cs" Inherits="OnlineTraining.Answer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="Stylesheet">
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">

        function ConfirmDelete() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="submenu">
         <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Answer"></asp:Literal>
        </h1>
        <span style="padding-top: 3px;">
            <asp:Button ID="bNew" class="btn btn-info margin-bottom-20" Text="New" runat="server"
                OnClick="bNew_Click" /></span>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <!-- tile body -->
                        <div class="tile-body" style="width: 100%;">
                            <div id="divQuestion" runat="server" class="form-horizontal" style="width: 100%;">
                                <div class="form-group" style="float: left; width: 100%;">
                                    <label class="col-sm-1 control-label" for="" style="float: left; text-align: left;">
                                        <asp:Label Text="Department:" runat="server" ID="Label3" /></label>
                                    <div class="col-sm-11">
                                        <asp:DropDownList runat="server" class="chosen-select form-control" ID="ddldepartment"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddldepartment_SelectedIndexChanged"
                                            Width="103%">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group" style="float: left; width: 100%;">
                                    <label class="col-sm-1 control-label" for="" style="float: left; text-align: left;">
                                        <asp:Label Text="Topic:" runat="server" ID="Label5" /></label>
                                    <div class="col-sm-11">
                                        <asp:DropDownList runat="server" class="chosen-select form-control" ID="dTopic" AutoPostBack="True"
                                            Width="103%" OnSelectedIndexChanged="dTopic_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-1 control-label" for="" style="float: left; text-align: left;">
                                        <asp:Label Text="Question:" runat="server" ID="Label1" /></label>
                                    <div class="col-sm-11">
                                        <asp:DropDownList runat="server" class="chosen-select form-control" ID="dQuestion"
                                            Width="100%" AutoPostBack="True" OnSelectedIndexChanged="dQuestion_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /tile body -->
                    </div>
                    <!-- end col 6 -->
                </div>
                <div class="col-md-12">
                    <asp:Panel ID="pAdd" runat="server" CssClass="tile-body">
                        <section class="tile cornered">
         <div class="tile-body">
                            <div class="row">
                                <!--col-6-->
                                <div class="col-md-10">
                                <div id="TextBoxContainer">
                                </div>
                                    <!-- tile body -->
                                    <div class="tile-body">
                                        <div id="basicvalidations" runat="server" class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" for="" style="margin-left:-2%;" >
                                                    <asp:Label Text="Answer Name" runat="server" ID="lDepartmentName" />:</label>
                                                <div class="col-sm-8" >
                                                    <asp:TextBox ID="tAnswerName" runat="server" Width="100%" class="form-control parsley-validated"></asp:TextBox>
                                                </div>
                                            </div><br />

                                             <div class="form-group">                                            
                                                <b>Download Template: </b>
                                                <%--<asp:Button ID="btnDownload" runat="server" Text="Download" onclick="btnDownload_Click" /> --%> 
                                                
                                                 <asp:ImageButton runat="server" ID="imgExcel" ClientIDMode="Static" ImageUrl="~/App_Themes/images/Excel.png"
                    Width="40" Height="40" ToolTip="Click to download Template details" OnClick="btnDownload_Click" />                                             
                                            </div>

                                             <div class="form-group">
                                             <div class="col-sm-offset-5 col-sm-6" style="margin-left:30%; margin-top:-5%">
                                            <b>Please Select Excel File: </b>
                                            </div>

                                            <div class="col-sm-offset-5 col-sm-6" style="margin-left:52%; margin-top:-5%">
                                            <asp:FileUpload ID="fileuploadExcel" runat="server" />&nbsp;&nbsp; 
                                            </div>

                                            <div class="form-group form-footer">
                                             <div class="col-sm-offset-5 col-sm-6" style="margin-left:78%; margin-top:-6%">
                                                <asp:Button ID="btnImport" runat="server" Text="Import Data" onclick="btnImport_Click" class="btn btn-primary" />                                               
                                             </div>
                                            </div>                                       
                                         </div>                                      
                                             <div class="form-group form-footer">
                                <div class="col-sm-offset-5 col-sm-6" style="margin-left:45%; margin-top:-3%">
                                    <asp:Button ID="bSave" Text="Save" runat="server" class="btn btn-primary" OnClick="bSave_Click" />
                                    <asp:Button ID="Button1" Text="Cancel" runat="server" class="btn btn-default" OnClick="Cancel1_Click" />
                                </div>
                            </div>                                                
                                        </div>
                                    </div>               
                                                
                                         
                                    <!-- /tile body -->
                                </div>                                
                                        <asp:TextBox ID="tShortName" runat="server" class="form-control parsley-validated" Visible="false"></asp:TextBox>
                                                
                                    <!-- /tile body -->
                                </div>
                                <!-- end col 6 -->
                                  <div class="cboth">
                            </div>                                         
                </div>
                </div>
                </asp:Panel> </section>
                <br />
                <div class="table-responsive">
                    <asp:Repeater ID="rptrAnswer" runat="server" OnItemCommand="rptrAnswer_ItemCommand"
                        DataSourceID="" OnItemDataBound="rptrAnswer_ItemDataBound">
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr class="new-color">
                                    <th width="20%">
                                        <asp:Label Text="Question" runat="server" ID="Label6" />
                                    </th>
                                    <th width="75%">
                                        <asp:Label Text="Selected Answer" runat="server" ID="lDepartmentName" />
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("QuestionName") %>' />
                                </td>
                                <td>
                                    <asp:Repeater ID="rptrAnswer1" runat="server" OnItemCommand="rptrAnswer1_ItemCommand"
                                        DataSourceID="" OnItemDataBound="rptrAnswer1_ItemDataBound">
                                        <ItemTemplate>
                                            <div style="float: left; width: 100%; line-height: 30px;">
                                                <div style="float: left; width: 68%;">
                                                    <asp:CheckBox runat="server" ID="chkAnswer" />
                                                    <asp:Label ID="lblAnswer" runat="server" Text='<%# Eval("AnswerName") %>' />
                                                    <%--<asp:Label ID="lblOptions" runat="server" Text='<%# Eval("Options") %>' />--%>
                                                </div>
                                                <div style="float: right; width: 20%; padding-bottom: 15px;">
                                                    <asp:LinkButton class="btn btn-primary" ID="lnkEdit" runat="server" Text="Edit" CommandName="edit" />
                                                    &nbsp;
                                                    <asp:LinkButton class="btn btn-danger" ID="lnkDelete" runat="server" Text="Delete"
                                                        OnClientClick="return ConfirmDelete();" CommandName="delete" />
                                                    <asp:HiddenField runat="server" ID="hAnswerID" Value='<%# Eval("AnswerID")%>' />
                                                </div>
                                                <div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hQuestionID" Value='<%# Eval("QuestionID")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="Div1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                            <div class="form-group form-footer">
                                <div class="col-sm-offset-5 col-sm-6">
                                    <asp:Button ID="Button2" Text="Save" runat="server" class="btn btn-primary" OnClick="bAnswerKeySave_Click" />
                                    <asp:Button ID="Button3" Text="Cancel" runat="server" class="btn btn-default" OnClick="CancelAnswerKey_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
