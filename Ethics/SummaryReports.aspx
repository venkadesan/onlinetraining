﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="SummaryReports.aspx.cs" Inherits="OnlineTraining.SummaryReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="Stylesheet">
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(

        /* This is the function that will get executed after the DOM is fully loaded */
  function () {
      $(".dt").datepicker({
          changeMonth: true, //this option for allowing user to select month
          changeYear: true //this option for allowing user to select from year range
      });
  }
);
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div class="submenu">
       <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Report"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Summary Report"></asp:Literal>
        </h1>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <!-- col 6 -->
                    <div class="col-md-6">
                        <!-- tile body -->
                        <div class="tile-body">
                            <div id="basicvalidations" parsley-validate="" role="form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="From Date" runat="server" ID="lFromDate" />:</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" class="chosen-select form-control dt"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="Department" runat="server" ID="ldropdown" />:</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddldepartment" runat="server" CssClass="form-control" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddldepartment_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /tile body -->
                    </div>
                    <!--col-6-->
                    <div class="col-md-6">
                        <!-- tile body -->
                        <div class="tile-body">
                            <div id="basicvalidations" runat="server" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="To Date" runat="server" ID="lToDate" />:</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" class="chosen-select form-control dt"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="Select Topic" runat="server" ID="Label4" />:</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList runat="server" class="chosen-select form-control" ID="dTopic">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <!-- /tile body -->
                        </div>
                        <!-- col 6 -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="Form1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                            <div class="form-group form-footer">
                                <div class="col-sm-offset-5 col-sm-6">
                                    <asp:Button ID="bSave" Text="Search" runat="server" class="btn btn-primary" OnClick="btnSubmitreg_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
        </div>
        <div class="table-responsive">
            <div style="float: right;">
                <asp:ImageButton runat="server" ID="imgExcel" ClientIDMode="Static" ImageUrl="~/App_Themes/images/Excel.png"
                    Width="40" Height="40" ToolTip="Click to download report details" OnClick="imgExcel_Click" />
            </div>
            <asp:Repeater ID="rptrReports" runat="server" OnItemCommand="rptrAnswer_ItemCommand"
                DataSourceID="" OnItemDataBound="rptrAnswer_ItemDataBound">
                <HeaderTemplate>
                    <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                        <tr  class="new-color">
                            <th width="5%">
                                <asp:Label Text="S.No" runat="server" ID="lSNo" />
                            </th>
                            <th>
                                <asp:Label Text="Topic" runat="server" ID="Label1" />
                            </th>
                            <th>
                                <asp:Label Text="Total Participant" runat="server" ID="lReasonName" />
                            </th>
                            <th>
                                <asp:Label Text="No.of Succeed" runat="server" ID="Label2" />
                            </th>
                            <th>
                                <asp:Label Text="No.of not Succeed" runat="server" ID="Label5" />
                            </th>
                        </tr>
                </HeaderTemplate>

                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="sno" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("topic") %>' />
                        </td>
                        <td>
                            <asp:LinkButton ID="lblTotal" runat="server" Text='<%# Eval("Total") %>' CommandName="Total" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lblPass" runat="server" Text='<%# Eval("Pass") %>' CommandName="Pass"/>
                        </td>
                        <td>
                            <asp:LinkButton ID="lblFail" runat="server" Text='<%# Eval("Fail") %>' CommandName="Fail" />
                        </td>
                    </tr>
                </ItemTemplate>
                
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            

            <div style="float: right;">
                <asp:ImageButton runat="server" ID="ParticipateExcelImage" ClientIDMode="Static" ImageUrl="~/App_Themes/images/Excel.png"
                    Width="40" Height="40" ToolTip="Click to download report details" OnClick="participateExcel_Click" />
            </div>
          <asp:Repeater ID="rptParticipateReports" runat="server" OnItemCommand="rptParticipateDetails_ItemCommand"
                DataSourceID="" OnItemDataBound="rptParticipateDetails_ItemDataBound">
                <HeaderTemplate>
                    <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                        <tr  class="new-color">
                            <th width="5%">
                                <asp:Label Text="S.No" runat="server" ID="lSNo" />
                            </th>
                            <th>
                                <asp:Label Text="Name" runat="server" ID="lblName" />
                            </th>
                            <th>
                                <asp:Label Text="EmailID" runat="server" ID="Label2" />
                            </th>
                            <th>
                                <asp:Label Text="ExamDate" runat="server" ID="Label5" />
                            </th>
                            <th>
                                <asp:Label Text="ExamTime" runat="server" ID="Label6" />
                            </th>
                            
                            <th>
                                <asp:Label Text="Score" runat="server" ID="Label7" />
                            </th>
                        </tr>
                </HeaderTemplate>
                
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="lblSno" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                        </td>
                        <td>
                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' />
                        </td>
                        <td>
                            <asp:Label ID="lblEmailId" runat="server" Text='<%# Eval("EmailId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="lblExamDate" runat="server" Text='<%# Eval("ExamDate") %>' />
                        </td>
                        <td>
                            <asp:Label ID="lblExamTime" runat="server" Text='<%# Eval("ExamTime") %>' />
                        </td>
                        <td>
                            <asp:Label ID="lblScore" runat="server" Text='<%# Eval("PassPercentage") %>' />
                        </td>

                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
