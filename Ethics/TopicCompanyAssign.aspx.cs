﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTraining.Code;
using OnlineTrainingBL;
using OnlineTrainingPL;

namespace OnlineTraining
{
    public partial class TopicCompanyAssign :PageBase
    {       
        PLTopic objTopicPL = new PLTopic();
        BLTopic objTopicBL = new BLTopic();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                Department();
                BindCompany();
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void Department()
        {
            try
            {
                BLDepartment objbldept = new BLDepartment();
                PLDepartment objpldept = new PLDepartment();

                objpldept.CompanyID = UserSession.CompanyIDCurrent;
                objpldept.GroupId = UserSession.GroupID;
                // objPLTopic.LocationID = UserSession.LocationIDCurrent;
                objpldept.DepartmentId = 0;
                DataSet Ds = new DataSet();
                Ds = objbldept.GetDepartmentDetails(objpldept);

                if (Ds.Tables[0].Rows.Count > 0)
                {
                    this.ddldepartment.DataSource = Ds;
                    this.ddldepartment.DataTextField = "DepartmentName";
                    this.ddldepartment.DataValueField = "DepartmentId";
                    this.ddldepartment.DataBind();
                }
                else
                {
                    this.ddldepartment.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message,Common.ErrorType.Error);
            }
        }

        private void BindCompany()
        {
            try
            {
                objTopicPL.Groupid = UserSession.GroupID;
                DataSet Ds = new DataSet();
                Ds = objTopicBL.GetCompany(objTopicPL.Groupid);
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    ddlcompany.DataSource = Ds;
                    ddlcompany.DataTextField = "CompanyName";
                    ddlcompany.DataValueField = "CompanyId";
                    ddlcompany.DataBind();

                    ddlcompany_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlcompany.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void ddlcompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDetails();
            divmsg.Visible = false;
            lblError.Text = string.Empty;
        }

        private void BindDetails()
        {
            try
            {
                int companyid = 0;
                int departmentid = 0;
                Common.DDVal(ddlcompany, out companyid);
                Common.DDVal(ddldepartment, out departmentid);
                objTopicPL.Groupid = UserSession.GroupID;
                objTopicPL.CompanyID = companyid;
                objTopicPL.DepartmentId = departmentid;
                DataSet Ds = new DataSet();
                Ds = objTopicBL.GetAssignedTopics(objTopicPL);  
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    rptrTopic.DataSource = Ds;
                    rptrTopic.DataBind();
                }
                else
                {
                    rptrTopic.DataSource = null;
                    rptrTopic.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void btnAssign_Click(object sender, EventArgs e)
        {
            try
            {
                int companyid = 0;
                if (!Common.DDVal(ddlcompany, out companyid))
                {
                    NotifyMessages("Select Company", Common.ErrorType.Error);
                    return;
                }
                int departmentid = 0;
                Common.DDVal(ddldepartment, out departmentid);
                objTopicPL.CompanyID = companyid;
                objTopicPL.Groupid = UserSession.GroupID;
                objTopicPL.DepartmentId = departmentid;
                objTopicPL.CUID = UserSession.UserID;
                DataTable dtassigndet = new DataTable();
                dtassigndet.Columns.Add("TopicId", typeof(int));
                foreach (RepeaterItem ritopicdet in rptrTopic.Items)
                {
                    CheckBox chkMSFVisible = (CheckBox)ritopicdet.FindControl("chkMSFVisible");
                    if (chkMSFVisible != null)
                    {
                        if (chkMSFVisible.Checked)
                        {
                            HiddenField hTopicID = (HiddenField)ritopicdet.FindControl("hTopicID");
                            if (hTopicID != null)
                            {
                                int topicid = Convert.ToInt32(hTopicID.Value);
                                dtassigndet.Rows.Add(topicid);
                            }
                        }
                    }
                }
                string xmlString = string.Empty;
                using (TextWriter writer = new StringWriter())
                {
                    dtassigndet.TableName = "TopicCompanyAssign";
                    dtassigndet.WriteXml(writer);
                    xmlString = writer.ToString();
                    objTopicPL.TopicXml = xmlString;
                }
                objTopicBL.AddCompanyTopicAssign(objTopicPL);
                NotifyMessages(objTopicPL.ErrorMessage, Common.ErrorType.Error);
                BindDetails();

            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindDetails();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}