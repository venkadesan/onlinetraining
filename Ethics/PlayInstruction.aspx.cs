﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using OnlineTraining.Code;
using OnlineTrainingBL;
using OnlineTrainingPL;
using OnlineTrainingPL.Master;

namespace OnlineTraining
{
    public partial class PlayInstruction : System.Web.UI.Page
    {

        BLPlayInstruction objBLPlayInstruction = new BLPlayInstruction();
        PLPlayInstruction objPLPlayInstruction = new PLPlayInstruction();

        protected void Page_Load(object sender, EventArgs e)
        {

          if(!IsPostBack)
          {
              LoadInstructions();
          }
        }

       


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (textinstruction.Text == string.Empty)
            {
                NotifyMessages("Please enter the instruction", Common.ErrorType.Error);
                return;
            }

            objPLPlayInstruction.CompanyId = UserSession.CompanyIDUser;
            objPLPlayInstruction.PlayInstructionText = this.textinstruction.Text.Trim();
            string[] result = objBLPlayInstruction.SaveUpdateInstructions(objPLPlayInstruction);
        }


        private void LoadInstructions()
        {
            try
            {
                int InstructionId=0;
                objPLPlayInstruction.CompanyId= UserSession.CompanyIDUser;
                objPLPlayInstruction.PlayInstructionId = InstructionId;
                DataSet ds = new DataSet();
                ds = objBLPlayInstruction.GetInstructions(objPLPlayInstruction);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    this.textinstruction.Text = ds.Tables[0].Rows[0]["PlayInstructions"].ToString();
                    this.textinstruction.DataBind();
                }
                else
                {
                    this.textinstruction.Text = string.Empty;
                }
            }

            catch (Exception ex)
            {

                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divmsg.Visible = false;
        }


    }
}