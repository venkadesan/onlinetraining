﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="dashboard.aspx.cs" Inherits="Ethics.dashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(
        /* This is the function that will get executed after the DOM is fully loaded */
            function () {
                $(".dt").datepicker({
                    changeMonth: true, //this option for allowing user to select month
                    changeYear: true //this option for allowing user to select from year range
                });
            }
        );
    </script>
    <style type="text/css">
        .card-container .card.card-amethyst .front
        {
            color: white;
            background-color: brown;
        }
        
        .card-container .card.card-cyan .front
        {
            color: white;
            background-color: orange;
        }
    </style>
    <script>
        function divdashboardClick(type) {
            document.getElementById('<%=hdntypeid.ClientID%>').value = type;

            //            var totalcount = '0';

            //            if (type == '1') {
            //                totalcount = document.getElementById('<%=lblFirstvalue.ClientID%>').innerText;
            //            }

            //            if (type == '2') {
            //                totalcount = document.getElementById('<%=lblsecondvalue.ClientID%>').innerText;
            //            }

            //            if (type == '3') {
            //                totalcount = document.getElementById('<%=lblThirdvalue.ClientID%>').innerText;
            //            }
            //            if (type == '4') {
            //                totalcount = document.getElementById('<%=lblFourthvalue.ClientID%>').innerText;
            //            }
            //           
            //            if (totalcount != '0')
            document.getElementById('<%= btnbindraw.ClientID%>').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="brownish-scheme">
        <!-- Wrap all page content here -->
        <div id="wrap">
            <!-- Make page fluid -->
            <div class="row">
                <!-- submenu -->
                <div class="submenu">
                    <h1>
                        <asp:Literal ID="lmainheader" runat="server" Text="Dashboard"></asp:Literal>
                    </h1>
                    <h1>
                        <asp:Literal ID="lsubheader" runat="server" Text="Dashboard"></asp:Literal>
                    </h1>
                </div>
                <!-- /submenu -->
                <!-- content main container -->
                <div class="main">
                    <div class="row border-bottom margin-vertical-15">
                        <div class="col-md-8 col-xs-offset-4">
                            <div id="basicvalidations" parsley-validate="" role="form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label" for="">
                                        From</label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="tfromdate" runat="server" class="dt form-control parsley-validated "></asp:TextBox>
                                        <ajax:CalendarExtender ID="calfromdate" runat="server" TargetControlID="tfromdate"
                                            Format="MM/dd/yyyy" />
                                    </div>
                                    <label class="col-sm-1 control-label margin-left15" for="">
                                        To</label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="ttodate" runat="server" class="form-control parsley-validated dt"></asp:TextBox>
                                        <ajax:CalendarExtender ID="caltodate" runat="server" TargetControlID="ttodate" Format="MM/dd/yyyy" />
                                    </div>
                                    <asp:Button ID="btngo" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btngo_Click" />
                                    <asp:Button ID="btnbindraw" runat="server" Text="bind" Style="display: none;" OnClick="btnbindraw_Click" />
                                    <asp:HiddenField ID="hdntypeid" Value="0" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- cards -->
                    <div class="row cards">
                        <div class="card-container col-lg-2 col-md-6 col-sm-12" style="height: 193px;">
                            <div class="card card-amethyst hover" onclick="divdashboardClick('1')">
                                <div class="front" id="First" runat="server" style="padding: 40px">
                                    <h1>
                                        <asp:Label ID="lblFirst" runat="server" Text="Total Training" ForeColor="White"></asp:Label></h1>
                                    <p id="users-count">
                                        <asp:Label ID="lblFirstvalue" runat="server" Text="0" ForeColor="White"></asp:Label></p>
                                </div>
                                <div class="back nopadding" id="FirstBack" runat="server">
                                    <!-- Button trigger modal -->
                                </div>
                            </div>
                        </div>
                        <div class="card-container col-lg-2 col-md-6 col-sm-12" style="height: 193px;">
                            <div class="card card-cyan hover" onclick="divdashboardClick('2')">
                                <div class="front" id="Second" runat="server" style="padding: 40px">
                                    <h1>
                                        <asp:Label ID="lblSecond" runat="server" Text="Online Test Appeared" ForeColor="White"></asp:Label></h1>
                                    <p id="P1">
                                        <asp:Label ID="lblsecondvalue" runat="server" Text="0" ForeColor="White"></asp:Label></p>
                                </div>
                                <div class="back nopadding" id="SecondBack" runat="server">
                                    <!-- Button trigger modal -->
                                </div>
                            </div>
                        </div>
                        <div class="card-container col-lg-2 col-md-6 col-sm-12" style="height: 193px;">
                            <div class="card card-green hover" onclick="divdashboardClick('3')">
                                <div class="front" id="Third" runat="server" style="padding: 40px">
                                    <h1>
                                        <asp:Label ID="lblThird" runat="server" Text="Online Test Cleared" ForeColor="White"></asp:Label></h1>
                                    <p id="P2">
                                        <asp:Label ID="lblThirdvalue" runat="server" Text="0" ForeColor="White"></asp:Label></p>
                                </div>
                                <div class="back nopadding" id="ThirdBack" runat="server">
                                    <!-- Button trigger modal -->
                                </div>
                            </div>
                        </div>
                        <div class="card-container col-lg-2 col-md-6 col-sm-12" style="height: 193px;">
                            <div class="card card-red hover" onclick="divdashboardClick('4')">
                                <div class="front" id="Fourth" runat="server" style="padding: 40px">
                                    <h1>
                                        <asp:Label ID="lblFourth" runat="server" Text="Online Test Not Cleared" ForeColor="White"></asp:Label></h1>
                                    <p id="P3">
                                        <asp:Label ID="lblFourthvalue" runat="server" Text="0" ForeColor="White"></asp:Label></p>
                                </div>
                                <div class="back nopadding" id="FourthBack" runat="server">
                                    <!-- Button trigger modal -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive" id="divtotalTraining" runat="server">
                        <asp:Repeater ID="rptrTotalTraining" runat="server">
                            <HeaderTemplate>
                                <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                    <tr class="new-color">
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            <asp:Label Text="Topic Name" runat="server" ID="lSNo" />
                                        </th>
                                        <th>
                                            <asp:Label Text="Training Date" runat="server" ID="ltraining" />
                                        </th>
                                        <th>
                                            <asp:Label Text="Training Time" runat="server" ID="Label3" />
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("EMailID") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lTopicName" runat="server" Text='<%# Eval("TopicName") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lTrainingDate" runat="server" Text='<%# Eval("TrainingDate") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lTrainingTime" runat="server" Text='<%# Eval("TrainingTime") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="table-responsive" id="divtotaltest" runat="server">
                        <asp:Repeater ID="rptrtotaltest" runat="server">
                            <HeaderTemplate>
                                <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                    <tr class="new-color">
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            <asp:Label Text="Topic Name" runat="server" ID="lSNo" />
                                        </th>
                                        <th>
                                            <asp:Label Text="Test Date" runat="server" ID="ltraining" />
                                        </th>
                                        <th>
                                            <asp:Label Text="Test Time" runat="server" ID="Label3" />
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("EMailID") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lTopicName" runat="server" Text='<%# Eval("TopicName") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lTestDate" runat="server" Text='<%# Eval("TestDate") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lTestTime" runat="server" Text='<%# Eval("TestTime") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="table-responsive" id="divpassfailDet" runat="server">
                        <asp:Repeater ID="rptrpassfail" runat="server">
                            <HeaderTemplate>
                                <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                    <tr class="new-color">
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            <asp:Label Text="Topic Name" runat="server" ID="lSNo" />
                                        </th>
                                        <th>
                                            <asp:Label Text="Test Date" runat="server" ID="ltraining" />
                                        </th>
                                        <th>
                                            <asp:Label Text="Test Time" runat="server" ID="Label3" />
                                        </th>
                                        <th>
                                            <asp:Label Text="Marks Got" runat="server" ID="Label1" />
                                        </th>
                                        <th>
                                            <asp:Label Text="Pass Percentage" runat="server" ID="Label2" />
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("EMailID") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lTopicName" runat="server" Text='<%# Eval("TopicName") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lTestDate" runat="server" Text='<%# Eval("TestDate") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lTestTime" runat="server" Text='<%# Eval("TestTime") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("GotMarks") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Text='<%# Eval("PassPercentage") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
