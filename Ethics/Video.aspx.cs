﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTraining.Code;
using OnlineTrainingPL;
using OnlineTrainingBL;
using System.Data;
using System.IO;
using OnlineTrainingPL.Master;
using System.Web.UI.HtmlControls;

namespace OnlineTraining
{
    public partial class Video : System.Web.UI.Page
    {
        PLTopic objTopicPL = new PLTopic();
        BLTopic objTopicBL = new BLTopic();
        long totalsecond = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Int64 time = 0;
                Int64.TryParse(this.totalmin.Value, out time);
                if (time != 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "redirect();", true);
                }
                if (!IsPostBack)
                {
                    lblTopicName.Text = "Online Training";
                    loadtopics();
                    Userwiseloadtopics();
                    ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "redirect();", true);
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }

        }

        private void BindPlayInstruction()
        {
            BLPlayInstruction objBLPlayInstruction = new BLPlayInstruction();
            PLPlayInstruction objPLPlayInstruction = new PLPlayInstruction();
            int InstructionId = 0;
            objPLPlayInstruction.CompanyId = UserSession.CompanyIDUser;
            objPLPlayInstruction.PlayInstructionId = InstructionId;
            DataSet ds = new DataSet();
            ds = objBLPlayInstruction.GetInstructions(objPLPlayInstruction);

            if (ds.Tables[0].Rows.Count > 0)
            {
                this.lblplayinstruction.Text = ds.Tables[0].Rows[0]["PlayInstructions"].ToString();
            }
            else
            {
                this.lblplayinstruction.Text = string.Empty;
            }
        }


        private void loadtopics()
        {
            objTopicPL.CompanyID = UserSession.CompanyIDUser;
            objTopicPL.TopicID = 0;
            DataSet Ds = new DataSet();
            Ds = objTopicBL.GetTopicByCompanyID(objTopicPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                ddltopics.DataSource = Ds;
                ddltopics.DataTextField = "TopicName";
                ddltopics.DataValueField = "TopicID";
                ddltopics.DataBind();
                ddltopics.Items.Insert(0, new ListItem("Select", "0"));
            }
            else
            {
                ddltopics.DataSource = Ds;
                ddltopics.DataBind();
            }
            btntest.Visible = false;
        }

        private void Userwiseloadtopics()
        {
            try
            {
                objTopicPL.CompanyID = UserSession.CompanyIDUser;
                objTopicPL.UserID = UserSession.UserID;
                objTopicPL.TopicID = 0;
                DataSet ds = new DataSet();
                ds = objTopicBL.GetTopicNameByUserID(objTopicPL);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddltopics.DataSource = ds;
                    ddltopics.DataTextField = "TopicName";
                    ddltopics.DataValueField = "TopicID";
                    ddltopics.DataBind();
                    ddltopics.Items.Insert(0, new ListItem("Select", "0"));
                }
                else
                {
                    ddltopics.DataSource = ds;
                    ddltopics.DataBind();
                }
                btntest.Visible = false;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void LoadDetails()
        {
            BindPlayInstruction();

            objTopicPL.CompanyID = UserSession.CompanyIDUser;
            objTopicPL.LocationID = UserSession.LocationIDCurrent;
            int topicid = 0;
            if (!Common.DDVal(ddltopics, out topicid))
            {
                NotifyMessages("Please select topic", Common.ErrorType.Error);
                return;
            }
            objTopicPL.TopicID = topicid;
            objTopicPL.UserID = UserSession.UserID;
            UserSession.topicid = topicid;
            objTopicPL.Groupid = UserSession.GroupIDCurrent;

            //DataSet dsMaximumCheck=new DataSet();
            //dsMaximumCheck = objTopicBL.CheckMaximumAttempts(objTopicPL);
            //if (dsMaximumCheck.Tables.Count > 0 && dsMaximumCheck.Tables[0].Rows.Count > 0)
            //{
            //    DataRow drAllowed = dsMaximumCheck.Tables[0].Rows[0];
            //    bool isallowed = Convert.ToBoolean(drAllowed["Allowed"].ToString());

            //    if (!isallowed)
            //    {
            //        NotifyMessages("Maximum Attempts Exceeds for you.",Common.ErrorType.Error);
            //        return;
            //    }
            //}

            DataSet Ds = new DataSet();
            Ds = objTopicBL.GetActiveTopicDetailsWithGroup(objTopicPL);
            string fileext = string.Empty;
            if (Ds.Tables[0].Rows.Count > 0)
            {
                //Ds.Tables[0].Rows[0]["TrainingDuration"].ToString()

                string[] split = Ds.Tables[0].Rows[0]["TrainingDuration"].ToString().Split(':');

                if (split.Length == 2)
                {
                    int hrsecod = Convert.ToInt32(split[0]) * 60;
                    hrsecod = hrsecod * 60;

                    int Minsecod = Convert.ToInt32(split[1]) * 60;

                    long totalMinutes = hrsecod + Minsecod;

                    totalsecond = totalMinutes;
                    totalmin.Value = totalMinutes.ToString();
                }

                bool isTestRequired = false;
                if (Ds.Tables[0].Rows[0]["isTestRequired"].ToString() != string.Empty)
                {
                    isTestRequired = Convert.ToBoolean(Ds.Tables[0].Rows[0]["isTestRequired"].ToString());
                }

                hdntestrequired.Value = isTestRequired.ToString();

                fileext = Path.GetExtension(Ds.Tables[0].Rows[0]["videoname"].ToString());

                string fileextension2 = Path.GetExtension(Ds.Tables[0].Rows[0]["videoname1"].ToString());
                string filename2 = Ds.Tables[0].Rows[0]["videoname1"].ToString();

                if (fileext == ".mp4")
                {
                    lblTopicName.Text = "Online Training -" + Ds.Tables[0].Rows[0]["TopicName"].ToString();
                    rpt.DataSource = Ds;
                    rpt.DataBind();
                    Repeater1.Visible = false;
                    btntest.Visible = false;
                    rpt.Visible = true;

                    myframeppt.Visible = false;
                }
                else
                    if (fileext == ".pdf")
                    {
                        lblTopicName.Text = "Online Training -" + Ds.Tables[0].Rows[0]["TopicName"].ToString();
                        rpt.Visible = false;
                        Repeater1.Visible = true;
                        //myframe.Attributes.Add("src", "http://localhost/"+Ds.Tables[0].Rows[0]["videoname"].ToString());
                        Repeater1.DataSource = Ds;
                        Repeater1.DataBind();
                        //btntest.Visible = true;
                        myframeppt.Visible = false;
                    }
                    else
                        if (fileext == ".ppt")
                        {
                            lblTopicName.Text = "Online Training -" + Ds.Tables[0].Rows[0]["TopicName"].ToString();
                            rpt.Visible = false;
                            Repeater1.Visible = false;
                            myframeppt.Visible = true;
                            myframeppt.Attributes.Add("src", "http://docs.google.com/gview?url=http://ifazility.com/onlinetraining/" + Ds.Tables[0].Rows[0]["videoname"].ToString() + "&embedded=true");

                            //btntest.Visible = true;
                        }

                if (fileextension2 != string.Empty)
                {
                    divsecondAttachments.Visible = true;

                    if (fileextension2 == ".mp4")
                    {
                        lblTopicName.Text = "Online Training -" + Ds.Tables[0].Rows[0]["TopicName"].ToString();
                        rptrvideo2.DataSource = Ds;
                        rptrvideo2.DataBind();
                        rptrpdf2.Visible = false;
                        btntest.Visible = false;
                        rptrvideo2.Visible = true;
                        myframeppt2.Visible = false;
                    }
                    else
                        if (fileextension2 == ".pdf")
                        {
                            lblTopicName.Text = "Online Training -" + Ds.Tables[0].Rows[0]["TopicName"].ToString();
                            rptrvideo2.Visible = false;
                            rptrpdf2.Visible = true;
                            //myframe.Attributes.Add("src", "http://localhost/"+Ds.Tables[0].Rows[0]["videoname"].ToString());
                            rptrpdf2.DataSource = Ds;
                            rptrpdf2.DataBind();
                            //btntest.Visible = true;
                            myframeppt2.Visible = false;
                        }
                        else
                            if (fileextension2 == ".ppt")
                            {
                                lblTopicName.Text = "Online Training -" + Ds.Tables[0].Rows[0]["TopicName"].ToString();
                                rptrvideo2.Visible = false;
                                rptrpdf2.Visible = false;
                                myframeppt2.Visible = true;
                                myframeppt2.Attributes.Add("src", "http://docs.google.com/gview?url=http://ifazility.com/onlinetraining/" + Ds.Tables[0].Rows[0]["videoname"].ToString() + "&embedded=true");

                                //btntest.Visible = true;
                            }
                }
                else
                {
                    divsecondAttachments.Visible = false;
                }
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {

        }

        protected void myVideo_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void myVideo_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void btntest_Click(object sender, EventArgs e)
        {
            Response.Redirect("onlinetraining.aspx");
        }

        protected void btngo_Click(object sender, EventArgs e)
        {
            divmsg.Visible = false;
            lblError.Text = string.Empty;
            int topicid = 0;
            Common.DDVal(ddltopics, out topicid);
            if (topicid != 0)
            {
                objTopicPL.TopicID = topicid;
                objTopicPL.UserID = UserSession.UserID;

                DataSet dsMaximumCheck = new DataSet();
                dsMaximumCheck = objTopicBL.CheckMaximumAttempts(objTopicPL);
                if (dsMaximumCheck.Tables.Count > 0 && dsMaximumCheck.Tables[0].Rows.Count > 0)
                {
                    DataRow drAllowed = dsMaximumCheck.Tables[0].Rows[0];
                    bool isallowed = Convert.ToBoolean(drAllowed["Allowed"].ToString());

                    if (!isallowed)
                    {
                        NotifyMessages("Maximum Attempts Exceeds for you for this Topic.", Common.ErrorType.Error);
                        return;
                    }
                }

                LoadDetails();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myScript", "redirect();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "timefuntion();", true);
            }
            //ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "redirect();", true);
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void btnTestRequied_Click(object sender, EventArgs e)
        {
            try
            {
                string isTestReq = hdntestrequired.Value.ToString();
                int topicid = 0;
                Common.DDVal(ddltopics, out topicid);
                objTopicPL.TopicID = topicid;
                objTopicPL.CUID = UserSession.UserID;
                objTopicPL.TrainingDate = DateTime.Now;
                objTopicBL.InsertTrainingDetails(objTopicPL);
                Response.Redirect(isTestReq.ToLower() == "true" ? "Agree.aspx" : "Thanks.aspx");
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlControl myVideo = (HtmlControl)e.Item.FindControl("myVideo");
                if (myVideo != null)
                {
                    myVideo.Attributes.Add("ended", "timeoutfuntion()");
                }
            }
        }
    }
}