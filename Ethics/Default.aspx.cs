﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTraining.Code;
using System.Data;
using System.Web.UI.HtmlControls;
using OnlineTrainingBL;
using OnlineTrainingPL.Master;
using OnlineTrainingPL;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Diagnostics;
using System.Text;

namespace OnlineTraining
{
    public partial class Default : PageBase
    {
        PLAnswer objAnswerPL = new PLAnswer();
        BLAnswer objAnswerBL = new BLAnswer();
        PLQuestion objQuestionPL = new PLQuestion();
        BLQuestion objQuestionBL = new BLQuestion();
        PLTopic objTopicPL = new PLTopic();
        BLTopic objTopicBL = new BLTopic();
        PLTest objTestPL = new PLTest();
        BLTest objTestBL = new BLTest();
        static int PersentageResultCounts = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                UserSession.LocationIDCurrent = 1;

                if (UserSession.CompanyIDCurrent > 0 && UserSession.LocationIDCurrent > 0 )
                {
                    LoadDetails();
                    //imgReYes.AlternateText = "Correct";
                    //imgReNo.AlternateText = "Wrong";
                    //ImgReNA.AlternateText = "Not Attend";
                    ShowAnswerButton();
                }
            }
        }

        private void ShowAnswerButton()
        {
            try
            {
                objTopicPL.TopicID = UserSession.topicid;
                objTopicPL.UserID = UserSession.UserID;

                DataSet dsMaximumCheck = new DataSet();
                dsMaximumCheck = objTopicBL.CheckMaximumAttempts(objTopicPL);
                if (dsMaximumCheck.Tables.Count > 0 && dsMaximumCheck.Tables[0].Rows.Count > 0)
                {
                    DataRow drAllowed = dsMaximumCheck.Tables[0].Rows[0];
                    bool isallowed = Convert.ToBoolean(drAllowed["Allowed"].ToString());

                    btnshowscore.Visible = !isallowed;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            LoadDetails();
        }

        public void LoadDetails()
        {
            objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
            objTopicPL.LocationID = UserSession.LocationIDCurrent;
            objTopicPL.TopicID = UserSession.topicid;
            objTopicPL.Groupid = UserSession.GroupIDCurrent;
            DataSet Ds = new DataSet();
            //Ds = objTopicBL.GetTopicDetails(objTopicPL);
            Ds = objTopicBL.GetActiveTopicDetailsWithGroup(objTopicPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                PersentageResultCounts = 0;
                rptrTopic.DataSource = Ds;
                rptrTopic.DataBind();
            }
            else
            {
                rptrTopic.DataSource = null;
                rptrTopic.DataBind();
            }
        }

        private void EmptyGrid()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TopicName");
            dt.Columns.Add("Description");
            dt.Columns.Add("PassPercentage");
            dt.Columns.Add("TopicID");
            DataRow dr;
            dr = dt.NewRow();
            dr["TopicName"] = "";
            dr["Description"] = "";
            dr["PassPercentage"] = "";
            dr["TopicID"] = 0;
            dt.Rows.Add(dr);
            rptrTopic.DataSource = dt;
            rptrTopic.DataBind();
        }

        protected void rptrTopic_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void rptrTopic_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    HtmlContainerControl dtPer = (HtmlContainerControl)e.Item.FindControl("dtPer");
                    HtmlContainerControl dPPer = (HtmlContainerControl)e.Item.FindControl("dPPer");
                    HiddenField hTopicID = (HiddenField)e.Item.FindControl("hTopicID");
                    HiddenField hPassPercentage = (HiddenField)e.Item.FindControl("hPassPercentage");
                    Label lblPassPercentage = (Label)e.Item.FindControl("lblPassPercentage");
                    Label lblTestPersentage = (Label)e.Item.FindControl("lblTestPersentage");
                    Repeater rptrAnswer = (Repeater)e.Item.FindControl("rptrAnswer");
                    DataSet Ds = new DataSet();
                    objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
                    objQuestionPL.LocationID = UserSession.LocationIDCurrent;
                    objQuestionPL.TopicID = Convert.ToInt32(hTopicID.Value);
                    Ds = objQuestionBL.GetQuestionDetails(objQuestionPL);
                    if (Ds.Tables[0].Rows.Count > 0)
                    {
                        rptrAnswer.DataSource = Ds;
                        rptrAnswer.DataBind();
                    }
                    else
                    {
                        rptrAnswer.DataSource = null;
                        rptrAnswer.DataBind();
                    }

                    for (int items = 0; items < rptrAnswer.Items.Count; items++)
                    {
                        Repeater rptrAnswer1 = (Repeater)rptrAnswer.Items[items].FindControl("rptrAnswer1");
                        HiddenField hQuestionID = rptrAnswer.Items[items].FindControl("hQuestionID") as HiddenField;
                        ImageButton imgResultYes = rptrAnswer.Items[items].FindControl("imgResultYes") as ImageButton;
                        ImageButton imgResultNO = rptrAnswer.Items[items].FindControl("imgResultNo") as ImageButton;
                        ImageButton ImgResultNA = rptrAnswer.Items[items].FindControl("ImgResultNA") as ImageButton;
                        DataSet Ds2 = new DataSet();
                        objAnswerPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                        Ds2 = objAnswerBL.GetAnswerKeyDetails(objAnswerPL);
                        if (Ds2.Tables[0].Rows.Count > 0)
                        {
                            string[] answerCount = new string[100];
                            string[] testAnswer = new string[100];
                            if (Ds2.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < Ds2.Tables[0].Rows.Count; i++)
                                {
                                    answerCount[i] = Ds2.Tables[0].Rows[i]["AnswerID"].ToString();
                                }
                            }
                            DataSet Ds1 = new DataSet();
                            objTestPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                            objTestPL.CompanyID = UserSession.CompanyIDCurrent1;
                            objTestPL.LocationID = UserSession.LocationIDCurrent;
                            objTestPL.CUID = UserSession.UserID;
                            Ds1 = objTestBL.CheckResults(objTestPL);
                            if (Ds1.Tables[0].Rows.Count > 0)
                            {
                                for (int j = 0; j < Ds1.Tables[0].Rows.Count; j++)
                                {
                                    testAnswer[j] = Ds1.Tables[0].Rows[j]["AnswerID"].ToString();
                                }
                            }
                            if (Ds2.Tables[0].Rows.Count == 0 || Ds1.Tables[0].Rows.Count == 0)
                            {
                                //rptrAnswer.Visible = false;
                                //rptrTopic.Visible = false;
                                imgResultYes.Visible = false;
                                imgResultNO.Visible = false;
                                ImgResultNA.Visible = true;
                            }
                            else
                            {
                                bool areEqual = answerCount.SequenceEqual(testAnswer);

                                if (areEqual)
                                {
                                    PersentageResultCounts++;
                                    imgResultYes.Visible = true;
                                    imgResultNO.Visible = false;
                                    ImgResultNA.Visible = false;
                                }
                                else
                                {
                                    imgResultYes.Visible = false;
                                    imgResultNO.Visible = true;
                                    ImgResultNA.Visible = false;
                                }

                                try
                                {
                                    objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
                                    objQuestionPL.LocationID = UserSession.LocationIDCurrent;
                                    objQuestionPL.QuestionID = 0;
                                    int TotalQuestionCount = 0;
                                    DataSet DsQuestion = new DataSet();
                                    DsQuestion = objQuestionBL.GetALLQuestionDetails(objQuestionPL);
                                    if (Ds.Tables[0].Rows.Count > 0)
                                        TotalQuestionCount = DsQuestion.Tables[0].Rows.Count;
                                    else
                                        TotalQuestionCount = DsQuestion.Tables[0].Rows.Count;
                                    double Persentage = (Convert.ToDouble(PersentageResultCounts) / TotalQuestionCount) * 100;
                                    string[] Value = Persentage.ToString().Split('.');
                                    int TestPersentage = 0;
                                    if (Convert.ToInt32(Value[0].ToString()) > 0)
                                    {
                                        TestPersentage = Convert.ToInt32(Value[0].ToString());
                                        bool isDecimal = Persentage.ToString().Contains('.');
                                        if (isDecimal)
                                        {
                                            if (Value[1].ToString() == null)
                                            {
                                                lblTestPersentage.Text = Value[0].ToString() + " %";
                                            }
                                            else if (Value[1].ToString().Length == 1)
                                            {
                                                lblTestPersentage.Text = Persentage.ToString() + " %";
                                            }
                                            else if (Value[1].ToString().Length > 1)
                                            {
                                                int Ditits = Convert.ToInt32(Value[1].ToString().Substring(0, 2));
                                                if (Ditits > 50)
                                                {
                                                    TestPersentage = TestPersentage + 1;
                                                    lblTestPersentage.Text = TestPersentage.ToString() + "%";
                                                }
                                                else
                                                {
                                                    lblTestPersentage.Text = TestPersentage.ToString() + "." + Ditits + "%";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            lblTestPersentage.Text = TestPersentage.ToString() + ".00 %";
                                        }
                                    }
                                }
                                catch { }
                            }
                        }
                    }
                }
                catch { }
            }
        }

        protected void rptrAnswer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void rptrAnswer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                
            }
        }

        #region "Pdf"
        protected void PdfDetails()
        {
            //Create document
            Document doc = new Document();
            //Create PDF Table
            PdfPTable tableLayout = new PdfPTable(4);

            //Create a PDF file in specific path
            PdfWriter.GetInstance(doc, new FileStream(Server.MapPath("Com.pdf"), FileMode.Create));

            //Open the PDF document
            doc.Open();

            //Add Content to PDF
            doc.Add(Add_Content_To_PDF(tableLayout));

            // Closing the document
            doc.Close();
            Process.Start(Server.MapPath("Com.pdf"));
            // btnGeneratePDFFile.Enabled = true;
            //btnOpenPDFFile.Enabled = false;
        }

        private PdfPTable Add_Content_To_PDF(PdfPTable tableLayout)
        {
            float[] headers = { 20, 20, 30, 30 };  //Header Widths
            tableLayout.SetWidths(headers);        //Set the pdf headers
            tableLayout.WidthPercentage = 80;       //Set the PDF File witdh percentage

            //Add Title to the PDF file at the top
            tableLayout.AddCell(new PdfPCell(new Phrase("Compliance with the Code of Business Ethics", new Font(Font.HELVETICA, 13, 1, new iTextSharp.text.Color(0, 0, 0)))) { Colspan = 4, Border = 0, PaddingBottom = 20, HorizontalAlignment = Element.ALIGN_CENTER });

            //Add header
            AddCellToHeader(tableLayout, "Cricketer Name");
            AddCellToHeader(tableLayout, "Height");
            AddCellToHeader(tableLayout, "Born On");
            AddCellToHeader(tableLayout, "Parents");

            //Add body
            AddCellToBody(tableLayout, "Sachin Tendulkar");
            AddCellToBody(tableLayout, "1.65 m");
            AddCellToBody(tableLayout, "April 24, 1973");
            AddCellToBody(tableLayout, "Ramesh Tendulkar, Rajni Tendulkar");

            AddCellToBody(tableLayout, "Mahendra Singh Dhoni");
            AddCellToBody(tableLayout, "1.75 m");
            AddCellToBody(tableLayout, "July 7, 1981");
            AddCellToBody(tableLayout, "Devki Devi, Pan Singh");

            AddCellToBody(tableLayout, "Virender Sehwag");
            AddCellToBody(tableLayout, "1.70 m");
            AddCellToBody(tableLayout, "October 20, 1978");
            AddCellToBody(tableLayout, "Aryavir Sehwag, Vedant Sehwag");

            AddCellToBody(tableLayout, "Virat Kohli");
            AddCellToBody(tableLayout, "1.75 m");
            AddCellToBody(tableLayout, "November 5, 1988");
            AddCellToBody(tableLayout, "Saroj Kohli, Prem Kohli");

            return tableLayout;
        }

        // Method to add single cell to the header
        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.HELVETICA, 8, 1, iTextSharp.text.Color.WHITE))) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 5, BackgroundColor = new iTextSharp.text.Color(0, 51, 102) });
        }

        // Method to add single cell to the body
        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.HELVETICA, 8, 1, iTextSharp.text.Color.BLACK))) { HorizontalAlignment = Element.ALIGN_CENTER, Padding = 5, BackgroundColor = iTextSharp.text.Color.WHITE });
        }
        #endregion

        protected void btnshowscore_Click(object sender, EventArgs e)
        {
            try
            {
                objTopicPL.TopicID = UserSession.topicid;
                DataSet Ds = new DataSet();
                //Ds = objTopicBL.GetTopicDetails(objTopicPL);
                Ds = objTopicBL.GetAnswerDetails(objTopicPL);
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    rptrAnswer.DataSource = Ds;
                    rptrAnswer.DataBind();
                }
                else
                {
                    rptrAnswer.DataSource = null;
                    rptrAnswer.DataBind();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}