﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTraining.Code;
using System.Data;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using OnlineTrainingPL;
using OnlineTrainingBL.Master;
using OnlineTrainingPL.Master;
using OnlineTrainingBL;
using OnlineTraining.Master;

namespace OnlineTraining
{
    public partial class Welcome : System.Web.UI.Page
    {
        KB.BLRoles.BLRoleCompany objRoleComp = new KB.BLRoles.BLRoleCompany();
        KB.BLRoles.BLRoleLocation objRoleLoc = new KB.BLRoles.BLRoleLocation();
        KB.BLCompany objCompany = new KB.BLCompany();
        KB.BLLocation objLocation = new KB.BLLocation();
        PLEmployee objEmployeePL = new PLEmployee();
        BLEmployee objEmployeeBL = new BLEmployee();
        PLTest objTestPL = new PLTest();
        BLTest objTestBL = new BLTest();

        PLTopic objTopicPL = new PLTopic();
        BLTopic objTopicBL = new BLTopic();

        KB.BLUser.BLUserLocation objuserLocation1 = new KB.BLUser.BLUserLocation();
        KB.BLUser.BLUserCompany objuserCompany = new KB.BLUser.BLUserCompany();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    this.txtemailid.Text = UserSession.UserLastName;
                    DataSet Ds = new DataSet();
                    objEmployeePL.EmailID = this.txtemailid.Text;
                    Ds = objEmployeeBL.MainLoginUserID(objEmployeePL);
                    if (Ds.Tables[0].Rows.Count > 0)
                    {
                        UserSession.UserIDs = Convert.ToInt32(Ds.Tables[0].Rows[0]["cuid"].ToString());
                    }
                 
                    LoadDDPermittedCompany();
                    LoadDDPermittedLocation();
                }
                catch { }
            }
        }

        private void LoadDDPermittedCompany()
        {
            DataSet ds = new DataSet();
           
            //ds = objRoleComp.GetRoleCompany(UserSession.RoleID, UserSession.CompanyIDUser);
            //ds = objuserCompany.GetUserCompany(UserSession.UserIDs);            

            //DataTable dFiltered = ds.Tables[0].Clone();
            //DataRow[] dRowUpdates = ds.Tables[0].Select(" UserCompanyID > 0", "companyname");
            //foreach (DataRow dr in dRowUpdates)
            //    dFiltered.ImportRow(dr);

            BLTopic obj = new BLTopic();
            //jll Group
            ds = obj.GetCompany(3);
            this.ddlcompany.DataSource = ds;
            this.ddlcompany.DataTextField = "CompanyName";
            this.ddlcompany.DataValueField = "CompanyID";
            this.ddlcompany.DataBind();

            //
            //get companies Country ID
            //
            DataSet dscomp = new DataSet();
            dscomp = objCompany.GetCompany(UserSession.CompanyIDCurrent);

            try
            {
                this.ddlcompany.ClearSelection();
                this.ddlcompany.Items.FindByValue(UserSession.CompanyIDCurrent.ToString()).Selected = true;
            }
            catch
            {

            }
        }

        private void LoadDDPermittedLocation()
        {
            DataSet ds = new DataSet();
            //ds = objuserLocation1.GetUserLocation(UserSession.RoleID, UserSession.CompanyIDCurrent);
            //ds = objuserLocation1.GetUserLocation(UserSession.UserIDs, Convert.ToInt32(ddlcompany.SelectedItem.Value));

            //DataTable dFiltered = ds.Tables[0].Clone();

            BLTopic obj = new BLTopic();
            //jll Group
            ds = obj.GetLocation(Convert.ToInt32(ddlcompany.SelectedItem.Value));
            
            if (ds.Tables[0].Rows.Count > 0)
            {
                //DataTable dFiltered = ds;
                //DataRow[] dRowUpdates = ds.Tables[0].Select(" UserLocationID > 0", "locationname");
                //foreach (DataRow dr in dRowUpdates)
                //    dFiltered.ImportRow(dr);

                this.ddlLocation.DataSource = ds;
                this.ddlLocation.DataTextField = "locationname";
                this.ddlLocation.DataValueField = "locationid";
                this.ddlLocation.DataBind();
            }
            else
            {
                this.ddlLocation.DataSource = null;
                this.ddlLocation.DataTextField = "locationname";
                this.ddlLocation.DataValueField = "locationid";
                this.ddlLocation.DataBind();
            }           
        }

        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadDDPermittedLocation();
            }
            catch { }
        }

        private bool IsPassed(string emailID)
        {
            ViewState["percentage"] = "";
            objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
            objTopicPL.LocationID = UserSession.LocationIDCurrent;
            DataSet Ds = new DataSet();
            Ds = objTopicBL.GetActiveTopicDetails(objTopicPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                int topID = 0;
                int.TryParse(Ds.Tables[0].Rows[0]["TopicID"].ToString(), out  topID);
                
                objTopicPL.TopicID = topID;
                DataSet ds1 = new DataSet();
                if(emailID !=null && emailID !=string.Empty)
                ds1 = objTopicBL.CheckPassedstatus(emailID, objTopicPL);
                if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                {
                    ViewState["percentage"] = ds1.Tables[0].Rows[0]["PassPercentage"].ToString();
                    return true;
                }
            }
            return false;
        }

        protected void btnSubmitreg_Click(object sender, EventArgs e)
        {
            objTestPL.CompanyID = Convert.ToInt32(ddlcompany.SelectedItem.Value); 
            objTestPL.LocationID = Convert.ToInt32(ddlLocation.SelectedItem.Value);
            objTestPL.AnswerName = UserSession.UserFirstName;
            objTestPL.CUID = UserSession.UserIDs;
            objEmployeePL.FirstName = txtfirstname.Text.Trim();
            objEmployeePL.LastName = txtlastName.Text.Trim();
            objEmployeePL.EmailID = txtemailid.Text.Trim();
            
            objEmployeePL.DepartmentName = txtprojectName.Text.Trim();
            objEmployeePL.Designation = txtDesignation.Text.Trim();
            objEmployeePL.EmployeeCode = txtEmployeeCode.Text.Trim();
            objEmployeePL.CompanyID = Convert.ToInt32(ddlcompany.SelectedItem.Value);
            //UserSession.CompanyIDCurrent1 = Convert.ToInt32(ddlcompany.SelectedItem.Value);
            UserSession.CompanyIDCurrent =  1;
            objEmployeePL.LocationID = Convert.ToInt32(ddlLocation.SelectedItem.Value);
            UserSession.LocationIDCurrent = Convert.ToInt32(ddlLocation.SelectedItem.Value);
            Session["LocationID"] = Convert.ToInt32(ddlLocation.SelectedItem.Value);
            objEmployeePL.EmployeeID = 0;
            Session["Name"] = objEmployeePL.FirstName + " " + objEmployeePL.LastName;
            Session["Designation"] = objEmployeePL.Designation;
            Session["CompanyName"] = ddlcompany.SelectedItem.Text;
            Session["EmployeeCode"] = objEmployeePL.EmployeeCode;
            Session["Location"] = ddlLocation.SelectedItem.Text;

            BLTopic obj = new BLTopic();
            int count = 0;

            count = obj.GetUser(txtemailid.Text.Trim());
            if (count == 0)
            {
                if (IsPassed(txtemailid.Text.Trim()))
                {
                    string message = "You Already done ! your score : " + ViewState["percentage"].ToString() + " % Thanks for Retry";
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append("<script type = 'text/javascript'>");
                    sb.Append("window.onload=function(){");
                    sb.Append("alert('");
                    sb.Append(message);
                    sb.Append("')};");
                    sb.Append("</script>");
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                    return;
                }
                else
                {
                    if (obj.GettotalAttempt(txtemailid.Text.Trim()) >= 3)
                    {
                        string message = "You have failed to clear the test in three attempts, please report to your Line Manager.";
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        sb.Append("<script type = 'text/javascript'>");
                        sb.Append("window.onload=function(){");
                        sb.Append("alert('");
                        sb.Append(message);
                        sb.Append("')};");
                        sb.Append("</script>");
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                        return;
                    }
                }
            }
            else {

                if (IsPassed(txtemailid.Text.Trim()))
                {
                    string message = "You Already done ! your score : " + ViewState["percentage"].ToString() + " % Thanks for Retry";
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append("<script type = 'text/javascript'>");
                    sb.Append("window.onload=function(){");
                    sb.Append("alert('");
                    sb.Append(message);
                    sb.Append("')};");
                    sb.Append("</script>");
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                    //return;
                }            
            }
            DataSet DS = new DataSet();
            DS = objTestBL.GetMainLogin(objTestPL);
            int Value=0;
            if (DS.Tables[0].Rows.Count > 0)
            {
                Value =Convert.ToInt32(DS.Tables[0].Rows[0]["userid"].ToString());
            }
            else
            {
                Value = objTestBL.InsertMainLogin(objTestPL);
            }
            objEmployeePL.MainUserID = UserSession.UserIDs;
            objEmployeePL.CUID = Value;
            string[] Values = objEmployeeBL.InsertUpdateEmployee(objEmployeePL);
            if (Convert.ToInt32(Values[1]) > 0)
            {
                UserSession.UserID = Convert.ToInt32(Values[1]);
                Response.Redirect("Ethics.aspx");
            }
        }
    }
}