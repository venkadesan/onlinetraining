﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true"
    CodeBehind="Declration.aspx.cs" Inherits="OnlineTraining.Declration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style>
        pre p
        {
            float: left;
            text-align: justify;
            margin-left: 50px;
            margin-right: 50px;
            width: 90%;
        }
        pre span
        {
            text-align: center;
            font-weight: bold;
            margin-left: 300px;
            font-size: 14px;
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <pre style="background-color: White; border: 0px; font-family: Verdana; color: Black;">
    <span>Compliance with the Code of Business Ethics</span>

       <p>
           JLL is committed to a corporate culture that embraces and promotes strong principles
           of business and professional ethics at every level. Ethical practices are inherent
           in our values, mission and strategy, and guide all of our interactions with clients,
           customers, vendors and employees. Ethical behaviour is a core responsibility of
           each and every employee of JLL. Our Code of Business Ethics contains ethics policies
           and basic principles to guide conduct and is required to be adhered to in both its
           spirit and letter. “JLL” means any or more of (i) Jones Lang LaSalle Property Consultants
           (India) Private Limited, (ii) Jones Lang LaSalle Building Operations (India) Private
           Limited, (iii) Jones Lang LaSalle group of companies, and (iv) Homebay Residential
           Private Limited. We request you to certify compliance with Code of Business Ethics
           as per your terms of employment by signing the following undertaking/declaration.
       </p>

<span>Employee Declaration </span>

<p>
    To, Chief Ethics Officer Jones Lang LaSalle Property Consultants (India) Private
    Limited Level 16, Tower C, Epitome, Building No. 5, DLF Cyber City, DLF Phase III,
    Gurgaon – 122 002, Haryana</p>


<span>DECLARATION PART 1: UNDERSTANDING OF JLL’S CODE OF BUSINESS ETHICS</span>

<p>
    I,
    <label runat="server" id="lblNames" clientidmode="Static">
    </label>
    <label runat="server" id="lblDesignations" clientidmode="Static">
    </label>
    at
    <label runat="server" id="lblClinetNames" clientidmode="Static">
    </label>
    declare that I have read the JLL’s Code of Business Ethics and have undergone the
    Ethics assessment. I have understood what is laid down in the Code of Business Ethics
    and how it relates to various aspects of facility and property management services
    and my duties of employment.
</p>
<span>DECLARATION PART 2: ASSURANCE OF ETHICAL CONDUCT IN THE PRESENT AND PAST</span>
<p>
    Since the time I have been employed with JLL: • I have not at any time violated
    any of the terms of the Code of Business Ethics. • I have reported all possible
    violations of the Code of Business Ethics to the Ethics Officer or via Ethics Hotline
    or Ethics website or to the HR Business Partner or to my supervisor. • I have used
    all communication systems (telephones, mobile phones, computers internet access
    etc.) provided by JLL or its clients as per Company/Client’s policy(s) and have
    not viewed, received or sent inappropriate materials which I was not supposed to.
    • I have taken all necessary steps to protect confidential information and intellectual
    property of JLL and/or its clients. • I have not indulged in any abusive, harassing
    or offensive conduct (verbal, physical or visual) or any acts of violence or physical
    intimidation or retaliatory treatment in response to any complaint of harassment
    made against me.. • I have not requested or accepted any improper gifts (whether
    in cash, entertainment or other inducements) or any bribe from JLL’s clients, vendors
    or subcontractors or their employees, officers or directors, except as a token gift,
    business meal or entertainment of small or nominal in value i.e. (INR 2500 or less).
    • In case I have made any purchasing decisions, they have been based solely in the
    best interests of JLL or its clients, and consistent with JLL’s/client’s procurement
    policies including obtaining at least three quotations from unrelated vendors and
    fair value evaluation. I have made full disclosures and obtained appropriate approvals
    before making purchasing commitments. • I have dealt fairly with JLL’s customers
    and suppliers. I have not taken unfair advantage of anyone through manipulation,
    concealment, misrepresentation of material facts or any other unfair dealing practice
    and have not entered into any improper or illegitimate transactions. • I have not
    offered, made or promised to make any illegal, improper or questionable payment
    or commitment of personal or JLL’s funds or other valuable consideration to vendors,
    government officials, clients or anyone else for purpose of procuring goods or services,
    or obtaining or retaining business or securing any improper advantage, directly
    or indirectly.
</p>


<span>DECLARATION PART 3: ASSURANCE OF ETHICAL CONDUCT IN THE FUTURE</span>
<p>
    Going forward, I will not breach any of the terms of the Code of Business Ethics
    at long as I am in employment with JLL (as mentioned but not limited to the examples
    above). I also understand that any failure to report any violations of the Code
    of Business Ethics at any point of time will be held against me and may have consequences
    that will adversely affect my career, may invite penalties or even cost me my employment.
    I clearly also understand that any violations of of the Code of Business Ethics
    by me at any point of time will have severe repercussions on my terms of employment
    and may even result in cessation of my employment and dismissal with disgrace from
    JLL. I acknowledge by my signature below that I have read, understood and agree
    to all terms of this declaration/ letter.
</p>

<asp:Button runat="server" Text="Acknowledge" class="btn btn-primary" OnClick="Unnamed1_Click" />
    </pre>
</asp:Content>
