﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="Video.aspx.cs" Inherits="OnlineTraining.Video" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style>
        video::-webkit-media-controls-fullscreen-button
        {
            display: none;
        }
        video::-webkit-media-controls-play-button
        {
        }
        video::-webkit-media-controls-timeline
        {
        }
        video::-webkit-media-controls-current-time-display
        {
        }
        video::-webkit-media-controls-time-remaining-display
        {
        }
        video::-webkit-media-controls-time-remaining-display
        {
        }
        video::-webkit-media-controls-mute-button
        {
        }
        video::-webkit-media-controls-toggle-closed-captions-button
        {
        }
        video::-webkit-media-controls-volume-slider
        {
        }
        
        
        pre p
        {
            float: left;
            text-align: justify;
            margin-left: 50px;
            margin-right: 50px;
            width: 90%;
        }
        pre span
        {
            text-align: center;
            font-weight: bold;
            margin-left: 300px;
            font-size: 14px;
            text-decoration: underline;
        }
        .span1
        {
            text-align: center;
            font-weight: bold;
            margin-left: 200px;
            font-size: 14px;
            text-decoration: underline;
        }
        .test
        {
            margin: 0;
            padding: 10px 0;
            font-size: 18px;
            font-weight: 300;
            text-transform: uppercase;
            display: inline-block;
            vertical-align: middle;
            line-height: 28px;
            font-family: 'Roboto' , 'Helvetica' , Arial, sans-serif;
            color: #625748;
        }
    </style>
    <%--  <script type='text/javascript'>
        document.getElementById('myVideo').addEventListener('ended', myHandler, false);
        function myHandler(e) {
            if (!e) { e = window.event; }
            alert('Video Finished');
            timeoutfuntion();
        }
    </script>--%>
    <script type="text/javascript">

        function Confirm() {
            var x = confirm("You have not cleared the test successfully.Please try again");
            if (x)
                return true;
            else
                return false;
        }


        //var url = "http://www.codemiles.com";
        var seconds = 0;
        function redirect() {

            seconds = $('input[id$=totalmin]').val();
            redirect1();
        }

        function timefuntion() {
            setTimeout(timeoutfuntion, (seconds * 1000));
        }

        function timeoutfuntion() {
            //window.location = "onlinetraining.aspx";
            document.getElementById('<%= btnTestRequied.ClientID%>').click();
            //            var isTestrequired = document.getElementById('<%= hdntestrequired.ClientID%>').value;
            //            var afterlower = isTestrequired.toLowerCase();
            //            if (afterlower == 'true') {
            //                window.location = "Agree.aspx";
            //            }
            //            else {
            //                
            //            }
        }

        function redirect1() {
            if (seconds <= 0) {

                //window.location = "onlinetraining.aspx";

                //window.location = "default.aspx"                

            } else {
                seconds--;

                $('input[id$=totalmin]').val(seconds);
                // multiply by 1000 because Date() requires miliseconds
                var date = new Date(seconds * 1000);
                var hh = date.getUTCHours();
                var mm = date.getUTCMinutes();
                var ss = date.getSeconds();
                // If you were building a timestamp instead of a duration, you would uncomment the following line to get 12-hour (not 24) time
                // if (hh > 12) {hh = hh % 12;}
                // These lines ensure you have two-digits
                if (hh < 10) { hh = "0" + hh; }
                if (mm < 10) { mm = "0" + mm; }
                if (ss < 10) { ss = "0" + ss; }
                // This formats your string to HH:MM:SS
                var t = hh + ":" + mm + ":" + ss;

                document.getElementById("pageInfo").innerHTML = t;
                setTimeout("redirect1()", 1000);
            }
        }      

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="submenu">
        <span class="test">
            <asp:Label ID="lblTopicName" runat="server" Font-Bold="true" />
            <label id="pageInfo" style="font-size: 26px; padding-left: 520px;">
            </label>
            <asp:HiddenField runat="server" ID="totalmin" />
        </span>
    </div>
    <%-- <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label></div>--%>
    <div class="alert alert-danger" style="margin-top: 12px; height: 40px; padding-top: 10px;
        padding-left: 420px" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <!-- tile body -->
                    <div class="tile-body">
                        <div id="Div1" parsley-validate="" role="form" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="">
                                    <asp:Label Text="Select Topics" runat="server" ID="Label3" />:</label>
                                <div class="col-sm-6">
                                    <asp:DropDownList runat="server" class="chosen-select form-control" ID="ddltopics">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /tile body -->
                </div>
                <div class="col-md-4">
                    <!-- tile body -->
                    <div class="tile-body">
                        <div id="Div2" parsley-validate="" role="form" class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <asp:Button ID="btngo" class="btn btn-primary" runat="server" Text="Start Training"
                                        OnClick="btngo_Click" />
                                    <asp:Button ID="btntest" runat="server" Text="Start Test" CssClass="btn btn-info"
                                        OnClick="btntest_Click" />
                                    <asp:Button ID="btnTestRequied" runat="server" Style="display: none;" Text="Button"
                                        OnClick="btnTestRequied_Click" />
                                    <asp:HiddenField ID="hdntestrequired" Value="True" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /tile body -->
                </div>
            </div>
        </div>
        <br />
        <div class="table-responsive">
            <!--col-6-->
            <div class="col-md-12" style="border: 1px silver">
                <!-- tile body -->
                <div class="tile-body">
                    <div id="basicvalidations" runat="server" class="form-horizontal">
                        <div class="form-group" style="align-content: center">
                            <asp:Label ID="lblplayinstruction" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
                <!-- /tile body -->
            </div>
        </div>
        <br />
        <div class="table-responsive">
            <asp:Repeater ID="rpt" runat="server">
                <ItemTemplate>
                    <center>
                        <video id="myVideo" width="640px" height="360px" controls="controls">
  <%--<source src="file/1262eaaf-00eb-4ef1-9292-0aa92928c265_How does a Centrifugal pump work _.mp4" type="video/mp4">--%>
  <source src='<%# Eval("videoname") %>'  type="video/mp4">
  <source src="your_video_fil
  e.mp4" type="video/ogg">
  Your browser does not support HTML5 video.
</video>
                    </center>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                    <iframe id="myframe" height="500px" width="950px" src='<%# Eval("videoname") %>'
                        runat="server" style="border: none;"></iframe>
                </ItemTemplate>
            </asp:Repeater>
            <iframe id="myframeppt" height="500px" src="" runat="server" style="border: none;
                width: 100%;"></iframe>
        </div>
        <br />
        <br />
        <br />
        <div class="table-responsive" id="divsecondAttachments" runat="server">
            <asp:Repeater ID="rptrvideo2" runat="server">
                <ItemTemplate>
                    <center>
                        <video id="myVideo" width="640px" height="360px" controls="controls">
  <%--<source src="file/1262eaaf-00eb-4ef1-9292-0aa92928c265_How does a Centrifugal pump work _.mp4" type="video/mp4">--%>
  <source src='<%# Eval("videoname1") %>'  type="video/mp4">
  <source src="your_video_fil
  e.mp4" type="video/ogg">
  Your browser does not support HTML5 video.
</video>
                    </center>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Repeater ID="rptrpdf2" runat="server">
                <ItemTemplate>
                    <iframe id="myframe" height="500px" width="950px" src='<%# Eval("videoname1") %>'
                        runat="server" style="border: none;"></iframe>
                </ItemTemplate>
            </asp:Repeater>
            <iframe id="myframeppt2" height="500px" src="" runat="server" style="border: none;
                width: 100%;"></iframe>
        </div>
        <br />
    </div>
    <script type='text/javascript'>
        document.getElementById('myVideo').addEventListener('ended', myHandler, false);
        function myHandler(e) {
            if (!e) { e = window.event; }
            //alert("Video Finished");
            //window.location = "onlinetraining.aspx";
        }
    </script>
</asp:Content>
