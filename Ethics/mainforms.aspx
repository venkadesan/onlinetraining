﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="mainforms.aspx.cs" Inherits="OnlineTraining.mainforms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="Stylesheet">
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#limainforms').addClass("active");
            //$("#lidepartment").parent().parent().removeClass("dropdown a1");
            $("#limainforms").parent().parent().addClass("dropdown a1 open");
        });

        function ConfirmDelete() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <head>
        <title></title>
    </head>
    <div class="submenu">
        <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Main Menu"></asp:Literal>
        </h1>
        <span style="padding-top: 3px;">
            <asp:Button ID="bNew" class="btn btn-info margin-bottom-20" Text="New" runat="server"
                ToolTip="Click button to add record" OnClick="bNew_Click" /></span>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class="col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pAdd" runat="server">
                    <section class="tile cornered">
                            <div class="tile-body tile cornered">
                                <div class="row">
                                    <!--col-6-->
                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="basicvalidations" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for=""  style="text-align:left">
                                                        <asp:Label Text="Page Text" runat="server" ID="lblpagetext" />:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtpagetext" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                          
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>

                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div1" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for=""  style="text-align:left"> 
                                                        <asp:Label Text="Page Name (url)" runat="server" ID="lblpagename" />:</label>
                                                    <div class="col-sm-8">
                                                         <div class="col-sm-8">
                                                            <asp:TextBox ID="txtpagename"  class="form-control parsley-validated" AutoCompleteType="None" runat="server"  />
                                                     </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    <!-- end col 6 -->
                                </div>
                                 <!-- end col 6 -->
                                </div>
                                
                                 <div class="row">
                                    <!--col-6-->
                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div4" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for=""  style="text-align:left">
                                                        <asp:Label Text="Order" runat="server" ID="Label4" />:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtOrder"  class="form-control parsley-validated" AutoCompleteType="None" runat="server" onkeypress="return isNumber(event)"  />
                                                    </div>
                                                </div>
                                          
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    
                                      <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div2" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for=""  style="text-align:left">
                                                        <asp:Label Text="Css Class" runat="server" ID="Label3" />:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="txtCss"  class="form-control parsley-validated" AutoCompleteType="None" runat="server"  />
                                                    </div>
                                                </div>
                                          
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    


                             
                                 <!-- end col 6 -->
                                </div>
                                 <div class="row">
                                       <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div5" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for=""  style="text-align:left"> 
                                                        <asp:Label Text="Is Visible" runat="server" ID="Label5" />:</label>
                                                    <div class="col-sm-8">
                                                         <div class="col-sm-8">
                                                            <asp:CheckBox ID="chkisvisible" runat="server" ></asp:CheckBox>
                                                     </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    <!-- end col 6 -->
                                </div>
                                </div>

                               <div class="row">
                                    <div class="col-md-12">
                                        <div id="Form1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                                            <div class="form-group form-footer">
                                                <div class="col-sm-offset-5 col-sm-6">
                                                    <asp:Button ID="bSave" Text="Save" runat="server" class="btn btn-primary" 
                                                        onclick="bSave_Click" />
                                                    <asp:Button ID="Cancel" Text="Cancel" runat="server" class="btn btn-default" onclick="Cancel_Click"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                         </div>
           
            </section>
                </asp:Panel>
            </div>
            <asp:Panel runat="server" ID="pDet">
                <asp:Repeater ID="rptrmainforms" runat="server" DataSourceID="" OnItemCommand="rptrInspection_ItemCommand">
                    <HeaderTemplate>
                        <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                            <tr>
                                <th width="5%">
                                    <asp:Label Text="S.No" runat="server" ID="rlSNo" />
                                </th>
                                <th>
                                    <asp:Label Text="Mainforms Page Name" runat="server" ID="rlMainFormsPageName" />
                                </th>
                                <th>
                                    <asp:Label Text="Mainforms Page Text" runat="server" ID="rlMainFormsPageText" />
                                </th>
                                <th width="25%" style="text-align: center;">
                                    <asp:Label Text="Options" runat="server" ID="Label1" />
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("MainFormsPageName") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblSName" runat="server" Text='<%# Eval("MainFormsPageText") %>' />
                            </td>
                            <td class="actions text-center">
                                <asp:LinkButton class="btn btn-primary" CommandArgument='<%# Eval("MainFormsID") %>'
                                    ID="lnkEdit" runat="server" Text="Edit" CommandName="edit" />
                                &nbsp;
                                <asp:LinkButton class="btn btn-danger" ID="lnkDelete" runat="server" Text="Delete"
                                    OnClientClick="return ConfirmDelete();" CommandArgument='<%# Eval("MainFormsID") %>'
                                    CommandName="delete" />
                            </td>
                        </tr>
                        <%--<asp:HiddenField runat="server" ID="hDepartmentID" Value='<%# Eval("DepartmentID")%>' />--%>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                        <div class="">
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </asp:Panel>
            <div class="form-group form-footer">
                <div class="col-sm-offset-11 col-sm-6">
                    <%-- <asp:Button ID="bAdd" Text="Save" runat="server" ToolTip="Click button to save record"
                            TabIndex="40" class="btn btn-primary" OnClick="bAdd_Click" />--%>
                </div>
            </div>
        </div>
    </div>
    </div>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
