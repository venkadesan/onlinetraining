﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTrainingPL;
using OnlineTrainingBL;
using System.Web.UI.HtmlControls;
using OnlineTraining.Code;
using MB = Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Text;
using NPOI.HSSF.UserModel;

namespace OnlineTraining
{
    public partial class Question : PageBase
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion


        PLQuestion objQuestionPL = new PLQuestion();
        BLQuestion objQuestionBL = new BLQuestion();

        MB.BLCompany objCompany = new MB.BLCompany();
        MB.BLLocation objLocation = new MB.BLLocation();
        HtmlGenericControl divMessage = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                LoadDepartment();
                //LoadDDlTopic();
                LoadDetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        public void LoadDetails()
        {
            int TopicID = 0;
            Common.DDVal(this.dTopic, out TopicID);
            objQuestionPL.TopicID = TopicID;
            objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
            // objQuestionPL.LocationID = UserSession.LocationIDCurrent;
            objQuestionPL.QuestionID = 0;

            DataSet Ds = new DataSet();
            Ds = objQuestionBL.GetQuestionDetails(objQuestionPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrQuestion.DataSource = Ds;
                rptrQuestion.DataBind();
            }
            else
            {
                EmptyGrid();
            }
        }

        private void LoadDepartment()
        {
            BLDepartment objbldept = new BLDepartment();
            PLDepartment objpldept = new PLDepartment();

            objpldept.CompanyID = UserSession.CompanyIDCurrent;
            objpldept.GroupId = UserSession.GroupID;
            // objPLTopic.LocationID = UserSession.LocationIDCurrent;
            objpldept.DepartmentId = 0;
            DataSet Ds = new DataSet();
            Ds = objbldept.GetDepartmentDetails(objpldept);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.ddldepartment.DataSource = Ds;
                this.ddldepartment.DataTextField = "DepartmentName";
                this.ddldepartment.DataValueField = "DepartmentId";
                this.ddldepartment.DataBind();
            }
            else
            {
                this.ddldepartment.Items.Clear();
            }

            this.ddldepartment_SelectedIndexChanged(null, null);
        }

        private void LoadDDlTopic()
        {
            BLTopic objBLTopic = new BLTopic();
            PLTopic objPLTopic = new PLTopic();

            int departmentid = 0;
            if (!Common.DDVal(ddldepartment, out departmentid))
            {
                NotifyMessages("Please select department", Common.ErrorType.Error);
                return;
            }

            objPLTopic.CompanyID = UserSession.CompanyIDCurrent;
            objPLTopic.Groupid = UserSession.GroupID;
            objPLTopic.DepartmentId = departmentid;
            objPLTopic.TopicID = 0;
            DataSet Ds = new DataSet();
            Ds = objBLTopic.getTopicByDepartment(objPLTopic);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.dTopic.DataSource = Ds;
                this.dTopic.DataTextField = "TopicName";
                this.dTopic.DataValueField = "TopicID";
                this.dTopic.DataBind();
            }
            else
            {
                this.dTopic.Items.Clear();
            }

            this.dTopic_SelectedIndexChanged(null, null);
        }

        protected void dTopic_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDetails();
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        private void EmptyGrid()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("QuestionName");
            dt.Columns.Add("ShortName");
            dt.Columns.Add("QuestionID");
            DataRow dr;
            dr = dt.NewRow();
            dr["QuestionName"] = "";
            dr["ShortName"] = "";
            dr["QuestionID"] = 0;
            dt.Rows.Add(dr);
            rptrQuestion.DataSource = dt;
            rptrQuestion.DataBind();

            foreach (RepeaterItem rpt in rptrQuestion.Items)
            {
                Label lbl = (Label)rpt.FindControl("Label2");
                LinkButton lnkEdit = (LinkButton)rpt.FindControl("lnkEdit");
                LinkButton lnkDelete = (LinkButton)rpt.FindControl("lnkDelete");
                lbl.Visible = false;
                lnkEdit.Visible = false;
                lnkDelete.Visible = false;
            }
        }

        public void BindToCtrls()
        {
            DataSet Ds = new DataSet();
            DataSet Ds1 = new DataSet();
            int TopicID = 0;
            Common.DDVal(this.dTopic, out TopicID);
            objQuestionPL.TopicID = TopicID;
            objQuestionPL.QuestionID = this.ID.Value;
            objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
            // objQuestionPL.LocationID = UserSession.LocationIDCurrent; 
            Ds = objQuestionBL.GetQuestionDetails(objQuestionPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.tQuestionName.Text = Ds.Tables[0].Rows[0]["QuestionName"].ToString();
                //this.tShortName.Text = Ds.Tables[0].Rows[0]["ShortName"].ToString();            
            }
        }

        protected void rptrQuestion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindToCtrls();
            }
            else if (e.CommandName == "delete")
            {
                this.ID = Convert.ToInt32(e.CommandArgument);
                objQuestionPL.QuestionID = this.ID.Value;
                string message = objQuestionBL.DeleteQuestionDetails(objQuestionPL);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages(message, Common.ErrorType.Information);
                LoadDetails();
            }
        }

        protected void rptrQuestion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hQuestionID = (HiddenField)e.Item.FindControl("hQuestionID");
                LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lnkEdit");
                LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
                Repeater rpRepeater = (Repeater)e.Item.FindControl("RptProductDetails");

                lnkEdit.CommandArgument = hQuestionID.Value.ToString();
                lnkDelete.CommandArgument = hQuestionID.Value.ToString();
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string connString = "";
            string strFileType = Path.GetExtension(fileuploadExcel.PostedFile.FileName).ToLower();
            string fName = fileuploadExcel.PostedFile.FileName;
            string uploadFolderPath = "~/ExcelFile/";
            string filePath = HttpContext.Current.Server.MapPath(uploadFolderPath);
            fileuploadExcel.SaveAs(filePath + "\\" + fName);
            string path = filePath + fName;

            if (strFileType.Trim() == ".xls")
            {
                connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            }
            else if (strFileType.Trim() == ".xlsx")
            {
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }
            string query = "SELECT [Question No],[Question Name] FROM [Sheet1$]";
            OleDbConnection conn = new OleDbConnection(connString);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            OleDbCommand cmd = new OleDbCommand(query, conn);
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            ImportExcel(ds);
            da.Dispose();
            conn.Close();
            conn.Dispose();
            ButtonStatus(Common.ButtonStatus.Cancel);
            LoadDetails();
        }

        private void ImportExcel(DataSet ds)
        {
            StringBuilder sb1 = new StringBuilder();
            sb1.Append("<root>");

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                {
                    sb1.Append("<question ");
                    sb1.Append(" questionno =\"" + RemoveEscpeCharacter(row["Question No"].ToString()) + "\"");
                    sb1.Append(" questionname =\"" + RemoveEscpeCharacter(row["Question Name"].ToString()) + "\"");
                    sb1.Append(" />");
                }
            }
            sb1.Append("</root>");

            objQuestionPL.ExcelUploadXMLIs = sb1.ToString();

            objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;

            int TopicID = 0;
            Common.DDVal(this.dTopic, out TopicID);
            objQuestionPL.TopicID = TopicID;
            objQuestionPL.CUID = UserSession.UserID;
            try
            {
                NotifyMessages(objQuestionBL.ImportQuestion(objQuestionPL), Common.ErrorType.Information);
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        private string RemoveEscpeCharacter(string val)
        {
            return val.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&#39;");
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            if (tQuestionName.Text == string.Empty)
            {
                NotifyMessages("Please enter the Question Name", Common.ErrorType.Error);
                tQuestionName.Focus();
                return;
            }
            int TopicID = 0;
            Common.DDVal(this.dTopic, out TopicID);
            objQuestionPL.TopicID = TopicID;
            objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
            //objQuestionPL.LocationID = UserSession.LocationIDCurrent;
            objQuestionPL.QuestionName = this.tQuestionName.Text.Trim();
            //objQuestionPL.ShortName= this.tShortName.Text.Trim();
            objQuestionPL.CUID = Convert.ToInt32(UserSession.UserID);
            try
            {
                if (this.IsAdd)
                {
                    objQuestionPL.QuestionID = 0;
                    int message = objQuestionBL.InsertUpdateQuestion(objQuestionPL);
                    objQuestionPL.QuestionID = message;
                    string messages = string.Empty;

                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(objQuestionPL.ErrorMessage, Common.ErrorType.Information);
                    LoadDetails();
                }
                else if (this.ID != null)
                {
                    objQuestionPL.QuestionID = this.ID.Value;
                    int messagea = objQuestionBL.InsertUpdateQuestion(objQuestionPL);

                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(objQuestionPL.ErrorMessage, Common.ErrorType.Information);
                    LoadDetails();
                }
                else
                {
                    return;
                }
            }
            catch (ApplicationException ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void Cancel1_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            LoadDetails();
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.tQuestionName.Text = string.Empty;
            //this tShortName.Text = string.Empty;
            this.bNew.Visible = false;
            this.divmsg.Visible = false;

            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tQuestionName.Focus();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.tQuestionName.Focus();
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                this.ID = 0;
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            var workbook = new HSSFWorkbook();
            var sheet1 = workbook.CreateSheet("Sheet1");

            int TopicID = 0;

            Common.DDVal(this.dTopic, out TopicID);
            BLQuestion objblQuestion = new BLQuestion();
            PLQuestion objplQuestion = new PLQuestion();

            objplQuestion.CompanyID = UserSession.CompanyIDCurrent;
            objplQuestion.TopicID = Convert.ToInt32(TopicID);
            DataSet Ds = new DataSet();
            Ds = objQuestionBL.GetQuestionDetails(objQuestionPL);

            int Row = 1, col = 0;
            foreach (DataRow row in Ds.Tables[0].Rows)
            {
                sheet1.CreateRow(Row).CreateCell(col).SetCellValue(row["Question No"].ToString());
                Row = Row + 1;
            }

            int Row1 = 1, col1 = 1;
            foreach (DataRow row in Ds.Tables[0].Rows)
            {
                sheet1.CreateRow(Row1).CreateCell(col1).SetCellValue(row["Question Name"].ToString());
                Row1 = Row1 + 1;
            }

            sheet1.SetColumnWidth(0, 20 * 256);

            var headerRow = sheet1.CreateRow(0);
            var boldFont = workbook.CreateFont();
            boldFont.FontHeightInPoints = 11;
            boldFont.FontName = "Calibri";
            boldFont.Boldweight = HSSFFont.BOLDWEIGHT_BOLD;

            var cell = headerRow.CreateCell(0);
            cell.SetCellValue("Question No");
            cell.CellStyle = workbook.CreateCellStyle();
            cell.CellStyle.SetFont(boldFont);

            cell = headerRow.CreateCell(1);
            cell.SetCellValue("Question Name");
            cell.CellStyle = workbook.CreateCellStyle();
            cell.CellStyle.SetFont(boldFont);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            Response.Clear();
            string fileName = "Template-" + DateTime.Now.ToString() + DateTime.Now.Millisecond.ToString() + ".xls";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(output.ToArray());
            Response.End();
        }

        protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDDlTopic();
        }
    }
}