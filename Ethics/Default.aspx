﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="OnlineTraining.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">

        var id = "progressbar";
        var value = 20;
        function GetPassPercentage(id, value) {
            $("#" + id).progressbar({
                value: value
            });
        }

    </script>
    <script type="text/javascript">
        function Confirm() {
            var x = confirm("You have not score the required percentage. Kindly try again ??");
            if (x)
                return true;
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="submenu">
        <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Dashboard"></asp:Literal>
        </h1>
        <span style="padding-top: 3px;">
            <asp:Button ID="bNew" class="btn btn-info margin-bottom-20" Text="New" Visible="false"
                runat="server" OnClick="bNew_Click" /></span>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label></div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <br />
                 <div style="float: right;">
                                        <asp:Button ID="btnshowscore" CssClass="btn btn-success" runat="server" 
                                            Text="Show Answer" onclick="btnshowscore_Click" />
                                    </div>
                                    
                                    
                                      <div class="table-responsive">
                    <asp:Repeater ID="rptrAnswer" runat="server" DataSourceID="" >
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr  class="new-color">
                                    <th width="5%">
                                        <asp:Label Text="Q.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Question" runat="server" ID="Label6" />
                                    </th>
                                    
                                    <th>
                                        <asp:Label Text="Answers" runat="server" ID="Label9" />
                                    </th>
                                   
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="SLNo" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("QuestionName") %>' />
                                </td>
                              
                                 <td>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("Answers") %>' />
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hQuestionID" Value='<%# Eval("QuestionID")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>


                <div class="table-responsive">
                    <asp:Repeater ID="rptrTopic" runat="server" OnItemCommand="rptrTopic_ItemCommand"
                        DataSourceID="" OnItemDataBound="rptrTopic_ItemDataBound">
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr style="background-color: #bc1a1b; color: White;">
                                <td>
                                    <%--Topic Name:--%>
                                    <span style="font-weight: bold;">
                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("TopicName") %>' /></span>
                                    <span style="float: right; font-weight: bold;">Pass Percentage:
                                        <asp:Label ID="lblPassPercentage" runat="server" Text='<%# Eval("PassPercentage") %>' />%</span>
                                    <asp:HiddenField runat="server" ID="hResult" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="Div1" class="progress progress-striped" runat="server" visible="false">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuemin="0"
                                            aria-valuemax="100" style="background-color: white; color: Black;" runat="server"
                                            id="dPPer">
                                            <span>
                                                <%# Eval("PassPercentage") %>
                                                % Pass Percentage </span>
                                        </div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <span style="float: left; color: Black; width: 90px;">Your score is : </span><span
                                            style="float: left; color: Black; width: 200px;">
                                            <asp:Label runat="server" ID="lblTestPersentage" />
                                            <%--<asp:Label runat="server" ID="lblTrue" Text="True" style="color:Green;"></asp:Label>
                                            <asp:Label runat="server" ID="lblFalse" Text="False" style="color:Red;"></asp:Label>--%>
                                        </span>
                                    </div>
                                    <div>
                                        <asp:Image runat="server" ID="imgReYes" Height="20" Width="20" ImageUrl="App_Themes/images/yes.png" />
                                        <asp:Label runat="server" Text="Correct" ID="Label3" />
                                        <asp:Image runat="server" ID="imgReNo" Height="20" Width="20" ImageUrl="App_Themes/images/no.png" />
                                        <asp:Label runat="server" Text="Wrong" ID="Label4" />
                                        <asp:Image runat="server" ID="ImgReNA" Height="40" Width="40" ImageUrl="App_Themes/images/NA.png" />
                                        <asp:Label runat="server" Text="Not Attend" ID="Label5" /></div>
                                   
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Repeater ID="rptrAnswer" runat="server" OnItemCommand="rptrAnswer_ItemCommand"
                                        DataSourceID="" OnItemDataBound="rptrAnswer_ItemDataBound">
                                        <HeaderTemplate>
                                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                                <tr style="background-color: #808080; color: White;">
                                                    <th width="5%">
                                                        <asp:Label Text="Q.No" runat="server" ID="lSNo" />
                                                    </th>
                                                    <th>
                                                        <asp:Label Text="Question" runat="server" ID="Label6" />
                                                    </th>
                                                    <th>
                                                        <asp:Label Text="Result" runat="server" ID="Label1" />
                                                    </th>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("QuestionName") %>' />
                                                </td>
                                                <td>
                                                    <asp:ImageButton runat="server" ID="imgResultYes" Height="20" Width="20" ImageUrl="App_Themes/images/yes.png" />
                                                    <asp:ImageButton runat="server" ID="imgResultNo" Height="20" Width="20" ImageUrl="App_Themes/images/no.png" />
                                                    <asp:ImageButton runat="server" ID="ImgResultNA" Height="40" Width="40" ImageUrl="App_Themes/images/NA.png" />
                                                </td>
                                            </tr>
                                            <asp:HiddenField runat="server" ID="hQuestionID" Value='<%# Eval("QuestionID")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hTopicID" Value='<%# Eval("TopicID")%>' />
                            <asp:HiddenField runat="server" ID="hPassPercentage" Value='<%# Eval("PassPercentage")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                
              

            </div>
        </div>
    </div>
</asp:Content>
