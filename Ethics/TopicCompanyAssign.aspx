﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="TopicCompanyAssign.aspx.cs" Inherits="OnlineTraining.TopicCompanyAssign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="App_Themes/jquery/jquery-1.6.2.min.js"></script>
    <script type="text/javascript">
        function unCheckAll() {
            $(".chkVisible input").each(function () {
                $(this).attr('checked', false);
            });
        }

        $(document).ready(function () {
            $('.Visible input').click(function () {
                $(".chkVisible input").each(function () {
                    $(this).attr('checked', $('.Visible input').attr('checked'));
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="submenu">
        <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Topic-Company Assign"></asp:Literal>
        </h1>
    </div>
<%--    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label></div>--%>
        
         <div class="alert alert-danger" style="margin-top: 12px;height:40px;padding-top:10px;padding-left:420px" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pAdd" runat="server" CssClass="tile-body">
                    <section class="tile cornered">
                             
         <div class="tile-body">
                    <div class="row">
                        <!--col-6-->
                        <div class="col-md-4">
                            <!-- tile body -->
                            <div class="tile-body">
                                <div id="basicvalidations" runat="server" class="form-horizontal">
                                    <div class="form-group">
                                       <label class="col-sm-4 control-label" for="">
                                          <asp:Label Text="Company" runat="server" ID="lcompany" />:</label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddlcompany" runat="server" CssClass="form-control"
                                            onselectedindexchanged="ddlcompany_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /tile body -->
                        </div>
                         <div class="col-md-4">
                            <!-- tile body -->
                            <div class="tile-body">
                                <div id="Div2" runat="server" class="form-horizontal">
                                    <div class="form-group">
                                       <label class="col-sm-4 control-label" for="" >
                                          <asp:Label Text="Department" runat="server" ID="Label1" />:</label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddldepartment" runat="server" CssClass="form-control" AutoPostBack="True" onselectedindexchanged="ddldepartment_SelectedIndexChanged" 
                                            ></asp:DropDownList>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /tile body -->
                        </div>
                         <div class="col-md-4">
                            <!-- tile body -->
                            <div class="tile-body">
                                <div id="Div1" runat="server" class="form-horizontal">
                                    <div class="form-group">
                                            <div class="col-sm-8">
                                                <asp:Button ID="btnAssign"  runat="server" Text="Assign" class="btn btn-primary"
                                                    onclick="btnAssign_Click"></asp:Button>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /tile body -->
                        </div>
            </div>
            </div>
            <div class="cboth">
            </div>
            </section>
                </asp:Panel>
                <br />
                <div class="table-responsive">
                    <asp:Repeater ID="rptrTopic" runat="server">
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <th width="5%">
                                        <asp:Label Text="S.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Topic Name" runat="server" ID="lDepartmentName" />
                                    </th>
                                    <th width="25%">
                                        <asp:CheckBox ID="chkAllVisible" CssClass="Visible" runat="server" ClientIDMode="Static" /><asp:Label
                                            Text="Select All" runat="server" ID="lvisible" />
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("TopicName") %>' />
                                </td>
                                <td class="actions text-center">
                                    <asp:CheckBox CssClass="chkVisible" ID="chkMSFVisible" Checked='<%# Eval("type") %>'
                                        runat="server" />
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hTopicID" Value='<%# Eval("TopicID")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <%-- </ContentTemplate> </asp:UpdatePanel>--%>
    </div>
</asp:Content>
