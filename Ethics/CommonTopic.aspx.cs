﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using OnlineTraining.Code;
using OnlineTrainingBL;
using OnlineTrainingPL;

namespace OnlineTraining
{
    public partial class CommonTopic : PageBase
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        PLTopic objTopicPL = new PLTopic();
        BLTopic objTopicBL = new BLTopic();

        PLDepartment objDepartmentPL = new PLDepartment();
        BLDepartment objDepartmentBL = new BLDepartment();

        Kowni.BusinessLogic.BLCompany objCompany = new Kowni.BusinessLogic.BLCompany();
        Kowni.BusinessLogic.BLLocation objLocation = new Kowni.BusinessLogic.BLLocation();
        HtmlGenericControl divMessage = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                LoadDetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }



        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        public void LoadDetails()
        {
            objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
            //objTopicPL.LocationID = UserSession.LocationIDCurrent;
            objTopicPL.TopicID = 0;
            DataSet Ds = new DataSet();
            Ds = objTopicBL.GetCommonTopicDetails(objTopicPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrTopic.DataSource = Ds;
                rptrTopic.DataBind();
            }
            else
            {
                rptrTopic.DataSource = null;
                rptrTopic.DataBind();
            }
        }


        public void BindToCtrls()
        {
            DataSet Ds = new DataSet();
            DataSet Ds1 = new DataSet();
            objTopicPL.TopicID = this.ID.Value;
            objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
            //objTopicPL.LocationID = UserSession.LocationIDCurrent;
            Ds = objTopicBL.GetCommonTopicDetails(objTopicPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.tTopicName.Text = Ds.Tables[0].Rows[0]["TopicName"].ToString();
                this.tDescription.Text = Ds.Tables[0].Rows[0]["Description"].ToString();
                this.tPassPercentage.Text = Ds.Tables[0].Rows[0]["PassPercentage"].ToString();
                this.tResolutionTime.Text = Ds.Tables[0].Rows[0]["ResolutionTime"].ToString();

                this.txttrainingdurationtime.Text = Ds.Tables[0].Rows[0]["TrainingDuration"].ToString();
                this.ttestinstruction.Text = Ds.Tables[0].Rows[0]["testInstruction"].ToString();

                this.hdnguid.Value = Ds.Tables[0].Rows[0]["FileName"].ToString();
                this.hdnactual.Value = Ds.Tables[0].Rows[0]["displayname"].ToString();

                //this.ChkActive.Checked=
            }
        }

        protected void rptrTopic_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindToCtrls();
            }
            else if (e.CommandName == "delete")
            {
                this.ID = Convert.ToInt32(e.CommandArgument);
                objTopicPL.TopicID = this.ID.Value;
                string message = objTopicBL.DeleteCommonTopicDetails(objTopicPL);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages(message, Common.ErrorType.Information);
                LoadDetails();
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void ChkRdActive_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chk = (CheckBox)sender;
                RepeaterItem item = (RepeaterItem)chk.NamingContainer;
                HiddenField hTopicID = (HiddenField)item.FindControl("hTopicID");
                if (hTopicID != null)
                {
                    if (string.IsNullOrEmpty(hTopicID.Value) == false)
                    {
                        objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
                        objTopicPL.TopicID = Convert.ToInt32(hTopicID.Value);
                        objTopicPL.IsActive = chk.Checked;
                        int message = objTopicBL.UpdateTopicStatus(objTopicPL);
                        if (objTopicPL.ErrorMessage != string.Empty)
                        {
                            if (objTopicPL.ErrorMessage.ToString() == "Updated Successfully")
                            {
                                ButtonStatus(Common.ButtonStatus.Cancel);
                                NotifyMessages(objTopicPL.ErrorMessage, Common.ErrorType.Information);
                                LoadDetails();
                            }
                            else if (objTopicPL.ErrorMessage.ToString() == "Set atleast one Topic should be active.")
                            {
                                ButtonStatus(Common.ButtonStatus.Cancel);
                                NotifyMessages(objTopicPL.ErrorMessage, Common.ErrorType.Error);
                                LoadDetails();
                            }
                        }
                    }
                }
            }
            catch (ApplicationException ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        //protected void btnSubmit_Click(object sender, EventArgs e)
        //{           
        //    string strFileType = Path.GetExtension(FileUpload1.PostedFile.FileName).ToLower();
        //    string fName = FileUpload1.PostedFile.FileName;
        //    string uploadFolderPath = "~/File/";
        //    string filePath = HttpContext.Current.Server.MapPath(uploadFolderPath);
        //    FileUpload1.SaveAs(filePath + "\\" + fName);
        //    string path = filePath + fName;

        //    if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
        //    {
        //        string fnv = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
        //        string[] fileVExtArr = fnv.Split('.');
        //        string fileVExt = "";
        //        if (fileVExtArr.Length > 1)
        //        {
        //            fileVExt = fileVExtArr[1];
        //            fileVExt = fileVExt.ToLower();
        //        }
        //        fnv = System.IO.Path.GetFileNameWithoutExtension(FileUpload1.PostedFile.FileName);
        //        if (!fileVExt.Equals("mp4") && !fileVExt.Equals("flv") && !fileVExt.Equals("3gp") && !fileVExt.Equals("avi")) 
        //        {
        //            Page.ClientScript.RegisterStartupScript(this.GetType(), "22222", "<script> alert('File type not matched. Please upload only mp4,3gp,avi and flv file') </script>", false);                   
        //        }

        //       string SaveVideoLocation = Server.MapPath("File") + "\\" + fnv;
        //       FileUpload1.PostedFile.SaveAs(SaveVideoLocation);

        //       try
        //       {
        //           NotifyMessages("Uploaded Successfully", Common.ErrorType.Error);
        //       }
        //       catch (Exception ex)
        //       {
        //           NotifyMessages(ex.Message, Common.ErrorType.Error);
        //       }
        //    }
        //}

        //protected void btnSubmit_Click(object sender, EventArgs e)
        //{            

        //}        

        protected void bSave_Click(object sender, EventArgs e)
        {
            if (tTopicName.Text == string.Empty)
            {
                NotifyMessages("Please enter the Topic Name", Common.ErrorType.Error);
                return;
            }
            if (tPassPercentage.Text == string.Empty)
            {
                NotifyMessages("Please enter the Pass Percentage", Common.ErrorType.Error);
                return;
            }
            int PValue = 0;
            if (!Int32.TryParse(this.tPassPercentage.Text, out PValue))
            {
                NotifyMessages("Please enter the Pass Percentage", Common.ErrorType.Error);
                return;
            }

            if (tResolutionTime.Text == string.Empty)
            {
                NotifyMessages("Please enter the Resolution Time", Common.ErrorType.Error);
                return;
            }

            if (txttrainingdurationtime.Text == string.Empty)
            {
                NotifyMessages("Please enter the Training Duration Time", Common.ErrorType.Error);
                return;
            }

           

            if (ChkActive.Checked == false)
            {
                if (this.IsAdd)
                {
                    objTopicPL.TopicID = 0;
                   
                    //objTopicPL.FileName = fName;
                    objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
                    string message = objTopicBL.CheckActiveTopic(1, objTopicPL);
                    if (message != string.Empty)
                    {
                        if (message == "No record found")
                        {
                            NotifyMessages("Set topic as active status", Common.ErrorType.Error);
                            return;
                        }
                    }
                }
                else if (this.ID != null)
                {
                    objTopicPL.TopicID = this.ID.Value;
                    objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
                    string message = objTopicBL.CheckActiveTopic(2, objTopicPL);
                    if (message != string.Empty)
                    {
                        if (message == "No record found")
                        {
                            NotifyMessages("Set topic as active status", Common.ErrorType.Error);
                            return;
                        }
                    }
                }
            }

            objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
            // objTopicPL.LocationID = UserSession.LocationIDCurrent;
            objTopicPL.TopicName = this.tTopicName.Text.Trim();

            if (!FileUpload1.HasFile)
            {
                objTopicPL.FileName = hdnguid.Value;
                objTopicPL.DisplayName = hdnactual.Value;
            }
            else
            {
                Guid g;
                g = Guid.NewGuid();

                string strFileType = Path.GetExtension(FileUpload1.PostedFile.FileName).ToLower();
                string fName = FileUpload1.PostedFile.FileName;
                string fileextension = System.IO.Path.GetExtension(FileUpload1.FileName);
                string uploadFolderPath = "~/File/";
                string filePath = HttpContext.Current.Server.MapPath(uploadFolderPath);
                FileUpload1.SaveAs(filePath + "\\" + g + fileextension);
                string path = filePath + g + fileextension;

                objTopicPL.FileName = g + fileextension;
                objTopicPL.DisplayName = fName;
            }
            //objTopicPL.FileName = g + fileextension;
            //objTopicPL.DisplayName = fName;
            objTopicPL.Percentage = PValue;
            objTopicPL.Description = this.tDescription.Text.Trim();
            objTopicPL.ResolutionTime = this.tResolutionTime.Text.Trim();
            objTopicPL.CUID = Convert.ToInt32(UserSession.UserID);
            objTopicPL.IsActive = ChkActive.Checked;
            objTopicPL.Groupid = UserSession.GroupID;
            objTopicPL.TestDuration = txttrainingdurationtime.Text;
            objTopicPL.TestInstruction = ttestinstruction.Text;
            try
            {
                if (this.IsAdd)
                {
                    objTopicPL.TopicID = 0;
                    int message = objTopicBL.InsertUpdateCommonTopic(objTopicPL);
                    objTopicPL.TopicID = message;
                    string messages = string.Empty;

                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(objTopicPL.ErrorMessage, Common.ErrorType.Information);
                    LoadDetails();
                }
                else if (this.ID != null)
                {
                    objTopicPL.TopicID = this.ID.Value;
                    int messagea = objTopicBL.InsertUpdateCommonTopic(objTopicPL);

                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(objTopicPL.ErrorMessage, Common.ErrorType.Information);
                    LoadDetails();
                }
                else
                {
                    return;
                }
            }
            catch (ApplicationException ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void dNoofAttempts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Cancel1_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            LoadDetails();
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.tTopicName.Text = tDescription.Text = tPassPercentage.Text = tResolutionTime.Text = string.Empty;
            this.ChkActive.Checked = false;
            txttrainingdurationtime.Text = ttestinstruction.Text = string.Empty;
            hdnguid.Value = hdnactual.Value = string.Empty;
            this.bNew.Visible = false;
            this.divmsg.Visible = false;

            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tTopicName.Focus();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.tTopicName.Focus();
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                this.ID = 0;
            }
        }
    }
}