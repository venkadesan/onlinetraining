﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="OnlineTraining.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%--<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Sign in to eFit Facility</title>
    <link rel="icon" type="image/ico" href="App_Themes/images/favicon.ico" />
    <link href="App_Themes/essent/log-pass-act.css" rel="stylesheet" type="text/css" />
</head>--%>
<head>
    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>Sign in to Online Training</title>
    <meta content="" name="description">
    <meta content="" name="author">
    <!-- Mobile Specific Metas
  ================================================== -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <!-- CSS
  ================================================== -->
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <link href="App_Themes/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="App_Themes/css/animate.min.css" />
    <style class="cssdeck">
@import url(http://fonts.googleapis.com/css?family=Roboto:100);
@import url(http://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css);
@import url(http://fonts.googleapis.com/css?family=Roboto:400,100,900italic,700italic,900,700,500italic,500,400italic,300italic,300,100italic&subset=latin,latin-ext);
@import url(http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700&subset=latin,latin-ext);

body {
  background:url(App_Themes/images/turknet_musteri_hizmetleri_02.JPG) no-repeat;
    font-family: 'Roboto';
}
.flat-form {
  background: #006599;
  margin: 150px 180px 0px auto;
  width: 390px;
  height: 340px;
  position: relative;
  font-family: 'Roboto';
  
}
.tabs {
  background: #003b6c;
  height: 40px;
  margin: 0;
  padding: 0;
  list-style-type: none;
  width: 100%;
  position: relative;
  display: block;
  margin-bottom: 20px;
  
}
.tabs li {
  display: block;
  float: left;
  margin: 0;
  padding: 0;
}
.tabs a {
  background: #003b6c;
  display: block;
  float: left;
  text-decoration: none;
  color: white;
  font-size: 16px;
  padding: 12px 22px 12px 22px;
  
  /*border-right: 1px solid @tab-border;*/

}
.tabs li:last-child a {
  border-right: none;
  width: 174px;
  padding-left: 0;
  padding-right: 0;
  text-align: center;
}
.tabs a.active {
  background: #006599;
  border-right: none;
  -webkit-transition: all 0.5s linear;
	-moz-transition: all 0.5s linear;
	transition: all 0.5s linear;
}
.form-action {
  padding: 0 20px;
  position: relative;
}

.form-action h1 {
  font-size: 42px;
  padding-bottom: 10px;
  color: white;
}
.form-action p {
  font-size: 12px;
  padding-bottom: 10px;
  line-height: 25px;
  color: white;
}
form {
  padding-right: 20px !important;
}
form input[type=text],
form input[type=password],
form input[type=submit] {
  font-family: 'Roboto';
}

form input[type=text],
form input[type=password] {
  width: 100%;
  height: 40px;
  margin-bottom: 10px;
  padding-left: 15px;
  none repeat scroll 0 0 rgba(255, 255, 255, 0.6)
  border: none;
  color: #006599;
  outline: none;
}

.dark-box {
  background: #5e0400;
  box-shadow: 1px 3px 3px #3d0100 inset;
  height: 40px;
  width: 50px;
}
.form-action .dark-box.bottom {
  position: absolute;
  right: 0;
  bottom: -24px;
}
.tabs + .dark-box.top {
  position: absolute;
  right: 0;
  top: 0px;
}
.show {
  display: block;
}
.hide {
  display: none;
}

.button {
    border: none;
    display: block;
    background: #e22121;
    height: 40px;
    width: 80px;
    color: #ffffff;
    text-align: center;
    border-radius: 5px;
    /*box-shadow: 0px 3px 1px #2075aa;*/
  	-webkit-transition: all 0.15s linear;
	  -moz-transition: all 0.15s linear;
	  transition: all 0.15s linear;
}

.button:hover {
  background: #ce0000;
  /*box-shadow: 0 3px 1px #237bb2;*/
}

.button:active {
  background: #ce0000;
  /*box-shadow: 0 3px 1px #0f608c;*/
}

::-webkit-input-placeholder {
  color: #e74c3c;
}
:-moz-placeholder {
  /* Firefox 18- */
  color: #e74c3c;
}
::-moz-placeholder {
  /* Firefox 19+ */
  color: #006599;
}
:-ms-input-placeholder {
  color: #e74c3c;
}</style>
    <script type="text/jscript">
        function validatePassword() {

            var p = document.getElementById("<%=txtPassword.ClientID%>").value.trim();
            if (p.length < 8) {
                alert("Your password must be at least 8 characters");
                return;
            }
            if (p.search(/[a-z]/i) < 0) {
                alert("Your password must contain at least one letter.");
                return;
            }

            if (p.search(/[0-9]/) < 0) {
                alert("Your password must contain at least one digit.");
                return;
            }
            var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
            for (var i = 0; i < p.value.length; i++) {
                if (iChars.indexOf(p.value.charAt(i)) != -1) {
                    alert("The Field No 2 has special characters. \nSpecial Characters are not allowed");
                    return false;
                }
            }
        }
        function Validation1() {
            var EmailID = document.getElementById("<%=txtUserName.ClientID%>").value.trim();
            var tPassword = document.getElementById("<%=txtPassword.ClientID%>").value.trim();

            if (EmailID == "") {
                alert('Please enter the EmailID');
                return false;
            }
            if (tPassword == "") {
                alert("Error: Password cannot be blank!");
                tPassword.focus();
                return false;
            }
            var re = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}";
            if (!re.test(tPassword.value)) {
                alert("Error: Password must contain: Minimum 8 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character");
                tPassword.focus();
                return false;
            }
        }    

//       
        }
    </script>
</head>
<%--<body>
    <div id="plLogin">
        <div class="topbar">
            <div class="logo_box">
                <img src="App_Themes/essent/images/logo_medium.png" />
                <img src="App_Themes/essent/images/logo_medium1.png" class="right" /></div>
        </div>
        <div class="redbar">
            <div class="redbox_cont">
                <h2 class="left">
                    Login</h2>
            </div>
        </div>
        <div class="container">
            <div class="s60">
            </div>
            <div class="login">
                <form runat="server" id="Form1">
                <asp:ScriptManager ID="ScriptManager1" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div id="divmsg" runat="server">
                            &nbsp;</div>
                        <div class="form_hd">
                            <span id="Label1">User ID</span>
                        </div>
                        <div class="form">
                            <asp:TextBox runat="server" ID="txtUserName" class="tbox1"></asp:TextBox>
                        </div>
                        <div class="form_hd">
                            <span id="Label2">Password</span>
                        </div>
                        <div class="form">
                            <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" class="tbox1"></asp:TextBox>
                        </div>
                        <div class="form_logbtn">
                            <asp:Button Text="Login" ID="imgLogin" runat="server" class="btn_login" OnClick="Submit1_Click" /></div>
                        <br />
                        <div class="rememberme">
                            <input id="chkRememberMe" type="checkbox" name="chkRememberMe" /><label for="chkRememberMe">Remember
                                me</label></div>
                         <div class="forgotpassword">
                            <a href="forgotpassword.aspx">Forgot password</a></div>
                        <div class="cboth">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
</body>--%>
<body>
    <form id="Form1" method="post" runat="server">
    <div class="container">
        <div class="flat-form" style="height: 280px;">
            <ul class="tabs">
                <li><a class="active" href="#login">Login</a></li>
                <li><a href="#reset">Reset Password</a> </li>
            </ul>
            <div class="form-action show" id="login">
                <p runat="server" id="divmsg" visible="false">
                    <asp:Label runat="server" Style="color: #FFFFFF !important; margin-left: 5px;" ID="lblError"></asp:Label>
                </p>
                <div>
                    <ul>
                        <li>
                            <asp:TextBox runat="server" ID="txtUserName" placeholder="UserID"></asp:TextBox>
                        </li>
                        <li style="margin-top: 20px;">
                            <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" placeholder="Password"></asp:TextBox>
                        </li>
                        <li style="margin-top: 20px; margin-bottom: 20px;"><span style="float: left; width: 30%;">
                            <asp:Button runat="server" ID="imgLogin" Text="Login" class="button" OnClick="Submit1_Click" /></span>
                            <asp:HiddenField ID="hdnurl" runat="server" />
                            <%--<span>
                                <input type="submit" class="button" value="Sign up" data-toggle="modal" data-target="#myModal"></span>--%>
                        </li>
                    </ul>
                </div>
            </div>
            <!--/#login.form-action-->
            <div class="form-action hide" id="register">
                <h1>
                    Register</h1>
                <p>
                    <br />
                </p>
                <div id="Div1" runat="server">
                    <ul>
                        <li>
                            <asp:TextBox runat="server" placeholder="EmailID or MobileNumber" ID="TextBox1"></asp:TextBox>
                        </li>
                        <li>
                            <asp:TextBox runat="server" ID="TextBox2" placeholder="Password"></asp:TextBox>
                        </li>
                        <li>
                            <asp:Button runat="server" class="button" ID="Button2" Text="Sign Up" />
                        </li>
                    </ul>
                </div>
            </div>
            <!--/#register.form-action-->
            <div class="form-action hide" id="reset">
                <h1 style="visibility: hidden;">
                    Reset Password</h1>
                <div>
                    <ul>
                        <li>
                            <asp:TextBox runat="server" placeholder="EmailID or MobileNumber" ID="txtREmail"></asp:TextBox>
                        </li>
                        <li>
                            <asp:TextBox runat="server" placeholder="Password" ID="txtRPassword" TextMode="Password"
                                Visible="false"></asp:TextBox>
                        </li>
                        <li>
                            <asp:TextBox runat="server" placeholder="ConfirmPassword" ID="txtRConfirmPassword"
                                TextMode="Password" Visible="false"></asp:TextBox>
                        </li>
                        <li style="margin-top: 20px;"><span style="float: left; width: 30%;">
                            <asp:Button runat="server" ID="Button5" Text="Reset" class="button" OnClientClick="return Validation();"
                                OnClick="ResetPassword_Click" />
                        </span><span>
                            <asp:Button runat="server" ID="Button3" Text="Cancel" class="button" OnClick="ResetCancel_Click" /></span>
                        </li>
                    </ul>
                </div>
            </div>
            <!--/#register.form-action-->
        </div>
    </div>
    </form>
</body>
</html>
