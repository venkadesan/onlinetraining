﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="OnlineTraining.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<style>
    .default
    {
        background: #292929;
        border: 1px solid #111;
        border-radius: 5px;
        overflow: hidden;
        box-shadow: 0 0 5px #333;
    }
    .default div
    {
        background-color: #1a82f7;
        background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#0099FF), to(#1a82f7));
        background: -webkit-linear-gradient(top, #0099FF, #1a82f7);
        background: -moz-linear-gradient(top, #0099FF, #1a82f7);
        background: -ms-linear-gradient(top, #0099FF, #1a82f7);
        background: -o-linear-gradient(top, #0099FF, #1a82f7);
    }
</style>
<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
    <script type="text/javascript" src="progressbar.js"></script>
    <link rel="stylesheet" type="text/css" href="progressbar.css">
    <link rel="stylesheet" type="text/css" href="skins/tiny-green/progressbar.css">
</head>
<body>
    <div id="progressBar" class="tiny-green">
        <div>
        </div>
    </div>
    <script>
        progressBar(75, $('#progressBar'));
    </script>
</body>
</html>
