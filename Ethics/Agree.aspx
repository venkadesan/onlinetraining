﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="Agree.aspx.cs" Inherits="OnlineTraining.Agree" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function ConfirmDecline() {
            var x = confirm("Are you sure you want to decline?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="submenu">
        <asp:Label ID="lmainheader" style="font-size: 28px;" runat="server" Text="Instruction"></asp:Label>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class="col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <section class="tile cornered">
                            <div class="tile-body tile cornered">
                                
                                <div class="row">
                                    <!--col-6-->
                                    <div class="col-md-12">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="basicvalidations" runat="server" class="form-horizontal">
                                              <div class="form-group">
                                                   <asp:Label ID="lblinstruction" runat="server" Text="Label"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-12">
                                        <div id="Form1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                                            <div class="form-group form-footer">
                                                <div class="col-sm-offset-5 col-sm-6">
                                                    <asp:Button ID="bagree" Text="Accept" runat="server" 
                                                        ToolTip="Click button to Accept Instruction" TabIndex="3" 
                                                        class="btn btn-primary" onclick="bagree_Click"   />
                                                    <asp:Button ID="bdecline" Text="Decline" runat="server" 
                                                        ToolTip="Click button to Decline Instruction" TabIndex="4" 
                                                        class="btn btn-default" onclick="bdecline_Click" OnClientClick="return ConfirmDecline();" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                    </section>
            </div>
        </div>
    </div>
</asp:Content>
