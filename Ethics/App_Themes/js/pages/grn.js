function pageLoad(sender, args) {
    if (args.get_isPartialLoad()) {
        $("input[type=text][id*=tGRNQuantity]").tooltip({ position: "right center" });
        $("input[type=text][id*=tGRNUnitPrice]").tooltip({ position: "right center" });
        $("input[type=text][id*=tGRNMonths]").tooltip({ position: "right center" });

        $(".positive").numeric({ negative: false });
        $(".numeric").numeric();
    }
}

$(document).ready(
	function () {

	    $(".positive").numeric({ negative: false });
	    $(".numeric").numeric();

	    $("input[type=text][id*=tGRNQuantity]").tooltip({ position: "right center" });
	    $("input[type=text][id*=tGRNUnitPrice]").tooltip({ position: "right center" });
	    $("input[type=text][id*=tGRNMonths]").tooltip({ position: "right center" });
	}
);