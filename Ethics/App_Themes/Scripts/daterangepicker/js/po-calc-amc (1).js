function pageLoad(sender, args) {
    if (args.get_isPartialLoad()) {

        $('.dt').daterangepicker({
            dateFormat: 'mm/dd/yy',
            datepickerOptions: {
                changeMonth: true,
                changeYear: true
            }
        });
        
        $(".positive").numeric({ negative: false });
        $(".numeric").numeric();

        $("input[type=text][id*=tPOItemCapacity]").bind("keyup", grandTotalCalc);
        $("input[type=text][id*=tPOItemQuantity]").bind("keyup", grandTotalCalc);
        $("input[type=text][id*=tPOItemPrice]").bind("keyup", grandTotalCalc);
        $("input[type=text][id*=tPOMonths]").bind("keyup", grandTotalCalc);




        $("input[type=text][id*=lSum]").bind("keyup", grandTotalCalc);
        $("input[type=text][id*=tSum]").bind("keyup", grandTotalCalc);


        /*
        $("[id*=txtVendor]").click(showHide);
        $("[id*=txtShipAddr]").click(showHide);
        $("[id*=txtBillAddr]").click(showHide);
        $("[id*=txtImpNotesTitle]").click(showHide);
        */

        $(".positive").numeric({ negative: false, opacity: 0.7 });
        $(".numeric").numeric();

        $("input[type=text][id*=tPOItemCapacity]").tooltip({ position: "right center" });
        $("input[type=text][id*=tPOItemQuantity]").tooltip({ position: "right center" });
        $("input[type=text][id*=tPOItemPrice]").tooltip({ position: "right center" });
        $("input[type=text][id*=tPOMonths]").tooltip({ position: "right center" });
    }
}

$(document).ready(
	        function () {
                
	            $('.dt').daterangepicker({
	                dateFormat: 'mm/dd/yy',
	                datepickerOptions: {
	                    changeMonth: true,
	                    changeYear: true
	                }
	            });
                
	            $("input[type=text][id*=tPOItemCapacity]").bind("keyup", grandTotalCalc);
	            $("input[type=text][id*=tPOItemQuantity]").bind("keyup", grandTotalCalc);
	            $("input[type=text][id*=tPOItemPrice]").bind("keyup", grandTotalCalc);
	            $("input[type=text][id*=tPOMonths]").bind("keyup", grandTotalCalc);

	            $("input[type=text][id*=lSum]").bind("keyup", grandTotalCalc);
	            $("input[type=text][id*=tSum]").bind("keyup", grandTotalCalc);

                /*
	            $("[id*=txtVendor]").click(showHide);
	            $("[id*=txtShipAddr]").click(showHide);
	            $("[id*=txtBillAddr]").click(showHide);
	            $("[id*=txtImpNotesTitle]").click(showHide);
                */




	            $(".positive").numeric({ negative: false });
	            $(".numeric").numeric();


	            $("input[type=text][id*=tPOItemCapacity]").tooltip({ position: "right center" });
	            $("input[type=text][id*=tPOItemQuantity]").tooltip({ position: "right center" });
	            $("input[type=text][id*=tPOItemPrice]").tooltip({ position: "right center" });
	            $("input[type=text][id*=tPOMonths]").tooltip({ position: "right center" });

	            $("input[type=text][id*=tQty]").bind("keyup", priceCalc).bind("keyup", goToCtrl);
	            $("input[type=text][id*=tPrice]").bind("keyup", priceCalc).bind("keyup", goToCtrl);
	            $("input[type=text][id*=tMonths]").bind("keyup", priceCalc).bind("keyup", goToCtrl);

	            $(".positive").numeric({ negative: false });
	            $(".numeric").numeric();
	        }
        );

function showHide(e) {
    var id = $(this).attr("id").replace("txt", "div");
    $(this).css("display", "none");
    $("#" + id).css("display", "block");
    $("#" + id.replace("div", "d")).focus();
}

function grandTotalCalc(e) {

    var ids = $(this).attr("id");
    var id = ids.substring(ids.lastIndexOf("_") + 1);

    $("[id*=lPOTotalPrice_" + id + "]").calc(
	                "capacity * qty * price * months",
	                {
	                    capacity: $("input[type=text][id*=tPOItemCapacity_" + id + "]"),
	                    qty: $("input[type=text][id*=tPOItemQuantity_" + id + "]"),
	                    price: $("input[type=text][id*=tPOItemPrice_" + id + "]"),
	                    months: $("input[type=text][id*=tPOMonths_" + id + "]")
	                },
	                function (s) {
	                    return s.toFixed(2);
	                },
	                function ($this) {
	                    var sum = $("[id*=lPOTotalPrice]").sum();

	                    //if po total item quantity > pr total item quantity change the color to red....
	                    var v1 = parseFloat($("[id*=tPOItemQuantity_" + id + "]").val()) + parseFloat($("[id*=lPOTotalOrderedrItems_" + id + "]").text());
	                    var v2 = parseFloat($("[id*=lQty_" + id + "]").text());
	                    if (v1 > v2) {
	                        $("[id*=tPOItemQuantity_" + id + "]").css("background-color", "red").css("color", "white").css("font-weight", "bold");
	                    }
	                    else {
	                        $("[id*=tPOItemQuantity_" + id + "]").css("background-color", "#fff").css("color", "black").css("font-weight", "normal");
	                    }

	                    //if po total months > pr total months change the color to red....
	                    v1 = parseFloat($("[id*=tPOMonths_" + id + "]").val()) + parseFloat($("[id*=lPOTotalOrderedrMonths_" + id + "]").text());
	                    v2 = parseFloat($("[id*=lMonths_" + id + "]").text());
	                    if (v1 > v2) {
	                        $("[id*=tPOMonths_" + id + "]").css("background-color", "red").css("color", "white").css("font-weight", "bold");
	                    }
	                    else {
	                        $("[id*=tPOMonths_" + id + "]").css("background-color", "#fff").css("color", "black").css("font-weight", "normal");
	                    }

	                    //if po total item quantity > pr total item quantity change the color to red....
	                    var v1 = parseFloat($("[id*=tPOItemCapacity_" + id + "]").val()) + parseFloat($("[id*=lPOTotalOrderedCapacity_" + id + "]").text());
	                    var v2 = parseFloat($("[id*=lCapacity_" + id + "]").text());
	                    if (v1 > v2) {
	                        $("[id*=tPOItemCapacity_" + id + "]").css("background-color", "red").css("color", "white").css("font-weight", "bold");
	                    }
	                    else {
	                        $("[id*=tPOItemCapacity_" + id + "]").css("background-color", "#fff").css("color", "black").css("font-weight", "normal");
	                    }

	                    //is this AMC PO - NO ???
	                    if ($("[id*=tPOItemCapacity_" + id + "]").val() == "undefined") {
	                        //if po total months > pr total months change the color to red....
	                        v1 = parseFloat($("[id*=tPOItemPrice_" + id + "]").val()) + parseFloat($("[id*=lPOTotalOrderedrItemsPrice_" + id + "]").text());
	                        v2 = parseFloat($("[id*=lUnitPrice_" + id + "]").text());
	                        if (v1 > v2) {
	                            $("[id*=tPOItemPrice_" + id + "]").css("background-color", "red").css("color", "white").css("font-weight", "bold");
	                        }
	                        else {
	                            $("[id*=tPOItemPrice_" + id + "]").css("background-color", "#fff").css("color", "black").css("font-weight", "normal");
	                        }

	                        //if po total item price > pr total item price change the color to red....
	                        if ((parseFloat($("[id*=lPOTotalPrice_" + id + "]").text()) + parseFloat($("[id*=lPOTotalOrderedrItemsPrice_" + id + "]").text())) > parseFloat($("[id*=lItemPrice_" + id + "]").text())) {
	                            $("[id*=lPOTotalPrice_" + id + "]").css("background-color", "red").css("color", "white").css("font-weight", "bold");
	                        }
	                        else {
	                            $("[id*=lPOTotalPrice_" + id + "]").css("background-color", "#fff").css("color", "black").css("font-weight", "normal");
	                        }
	                    }

	                    else {
	                    }

	                    $("[id*=lGrandTotalPrice]").text(
			                    sum.toFixed(2)
		                    );
	                    $("[id*=lSumGT]").text(
			                    sum.toFixed(2)
		                    );

	                    //if AMC calculate the total price - change color
	                    if ($("[id*=tPOItemCapacity_" + id + "]").val() != "undefined") {
	                        var v1 = parseFloat($("[id*=lGrandTotalPrice]").text());
	                        var v2 = parseFloat($("[id*=lPRCliPOValue]").text());
	                        if (v1 > v2) {
	                            $("[id*=lGrandTotalPrice]").css("background-color", "red").css("color", "white").css("font-weight", "bold");
	                        }
	                        else {
	                            $("[id*=lGrandTotalPrice]").css("background-color", "#fff").css("color", "black").css("font-weight", "normal");
	                        }
	                    }
	                }
                    );

    $("[id*=lSumVAT]").calc(
	                    "(((totalprice * sumvagtp) / 100)* sumvat) / 100",
	                    {
	                        totalprice: $("[id*=lGrandTotalPrice]"),
	                        sumvagtp: $("[id*=tSumVaGTP]"),
	                        sumvat: $("[id*=tSumVAT]")

	                    },
	                    function (s) {
	                        $("[id*=lSumVAT]").text(s.toFixed(2));
	                        return s.toFixed(2);
	                    },
	                    function (s) {
	                    }
                    );

    $("[id*=lSumST]").calc(
	                    "(((totalprice * subsergtp) / 100)* subst) / 100",
	                    {
	                        totalprice: $("[id*=lGrandTotalPrice]"),
	                        subsergtp: $("[id*=tSumSerGTP]"),
	                        subst: $("[id*=tSumST]")

	                    },
	                    function (s) {
	                        $("[id*=lSumST]").text(s.toFixed(2));
	                        return s.toFixed(2);
	                    },
	                    function (s) {
	                    }
                    );

    $("[id*=lGrandPrice]").calc(
	                    "totalprice + servicetax + vat + shippingfright + nontaxable",
	                    {
	                        totalprice: $("[id*=lGrandTotalPrice]"),
	                        servicetax: $("[id*=lSumST]"),
	                        vat: $("[id*=lSumVAT]"),
	                        shippingfright: $("input[type=text][id*=tSumSF]"),
	                        nontaxable: $("input[type=text][id*=tSumNT]")
	                    },
	                    function (s) {
	                        $("[id*=lGrandPrice]").text(s.toFixed(2));
	                        return s.toFixed(2);
	                    },
	                    function (s) {
	                    }
                    );
}

function priceCalc(e) {
    var ids = $(this).attr("id");
    var id = ids.substring(ids.lastIndexOf("_") + 1);

    $("[id*=lBTotalPrice_" + id + "]").calc(
	                "qty * price * others",
	                {
	                    qty: $("input[type=text][id*=tQty_" + id + "]"),
	                    price: $("input[type=text][id*=tPrice_" + id + "]"),
	                    others: $("input[type=text][id*=tMonths_" + id + "]")
	                },
	                function (s) {
	                    return s.toFixed(2);
	                },
	                function ($this) {

	                    var sum = $("[id*=lBTotalPrice]").sum();
	                    $("[id*=lBGrandTotalPrice]").text(
			                sum.toFixed(2)
		                );
	                }
                );
}

function showdialog(form) {
    var result = window.showModalDialog("importitems.aspx", form, "dialogWidth:300px; dialogHeight:201px; center:yes");
}