﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="CommonTopic.aspx.cs" Inherits="OnlineTraining.CommonTopic" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="Stylesheet" />
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">   

        function ConfirmDelete() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }

        $(document).ready(

        /* This is the function that will get executed after the DOM is fully loaded */
  function () {
      $(".dt").datepicker({
          changeMonth: true, //this option for allowing user to select month
          changeYear: true //this option for allowing user to select from year range
      });
  }
);

 $(document).ready(

 function () {    

    $('.time').timepicker(
    {
        timeFormat: "HH:mm",
        hourMin: 1,
        hourMax: 23
    });       
      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div class="submenu">
         <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Common Topic"></asp:Literal>
        </h1>
        <span style="padding-top: 3px;">
            <asp:Button ID="bNew" class="btn btn-info margin-bottom-20" Text="New" runat="server"
                OnClick="bNew_Click" /></span>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label></div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pAdd" runat="server" CssClass="tile-body">
                    <section class="tile cornered">
                             
          <div class="tile-body tile cornered">
             

             
             <div class="row">

                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div5" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Topic Name" runat="server" ID="Label3" />:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="tTopicName" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    <!-- end col 6 -->
                                    
                                     <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div1" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Pass Percentage" runat="server" ID="Label14" />:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="tPassPercentage" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                </div>
                                
                                
                                <div class="row">
                                    <!--col-6-->
                                   

                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div6" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Description" runat="server" ID="Label16" />:</label>
                                                    <div class="col-sm-8">
                                                         <asp:TextBox ID="tDescription" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    
                                      <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div8" runat="server" class="form-horizontal">
                                                  <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Training Duration Time(hh:mm)" runat="server" ID="Label7" />:</label>
                                                    <div class="col-sm-8">
                                                       <asp:TextBox ID="txttrainingdurationtime" runat="server" class="time form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    <!-- end col 6 -->
                                </div>
                                <div class="row">
                                    <!--col-6-->
                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div7" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Test Time(hh:mm)" runat="server" ID="Label17" />:</label>
                                                    <div class="col-sm-8">
                                                       <asp:TextBox ID="tResolutionTime" runat="server" class="time form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                   <div class="col-md-6" style="display: none;">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div9" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-sm-8">
                                                         <asp:CheckBox ID="ChkActive" Text="Active Status" runat="server" Visible="true"></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    <!-- end col 6 -->
                                </div>
                             
                                
                                <div class="row">
                                    <div class="col-md-12">
                                <div class="tile-body">
                                    <div id="Div10" runat="server" class="form-horizontal">
                                        <div id="Div11" class="form-group" runat="server">
                                            <label class="col-sm-2 control-label" for="">
                                                <asp:Label Text="Test Instruction" runat="server" ID="Label2" />:</label>
                                            <div class="col-sm-10">
                                                <CKEditor:CKEditorControl ID="ttestinstruction" BasePath="~/ckeditor/" RemovePlugins="elementspath"
                                                    runat="server" ToolbarFull="Bold|Italic|Underline|Strike|-|Subscript|Superscript NumberedList|BulletedList|-|Outdent|Indent|Blockquote|CreateDiv
JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock
BidiLtr|BidiRtl
Link|Unlink|Anchor
Image|Flash|Table|HorizontalRule|Smiley|SpecialChar|PageBreak|Iframe
/
Styles|Format|Font|FontSize
TextColor|BGColor
Maximize|ShowBlocks|" Height="100"></CKEditor:CKEditorControl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </div>
                                
                                
                                <div class="row">
                                    <!--col-6-->
                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div3" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="">
                                                        <asp:Label Text="Please Select Excel File" runat="server" ID="Label1" />:</label>
                                                    <div class="col-sm-6">
                                                       <asp:FileUpload ID="FileUpload1" runat="server" />
                                                           <asp:Literal ID="ltPlayer" runat="server" ></asp:Literal>
                                                           <asp:HiddenField ID="hdnguid" runat="server"></asp:HiddenField>
                                                           <asp:HiddenField ID="hdnactual" runat="server"></asp:HiddenField>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                </div>
                                
                                
                                  <div class="row">
                                    <div class="col-md-12">
                                        <div id="Form1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                                            <div class="form-group form-footer">
                                                <div class="col-sm-offset-5 col-sm-6">
                                                   <asp:Button ID="bSave" Text="Save" runat="server" class="btn btn-primary" OnClick="bSave_Click" />
                                                     <asp:Button ID="Button1" Text="Cancel" runat="server" class="btn btn-default" OnClick="Cancel1_Click" />
                                                         
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                 
            </div>
            </section>
                </asp:Panel>
                <br />
                <div class="table-responsive">
                    <asp:Repeater ID="rptrTopic" runat="server" OnItemCommand="rptrTopic_ItemCommand"
                        DataSourceID="">
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr class="new-color">
                                    <th width="5%">
                                        <asp:Label Text="S.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Topic Name" runat="server" ID="lDepartmentName" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Pass Percentage" runat="server" ID="Label5" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Description" runat="server" ID="Label3" />
                                    </th>
                                    <th>
                                        <asp:Label Text="File Name" runat="server" ID="Label9" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Test Time(hh:mm)" runat="server" ID="Label12" />
                                    </th>
                                    <th width="25%" style="text-align: center">
                                        Actions
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("TopicName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("PassPercentage") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("Description") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label11" runat="server" Visible="false" Text='<%# Eval("filename") %>' />
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("displayname") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("ResolutionTime") %>' />

                                </td>
                                <td class="actions text-center">
                                    <asp:LinkButton class="btn btn-primary" ID="lnkEdit" runat="server" Text="Edit" CommandName="edit"
                                        CommandArgument='<%# Eval("CommonTopicID") %>' />
                                    &nbsp;
                                    <asp:LinkButton class="btn btn-danger" ID="lnkDelete" runat="server" Text="Delete"
                                        CommandArgument='<%# Eval("CommonTopicID") %>' CommandName="delete" OnClientClick="return ConfirmDelete();" />
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hTopicID" Value='<%# Eval("CommonTopicID")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <%-- </ContentTemplate> </asp:UpdatePanel>--%>
    </div>
</asp:Content>
