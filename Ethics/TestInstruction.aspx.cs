﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using OnlineTraining.Code;
using OnlineTrainingBL;
using OnlineTrainingPL;
using OnlineTrainingPL.Master;


namespace OnlineTraining
{
    public partial class TextInstruction : System.Web.UI.Page
    {

        BLTextInstruction objBLTextInstruction =new BLTextInstruction();
        PLTextInstruction objPLTextInstruction = new PLTextInstruction();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadInstructions();
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (textinstruction.Text == string.Empty)
            {
                NotifyMessages("Please enter the instruction", Common.ErrorType.Error);
                return;
            }

            objPLTextInstruction.CompanyId = UserSession.CompanyIDUser;
            objPLTextInstruction.TextInstructionText = this.textinstruction.Text.Trim();
            string[] result = objBLTextInstruction.SaveUpdateTextInstructions(objPLTextInstruction);
        }

        private void LoadInstructions()
        {
            try
            {
                int InstructionId = 0;
                objPLTextInstruction.CompanyId = UserSession.CompanyIDUser;
                objPLTextInstruction.TextInstructionId = InstructionId;
                DataSet ds = new DataSet();
                ds = objBLTextInstruction.GetTextInstructions(objPLTextInstruction);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    this.textinstruction.Text = ds.Tables[0].Rows[0]["TextInstructions"].ToString();
                    this.textinstruction.DataBind();
                }
                else
                {
                    this.textinstruction.Text = string.Empty;
                }
            }

            catch (Exception ex)
            {

                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            divmsg.Visible = false;
        }
    }
}