﻿
//Created By : Karthik.K
//Create Date : 15-Feb-2016
//Last Modified : 22-Feb-2016
//Form Name : UserDepartmentAssign

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Ethics.com.i2isoftwares;
using OnlineTraining.Code;
using OnlineTrainingBL;
using OnlineTrainingPL;
using OnlineTrainingPL.Master;


namespace OnlineTraining
{
    public partial class UserDepartmentAssign : System.Web.UI.Page
    {

        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        PLTopic objTopicPL = new PLTopic();
        BLTopic objTopicBL = new BLTopic();

        BLDepartment objbldept = new BLDepartment();
        PLDepartment objpldept = new PLDepartment();

        BLUserDepartment objbluserdept = new BLUserDepartment();
        PLUserDepartment objpluserdept = new PLUserDepartment();


        PLUser objUserNamePL = new PLUser();
        BLUser objUserNameBL = new BLUser();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserDepartment();
                LoadUserTopic();
                BindDetails();
                BindDetailsGrid();
                txtStartDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtEndDate.Text = AddBusinessDays(DateTime.Now, 7).ToString("MM/dd/yyyy");
                BindAssignedUsers();
            }
        }


        private void UserDepartment()
        {
            try
            {
                objpldept.CompanyID = UserSession.CompanyIDCurrent;
                objpldept.GroupId = UserSession.GroupID;
                objpldept.DepartmentId = 0;
                DataSet Ds = new DataSet();
                Ds = objbldept.GetDepartmentDetails(objpldept);

                if (Ds.Tables[0].Rows.Count > 0)
                {
                    this.ddlDepartment.DataSource = Ds;
                    this.ddlDepartment.DataTextField = "DepartmentName";
                    this.ddlDepartment.DataValueField = "DepartmentId";
                    this.ddlDepartment.DataBind();
                }
                else
                {
                    this.ddlDepartment.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }



        private void LoadUserTopic()
        {
            BLTopic objBLTopic = new BLTopic();
            PLTopic objPLTopic = new PLTopic();

            int departmentid = 0;
            if (!Common.DDVal(ddlDepartment, out departmentid))
            {
                NotifyMessages("Please select department", Common.ErrorType.Error);
                return;
            }

            objPLTopic.CompanyID = UserSession.CompanyIDCurrent;
            objPLTopic.Groupid = UserSession.GroupID;
            objPLTopic.DepartmentId = departmentid;
            objPLTopic.TopicID = 0;
            DataSet Ds = new DataSet();
            Ds = objBLTopic.getTopicByDepartment(objPLTopic);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.ddlTopic.DataSource = Ds;
                this.ddlTopic.DataTextField = "TopicName";
                this.ddlTopic.DataValueField = "TopicID";
                this.ddlTopic.DataBind();
            }
            else
            {
                this.ddlTopic.Items.Clear();
            }

            this.ddlTopic_SelectedIndexChanged(null, null);
        }


        private void BindDetails()
        {
            try
            {
                int companyid = 0;
                int departmentid = 0;
                Common.DDVal(ddlDepartment, out departmentid);
                objTopicPL.Groupid = UserSession.GroupID;
                objTopicPL.CompanyID = companyid;
                objTopicPL.DepartmentId = departmentid;
                DataSet Ds = new DataSet();
                Ds = objTopicBL.GetAssignedTopics(objTopicPL);
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    ddlTopic.DataSource = Ds;
                    ddlTopic.DataBind();
                }
                else
                {
                    ddlTopic.DataSource = null;
                    ddlTopic.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        private void BindDetailsGrid()
        {
            try
            {

                int departmentid;
                int topicid;
                Common.DDVal(ddlDepartment, out departmentid);
                Common.DDVal(ddlTopic, out topicid);
                objUserNamePL.CompanyID = UserSession.CompanyIDUser;
                objUserNamePL.Department = departmentid;
                objUserNamePL.Topic = topicid;
                DataSet Ds = new DataSet();
                Ds = objUserNameBL.GetUserBind(objUserNamePL);
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    rptruser.DataSource = Ds;
                    rptruser.DataBind();
                }
                else
                {
                    rptruser.DataSource = null;
                    rptruser.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }



        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void ddlUserDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindDetailsGrid();
                divmsg.Visible = false;
                lblError.Text = string.Empty;
                LoadUserTopic();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public DateTime AddBusinessDays(DateTime dt, int nDays)
        {
            int weeks = nDays / 5;
            nDays %= 5;
            while (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                dt = dt.AddDays(1);

            while (nDays-- > 0)
            {
                dt = dt.AddDays(1);
                if (dt.DayOfWeek == DayOfWeek.Saturday)
                    dt = dt.AddDays(2);
            }
            return dt.AddDays(weeks * 7);
        }


        protected void ddlTopic_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindDetailsGrid();
                BindAssignedUsers();
                divmsg.Visible = false;
                lblError.Text = string.Empty;
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        protected void bSave_Click(object sender, EventArgs e)
        {
            int departmentid = 0;
            if (!Common.DDVal(ddlDepartment, out departmentid))
            {
                NotifyMessages("Select Department", Common.ErrorType.Error);
                return;
            }

            int topicid = 0;

            if (!Common.DDVal(ddlTopic, out topicid))
            {
                NotifyMessages("Select Topic", Common.ErrorType.Error);
                return;
            }

            if (txtStartDate.Text == string.Empty)
            {
                NotifyMessages("Please choose the Start Date", Common.ErrorType.Error);
                return;
            }

            if (txtEndDate.Text == string.Empty)
            {
                NotifyMessages("Please choose the End Date", Common.ErrorType.Error);
                return;
            }

            objpluserdept.CompanyId = UserSession.CompanyIDUser;
            objpluserdept.GroupId = UserSession.GroupID;
            objpluserdept.DepartmentId = departmentid;
            objpluserdept.TopicId = topicid;
            objpluserdept.UserDepartmentId = Convert.ToInt32(hdnuserdepartmentid.Value);
            objpluserdept.UId = UserSession.UserID;
            objpluserdept.Editor = this.textinstruction.Text.Trim();
            objpluserdept.StartDate = this.txtStartDate.Text.Trim();
            objpluserdept.EndDate = this.txtEndDate.Text.Trim();
            objpldept.CUID = Convert.ToInt32(UserSession.UserID);
            DataTable dtAssignUser = new DataTable();
            dtAssignUser.Columns.Add("UserID", typeof(int));
            dtAssignUser.Columns.Add("EmailID", typeof(string));
            dtAssignUser.Columns.Add("FistName", typeof(string));
            foreach (RepeaterItem userDetail in rptruser.Items)
            {
                CheckBox chkMSFVisible = (CheckBox)userDetail.FindControl("chkMSFVisible");
                if (chkMSFVisible != null)
                {
                    if (chkMSFVisible.Checked)
                    {
                        HiddenField hUserID = (HiddenField)userDetail.FindControl("hUserID");
                        HiddenField hEmailID = (HiddenField)userDetail.FindControl("hEmailID");
                        HiddenField hFirstName = (HiddenField)userDetail.FindControl("hFirstName");
                        if (hUserID != null && hEmailID != null && hFirstName != null)
                        {
                            int userid = Convert.ToInt32(hUserID.Value);
                            string mailId = hEmailID.Value;
                            string firstname = hFirstName.Value;
                            dtAssignUser.Rows.Add(userid, mailId, firstname);
                        }
                    }
                }

            }

            //if (dtAssignUser.Rows.Count == 0)
            //{
            //    NotifyMessages("Please Select Atlest one User", Common.ErrorType.Error);
            //    return;
            //}


            string xmlString = string.Empty;
            using (TextWriter writer = new StringWriter())
            {
                dtAssignUser.TableName = "UserDepartmentAssign";
                dtAssignUser.WriteXml(writer);
                xmlString = writer.ToString();
                objpluserdept.UserXml = xmlString;
            }
            string[] result = objbluserdept.InsertUpdateUserDepartment(objpluserdept);

            foreach (DataRow druserDet in dtAssignUser.Rows)
            {
                string firstname = druserDet["FistName"].ToString();
                string emailid = druserDet["EmailID"].ToString();
                string subject = ddlTopic.SelectedItem.Text + " : " + txtStartDate.Text + " - " + txtEndDate.Text;
                string body = textinstruction.Text.Trim();

                Mailfucntion(firstname, emailid, subject, UserdepartParameters(body), string.Empty);
            }

            NotifyMessages(result[0] + " & Mail Send Sucessfully.", Common.ErrorType.Error);
            ClearData();
        }

        private void ClearData()
        {
            txtStartDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
            txtEndDate.Text = DateTime.Now.AddDays(9).ToString("MM/dd/yyyy");
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "unselectAll", "<script type='text/javascript'>unCheckAll();</script>", false);

        }

        private string UserdepartParameters(string Users)
        {
            try
            {
                Users = Users.Replace("#Department#", ddlDepartment.SelectedItem.Text);
                Users = Users.Replace("#Topic#", ddlTopic.SelectedItem.Text);
                Users = Users.Replace("#StartDate#", txtStartDate.Text);
                Users = Users.Replace("#EndDate#", txtEndDate.Text);
                Users = Users.Replace("#Password#", "Password0");
            }
            catch (Exception ex)
            {
                NotifyMessages("Type Parameters", Common.ErrorType.Error);

            }
            return Users;
        }

        protected void btn_ResendEmail(object sender, EventArgs e)
        {
            try
            {
                ImageButton rs = (ImageButton)sender;

                if (rs != null)
                {
                    RepeaterItem riUser = (RepeaterItem)rs.Parent;
                    if (riUser != null)
                    {
                        HiddenField hEmailID = (HiddenField)riUser.FindControl("hEmailID");
                        HiddenField hFirstName = (HiddenField)riUser.FindControl("hFirstName");
                        CheckBox chkMSFVisible = (CheckBox)riUser.FindControl("chkMSFVisible");
                        if (hEmailID != null && chkMSFVisible != null && hFirstName != null)
                        {
                            if (chkMSFVisible.Checked)
                            {
                                string emailid = hEmailID.Value;
                                string firstname = hFirstName.Value; ListItem litopic = ddlTopic.SelectedItem;
                                if (litopic != null)
                                {
                                    string subject = litopic.Text + " : " + txtStartDate.Text + " - " + txtEndDate.Text;
                                    string body = textinstruction.Text.Trim();

                                    Mailfucntion(firstname, emailid, subject, UserdepartParameters(body), string.Empty);
                                    NotifyMessages("Your Email Sent Successfully", Common.ErrorType.Error);
                                }
                            }

                            else
                            {
                                NotifyMessages("Please choose any username", Common.ErrorType.Error);

                            }



                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                this.divmsg.Visible = false;
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        private void BindAssignedUsers()
        {
            try
            {
                DataSet dsuserdet = new DataSet();
                int departmentid = 0;
                Common.DDVal(ddlDepartment, out departmentid);
                int topicid = 0;
                Common.DDVal(ddlTopic, out topicid);
                objpluserdept.CompanyId = UserSession.CompanyIDUser;
                objpluserdept.DepartmentId = departmentid;
                objpluserdept.TopicId = topicid;
                dsuserdet = objbluserdept.GetUserBind(objpluserdept);
                if (dsuserdet != null && dsuserdet.Tables.Count > 0 && dsuserdet.Tables[0].Rows.Count > 0)
                {
                    //foreach (DataRow drUserdet in dsuserdet.Tables[0].Rows)
                    //{
                    //    string userid = drUserdet["UserID"].ToString();
                    //    foreach (RepeaterItem riuser in rptruser.Items)
                    //    {
                    //        HiddenField hdnuserid = (HiddenField)riuser.FindControl("hUserID");
                    //        CheckBox chkvisible = (CheckBox)riuser.FindControl("chkMSFVisible");
                    //        if (hdnuserid != null && chkvisible != null)
                    //        {
                    //            if (hdnuserid.Value == userid)
                    //            {
                    //                chkvisible.Checked = true;
                    //            }
                    //        }
                    //    }
                    //}

                    DataRow druser = dsuserdet.Tables[0].Rows[0];
                    textinstruction.Text = druser["Editor"].ToString();
                    //if (druser["StartDate"].ToString() != string.Empty)
                    //{
                    //    txtStartDate.Text = Convert.ToDateTime(druser["StartDate"].ToString()).ToShortDateString();
                    //}

                    //if (druser["EndDate"].ToString() != string.Empty)
                    //{
                    //    txtEndDate.Text = Convert.ToDateTime(druser["EndDate"].ToString()).ToShortDateString();
                    //}


                }
                else
                {
                    txtStartDate.Text = DateTime.Now.ToShortDateString();
                    txtEndDate.Text = DateTime.Now.ToShortDateString();
                    textinstruction.Text = string.Empty;
                    foreach (RepeaterItem riuser in rptruser.Items)
                    {
                        CheckBox chkvisible = (CheckBox)riuser.FindControl("chkMSFVisible");
                        if (chkvisible != null)
                        {
                            chkvisible.Checked = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void Mailfucntion(string FirstName, string EmailID, string Subject, string Body, string CCEmailID)
        {

            try
            {
                Body = Body.Replace("#UserEmailID#", EmailID);
                Body = Body.Replace("#FirstName#", FirstName);

                string CC = string.Empty;
                MailService objmailservice = new MailService();
                string username, password, smtp, smtpport, frommail;
                username = ConfigurationManager.AppSettings.Get("mailusername");
                password = ConfigurationManager.AppSettings.Get("mailpassword");
                smtp = ConfigurationManager.AppSettings.Get("smtpaddress");
                smtpport = ConfigurationManager.AppSettings.Get("smtpport");
                frommail = ConfigurationManager.AppSettings.Get("FromMail");

                bool issent = objmailservice.SendMailMessage(frommail, EmailID,
                      CCEmailID, string.Empty, Subject, Body, string.Empty,
                      username,
                      password,
                      smtp,
                      smtpport, "Ethics Onlinetest");
            }
            catch (Exception)
            {

                throw;
            }


        }

        protected void btn_ResendAllEmail(object sender, ImageClickEventArgs e)
        {
            try
            {
                int isentCount = 0;
                foreach (RepeaterItem riUser in rptruser.Items)
                {
                    if (riUser != null)
                    {
                        HiddenField hEmailID = (HiddenField)riUser.FindControl("hEmailID");
                        HiddenField hFirstName = (HiddenField)riUser.FindControl("hFirstName");
                        CheckBox chkMSFVisible = (CheckBox)riUser.FindControl("chkMSFVisible");
                        if (hEmailID != null && chkMSFVisible != null && hFirstName != null)
                        {
                            if (chkMSFVisible.Checked)
                            {
                                string emailid = hEmailID.Value;
                                string firstname = hFirstName.Value;
                                ListItem litopic = ddlTopic.SelectedItem;
                                if (litopic != null)
                                {
                                    string subject = litopic.Text + " : " + txtStartDate.Text + " - " + txtEndDate.Text;
                                    string body = textinstruction.Text.Trim();
                                    Mailfucntion(firstname, emailid, subject, UserdepartParameters(body), string.Empty);
                                    NotifyMessages("Your Email Sent Successfully", Common.ErrorType.Error);
                                    isentCount++;
                                }
                            }
                        }
                    }
                }
                if (isentCount == 0)
                {
                    NotifyMessages("No User Found", Common.ErrorType.Error);
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }
    }


}