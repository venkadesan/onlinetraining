﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTrainingPL;
using OnlineTrainingBL;
using OnlineTraining.Code;
using MB = Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Web.Script.Serialization;
using System.Text;
using OnlineTrainingPL.Master;


namespace OnlineTraining
{
    public partial class Succeeded : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (UserSession.IsTestFinished)
                {
                    //pass image
                }
                else
                {
                    //fail image
                }
            }

        }

        protected void Unnamed1_Click(object sender, EventArgs e)
        {
            //UserSession.IsTestFinished = true;
            Response.Redirect("Default.aspx", true);

          

        }     
    }
}