﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="Topic.aspx.cs" Inherits="OnlineTraining.Topic" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%-- <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="Stylesheet" />
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="App_Themes/jquery/jquery-1.6.2.min.js"></script>
    <script type="text/javascript">

        function ConfirmDelete() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }

        $(document).ready(

        /* This is the function that will get executed after the DOM is fully loaded */
            function () {
                $(".dt").datepicker({
                    changeMonth: true, //this option for allowing user to select month
                    changeYear: true //this option for allowing user to select from year range
                });
            }
        );

        //        $(document).ready(
        //     function () {

        //         $('.time').timepicker(
        //         {
        //             timeFormat: "HH:mm",
        //             hourMin: 1,
        //             hourMax: 23
        //         });
        //     });       
      
    </script>
    <script type="text/javascript">
        var validFilesTypes = ["mp4", "3gb"];

        function CheckExtension(file) {

            var filePath = file.value;
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
            var isValidFile = false;

            for (var i = 0; i < validFilesTypes.length; i++) {
                if (ext == validFilesTypes[i]) {
                    isValidFile = true;
                    break;
                }
            }

            if (!isValidFile) {
                file.value = null;
                alert("Invalid File. Valid extensions are:\n\n" + validFilesTypes.join(", "));
            }

            return isValidFile;
        }
    </script>
    <script type="text/javascript">
        function CheckFile(file) {
            var isValidFile = CheckExtension(file);

            return isValidFile;
        }
    </script>
    <script type="text/javascript">
        var validPDFFilesTypes = ["pdf", "ppt", "pptx", "ppts"];

        function CheckPDFExtension(file) {

            var filePath = file.value;
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
            var isValidPdfFile = false;

            for (var i = 0; i < validPDFFilesTypes.length; i++) {
                if (ext == validPDFFilesTypes[i]) {
                    isValidPdfFile = true;
                    break;
                }
            }

            if (!isValidPdfFile) {
                file.value = null;
                alert("Invalid File. Valid extensions are:\n\n" + validPDFFilesTypes.join(", "));
            }

            return isValidPdfFile;
        }
    </script>
    <script type="text/javascript">
        function CheckPDFFile(file) {
            var isValidPdfFile = CheckPDFExtension(file);

            return isValidPdfFile;
        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

    </script>
    <style type="text/css">
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
        }
        .modalPopup
        {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #e09929;
            border-radius: 12px;
            padding: 0;
        }
        .modalPopup .header
        {
            background-color: #e09929;
            height: 30px;
            color: White;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
        }
        .modalPopup .body
        {
            min-height: 50px;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
        }
        .modalPopup .footer
        {
            padding: 6px;
        }
        .modalPopup .yes, .modalPopup .no
        {
            height: 23px;
            color: White;
            line-height: 23px;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
            border-radius: 4px;
        }
        .modalPopup .yes
        {
            background-color: #2FBDF1;
            border: 1px solid #0DA9D0;
        }
        .modalPopup .no
        {
            background-color: #9F9F9F;
            border: 1px solid #5C5C5C;
        }
    </style>
    <script type="text/javascript">

        function DeleteVideoFile() {
            var x = confirm("Are you sure you want to delete?");
            if (x) {

                document.getElementById('<%=hdndisplyname1.ClientID %>').value = '';
                document.getElementById('<%=hdnfilename1.ClientID %>').value = '';
                document.getElementById('<%=lbldisplyname1.ClientID %>').innerHTML = '';
                document.getElementById('<%=btnDelete.ClientID %>').style.visibility = "hidden";
            }
        }

        function DeletePDFFile() {
            var x = confirm("Are you sure you want to delete?");
            if (x) {

                document.getElementById('<%=hdndisplyname2.ClientID %>').value = '';
                document.getElementById('<%=hdnfilename2.ClientID %>').value = '';
                document.getElementById('<%=lbldisplyname2.ClientID %>').innerHTML = '';
                document.getElementById('<%=btnRemove.ClientID %>').style.visibility = "hidden";
            }
        }

        function unCheckAll() {
            $(".chkVisible input").each(function () {
                $(this).attr('checked', false);
            });
        }

        $(document).ready(function () {
            $('.Visible input').click(function () {
                $(".chkVisible input").each(function () {
                    $(this).attr('checked', $('.Visible input').attr('checked'));
                });
            });

        });
    </script>
    <script language="javascript">
        <!--
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div class="submenu">
        <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Topic"></asp:Literal>
        </h1>
        <span style="padding-top: 3px;">
            <asp:Button ID="bNew" class="btn btn-info margin-bottom-20" Text="New" runat="server"
                OnClick="bNew_Click" />
            <asp:Button ID="bglobalAdd" class="btn btn-info margin-bottom-20" Text="Add From Global"
                runat="server" OnClientClick="unCheckAll();" />
        </span>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <%--    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label></div>--%>
    <div class="alert alert-danger" style="margin-top: 12px; height: 40px; padding-top: 10px;
        padding-left: 420px" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pAdd" runat="server">
                    <section class="tile cornered">
                             
         <div class="tile-body tile cornered">
             

             
             <div class="row">
                                    <!--col-6-->
                                    <div class="col-md-12">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div4" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="">
                                                        <asp:Label Text="Department" runat="server" ID="lCategoryName" />:</label>
                                                    <div class="col-sm-10">
                                                        <asp:DropDownList ID="ddldepartment" runat="server" class="chosen-select form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>

                                 
                                    <!-- end col 6 -->
                                </div>
                                
                                
                                <div class="row">
                                
                                   <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div5" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Topic Name" runat="server" ID="Label3" />:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="tTopicName" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>

                                    <!--col-6-->
                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div1" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Pass Percentage" runat="server" ID="Label14" />:</label>
                                                    <div class="col-sm-8">
                                                        <asp:TextBox ID="tPassPercentage" onkeypress="return isNumberKey(event)" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>

                                   
                                    <!-- end col 6 -->
                                </div>
                                <div class="row">
                                    
                                     <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div6" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Description" runat="server" ID="Label16" />:</label>
                                                    <div class="col-sm-8">
                                                         <asp:TextBox ID="tDescription" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>

                                    <!--col-6-->
                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div7" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Test Time(hh:mm)" runat="server" ID="Label17" />:</label>
                                                    <div class="col-sm-8">
                                                       <asp:TextBox ID="tResolutionTime" runat="server" class="time form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                   
                                    <!-- end col 6 -->
                                </div>
                                
                                
                                <div class="row">
                                    
                                     <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div8" runat="server" class="form-horizontal">
                                                  <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Training Duration Time(hh:mm)" runat="server" ID="Label7" />:</label>
                                                    <div class="col-sm-8">
                                                       <asp:TextBox ID="txttrainingdurationtime" runat="server" class="time form-control parsley-validated"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    
                                        <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div13" runat="server" class="form-horizontal">
                                                  <div class="form-group">
                                                    <label class="col-sm-4 control-label" for="">
                                                        <asp:Label Text="Attempts" runat="server" ID="Label18" />:</label>
                                                    <div class="col-sm-8">
                                                       <asp:TextBox ID="txtAttempts" onkeypress="return isNumber(event)" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    <!--col-6-->
                                    <div class="col-md-12">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div9" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-sm-2">
                                                         <asp:CheckBox ID="ChkActive" runat="server" Text="Active"></asp:CheckBox>
                                                    </div>
                                                    <div class="col-sm-8">
                                                         <asp:CheckBox ID="Chktestrequied" runat="server" Text="Test Required" ></asp:CheckBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    <!-- end col 6 -->
                                </div>
                                
                                <%--<div class="row">
                                    <div class="col-md-12">
                                <div class="tile-body">
                                    <div id="Div10" runat="server" class="form-horizontal">
                                        <div id="Div11" class="form-group" runat="server">
                                            <label class="col-sm-2 control-label" for="">
                                                <asp:Label Text="Test Instruction" runat="server" ID="Label2" />:</label>
                                            <div class="col-sm-10">
                                                <CKEditor:CKEditorControl ID="ttestinstruction" BasePath="~/ckeditor/" RemovePlugins="elementspath"
                                                    runat="server" ToolbarFull="Bold|Italic|Underline|Strike|-|Subscript|Superscript NumberedList|BulletedList|-|Outdent|Indent|Blockquote|CreateDiv
JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock
BidiLtr|BidiRtl
Link|Unlink|Anchor
Image|Flash|Table|HorizontalRule|Smiley|SpecialChar|PageBreak|Iframe
/
Styles|Format|Font|FontSize
TextColor|BGColor
Maximize|ShowBlocks|" Height="250"></CKEditor:CKEditorControl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </div>--%>
                                
                                
                                <div class="row">
                                    <!--col-6-->
                                    <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div3" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="">
                                                        <asp:Label Text="Upload VideoFile " runat="server" ID="Label1" />:</label>
                                                    <div class="col-sm-6">
                                                       <asp:FileUpload ID="FileUpload1" runat="server" onchange="return CheckFile(this)" />
                                                           <asp:Literal ID="ltPlayer" runat="server" ></asp:Literal>
                                                           <asp:HiddenField ID="hdndisplyname1" runat="server"></asp:HiddenField>
                                                           <asp:HiddenField ID="hdnfilename1" runat="server"></asp:HiddenField>
                                                           <asp:Label ID="lbldisplyname1" runat="server" Text="Label"></asp:Label>
                                                          <input id="btnDelete" tooltip="Delete Project" onclick="DeleteVideoFile();" runat="server" type="button" value="x" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                    
                                     <div class="col-md-6">
                                        <!-- tile body -->
                                        <div class="tile-body">
                                            <div id="Div12" runat="server" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-6 control-label" for="">
                                                        <asp:Label Text="Upload Documents" runat="server" ID="Label8" />:</label>
                                                    <div class="col-sm-6">
                                                       <asp:FileUpload ID="fuupload2" runat="server" onchange="return CheckPDFFile(this)" />
                                                       <asp:HiddenField ID="hdndisplyname2" runat="server"></asp:HiddenField>
                                                           <asp:HiddenField ID="hdnfilename2" runat="server"></asp:HiddenField>
                                                           <asp:Label ID="lbldisplyname2" runat="server" Text="Label"></asp:Label>
                                                           <input id="btnRemove" tooltip="Delete Project" onclick="DeletePDFFile();" runat="server" type="button" value="x" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /tile body -->
                                    </div>
                                </div>
                                
                                
                                  <div class="row">
                                    <div class="col-md-12">
                                        <div id="Form1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                                            <div class="form-group form-footer">
                                                <div class="col-sm-offset-5 col-sm-6">
                                                   <asp:Button ID="bSave" Text="Save" runat="server" class="btn btn-primary" OnClick="bSave_Click" />
                                                     <asp:Button ID="Button1" Text="Cancel" runat="server" class="btn btn-default" OnClick="Cancel1_Click" />
                                                         
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                 
            </div>
             </section>
                </asp:Panel>
                <br />
                <div class="table-responsive">
                    <asp:Repeater ID="rptrTopic" runat="server" OnItemCommand="rptrTopic_ItemCommand"
                        DataSourceID="" OnItemDataBound="rptrTopic_ItemDataBound">
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr class="new-color">
                                    <th width="5%">
                                        <asp:Label Text="S.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Topic Name" runat="server" ID="lDepartmentName" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Pass Percentage" runat="server" ID="Label5" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Description" runat="server" ID="Label3" />
                                    </th>
                                    <th>
                                        Active Status
                                    </th>
                                    <th>
                                        <asp:Label Text="File Name" runat="server" ID="Label9" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Test Time(hh:mm)" runat="server" ID="Label12" />
                                    </th>
                                    <th width="25%" style="text-align: center">
                                        Actions
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("TopicName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("PassPercentage") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("Description") %>' />
                                </td>
                                <td>
                                    <asp:CheckBox ID="ChkRdActive" OnCheckedChanged="ChkRdActive_CheckedChanged" AutoPostBack="True" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="Label11" runat="server" Visible="false" Text='<%# Eval("filename") %>' />
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("displayname") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("ResolutionTime") %>' />
                                </td>
                                <td class="actions text-center">
                                    <asp:LinkButton class="btn btn-primary" ID="lnkEdit" runat="server" Text="Edit" CommandName="edit" />
                                    &nbsp;
                                    <asp:LinkButton class="btn btn-danger" ID="lnkDelete" runat="server" Text="Delete"
                                        CommandName="delete" OnClientClick="return ConfirmDelete();" />
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hTopicID" Value='<%# Eval("TopicID")%>' />
                            <asp:HiddenField runat="server" ID="HdnActive" Value='<%# Eval("IsActive")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
    <%-- </ContentTemplate> </asp:UpdatePanel>--%>
    <div class="row">
        <!-- col 8 -->
        <asp:Button ID="btnShowPopup" runat="server" Style="display: none" />
        <ajax:ModalPopupExtender ID="mpeDepartmentGlobal" runat="server" TargetControlID="bglobalAdd"
            PopupControlID="pnlpopup" CancelControlID="btnglobalcancel" BackgroundCssClass="modalBackground">
        </ajax:ModalPopupExtender>
        <asp:Panel ID="pnlpopup" runat="server" BackColor="White" Height="540px" Width="700px"
            CssClass="modalPopup" Style="display: none">
            <br />
            <div style="text-align: center; font-weight: bold;">
                Add Topic From Global</div>
            <div style="height: 450px; overflow-y: scroll; float: left; width: 100%;">
                <div class="table-responsive">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="">
                            <asp:Label Text="Department" runat="server" ID="Label15" />:</label>
                        <div class="col-sm-8">
                            <asp:DropDownList ID="ddlDepartmentCommon" CssClass="" runat="server" class="chosen-select form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <asp:Repeater ID="rptrcommontopic" runat="server" DataSourceID="">
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr class="new-color">
                                    <th width="5%">
                                        <asp:Label Text="S.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Topic Name" runat="server" ID="lDepartmentName" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Pass Percentage" runat="server" ID="Label5" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Description" runat="server" ID="Label3" />
                                    </th>
                                    <th>
                                        <asp:Label Text="File Name" runat="server" ID="Label9" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Test Time(hh:mm)" runat="server" ID="Label12" />
                                    </th>
                                    <th width="25%">
                                        <asp:CheckBox ID="chkAllVisible" CssClass="Visible" runat="server" ClientIDMode="Static" /><asp:Label
                                            Text="Select All" runat="server" ID="lCallTypeName" />
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lbltopicname" runat="server" Text='<%# Eval("TopicName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblpasspercentage" runat="server" Text='<%# Eval("PassPercentage") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lbldescription" runat="server" Text='<%# Eval("Description") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblfilename" runat="server" Visible="false" Text='<%# Eval("filename") %>' />
                                    <asp:Label ID="lbldisplyname" runat="server" Text='<%# Eval("displayname") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblresolutiontime" runat="server" Text='<%# Eval("ResolutionTime") %>' />
                                    <asp:Label ID="lblTrainingDuration" Visible="False" runat="server" Text='<%# Eval("TrainingDuration") %>' />
                                    <asp:Label ID="lbltestInstruction" runat="server" Text='<%# Eval("testInstruction") %>' />
                                </td>
                                <td class="actions text-center">
                                    <asp:CheckBox CssClass="chkVisible" ID="chkMSFVisible" runat="server" />
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hTopicID" Value='<%# Eval("CommonTopicID")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="Div2" parsley-validate="" role="form" runat="server" class="form-horizontal">
                        <div class="form-group form-footer">
                            <div class="col-sm-offset-5 col-sm-6">
                                <asp:Button ID="btnglobalsave" Text="Save" runat="server" class="btn btn-primary"
                                    OnClick="btnglobalsave_Click" />
                                <asp:Button ID="btnglobalcancel" Text="Cancel" runat="server" class="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
