﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" enableEventValidation ="false"  AutoEventWireup="true" CodeBehind="QuestionAnswerReports.aspx.cs" Inherits="OnlineTraining.QuestionAnswerReports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
        var id = "progressbar";
        var value = 20;
        function GetPassPercentage(id, value) {
            $("#" + id).progressbar({
                value: value
            });
        }

    </script>
    <script type="text/javascript">
        function Confirm() {
            var x = confirm("You have not score the required percentage. Kindly try again ??");
            if (x)
                return true;
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="submenu">
         <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Report"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Question/Answer Report"></asp:Literal>
        </h1>
        <span style="padding-top: 3px;">
            <asp:Button ID="bNew" class="btn btn-info margin-bottom-20" Text="New" Visible="false"
                runat="server" /></span>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label></div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <!-- tile body -->
                    <div class="tile-body">
                        <div id="Div1" parsley-validate="" role="form" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="">
                                    <asp:Label Text="User Name" runat="server" ID="Label3" />:</label>
                                <div class="col-sm-8">
                                    <asp:DropDownList runat="server" AutoPostBack="true" class="chosen-select form-control"
                                        ID="dUser" OnSelectedIndexChanged="dUser_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /tile body -->
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <!-- tile body -->
                        <div class="tile-body">
                            <div id="basicvalidations" parsley-validate="" role="form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="Select Topic" runat="server" ID="Label4" />:</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList runat="server" AutoPostBack="true" class="chosen-select form-control"
                                            ID="dTopic" OnSelectedIndexChanged="dTopic_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /tile body -->
                    </div>
                </div>
                <br />
                <%-- <div class="table-responsive">
                    <asp:Repeater ID="rptrAnswer" runat="server" OnItemCommand="rptrAnswer_ItemCommand"
                        DataSourceID="" OnItemDataBound="rptrAnswer_ItemDataBound">
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr style="background-color: #808080; color: White;">
                                    <th width="5%">
                                        <asp:Label Text="Q.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Question" runat="server" ID="Label6" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Answer" runat="server" ID="Label1" />
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("QuestionName") %>' />
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblResultYes" Height="20" Width="20" ImageUrl="App_Themes/images/yes.png" />
                                    <asp:Label runat="server" ID="lblResultNo" Height="20" Width="20" ImageUrl="App_Themes/images/no.png" />
                                    <asp:Label runat="server" ID="lblResultNA" Height="40" Width="40" ImageUrl="App_Themes/images/NA.png" />
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hQuestionID" Value='<%# Eval("QuestionID")%>' />
                             <asp:HiddenField runat="server" ID="hTopicID" Value='<%# Eval("TopicID")%>' />
                            <asp:HiddenField runat="server" ID="hPassPercentage" Value='<%# Eval("PassPercentage")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>--%>
                <div class="table-responsive">
                    <div style="float: right;">
                        <asp:ImageButton runat="server" ID="imgExcel" ClientIDMode="Static" ImageUrl="~/App_Themes/images/Excel.png"
                            Width="40" Height="40" ToolTip="Click to download report details" OnClick="imgExcel_Click" />
                    </div>
                    <asp:Repeater ID="rptrAnswer" runat="server" OnItemCommand="rptrAnswer_ItemCommand"
                        DataSourceID="" >
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr  class="new-color">
                                    <th width="5%">
                                        <asp:Label Text="Q.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Question" runat="server" ID="Label6" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Answer" runat="server" ID="Label1" />
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="SLNo" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("QuestionName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lbltext" runat="server" Text='<%# Eval("Answer") %>'></asp:Label>
                                    <%-- <asp:Label runat="server" ID="imgResultYes" Height="20" Width="20" ImageUrl="App_Themes/images/yes.png"></asp:Label>
                                    <asp:Label runat="server" ID="imgResultNo" Height="20" Width="20" ImageUrl="App_Themes/images/no.png"></asp:Label>
                                    <asp:Label runat="server" ID="ImgResultNA" Height="40" Width="40" ImageUrl="App_Themes/images/NA.png"></asp:Label>--%>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hQuestionID" Value='<%# Eval("QuestionID")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
