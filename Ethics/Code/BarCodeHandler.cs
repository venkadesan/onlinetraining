﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text.pdf;

namespace OnlineTraining.Code
{
    public class BarCodeHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(System.Web.HttpContext context)
        {
            HttpRequest Request = context.Request;
            HttpResponse response = context.Response;

            Barcode128 bc39 = new Barcode128();

            if (Request.QueryString["mybarcodeval"] != null && Request.QueryString["mybarcodeval"].ToString() != string.Empty)
            {
                bc39.Code = Request.QueryString["mybarcodeval"].ToString();

                System.Drawing.Image bc = bc39.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White);
                response.ContentType = "image/gif";

                bc.Save(response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
            }
        }
    }
}