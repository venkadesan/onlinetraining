﻿using System;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net.Repository;
using log4net;
using log4net.Appender;
using log4net.Config;
using OnlineTraining.Code;
using KCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.HtmlControls;

/// <summary>
/// Base class to all pages in the web site. Provides functionality that is 
/// shared among all pages. Functionality offered includes: page render timing, 
/// gridview sorting, shopping cart access, viewstate provider access, and 
/// Javascript to be sent to the browser.
/// </summary>
/// <remarks>
/// GoF Design Patterns: Template Method.
/// 
/// The Template Methods Design Pattern is implemented by the StartTime property and 
/// virtual (Overridable in VB) PageRenderTime property.  Each page that derives from this 
/// base class has the option to use the properties as is, or override it with its own 
/// implementation. This base class provides the template for the page render timing facility. 
/// </remarks>
public class PageBase : Page
{

    Kowni.BusinessLogic.BLCompanyMainForms objMainForms = new Kowni.BusinessLogic.BLCompanyMainForms();
    Kowni.BusinessLogic.BLCompanySubForms objSubForms = new Kowni.BusinessLogic.BLCompanySubForms();

    protected void Page_PreInit(object sender, EventArgs e)
    {

    }


    protected void Getmainandsubform(string subformurl, out string mainform, out string subform)
    {

        mainform = string.Empty;
        subform = string.Empty;
        DataSet dsMainMenus = new DataSet();

        dsMainMenus = objMainForms.GetCompanyMainForms(UserSession.UserCompanyID, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), 1);

        if (dsMainMenus.Tables.Count > 0)
        {
            foreach (DataRow drmainmenu in dsMainMenus.Tables[0].Rows)
            {
                DataSet dsSubMenus = objSubForms.GetCompanySubForms(UserSession.UserCompanyID, Convert.ToInt32(drmainmenu["MainFormsID"].ToString()), 1);
                DataRow[] drsubmenu = dsSubMenus.Tables[0].Select("SubFormsPageName='" + subformurl + "'");
                if (drsubmenu.Length > 0)
                {
                    mainform = drmainmenu["MainFormsPageText"].ToString();
                    subform = drsubmenu[0]["SubFormsPageText"].ToString();
                }
            }

            //if (dsMenus.Tables[0].Rows.Count > 0)
            //{
            //    DataRow[] drmenu = dsMenus.Tables[0].Select("SubFormsPageName='" + subformurl + "'");
            //    if (drmenu.Length > 0)
            //    {
            //        mainform = drmenu[0]["MainFormsPageText"].ToString();
            //        subform = drmenu[0]["SubFormsPageText"].ToString();
            //    }
            //}

        }
    }
}
