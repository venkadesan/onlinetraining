﻿
using OnlineTraining.Code;
using OnlineTrainingBL;
using OnlineTrainingPL;
using OnlineTrainingPL.Master;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace OnlineTraining
{
    public partial class Agree : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BindInstruction();
                BindTextInstruction();
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        private void BindInstruction()
        {
            try
            {
                PLTopic objTopicPL = new PLTopic();
                BLTopic objTopicBL = new BLTopic();
                objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
                objTopicPL.LocationID = UserSession.LocationIDCurrent;
                objTopicPL.TopicID = UserSession.topicid;
                objTopicPL.Groupid = UserSession.GroupIDCurrent;
                DataSet dataSet = new DataSet();
                DataSet detailsWithGroup = objTopicBL.GetActiveTopicDetailsWithGroup(objTopicPL);
                if (detailsWithGroup.Tables[0].Rows.Count <= 0)
                    return;
                this.lblinstruction.Text = detailsWithGroup.Tables[0].Rows[0]["testInstruction"].ToString();
            }
            catch (Exception ex)
            {
                this.NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        private void BindTextInstruction()
        {
            BLTextInstruction blTextInstruction = new BLTextInstruction();
            PLTextInstruction plTextInstruction = new PLTextInstruction();
            int num = 0;
            plTextInstruction.CompanyId = UserSession.CompanyIDUser;
            plTextInstruction.TextInstructionId = num;
            DataSet dataSet1 = new DataSet();
            DataSet textInstructions = blTextInstruction.GetTextInstructions(plTextInstruction);
            if (textInstructions.Tables[0].Rows.Count > 0)
            {
                plTextInstruction.UserId = UserSession.UserID;
                plTextInstruction.TopicId = UserSession.topicid;
                DataSet dataSet2 = new DataSet();
                DataSet noofAttempts = blTextInstruction.GetNoofAttempts(plTextInstruction);
                string str = textInstructions.Tables[0].Rows[0]["TextInstructions"].ToString();
                if (noofAttempts.Tables.Count > 0 && noofAttempts.Tables[0].Rows.Count > 0)
                {
                    string newValue = noofAttempts.Tables[0].Rows[0][0].ToString();
                    noofAttempts.Tables[0].Rows[0][0].ToString();
                    str = str.Replace("#Attempts#", newValue).Replace("#FirstName#", UserSession.UserFirstName);
                }
                this.lblinstruction.Text = str;
            }
            else
                this.lblinstruction.Text = string.Empty;
        }

        protected void bagree_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("OnlineTraining.aspx");
        }

        protected void bdecline_Click(object sender, EventArgs e)
        {
            this.Response.Redirect("Video.aspx");
        }
    }
}
