﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTrainingPL;
using OnlineTrainingBL.Master;
using OnlineTraining.Code;
using System.Data;
using OnlineTrainingBL;
using System.Web.UI.HtmlControls;
using OnlineTrainingPL.Master;
using OnlineTraining.Master;
using System.IO;

namespace OnlineTraining
{
    public partial class PassReport : PageBase
    {
        PLEmployee objEmployeePL = new PLEmployee();
        BLEmployee objEmployeeBL = new BLEmployee();
        PLTopic objTopicPL = new PLTopic();
        BLTopic objTopicBL = new BLTopic();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                string dFirstDayOfThisMonth = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToShortDateString();
                DateTime firstOfNextMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1);
                string lastOfThisMonth = DateTime.Today.ToShortDateString();
                this.txtFromDate.Text = lastOfThisMonth;
                this.txtToDate.Text = lastOfThisMonth;

                LoadDepartment();
                LoadDetails();
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void LoadDepartment()
        {
            try
            {
                BLDepartment objbldept = new BLDepartment();
                PLDepartment objpldept = new PLDepartment();

                objpldept.CompanyID = UserSession.CompanyIDCurrent;
                objpldept.GroupId = UserSession.GroupID;
                // objPLTopic.LocationID = UserSession.LocationIDCurrent;
                objpldept.DepartmentId = 0;
                DataSet Ds = new DataSet();
                Ds = objbldept.GetDepartmentDetails(objpldept);

                if (Ds.Tables[0].Rows.Count > 0)
                {
                    this.ddldepartment.DataSource = Ds;
                    this.ddldepartment.DataTextField = "DepartmentName";
                    this.ddldepartment.DataValueField = "DepartmentId";
                    this.ddldepartment.DataBind();

                    ddldepartment_SelectedIndexChanged(null, null);
                }
                else
                {
                    this.ddldepartment.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }


        private void LoadTopic()
        {
            BLTopic objBLTopic = new BLTopic();
            PLTopic objPLTopic = new PLTopic();

            int departmentid = 0;
            if (!Common.DDVal(ddldepartment, out departmentid))
            {
                NotifyMessages("Please select department", Common.ErrorType.Error);
                return;
            }

            objPLTopic.CompanyID = UserSession.CompanyIDCurrent;
            objPLTopic.Groupid = UserSession.GroupID;
            objPLTopic.DepartmentId = departmentid;
            objPLTopic.TopicID = 0;
            DataSet Ds = new DataSet();
            Ds = objBLTopic.getTopicByDepartment(objPLTopic);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.dTopic.DataSource = Ds;
                this.dTopic.DataTextField = "TopicName";
                this.dTopic.DataValueField = "TopicID";
                this.dTopic.DataBind();

                this.dTopic.Items.Insert(0,new ListItem("All","0"));
               
            }
            else
            {
                this.dTopic.Items.Clear();
            }
        }

        public void LoadDetails()
        {
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(txtFromDate.Text, out FromDate);
            DateTime.TryParse(txtToDate.Text, out ToDate);
            objEmployeePL.CompanyID = UserSession.CompanyIDCurrent;
            objEmployeePL.LocationID = UserSession.LocationIDCurrent;
            int TopicID = 0;
            Common.DDVal(dTopic, out TopicID);
            objEmployeePL.MainUserID = UserSession.UserIDs;
            objEmployeePL.FromDate = FromDate;
            objEmployeePL.ToDate = ToDate;
            objEmployeePL.TopicID = TopicID;
            objEmployeePL.UserID = UserSession.UserID;
            DataSet Ds = new DataSet();
            Ds = objEmployeeBL.GetPassReports(objEmployeePL);
            if (Ds!=null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
            {
                rptrReports.DataSource = Ds;
                rptrReports.DataBind();
            }
            else
            {
                rptrReports.DataSource = null;
                rptrReports.DataBind();
            }
        }

        private void EmptyGrid()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CDate");
            dt.Columns.Add("Topic");
            dt.Columns.Add("FirstName");
            dt.Columns.Add("LastName");
            dt.Columns.Add("EmailID");
            dt.Columns.Add("testpercentage");
            dt.Columns.Add("passpercentage");
            DataRow dr;
            dr = dt.NewRow();
            dr["CDate"] = "";
            dr["Topic"] = "";
            dr["FirstName"] = "";
            dr["LastName"] = "";
            dr["EmailID"] = "";
            dr["testpercentage"] = "";
            dr["passpercentage"] = 0;
            dt.Rows.Add(dr);
            rptrReports.DataSource = dt;
            rptrReports.DataBind();

            foreach (RepeaterItem rpt in rptrReports.Items)
            {
                Label lbl = (Label)rpt.FindControl("sno");
                //LinkButton lnkEdit = (LinkButton)rpt.FindControl("lnkEdit");
                //LinkButton lnkDelete = (LinkButton)rpt.FindControl("lnkDelete");
                lbl.Visible = false;
                //lnkEdit.Visible = false;
                //lnkDelete.Visible = false;
            }
        }

        protected void btnSubmitreg_Click(object sender, EventArgs e)
        {
            LoadDetails();
        }

        protected void rptrAnswer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void rptrAnswer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hQuestionID = (HiddenField)e.Item.FindControl("hQuestionID");

                HiddenField testpercentage = (HiddenField)e.Item.FindControl("htestpercentage");
                HiddenField passpercentage = (HiddenField)e.Item.FindControl("hpasspercentage");
                ImageButton imgResultYes = (ImageButton)e.Item.FindControl("imgResultYes");
                ImageButton imgResultNo = (ImageButton)e.Item.FindControl("imgResultNo");
                if ((testpercentage.Value != string.Empty && testpercentage.Value != null) && (passpercentage.Value != string.Empty && passpercentage.Value != null))
                {
                    if (Convert.ToInt32(testpercentage.Value) > Convert.ToInt32(passpercentage.Value))
                    {
                        imgResultYes.Visible = true;
                        imgResultNo.Visible = false;
                    }
                    else
                    {
                        imgResultYes.Visible = false;
                        imgResultNo.Visible = true;
                    }
                }
                else
                {
                    imgResultYes.Visible = false;
                    imgResultNo.Visible = false;
                }
            }
        }

        public void ExportToExcel(DataTable dt, string FileName)
        {
            if (dt.Rows.Count > 0)
            {
                string filename = FileName + ".xls";
                System.IO.StringWriter tw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();

                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.
                //Response.ContentType = application/vnd.ms-excel;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendHeader("Content-Disposition",
                                      "attachment; filename=" + filename + "");
                this.EnableViewState = false;
                Response.Write(tw.ToString());
                Response.End();
            }
        }

        protected void dTopic_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDetails();
        }

        protected void imgExcel_Click(object sender, ImageClickEventArgs e)
        {
            int topicID = 0;
           
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(txtFromDate.Text, out FromDate);
            DateTime.TryParse(txtToDate.Text, out ToDate);
            objEmployeePL.CompanyID = UserSession.CompanyIDCurrent1;
            objEmployeePL.LocationID = UserSession.LocationIDCurrent;
            objEmployeePL.TopicID = topicID;

            objEmployeePL.MainUserID = UserSession.UserIDs;
            objEmployeePL.FromDate = FromDate;
            objEmployeePL.ToDate = ToDate;
            DataSet Ds = new DataSet();
            Ds = objEmployeeBL.GetPassReports(objEmployeePL);
            DataTable dt = new DataTable();
            if (Ds.Tables[0].Rows.Count > 0)
            {
                dt.Columns.Add("SNo");
                dt.Columns.Add("TestDate");
                dt.Columns.Add("FirstName");
                dt.Columns.Add("LastName");
                dt.Columns.Add("EmailID");
                dt.Columns.Add("TotalScore%");
                dt.Columns.Add("Result");
                DataRow dr;
                for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                {
                    dr = dt.NewRow();
                    dr["Sno"] = i + 1;
                    dr["TestDate"] = Ds.Tables[0].Rows[i]["CDate"].ToString();
                    dr["FirstName"] = Ds.Tables[0].Rows[i]["FirstName"].ToString();
                    dr["LastName"] = Ds.Tables[0].Rows[i]["LastName"].ToString();
                    dr["EmailID"] = Ds.Tables[0].Rows[i]["EmailID"].ToString();
                    dr["TotalScore%"] = Ds.Tables[0].Rows[i]["TestPercentage"].ToString();
                    if (Convert.ToInt32(Ds.Tables[0].Rows[i]["testpercentage"].ToString()) > Convert.ToInt32(Ds.Tables[0].Rows[i]["passpercentage"].ToString()))
                        dr["Result"] = "Pass";
                    else
                        dr["Result"] = "Fail";
                    dt.Rows.Add(dr);
                }
            }
            ExportToExcel(dt, "Passreports");
        }

        protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadTopic();
            }
            catch (Exception)
            {
                throw;
            }
        }      
    }
}
