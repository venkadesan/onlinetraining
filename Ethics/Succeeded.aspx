﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
 CodeBehind="Succeeded.aspx.cs" Inherits="OnlineTraining.Succeeded"%>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div runat="server" visible="True" id="divSecondPart">
        <center>
            <img src="App_Themes/images/You-Did-It-Congratulations-Banner-Picture.png" width="100%" />
            <br />
            <h1>
                Please click the below button to show your score !
            </h1>
            <asp:HiddenField runat="server" ID="HiddenField1" />
            <asp:Button ID="Button4" runat="server" Text="Show Score" class="btn btn-primary"
                OnClick="Unnamed1_Click" />
        </center>
        
    </div>

</asp:Content>
