﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTrainingPL;
using OnlineTrainingBL;
using System.Web.UI.HtmlControls;
using OnlineTraining.Code;
using MB = Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Web.Script.Serialization;
using System.Text;
using OnlineTrainingPL.Master;
using System.IO;
using System.Collections;
using System.Data.SqlClient;

namespace OnlineTraining
{
    public partial class OnlineTraining : System.Web.UI.Page
    {
        PLAnswer objAnswerPL = new PLAnswer();
        BLAnswer objAnswerBL = new BLAnswer();
        PLQuestion objQuestionPL = new PLQuestion();
        BLQuestion objQuestionBL = new BLQuestion();
        PLTopic objTopicPL = new PLTopic();
        BLTopic objTopicBL = new BLTopic();
        PLTest objTestPL = new PLTest();
        BLTest objTestBL = new BLTest();
        static int PersentageResultCounts = 0;

        long totalsecond = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Int64 time = 0;
                Int64.TryParse(this.totalmin.Value, out time);
                if (time != 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "redirect();", true);
                }
            }
            catch { }

            if (!IsPostBack)
            {
                LoadDetails();
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            LoadDetails();
        }

        [System.Web.Services.WebMethod]
        public static string GetCurrentTime()
        {
            return "Hello ";
        }

        public void LoadDetails()
        {
            objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
            objTopicPL.LocationID = UserSession.LocationIDCurrent;
            objTopicPL.TopicID = UserSession.topicid;
            objTopicPL.Groupid = UserSession.GroupIDCurrent;
            DataSet Ds = new DataSet();
            Ds = objTopicBL.GetActiveTopicDetailsWithGroup(objTopicPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                hTopicID.Value = Ds.Tables[0].Rows[0]["TopicID"].ToString();
                hPassPercentage.Value = Ds.Tables[0].Rows[0]["PassPercentage"].ToString();
                lblTopicName.Text = Ds.Tables[0].Rows[0]["TopicName"].ToString();
                //lblResolutionTime.Text = Ds.Tables[0].Rows[0]["ResolutionTime"].ToString(); 
                hResolutionTime.Value = Ds.Tables[0].Rows[0]["ResolutionTime"].ToString();

                string[] split = Ds.Tables[0].Rows[0]["ResolutionTime"].ToString().Split(':');

                int hrsecod = Convert.ToInt32(split[0]) * 60;
                hrsecod = hrsecod * 60;

                int Minsecod = Convert.ToInt32(split[1]) * 60;

                long totalMinutes = hrsecod + Minsecod;

                totalsecond = totalMinutes;
                totalmin.Value = totalMinutes.ToString();

                DataSet Dts = new DataSet();
                objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
                objQuestionPL.LocationID = UserSession.LocationIDCurrent;
                objQuestionPL.TopicID = Convert.ToInt32(hTopicID.Value);
                Dts = objQuestionBL.GetQuestionDetails(objQuestionPL);

                if (Dts.Tables[0].Rows.Count > 0)
                {
                    rptrAnswer.DataSource = Dts;
                    rptrAnswer.DataBind();
                    Button2.Visible = true;
                    //Button3.Visible = true;

                    ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "redirect();", true);
                }
                else
                {
                    rptrAnswer.DataSource = null;
                    rptrAnswer.DataBind();
                    Button2.Visible = false;
                    //Button3.Visible = false;
                }


            }
            else
            {
                hTopicID.Value = string.Empty;
                hPassPercentage.Value = string.Empty;

                rptrAnswer.DataSource = null;
                rptrAnswer.DataBind();
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        protected void rptrAnswer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hQuestionID = (HiddenField)e.Item.FindControl("hQuestionID");
                Repeater rptranswer1 = (Repeater)e.Item.FindControl("rptrAnswer1");

                DataSet Ds = new DataSet();
                objAnswerPL.AnswerID = 0;

                objAnswerPL.CompanyID = UserSession.CompanyIDCurrent;
                objAnswerPL.LocationID = UserSession.LocationIDCurrent;
                objAnswerPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                objAnswerPL.AnswerID = 0;
                Ds = objAnswerBL.GetAnswerDetails(objAnswerPL);
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    rptranswer1.DataSource = Ds;
                    rptranswer1.DataBind();
                }
                else
                {
                    rptranswer1.DataSource = null;
                    rptranswer1.DataBind();
                }
                //lnkEdit.CommandArgument = hQuestionID.Value.ToString();
                //lnkDelete.CommandArgument = hQuestionID.Value.ToString();
            }
        }

        public bool ReturnValues()
        {
            return false;
        }

        public string Validation()
        {
            string[] ActualValue = new string[999];
            string[] TestValue = new string[999];
            string ErrorQuestion = string.Empty;

            StringBuilder sbXML = new StringBuilder();
            sbXML.Append("<root>");
            Session["PassPercentage"] = hPassPercentage.Value;
            DataSet Ds = new DataSet();
            objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
            objQuestionPL.LocationID = UserSession.LocationIDCurrent;
            objQuestionPL.TopicID = Convert.ToInt32(hTopicID.Value);
            Ds = objQuestionBL.GetQuestionDetails(objQuestionPL);

            DataTable dtSlQuestion = new DataTable();
            dtSlQuestion.Columns.Add("Slno", typeof(int));
            dtSlQuestion.Columns.Add("QuestionId", typeof(int));

            if (Ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                {
                    ActualValue[i] = Ds.Tables[0].Rows[i]["QuestionID"].ToString();
                }
            }
            for (int items = 0; items < rptrAnswer.Items.Count; items++)
            {
                Repeater rptrAnswer1 = (Repeater)rptrAnswer.Items[items].FindControl("rptrAnswer1");
                HiddenField hQuestionID = rptrAnswer.Items[items].FindControl("hQuestionID") as HiddenField;
                Label lblNo = rptrAnswer.Items[items].FindControl("lblNo") as Label;

                dtSlQuestion.Rows.Add(Convert.ToInt32(lblNo.Text), Convert.ToUInt32(hQuestionID.Value));
                for (int answer = 0; answer < rptrAnswer1.Items.Count; answer++)
                {
                    CheckBox chkAnswer = (CheckBox)rptrAnswer1.Items[answer].FindControl("chkAnswer");
                    HiddenField hAnswerID = rptrAnswer1.Items[answer].FindControl("hAnswerID") as HiddenField;
                    if (chkAnswer.Checked)
                    {
                        if (!TestValue.Contains(hQuestionID.Value.ToString()))
                            TestValue[items] = hQuestionID.Value.ToString();
                    }
                }
            }

            string[] result = ActualValue.Except(TestValue).ToArray();

            StringBuilder objbuilder = new StringBuilder();
            for (int j = 0; j < result.Length; j++)
            {
                DataRow[] drSlno = dtSlQuestion.Select("QuestionId=" + Convert.ToUInt32(result[j]));
                string slno = drSlno[0]["Slno"].ToString();
                objbuilder.Append("Q.No" + slno).Append(" , ");
            }

            ErrorQuestion = objbuilder.ToString();
            return ErrorQuestion;
        }

        protected void bAnswerKeySave_Click(object sender, EventArgs e)
        {

            string ErrorMessage = Validation();
            if (ErrorMessage != string.Empty)
            {
                NotifyMessages("Please answer the below Questions" + ErrorMessage, Common.ErrorType.Error);
                return;
            }
            else
            {
                string[] ReturnValue = new string[999];
                string[] WithoutAnswerList = new string[999];
                StringBuilder sbXML = new StringBuilder();
                sbXML.Append("<root>");

                Session["PassPercentage"] = hPassPercentage.Value;
                for (int items = 0; items < rptrAnswer.Items.Count; items++)
                {
                    Repeater rptrAnswer1 = (Repeater)rptrAnswer.Items[items].FindControl("rptrAnswer1");
                    HiddenField hQuestionID = rptrAnswer.Items[items].FindControl("hQuestionID") as HiddenField;
                    for (int answer = 0; answer < rptrAnswer1.Items.Count; answer++)
                    {
                        CheckBox chkAnswer = (CheckBox)rptrAnswer1.Items[answer].FindControl("chkAnswer");
                        HiddenField hAnswerID = rptrAnswer1.Items[answer].FindControl("hAnswerID") as HiddenField;
                        if (chkAnswer.Checked)
                        {
                            sbXML.Append(
                                   "<testspec questionid=\"" + Convert.ToInt32(hQuestionID.Value) + "\"" +
                                   " answerid=\"" + Convert.ToInt32(hAnswerID.Value) + "\" />");
                        }
                    }
                }
                objTestPL.CompanyID = UserSession.CompanyIDCurrent1;
                objTestPL.LocationID = UserSession.LocationIDCurrent;
                objTestPL.TestID = 0;
                objTestPL.TopicID = Convert.ToInt32(hTopicID.Value);
                objTestPL.CUID = Convert.ToInt32(Session["UserID"]);
                string[] Values = objTestBL.InsertUpdateTest(objTestPL);
                sbXML.Append("</root>");
                if (Convert.ToInt32(Values[1]) > 0)
                {
                    objTestPL.XMLis = sbXML.ToString();
                    objTestPL.TestID = Convert.ToInt32(Values[1].ToString());
                    string[] Valuess = objTestBL.InsertUpdateTestSpec(objTestPL);
                    ReturnValue = Percentage(objTestPL.TestID);
                    if (ReturnValue == null)
                    {
                        return;
                    }
                    try
                    {
                        Session["ReturnValue"] = ReturnValue[0].ToString();
                        string[] Value = Session["ReturnValue"].ToString().Split('.');
                        int RValue = 0;
                        if (Value.Length > 0)
                        {
                            RValue = Convert.ToInt32(Value[0].ToString());
                            if (RValue > 0)
                            {
                                if (RValue.ToString().Contains('.'))
                                {
                                    if (Value[1].ToString().Length > 2)
                                    {
                                        if (Convert.ToInt32(Value[1].ToString().Substring(0, 2)) > 50)
                                            RValue = RValue + 1;
                                    }
                                }
                            }
                        }

                        if (RValue > Convert.ToInt32(hPassPercentage.Value))
                        {
                            objTestPL.PassPercentage = Convert.ToInt32(hPassPercentage.Value);
                            objTestPL.Declaration = "";
                            objTestPL.CUID = UserSession.UserID;
                            objTestBL.InsertDeclaration(objTestPL);
                            //divFirstPart.Visible = false;
                            //divSecondPart.Visible = true;
                             UserSession.IsTestFinished = true;
                            Response.Redirect("Succeeded.aspx");
                        }
                        else
                        {
                            ClientScriptManager CSM = Page.ClientScript;
                            if (!ReturnValues())
                            {
                                UserSession.IsTestFinished = false;
                                Response.Redirect("Default.aspx");
                            }
                            else
                            {
                                UserSession.IsTestFinished = true;
                            }
                            Response.Redirect("Succeeded.aspx");
                        }
                    }
                    catch { }
                }
            }
        }

        protected void btnautosave_Click(object sender, EventArgs e)
        {
            objQuestionPL.TopicID = Convert.ToInt32(hTopicID.Value);

            string ErrorMessage = string.Empty;
            if (ErrorMessage != string.Empty)
            {
                NotifyMessages("Please answer the below Questions" + ErrorMessage, Common.ErrorType.Error);
                return;
            }
            else
            {
                string[] ReturnValue = new string[999];
                string[] WithoutAnswerList = new string[999];

                StringBuilder sbXML = new StringBuilder();
                sbXML.Append("<root>");

                Session["PassPercentage"] = hPassPercentage.Value;
                for (int items = 0; items < rptrAnswer.Items.Count; items++)
                {
                    Repeater rptrAnswer1 = (Repeater)rptrAnswer.Items[items].FindControl("rptrAnswer1");
                    HiddenField hQuestionID = rptrAnswer.Items[items].FindControl("hQuestionID") as HiddenField;
                    for (int answer = 0; answer < rptrAnswer1.Items.Count; answer++)
                    {
                        CheckBox chkAnswer = (CheckBox)rptrAnswer1.Items[answer].FindControl("chkAnswer");
                        HiddenField hAnswerID = rptrAnswer1.Items[answer].FindControl("hAnswerID") as HiddenField;
                        if (chkAnswer.Checked)
                        {
                            sbXML.Append(
                                   "<testspec questionid=\"" + Convert.ToInt32(hQuestionID.Value) + "\"" +
                                   " answerid=\"" + Convert.ToInt32(hAnswerID.Value) + "\" />");
                        }
                    }
                }
                objTestPL.CompanyID = UserSession.CompanyIDCurrent1;
                objTestPL.LocationID = UserSession.LocationIDCurrent;
                objTestPL.TestID = 0;
                objTestPL.TopicID = Convert.ToInt32(hTopicID.Value);
                objTestPL.CUID = Convert.ToInt32(Session["UserID"]);
                string[] Values = objTestBL.InsertUpdateTest(objTestPL);
                sbXML.Append("</root>");
                if (Convert.ToInt32(Values[1]) > 0)
                {
                    objTestPL.XMLis = sbXML.ToString();
                    objTestPL.TestID = Convert.ToInt32(Values[1].ToString());
                    string[] Valuess = objTestBL.InsertUpdateTestSpec(objTestPL);
                    ReturnValue = Percentage(objTestPL.TestID);
                    if (ReturnValue == null)
                    {
                        return;
                    }
                    try
                    {
                        Session["ReturnValue"] = ReturnValue[0].ToString();
                        string[] Value = Session["ReturnValue"].ToString().Split('.');
                        int RValue = 0;
                        if (Value.Length > 0)
                        {
                            RValue = Convert.ToInt32(Value[0].ToString());
                            if (RValue > 0)
                            {
                                if (RValue.ToString().Contains('.'))
                                {
                                    if (Value[1].ToString().Length > 2)
                                    {
                                        if (Convert.ToInt32(Value[1].ToString().Substring(0, 2)) > 50)
                                            RValue = RValue + 1;
                                    }
                                }
                            }
                        }

                        if (RValue > Convert.ToInt32(hPassPercentage.Value))
                        {
                            objTestPL.PassPercentage = Convert.ToInt32(hPassPercentage.Value);
                            objTestPL.Declaration = "";
                            objTestPL.CUID = UserSession.UserID;
                            objTestBL.InsertDeclaration(objTestPL);
                            //divFirstPart.Visible = false;
                            //divSecondPart.Visible = true;
                        }
                        else
                        {
                            ClientScriptManager CSM = Page.ClientScript;
                            if (!ReturnValues())
                            {
                                UserSession.IsTestFinished = false;
                                Response.Redirect("Default.aspx");
                            }
                            else
                            {
                                UserSession.IsTestFinished = true;
                            }
                            Response.Redirect("Succeeded.aspx");
                        }
                    }
                    catch { }
                }
            }
        }

        //protected void Unnamed1_Click(object sender, EventArgs e)
        //{
        //    UserSession.IsTestFinished = true;
        //    Response.Redirect("default.aspx", true);           
        //}       

        public string[] Percentage(int TestID)
        {
            PersentageResultCounts = 0;
            string[] StringValue = new string[100];

            objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
            objQuestionPL.LocationID = UserSession.LocationIDCurrent;
            objQuestionPL.QuestionID = 0;
            int TotalQuestionCount = 0;
            DataSet DsQuestion = new DataSet();
            DsQuestion = objQuestionBL.GetALLQuestionDetails(objQuestionPL);
            string questionids = string.Empty;
            DataSet Ds = new DataSet();
            for (int items = 0; items < rptrAnswer.Items.Count; items++)
            {
                Repeater rptrAnswer1 = (Repeater)rptrAnswer.Items[items].FindControl("rptrAnswer1");
                HiddenField hQuestionID = rptrAnswer.Items[items].FindControl("hQuestionID") as HiddenField;
                Label lblNo = rptrAnswer.Items[items].FindControl("lblNo") as Label;
                DataSet Ds2 = new DataSet();
                objAnswerPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                Ds2 = objAnswerBL.GetAnswerKeyDetails(objAnswerPL);
                if (Ds2.Tables[0].Rows.Count > 0)
                {
                    string[] answerCount = new string[100];
                    string[] testAnswer = new string[100];
                    if (Ds2.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < Ds2.Tables[0].Rows.Count; i++)
                        {
                            answerCount[i] = Ds2.Tables[0].Rows[i]["AnswerID"].ToString();
                        }
                    }
                    DataSet Ds1 = new DataSet();
                    objTestPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                    objTestPL.CompanyID = UserSession.CompanyIDCurrent1;
                    objTestPL.LocationID = UserSession.LocationIDCurrent;
                    objTestPL.CUID = UserSession.UserID;
                    Ds1 = objTestBL.CheckResults(objTestPL);
                    if (Ds1.Tables[0].Rows.Count > 0)
                    {
                        for (int j = 0; j < Ds1.Tables[0].Rows.Count; j++)
                        {
                            testAnswer[j] = Ds1.Tables[0].Rows[j]["AnswerID"].ToString();
                        }
                    }

                    bool areEqual = answerCount.SequenceEqual(testAnswer);

                    if (areEqual)
                    {
                        PersentageResultCounts++;
                    }
                    try
                    {
                        //objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
                        //objQuestionPL.LocationID = UserSession.LocationIDCurrent;
                        //objQuestionPL.QuestionID = 0;
                        //int TotalQuestionCount = 0;
                        //DataSet DsQuestion = new DataSet();
                        //DsQuestion = objQuestionBL.GetALLQuestionDetails(objQuestionPL);
                        if (DsQuestion.Tables[0].Rows.Count > 0)
                            TotalQuestionCount = DsQuestion.Tables[0].Rows.Count;
                        else
                            TotalQuestionCount = DsQuestion.Tables[0].Rows.Count;
                        try
                        {
                            double Persentage = (Convert.ToDouble(PersentageResultCounts) / TotalQuestionCount) * 100;
                            StringValue[0] = Persentage.ToString();
                        }
                        catch
                        {
                            StringValue[0] = "0";
                        }
                    }
                    catch { }
                }
                else
                {
                    if (lblNo != null)
                    {
                        questionids += lblNo.Text + ",";
                    }
                }
            }

            questionids = questionids.TrimEnd(',');
            if (questionids.Trim() != string.Empty)
            {
                NotifyMessages("Q.No : " + questionids + " - No answers found.", Common.ErrorType.Error);
                return null;
            }
            objTestPL.CUID = UserSession.UserID;
            string[] Values = StringValue[0].Split('.');
            try
            {
                objTestPL.PassPercentage = Convert.ToInt32(Values[0].ToString());
                objTestPL.TestID = TestID;
                objTestBL.UpdatePercentage(objTestPL);
            }
            catch { }
            return StringValue;
        }

        //protected void CancelAnswerKey_Click(object sender, EventArgs e)
        //{
        //    LoadDetails();
        //}
    }
}