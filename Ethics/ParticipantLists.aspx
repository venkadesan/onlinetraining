﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="ParticipantLists.aspx.cs" Inherits="Ethics.ParticipantLists" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">

        function fnExcelReport(tablename) {
            var tab_text = "<table border='2px'><tr>";
            var textRange; var j = 0;
            tab = document.getElementById(tablename); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Cafe.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
    <div class="submenu">
        <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Report"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Participant's Report"></asp:Literal>
        </h1>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <!-- col 6 -->
                    <div class="col-md-6">
                        <!-- tile body -->
                        <div class="tile-body">
                            <div id="basicvalidations" parsley-validate="" role="form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="Department" runat="server" ID="ldropdown" />:</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddldepartment" runat="server" CssClass="form-control" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddldepartment_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /tile body -->
                    </div>
                    <!--col-6-->
                    <div class="col-md-6">
                        <!-- tile body -->
                        <div class="tile-body">
                            <div id="Div1" runat="server" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="Select Topic" runat="server" ID="Label4" />:</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList runat="server" class="chosen-select form-control" ID="dTopic" AutoPostBack="True"
                                            OnSelectedIndexChanged="dTopic_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <!-- /tile body -->
                        </div>
                        <!-- col 6 -->
                    </div>
                </div>
            </div>
            <br />
        </div>
        <div class="table-responsive">
            <div style="float: right;">
                <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="fnExcelReport('historytable')"
                    ToolTip="To Download XL Document">
                                <i class="fa fa-2x fa-download"></i></asp:LinkButton>
            </div>
            <asp:Repeater ID="rptrReports" runat="server">
                <HeaderTemplate>
                    <table cellpadding="0" class="table table-bordered" id="historytable" cellspacing="0"
                        border="0" width="100%">
                        <tr class="new-color">
                            <th width="5%">
                                <asp:Label Text="S.No" runat="server" ID="lSNo" />
                            </th>
                            <th>
                                <asp:Label Text="Participant Name" runat="server" ID="Label1" />
                            </th>
                            <th>
                                <asp:Label Text="Email" runat="server" ID="lReasonName" />
                            </th>
                            <th>
                                Start Date
                            </th>
                            <th>
                                End Date
                            </th>
                            <th>
                                <asp:Label Text="No.of Attempt" runat="server" ID="Label2" />
                            </th>
                            <th>
                                <asp:Label Text="No of Pass" runat="server" ID="Label5" />
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="sno" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("Participantname") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("username") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label7" runat="server" Text='<%# Eval("StartDate") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("EndDate") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label9" runat="server" Text='<%# Eval("Total") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label10" runat="server" Text='<%# Eval("Pass") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
