﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="PassReport.aspx.cs" Inherits="OnlineTraining.PassReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="Stylesheet" />
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(

        /* This is the function that will get executed after the DOM is fully loaded */
  function () {
      $(".dt").datepicker({
          changeMonth: true, //this option for allowing user to select month
          changeYear: true //this option for allowing user to select from year range
      });
  }
);
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="submenu">
        <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Report"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Pass Report"></asp:Literal>
        </h1>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <!-- col 6 -->
                    <div class="col-md-6">
                        <!-- tile body -->
                        <div class="tile-body">
                            <div id="basicvalidation" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="From Date" runat="server" ID="lFromDate" />:</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtFromDate" runat="server" TabIndex="1" class="chosen-select form-control dt"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="Department" runat="server" ID="ldropdown" />:</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddldepartment" runat="server" CssClass="form-control" 
                                            AutoPostBack="True" onselectedindexchanged="ddldepartment_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--col-6-->
                    <div class="col-md-6">
                        <!-- tile body -->
                        <div class="tile-body">
                            <div id="basicvalidations" runat="server" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="To Date" runat="server" ID="lToDate" />:</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtToDate" runat="server" TabIndex="2" class="chosen-select form-control dt"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="Select Topic" runat="server" ID="Label3" />:</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList runat="server" class="chosen-select form-control" ID="dTopic" OnSelectedIndexChanged="dTopic_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <!-- /tile body -->
                        </div>
                        <!-- col 6 -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="Form1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                            <div class="form-group form-footer">
                                <div class="col-sm-offset-5 col-sm-6">
                                    <asp:Button ID="bSave" Text="Search" runat="server" class="btn btn-primary" OnClick="btnSubmitreg_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <div style="float: right;">
                <asp:ImageButton runat="server" ID="imgExcel" ClientIDMode="Static" ImageUrl="~/App_Themes/images/Excel.png"
                    Width="40" Height="40" ToolTip="Click to download report details" OnClick="imgExcel_Click" />
            </div>
            <asp:Repeater ID="rptrReports" runat="server" OnItemCommand="rptrAnswer_ItemCommand"
                DataSourceID="" OnItemDataBound="rptrAnswer_ItemDataBound">
                <HeaderTemplate>
                    <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                        <tr  class="new-color">
                            <th width="5%">
                                <asp:Label Text="S.No" runat="server" ID="lSNo" />
                            </th>
                            <th>
                                <asp:Label Text="Test Date" runat="server" ID="lReasonName" />
                            </th>
                             <th>
                                <asp:Label Text="Topic" runat="server" ID="Label4" />
                            </th>
                            <th>
                                <asp:Label Text="First Name" runat="server" ID="Label5" />
                            </th>
                            <th>
                                <asp:Label Text="Last Name" runat="server" ID="Label9" />
                            </th>
                            <th>
                                <asp:Label Text="EmailID" runat="server" ID="Label11" />
                            </th>
                            <th>
                                <asp:Label Text="Total Score %" runat="server" ID="Label1" />
                            </th>
                            <th>
                                <asp:Label Text="Result" runat="server" ID="Label17" />
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="sno" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                        </td>
                        <td>
                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("CDate") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("TopicName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("FirstName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label10" runat="server" Text='<%# Eval("LastName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label12" runat="server" Text='<%# Eval("EmailID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("testpercentage") %>' />
                        </td>
                        <td>
                            <%--<asp:Label ID="lblResult" runat="server" Text="Fail" />--%>
                            <asp:ImageButton runat="server" ID="imgResultNo" Height="20" Width="20" ImageUrl="App_Themes/images/no.png" />
                            <asp:ImageButton runat="server" ID="imgResultYes" Height="20" Width="20" ImageUrl="App_Themes/images/yes.png" />
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="hTestPercentage" Value='<%# Eval("testpercentage") %>' />
                    <asp:HiddenField runat="server" ID="hPassPercentage" Value='<%# Eval("passpercentage") %>' />
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
