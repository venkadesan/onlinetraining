﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTrainingPL;
using OnlineTrainingBL.Master;
using OnlineTraining.Code;
using System.Data;
using OnlineTrainingBL;
using OnlineTraining.Master;

namespace Ethics
{
    public partial class ParticipantLists : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                LoadDepartment();
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void LoadDepartment()
        {
            try
            {
                BLDepartment objbldept = new BLDepartment();
                PLDepartment objpldept = new PLDepartment();

                objpldept.CompanyID = UserSession.CompanyIDCurrent;
                objpldept.GroupId = UserSession.GroupID;
                // objPLTopic.LocationID = UserSession.LocationIDCurrent;
                objpldept.DepartmentId = 0;
                DataSet Ds = new DataSet();
                Ds = objbldept.GetDepartmentDetails(objpldept);

                if (Ds.Tables[0].Rows.Count > 0)
                {
                    this.ddldepartment.DataSource = Ds;
                    this.ddldepartment.DataTextField = "DepartmentName";
                    this.ddldepartment.DataValueField = "DepartmentId";
                    this.ddldepartment.DataBind();

                    ddldepartment_SelectedIndexChanged(null, null);
                }
                else
                {
                    this.ddldepartment.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadTopic();
            }
            catch
            {

            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        private void LoadTopic()
        {
            BLTopic objBLTopic = new BLTopic();
            PLTopic objPLTopic = new PLTopic();

            int departmentid = 0;
            if (!Common.DDVal(ddldepartment, out departmentid))
            {
                NotifyMessages("Please select department", Common.ErrorType.Error);
                return;
            }

            objPLTopic.CompanyID = UserSession.CompanyIDCurrent;
            objPLTopic.Groupid = UserSession.GroupID;
            objPLTopic.DepartmentId = departmentid;
            objPLTopic.TopicID = 0;
            DataSet Ds = new DataSet();
            Ds = objBLTopic.getTopicByDepartment(objPLTopic);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.dTopic.DataSource = Ds;
                this.dTopic.DataTextField = "TopicName";
                this.dTopic.DataValueField = "TopicID";
                this.dTopic.DataBind();

                dTopic_SelectedIndexChanged(null, null);
            }
            else
            {
                this.dTopic.Items.Clear();
            }
        }

        protected void dTopic_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDetails();
        }

        public void LoadDetails()
        {
            int tpoicID = 0;
            if (!Common.DDVal(this.dTopic, out tpoicID))
            {
                NotifyMessages("Select Topic", Common.ErrorType.Error);
                return;
            }
            PLEmployee objEmployeePL = new PLEmployee();
            BLEmployee objEmployeeBL = new BLEmployee();
            objEmployeePL.CompanyID = UserSession.CompanyIDCurrent;
            objEmployeePL.LocationID = UserSession.LocationIDCurrent;
            objEmployeePL.TopicID = tpoicID;

            DataSet Ds = new DataSet();
            Ds = objEmployeeBL.GetParticipantsList(objEmployeePL);
            if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
            {
                rptrReports.DataSource = Ds;
                rptrReports.DataBind();
            }
            else
            {
                rptrReports.DataSource = null;
                rptrReports.DataBind();
            }
        }
    }
}