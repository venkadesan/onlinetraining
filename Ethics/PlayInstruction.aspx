﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
 CodeBehind="PlayInstruction.aspx.cs" Inherits="OnlineTraining.PlayInstruction" %>

   <%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     <script type="text/javascript" src="App_Themes/jquery/jquery-1.6.2.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
      <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label></div>
    <div class="main">
                                                                        <div class="row">
                                    <div class="col-md-12">
                                <div class="tile-body">
                                    <div id="TextEditor" runat="server" class="form-horizontal">
                                        <div id="Editor" class="form-group" runat="server">
                                            <label class="col-sm-2 control-label" for="">
                                                <asp:Label Text="Play Instructions" runat="server" ID="Label2" />:</label>
                                            <div class="col-sm-10">
                                                <CKEditor:CKEditorControl ID="textinstruction" BasePath="~/ckeditor/" RemovePlugins="elementspath"
                                                    runat="server" ToolbarFull="Bold|Italic|Underline|Strike|-|Subscript|Superscript NumberedList|BulletedList|-|Outdent|Indent|Blockquote|CreateDiv
JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock
BidiLtr|BidiRtl
Link|Unlink|Anchor
Image|Flash|Table|HorizontalRule|Smiley|SpecialChar|PageBreak|Iframe
/
Styles|Format|Font|FontSize
TextColor|BGColor
Maximize|ShowBlocks|" Height="250"></CKEditor:CKEditorControl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </div>
                                
                        <div class="row">
                                    <div class="col-md-12">
                                        <div id="Form1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                                            <div class="form-group form-footer">
                                                <div class="col-sm-offset-5 col-sm-6">
                                                   <asp:Button ID="bSave" Text="Save" runat="server" class="btn btn-primary" OnClick="btnSave_Click" />
                                                     <asp:Button ID="Button1" Text="Cancel" runat="server" class="btn btn-default" OnClick="btnCancel_Click" />
                                                         
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



        </div>
    
</asp:Content>
