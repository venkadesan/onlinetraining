﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTraining.Code;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Text;
using System.Configuration;

namespace OnlineTraining
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        KB.BLRoles.BLRoleCompany objRoleComp = new KB.BLRoles.BLRoleCompany();
        KB.BLRoles.BLRoleLocation objRoleLoc = new KB.BLRoles.BLRoleLocation();
        KB.BLCompany objCompany = new KB.BLCompany();
        KB.BLLocation objLocation = new KB.BLLocation();

        KB.BLUser.BLUserLocation objuserLocation1 = new KB.BLUser.BLUserLocation();
        KB.BLUser.BLUserCompany objuserCompany = new KB.BLUser.BLUserCompany();

        KB.BLCompanyMainForms objMainForms = new KB.BLCompanyMainForms();
        KB.BLCompanySubForms objSubForms = new KB.BLCompanySubForms();
        KB.BLRoleForms roleform = new KB.BLRoleForms();

        protected void Page_Load(object sender, EventArgs e)
        {
            UserSession.CompanyIDCurrent = 1;
            this.lblUSerName.Text = UserSession.UserFirstName + " (" + UserSession.RoleName + ")";
            if (!IsPostBack)
            {
                if (UserSession.UserID == 0)
                {
                    Response.Redirect("Login.aspx");
                }

                LoadMainForms();
                //lblCompanyName.Text = UserSession.UserFirstName;

                KB.BLGroup objusergroup = new KB.BLGroup();
                DataSet dsgroup = objusergroup.GetGroup(UserSession.GroupID);

                if (dsgroup != null && dsgroup.Tables.Count > 0 && dsgroup.Tables[0].Rows.Count > 0)
                {
                    //lblCompanyName.Text = dsgroup.Tables[0].Rows[0]["GroupName"].ToString();
                }

                //cmpylogo.Src = "http://ifazility.com/static/logo-company-snowaski.com/1205.png";
                cmpylogo.Src = "http://ifazility.com/static/logo-company-snowaski.com/" + UserSession.CompanyIDUser + ".png";

            }
        }

        public void LoadMainForms()
        {
            try
            {
                if (UserSession.UserID == 0)
                {
                    Response.Redirect("Login.aspx");
                }

                DataSet dsRoel = roleform.GetRoleFormShow(UserSession.RoleID, UserSession.UserCompanyID, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()));

                if (dsRoel.Tables[0].Rows.Count > 0)
                {

                    dsRoel.Tables[0].DefaultView.RowFilter = "isvisible=True";
                    DataTable mainID = dsRoel.Tables[0].DefaultView.ToTable(true, "mainformsID");

                    dsRoel.Tables[0].DefaultView.RowFilter = "isvisible=True";
                    DataTable subID = dsRoel.Tables[0].DefaultView.ToTable(true, "subformsID");

                    UserSession.CompanyIDCurrent = 1;
                    DataSet Ds = new DataSet();
                    DataSet Ds1 = new DataSet();
                    string Path = string.Empty;
                    int MainFormD = 0;
                    try
                    {
                        Path = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
                        System.IO.FileInfo Info = new System.IO.FileInfo(Path);
                        Path = Info.Name;
                    }
                    catch { }

                    Ds = objMainForms.GetCompanyMainForms(UserSession.UserCompanyID, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), 1);
                    //Ds = dsRoel.Tables[0].Select()

                    string[] selectedValues = { "ifazility" };
                    string[] FilterMenu = { "" };
                    bool isAdmin = selectedValues.Contains(UserSession.UserLastName.ToString().ToLower());
                    if (!isAdmin)
                    {
                        for (int i = 0; i < FilterMenu.Length; i++)
                        {
                            foreach (DataRow row in Ds.Tables[0].Rows)
                            {
                                if (row["MainFormsPageName"].ToString().ToLower() == FilterMenu[i].ToString())
                                    row.Delete();
                            }
                            Ds.Tables[0].AcceptChanges();
                        }
                    }
                    for (int k = 0; k < Ds.Tables[0].Rows.Count; k++)
                    {
                        string MainPageName = string.Empty;
                        string[] MainPageIDValue = new string[3];
                        try
                        {
                            MainPageName = Ds.Tables[0].Rows[k]["MainFormsPageName"].ToString();
                            MainPageIDValue = MainPageName.Split('.');
                        }
                        catch { }
                        DataRow[] dr = new DataRow[10];
                        Ds1 = objSubForms.GetCompanySubForms(UserSession.UserCompanyID, Convert.ToInt32(Ds.Tables[0].Rows[k]["MainFormsID"].ToString()), 1);
                        if (Ds1.Tables[0].Rows.Count > 0)
                        {
                            dr = Ds1.Tables[0].Select("SubFormsPageName='" + Path + "'");

                        }
                        int result1 = dr.Count(s => s != null);
                        if (result1 > 0)
                        {
                            MainFormD = Convert.ToInt32(Ds.Tables[0].Rows[k]["MainFormsID"].ToString());
                            break;
                        }
                    }
                    Ds = new DataSet();
                    Ds = objMainForms.GetCompanyMainForms(UserSession.UserCompanyID, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), 1);

                    string main = "";
                    foreach (DataRow mainformID in mainID.Rows)
                    {
                        main = main + mainformID["mainformsID"].ToString() + ",";
                    }
                    main.TrimEnd(',');

                    Ds.Tables[0].DefaultView.RowFilter = "MainFormsID in (" + main + ")";

                    DataTable dsTable = Ds.Tables[0].DefaultView.ToTable();

                    if (!isAdmin)
                    {
                        for (int i = 0; i < FilterMenu.Length; i++)
                        {
                            foreach (DataRow row in Ds.Tables[0].Rows)
                            {
                                if (row["MainFormsPageName"].ToString().ToLower() == FilterMenu[i].ToString())
                                    row.Delete();
                            }
                            Ds.Tables[0].AcceptChanges();
                        }
                    }
                    StringBuilder objBuilder = new StringBuilder();
                    for (int i = 0; i < dsTable.Rows.Count; i++)
                    {
                        string MainPageID = string.Empty;
                        string[] MainPageIDValue = new string[3];
                        try
                        {
                            MainPageID = dsTable.Rows[i]["MainFormsPageName"].ToString();
                            MainPageIDValue = MainPageID.Split('.');
                        }
                        catch { }

                        if (Convert.ToInt32(dsTable.Rows[i]["MainFormsID"]) == MainFormD)
                        {
                            objBuilder.Append("<li class=\"dropdown open\" id=\"p" + MainPageIDValue[0].ToString() + "\" runat=\"server\"><a href=\"" + dsTable.Rows[i]["MainFormsPageName"].ToString() + "\" class=\"dropdown-toggle\"");
                        }
                        else
                        {
                            objBuilder.Append("<li class=\"dropdown\" id=\"p" + MainPageIDValue[0].ToString() + "\" runat=\"server\"><a href=\"" + dsTable.Rows[i]["MainFormsPageName"].ToString() + "\" class=\"dropdown-toggle\"");
                        }
                        objBuilder.Append("data-toggle=\"dropdown\" title=\"" + dsTable.Rows[i]["MainFormsPageText"].ToString() + "\"><i class=\"" + dsTable.Rows[i]["CssClass"].ToString() + "\"><span class=\"overlay-label red\">");
                        objBuilder.Append("</span></i>" + dsTable.Rows[i]["MainFormsPageText"].ToString() + "<b class=\"fa fa-angle-left dropdown-arrow\"></b> </a>");
                        objBuilder.Append("<ul class=\"dropdown-menu\">");
                        Ds1 = new DataSet();
                        Ds1 = objSubForms.GetCompanySubForms(UserSession.UserCompanyID, Convert.ToInt32(dsTable.Rows[i]["MainFormsID"].ToString()), 1);

                        string sub = "";
                        foreach (DataRow mainformID in subID.Rows)
                        {
                            sub = sub + mainformID["SubFormsID"].ToString() + ",";
                        }
                        sub.TrimEnd(',');

                        Ds1.Tables[0].DefaultView.RowFilter = "SubFormsID in (" + sub + ")";
                        DataTable ds1Table = Ds1.Tables[0].DefaultView.ToTable();

                        //foreach (DataRow row in Ds1.Tables[0].Rows)
                        //{
                        //for (int count = 0; count < Ds1.Tables[0].Rows.Count; count++)
                        //    foreach (DataRow sub in subID.Rows)
                        //    {
                        //        if (Convert.ToInt32(Ds1.Tables[0].Rows[count]["SubFormsID"].ToString()) != Convert.ToInt32(sub["SubFormsID"].ToString()))
                        //        {
                        //            Ds1.Tables[0].Rows[count].Delete();
                        //            Ds1.Tables[0].AcceptChanges();
                        //            break;
                        //        }
                        //    }
                        //}

                        if (ds1Table.Rows.Count > 0)
                        {
                            for (int j = 0; j < ds1Table.Rows.Count; j++)
                            {
                                string SubPageID = string.Empty;
                                string[] SubPageIDValue = new string[3];
                                try
                                {
                                    SubPageID = dsTable.Rows[i]["MainFormsPageName"].ToString();
                                    SubPageIDValue = SubPageID.Split('.');
                                }
                                catch { }
                                if (Path == ds1Table.Rows[j]["SubFormsPageName"].ToString())
                                    objBuilder.Append("<li runat=\"server\" id=\"p" + SubPageIDValue[0].ToString() + "\" class=\"active\"><a id=\"" + ds1Table.Rows[j]["SubFormsID"].ToString() + "\" title=\"" + ds1Table.Rows[j]["SubFormsPageText"].ToString() + "\" href=\"" + ds1Table.Rows[j]["SubFormsPageName"].ToString() + "\" runat=\"server\">");
                                else
                                    objBuilder.Append("<li runat=\"server\" id=\"p" + SubPageIDValue[0].ToString() + "\"><a id=\"" + ds1Table.Rows[j]["SubFormsID"].ToString() + "\" title=\"" + ds1Table.Rows[j]["SubFormsPageText"].ToString() + "\" href=\"" + ds1Table.Rows[j]["SubFormsPageName"].ToString() + "\" runat=\"server\">");
                                objBuilder.Append("<i class=\"" + ds1Table.Rows[j]["CssClass"].ToString() + "\"><span class=\"overlay-label red80\"></span></i>" + ds1Table.Rows[j]["SubFormsPageText"].ToString() + "</a></li>");
                            }
                        }

                        objBuilder.Append("</ul>");
                        objBuilder.Append("</li>");
                    }
                    ltrlMenu.Text = objBuilder.ToString();
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void lnklogout_Click(object sender, EventArgs e)
        {
            if (!UserSession.FromFramework)
            {
                Session.Abandon();
                Session.Clear();
                bool isclear = UserSession.ClearSession;
                Response.Redirect("Login.aspx");
            }
            else
            {
                bool isclear = UserSession.ClearSession;
                Response.Redirect("http://" + Request.Url.Authority.ToLower() + "/", true);
            }
        }

    }

}