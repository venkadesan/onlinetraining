﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true"
    CodeBehind="Welcome.aspx.cs" Inherits="OnlineTraining.Welcome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="App_Themes/welcome/messi.css" rel="stylesheet" type="text/css">
    <link href="App_Themes/welcome/messi.min.css" rel="stylesheet" type="text/css">
    <link href="App_Themes/welcome/login_stilo.css" rel="stylesheet" type="text/css">
    <link href="App_Themes/welcome/signup-style.css" rel="stylesheet" type="text/css">
    <link href="App_Themes/welcome/responsiveslides.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">

        function Validation() {
            var txtfirstname = document.getElementById('<%=txtfirstname.ClientID %>').value;
            var txtlastName = document.getElementById('<%=txtlastName.ClientID %>').value;
            var txtemailid = document.getElementById('<%=txtemailid.ClientID %>').value;
            var txtEmployeeCode = document.getElementById('<%=txtEmployeeCode.ClientID %>').value;
            var txtDesignation = document.getElementById('<%=txtDesignation.ClientID %>').value;
            var txtprojectName = document.getElementById('<%=txtprojectName.ClientID %>').value;
            if (txtfirstname == "") {
                alert("Please enter the First Name");
                return false;
            }
            else if (txtlastName == "") {
                alert("Please enter the Last Name");
                return false;
            }
            else if (txtEmployeeCode == "") {
                alert("Please enter the Employee Code ");
                return false;
            }
            else if (txtDesignation == "") {
                alert("Please enter the Designation");
                return false;
            }
            else if (txtemailid == "") {
                alert("Please enter the EmailID");
                return false;
            }
            else if (txtprojectName == "") {
                alert("Please enter the Project Name / Department Name.");
                return false;
            }
        }
        function validateEmail(emailField) {
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

            if (emailField.value == "") {
                alert('Please enter emailID');
                return false;
            }
            if (reg.test(emailField.value) == false) {
                alert('Invalid Email Address');
                return false;
            }

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container_new">
        <div style="float: left;">
            <%--<image runat="server" Width="890" Height="670" ID="imag" ImageUrl="~/App_Themes/images/businessethicspage.jpg"/>--%>
            <img height="670" width="890" src="App_Themes/images/businessethicspage.jpg" />
        </div>
        <div class="log_right">
            <div class="login_box">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <div class="form_hd">
                                First Name*</div>
                            <div class="form">
                                <asp:TextBox runat="server" ID="txtfirstname" class="txtfield"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form_hd">
                                Last Name*</div>
                            <div class="form">
                                <asp:TextBox runat="server" ID="txtlastName" class="txtfield"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form_hd">
                                Employee Code *</div>
                            <div class="form">
                                <asp:TextBox runat="server" ID="txtEmployeeCode" class="txtfield"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form_hd">
                                Designation*</div>
                            <div class="form">
                                <asp:TextBox runat="server" ID="txtDesignation" class="txtfield"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form_hd">
                                Email Id *</div>
                            <div class="form">
                                <asp:TextBox runat="server" ID="txtemailid" onblur="validateEmail(this)" class="txtfield"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form_hd">
                                Client*</div>
                            <div class="form">
                                <asp:DropDownList runat="server" ID="ddlcompany" class="txtfield" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form_hd">
                                Facility*</div>
                            <div class="form">
                                <asp:DropDownList runat="server" ID="ddlLocation" class="txtfield">
                                </asp:DropDownList>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form_hd">
                                Project Name / Department Name*</div>
                            <div class="form">
                                <asp:TextBox runat="server" ID="txtprojectName" class="txtfield"></asp:TextBox></div>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button runat="server" ID="btnSubmitreg" Text="Go to Ethics" class="btn_submit"
                                OnClick="btnSubmitreg_Click" OnClientClick="return Validation()" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
