﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.HSSF.Record.Formula.Functions;
using OnlineTrainingPL;
using OnlineTrainingBL.Master;
using OnlineTraining.Code;
using System.Data;
using OnlineTrainingBL;
using OnlineTraining.Master;

namespace OnlineTraining
{
    public partial class SummaryReports : PageBase
    {
        PLEmployee objEmployeePL = new PLEmployee();
        BLEmployee objEmployeeBL = new BLEmployee();
        static int Total = 0;
        static int Pass = 0;
        static int Fail = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string dFirstDayOfThisMonth = DateTime.Today.AddDays(-(DateTime.Today.Day - 1)).ToShortDateString();
                DateTime firstOfNextMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1);
                string lastOfThisMonth = DateTime.Today.ToShortDateString();
                this.txtFromDate.Text = lastOfThisMonth;
                this.txtToDate.Text = lastOfThisMonth;

                Setheadertextdetails();
                LoadDepartment();
                //LoadTopic();
                LoadDetails();
                ParticipateExcelImage.Visible = false;
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void LoadDepartment()
        {
            try
            {
                BLDepartment objbldept = new BLDepartment();
                PLDepartment objpldept = new PLDepartment();

                objpldept.CompanyID = UserSession.CompanyIDCurrent;
                objpldept.GroupId = UserSession.GroupID;
                // objPLTopic.LocationID = UserSession.LocationIDCurrent;
                objpldept.DepartmentId = 0;
                DataSet Ds = new DataSet();
                Ds = objbldept.GetDepartmentDetails(objpldept);

                if (Ds.Tables[0].Rows.Count > 0)
                {
                    this.ddldepartment.DataSource = Ds;
                    this.ddldepartment.DataTextField = "DepartmentName";
                    this.ddldepartment.DataValueField = "DepartmentId";
                    this.ddldepartment.DataBind();

                    ddldepartment_SelectedIndexChanged(null, null);
                }
                else
                {
                    this.ddldepartment.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        private void LoadTopic()
        {
            BLTopic objBLTopic = new BLTopic();
            PLTopic objPLTopic = new PLTopic();

            int departmentid = 0;
            if (!Common.DDVal(ddldepartment, out departmentid))
            {
                NotifyMessages("Please select department", Common.ErrorType.Error);
                return;
            }

            objPLTopic.CompanyID = UserSession.CompanyIDCurrent;
            objPLTopic.Groupid = UserSession.GroupID;
            objPLTopic.DepartmentId = departmentid;
            objPLTopic.TopicID = 0;
            DataSet Ds = new DataSet();
            Ds = objBLTopic.getTopicByDepartment(objPLTopic);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.dTopic.DataSource = Ds;
                this.dTopic.DataTextField = "TopicName";
                this.dTopic.DataValueField = "TopicID";
                this.dTopic.DataBind();
            }
            else
            {
                this.dTopic.Items.Clear();
            }
        }

        public void LoadDetails()
        {
            try
            {
                int tpoicID = 0;

                Common.DDVal(this.dTopic, out tpoicID);
                DateTime FromDate = new DateTime();
                DateTime ToDate = new DateTime();
                DateTime.TryParse(txtFromDate.Text, out FromDate);
                DateTime.TryParse(txtToDate.Text, out ToDate);
                objEmployeePL.CompanyID = UserSession.CompanyIDCurrent;
                objEmployeePL.LocationID = UserSession.LocationIDCurrent;
                objEmployeePL.TopicID = tpoicID;
                objEmployeePL.MainUserID = UserSession.UserIDs;
                objEmployeePL.FromDate = FromDate;
                objEmployeePL.ToDate = ToDate;
                DataSet Ds = new DataSet();
                Ds = objEmployeeBL.GetSummaryReports(objEmployeePL);
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    rptrReports.DataSource = Ds;
                    rptrReports.DataBind();
                }
                else
                {
                    EmptyGrid();
                }
                this.rptrReports.DataSource = null;
            }
            catch (Exception ex)
            {
            }
        }

        private void EmptyGrid()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("Total");
            dt.Columns.Add("Pass");
            dt.Columns.Add("ClientName");
            dt.Columns.Add("Fail");
            DataRow dr;
            dr = dt.NewRow();

            dr["Total"] = 0;
            dr["Pass"] = 0;
            dr["ClientName"] = "";
            dr["Fail"] = 0;
            dt.Rows.Add(dr);
            rptrReports.DataSource = dt;
            rptrReports.DataBind();

            foreach (RepeaterItem rpt in rptrReports.Items)
            {
                Label lbl = (Label)rpt.FindControl("sno");
                LinkButton lblTotal = (LinkButton)rpt.FindControl("lblTotal");
                LinkButton lblPass = (LinkButton)rpt.FindControl("lblPass");
                LinkButton lblFail = (LinkButton)rpt.FindControl("lblFail");
                lbl.Visible = false;
                lblTotal.Visible = false;
                lblPass.Visible = false;
                lblFail.Visible = false;
            }
        }


        public void LoadParticipationDetails()
        {
            int tpoicID = 0;

            Common.DDVal(this.dTopic, out tpoicID);

            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(txtFromDate.Text, out FromDate);
            DateTime.TryParse(txtToDate.Text, out ToDate);
            objEmployeePL.CompanyID = UserSession.CompanyIDCurrent;
            objEmployeePL.LocationID = UserSession.LocationIDCurrent;
            objEmployeePL.TopicID = tpoicID;
            objEmployeePL.MainUserID = UserSession.UserIDs;
            objEmployeePL.FromDate = FromDate;
            objEmployeePL.ToDate = ToDate;
            DataSet Ds = new DataSet();
            Ds = objEmployeeBL.GetParticaptesDetails(objEmployeePL);
            if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
            {
                rptParticipateReports.DataSource = Ds;
                rptParticipateReports.DataBind();
            }
            else
            {
                this.rptParticipateReports.DataSource = null;
                rptParticipateReports.DataBind();
            }

        }

        public void LoadPassDetails()
        {
            int tpoicID = 0;
            Common.DDVal(this.dTopic, out tpoicID);

            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(txtFromDate.Text, out FromDate);
            DateTime.TryParse(txtToDate.Text, out ToDate);
            objEmployeePL.CompanyID = UserSession.CompanyIDCurrent;
            objEmployeePL.LocationID = UserSession.LocationIDCurrent;
            objEmployeePL.TopicID = tpoicID;
            objEmployeePL.MainUserID = UserSession.UserIDs;
            objEmployeePL.FromDate = FromDate;
            objEmployeePL.ToDate = ToDate;
            DataSet Ds = new DataSet();
            Ds = objEmployeeBL.GetParticaptesPassDetails(objEmployeePL);
            if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
            {
                rptParticipateReports.DataSource = Ds;
                rptParticipateReports.DataBind();
            }
            else
            {
                this.rptParticipateReports.DataSource = null;
                rptParticipateReports.DataBind();
            }
        }



        public void LoadFailDetails()
        {
            int tpoicID = 0;
            Common.DDVal(this.dTopic, out tpoicID);

            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(txtFromDate.Text, out FromDate);
            DateTime.TryParse(txtToDate.Text, out ToDate);
            objEmployeePL.CompanyID = UserSession.CompanyIDCurrent;
            objEmployeePL.LocationID = UserSession.LocationIDCurrent;
            objEmployeePL.TopicID = tpoicID;
            objEmployeePL.MainUserID = UserSession.UserIDs;
            objEmployeePL.FromDate = FromDate;
            objEmployeePL.ToDate = ToDate;
            DataSet Ds = new DataSet();
            Ds = objEmployeeBL.GetParticaptesFailDetails(objEmployeePL);
            if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
            {
                rptParticipateReports.DataSource = Ds;
                rptParticipateReports.DataBind();
            }
            else
            {
                this.rptParticipateReports.DataSource = null;
                rptParticipateReports.DataBind();
            }
        }
        public void ExportToExcel(DataTable dt, string FileName)
        {
            if (dt.Rows.Count > 0)
            {
                string filename = FileName + ".xls";
                System.IO.StringWriter tw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();

                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.
                //Response.ContentType = application/vnd.ms-excel;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendHeader("Content-Disposition",
                                      "attachment; filename=" + filename + "");
                this.EnableViewState = false;
                Response.Write(tw.ToString());
                Response.End();
            }
        }

        protected void imgExcel_Click(object sender, ImageClickEventArgs e)
        {
            int topicID = 0;
            Common.DDVal(this.dTopic, out topicID);
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(txtFromDate.Text, out FromDate);
            DateTime.TryParse(txtToDate.Text, out ToDate);
            objEmployeePL.CompanyID = UserSession.CompanyIDCurrent1;
            objEmployeePL.LocationID = UserSession.LocationIDCurrent;
            objEmployeePL.TopicID = topicID;

            objEmployeePL.MainUserID = UserSession.UserIDs;
            objEmployeePL.FromDate = FromDate;
            objEmployeePL.ToDate = ToDate;
            DataSet Ds = new DataSet();

            DataTable Dt = new DataTable();
            Dt.Columns.Add("SNo");
            Dt.Columns.Add("Topic");
            Dt.Columns.Add("Total");
            Dt.Columns.Add("Pass");
            Dt.Columns.Add("Fail");
            DataRow Dr;
            string[] TestValue = new string[999];

            Ds = objEmployeeBL.GetSummaryReports(objEmployeePL);
            if (Ds != null && Ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                {
                    Dr = Dt.NewRow();
                    Dr["SNo"] = i + 1;
                    Dr["Topic"] = Ds.Tables[0].Rows[i]["Topic"].ToString();
                    Dr["Total"] = Ds.Tables[0].Rows[i]["Total"].ToString();
                    Dr["Pass"] = Ds.Tables[0].Rows[i]["Pass"].ToString();
                    Dr["Fail"] = Ds.Tables[0].Rows[i]["Fail"].ToString();
                    Dt.Rows.Add(Dr);
                }
            }
            ExportToExcel(Dt, "Summaryreports");
        }


        public void ExcelOfParticipates(DataTable dt, string FileName)
        {
            if (dt.Rows.Count > 0)
            {
                string filename = FileName + ".xls";
                System.IO.StringWriter tw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();

                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.
                //Response.ContentType = application/vnd.ms-excel;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendHeader("Content-Disposition",
                                      "attachment; filename=" + filename + "");
                this.EnableViewState = false;
                Response.Write(tw.ToString());
                Response.End();
            }
        }


        protected void participateExcel_Click(object sender, ImageClickEventArgs e)
        {
            int topicID = 0;
            Common.DDVal(this.dTopic, out topicID);
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(txtFromDate.Text, out FromDate);
            DateTime.TryParse(txtToDate.Text, out ToDate);
            objEmployeePL.CompanyID = UserSession.CompanyIDCurrent1;
            objEmployeePL.LocationID = UserSession.LocationIDCurrent;
            objEmployeePL.TopicID = topicID;

            objEmployeePL.MainUserID = UserSession.UserIDs;
            objEmployeePL.FromDate = FromDate;
            objEmployeePL.ToDate = ToDate;
            DataSet Ds = new DataSet();

            DataTable Dt = new DataTable();
            Dt.Columns.Add("Sno");
            Dt.Columns.Add("Name");
            Dt.Columns.Add("EmailID");
            Dt.Columns.Add("ExamDate");
            Dt.Columns.Add("ExamTime");
            Dt.Columns.Add("PassPercentage");
            DataRow Dr;
            string[] TestValue = new string[999];

            Ds = objEmployeeBL.GetParticaptesDetails(objEmployeePL);
            if (Ds != null && Ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                {
                    Dr = Dt.NewRow();
                    Dr["Sno"] = i + 1;
                    Dr["Name"] = Ds.Tables[0].Rows[i]["Name"].ToString();
                    Dr["EmailID"] = Ds.Tables[0].Rows[i]["EmailID"].ToString();
                    Dr["ExamDate"] = Ds.Tables[0].Rows[i]["ExamDate"].ToString();
                    Dr["ExamTime"] = Ds.Tables[0].Rows[i]["ExamTime"].ToString();
                    Dr["PassPercentage"] = Ds.Tables[0].Rows[i]["PassPercentage"].ToString();
                    Dt.Rows.Add(Dr);
                }
            }
            ExportToExcel(Dt, "Summaryreports");
        }


        protected void btnSubmitreg_Click(object sender, EventArgs e)
        {
            LoadDetails();
            ParticipateExcelImage.Visible = false;
            rptParticipateReports.DataSource = null;
            rptParticipateReports.DataBind();
        }

        protected void rptParticipateDetails_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void rptrAnswer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            DataSet dt = new DataSet();

            if (e.CommandName == "Total")
            {
                LoadParticipationDetails();
            }

            if (e.CommandName == "Pass")
            {
                LoadPassDetails();    
            }

            if (e.CommandName == "Fail")
            {
                LoadFailDetails();
            }

            
           

            
            
            ParticipateExcelImage.Visible = rptParticipateReports.Items.Count > 0;
        }
        
     
        protected void rptrAnswer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Total++;
                //Label lblTotal = (Label)e.Item.FindControl("lblTotal");
                //Label lblPass = (Label)e.Item.FindControl("lblPass");
                //Label lblFail = (Label)e.Item.FindControl("lblFail");
                //HiddenField testpercentage = (HiddenField)e.Item.FindControl("htestpercentage");
                //HiddenField passpercentage = (HiddenField)e.Item.FindControl("hpasspercentage");

                //if ((testpercentage.Value != string.Empty && testpercentage.Value != null) && (passpercentage.Value != string.Empty && passpercentage.Value != null))
                //{
                //    if (Convert.ToInt32(testpercentage.Value) > Convert.ToInt32(passpercentage.Value))
                //    {
                //        Pass++;
                //    }
                //    else
                //    {
                //        Fail++;
                //    }
                //    lblTotal.Text = Total.ToString();
                //    lblPass.Text = Pass.ToString();
                //    lblFail.Text = Fail.ToString();
                //}
                //else
                //{
                //    lblTotal.Text = Total.ToString();
                //    lblPass.Text = Pass.ToString();
                //    lblFail.Text = Fail.ToString();
                //}
            }
        }




        protected void rptParticipateDetails_ItemDataBound(object source, RepeaterItemEventArgs e)
        {

        }

        protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadTopic();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}