﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="UserDepartmentAssign.aspx.cs" Inherits="OnlineTraining.UserDepartmentAssign" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="Stylesheet" />
    <script type="text/javascript" src="App_Themes/jquery/jquery-1.6.2.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(

        /* This is the function that will get executed after the DOM is fully loaded */
            function () {
                $(".dt").datepicker({
                    changeMonth: true, //this option for allowing user to select month
                    changeYear: true //this option for allowing user to select from year range
                });
            }
        );
    </script>
    <style type="text/css">
        .center
        {
            margin-left: 40px;
            width: 15%;
        }
    </style>
    <script type="text/javascript">
        function unCheckAll() {
            $(".chkVisible input").each(function () {
                $(this).attr('checked', false);
            });
        }

        $(document).ready(function () {
            $('.Visible input').click(function () {
                $(".chkVisible input").each(function () {
                    $(this).attr('checked', $('.Visible input').attr('checked'));
                });
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="submenu">
        <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="UserDepartment-Assign"></asp:Literal>
            <asp:HiddenField ID="hdnuserdepartmentid" Value="0" runat="server" />
        </h1>
    </div>
    <%--<div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label></div>--%>
    <div class="alert alert-danger" style="margin-top: 12px; height: 40px; padding-top: 10px;
        padding-left: 420px" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">
            ×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <asp:Panel ID="pAdd" runat="server" CssClass="tile-body">
                    <section class="tile cornered">
                             
         <div class="tile-body">
                    <div class="row">
                        <!--col-6-->
                        
                        <div class="col-md-6">
                            <!-- tile body -->
                            <div class="tile-body">
                                <div id="basicvalidations" runat="server" class="form-horizontal">
                                    <div class="form-group">
                                       <label class="col-sm-4 control-label" for="">
                                          <asp:Label Text="Department" runat="server" ID="lDepartment" />:</label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control"
                                            onselectedindexchanged="ddlUserDepartment_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /tile body -->
                        </div>
                        

                         <div class="col-md-6">
                            <!-- tile body -->
                            <div class="tile-body">
                                <div id="topic" runat="server" class="form-horizontal">
                                    <div class="form-group">
                                       <label class="col-sm-4 control-label" for="" >
                                          <asp:Label Text="UserTopic Assign" runat="server" ID="lTopic" />:</label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddlTopic" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlTopic_SelectedIndexChanged" >
                                                </asp:DropDownList>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /tile body -->
                        </div>  
                        
                          <div class="col-md-6">
                        <!-- tile body -->
                        <div class="tile-body">
                            <div id="fromDate" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">
                                        <asp:Label Text="Start Date" runat="server" ID="lFromDate" />:</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtStartDate" runat="server" TabIndex="1" class="chosen-select form-control"></asp:TextBox>
                                           <cc1:CalendarExtender ID="calstartdate" runat="server" TargetControlID="txtStartDate"
                                      Format="MM/dd/yyyy">
                                </cc1:CalendarExtender>        
                                    </div>
                                </div>
                            </div>
                        </div> 
                        </div>

                         <div class="col-md-6">
                        <div class="tile-body">
                            <div id="toDate" runat="server" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="">       
                                        <asp:Label Text="End Date" runat="server" ID="lToDate" />:</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtEndDate" runat="server" TabIndex="2" class="chosen-select form-control"></asp:TextBox>
                                           <cc1:CalendarExtender ID="calendate" runat="server" TargetControlID="txtEndDate"
                                      Format="MM/dd/yyyy">
                                </cc1:CalendarExtender>     
                                    </div>
                                </div>
                            </div>
                            <!-- /tile body -->
                        </div></div>
                        
                <div class="table-responsive">
                    <asp:Repeater ID="rptruser" runat="server">
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <th width="5%">
                                        <asp:Label Text="S.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="User Name" runat="server" ID="lUserName" />
                                    </th>
                                  
                                    <th width="25%">
                                        <asp:CheckBox ID="chkAllVisible" CssClass="Visible" runat="server" ClientIDMode="Static" /><asp:Label
                                            Text="Select All" runat="server" ID="lvisible" />
                                    </th>
                                    <th>
                                        <asp:Label runat="server" Text="Send Email"></asp:Label>
                                    </th>
                                    
                                   
                                  
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("UserName") %>' />
                                </td>
                               
                                <td class="actions text-center">
                                    <asp:CheckBox CssClass="chkVisible" ID="chkMSFVisible"
                                        runat="server" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="emailResend" runat="server" style="cursor:pointer;margin-left:25px" ToolTip="Send Email" ImageUrl="App_Themes/images/resend_mail.png" Onclick="btn_ResendEmail" />
                                </td>
                                
                            
                            </tr>
                            <asp:HiddenField runat="server" ID="hUserID" Value='<%# Eval("UserID")%>' />
                            <asp:HiddenField runat="server" ID="hEmailID" Value='<%# Eval("EmailID")%>' />
                            <asp:HiddenField runat="server" ID="hFirstName" Value='<%# Eval("FirstName")%>' />

                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                                                    <asp:ImageButton ID="emailResend" runat="server" style="cursor:pointer;margin-left:25px;float:right;" ToolTip="Send to All Email" img src="App_Themes/images/resend_mail.png" Onclick="btn_ResendAllEmail" />


                                <div class="row">
                                    <div class="col-md-12">
                                <div class="tile-body">
                                    <div id="TextEditor" runat="server" class="form-horizontal">
                                        <div id="Editor" class="form-group" runat="server">
                                            <label class="col-sm-2 control-label" for="">
                                                <asp:Label Text="Text Instruction" runat="server" ID="Label2" />: <br/><br/><br/>#FirstName#<br/>#UserEmailID# <br/>#Department# <br/>#Topic#<br/>#StartDate#<br/>#EndDate# </label>
                                            <div class="col-sm-10">
                                                <CKEditor:CKEditorControl ID="textinstruction" BasePath="~/ckeditor/" RemovePlugins="elementspath"
                                                    runat="server" ToolbarFull="Bold|Italic|Underline|Strike|-|Subscript|Superscript NumberedList|BulletedList|-|Outdent|Indent|Blockquote|CreateDiv
                                                    JustifyLeft|JustifyCenter|JustifyRight|JustifyBlock
                                                    BidiLtr|BidiRtl
                                                    Link|Unlink|Anchor
                                                    Image|Flash|Table|HorizontalRule|Smiley|SpecialChar|PageBreak|Iframe
                                                    /
                                                    Styles|Format|Font|FontSize
                                                    TextColor|BGColor
                                                    Maximize|ShowBlocks|" Height="250"></CKEditor:CKEditorControl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </div>
                                
                  
                    </div>
                        
                        <div class="row">
                                    <div class="col-md-12">
                                        <div id="Form1" parsley-validate="" role="form" runat="server" class="form-horizontal">
                                            <div class="form-group form-footer">
                                                <div class="col-sm-offset-5 col-sm-6">
                                                   <asp:Button ID="bSave" Text="Save" runat="server" class="btn btn-primary" OnClick="bSave_Click" />
                                                     <asp:Button ID="Button1" Text="Cancel" runat="server" class="btn btn-default" OnClick="Cancel_Click" />
                                                         
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                   


          
            </div>
            <div class="cboth">
            </div>
            </section>
                </asp:Panel>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
