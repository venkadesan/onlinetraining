﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ethics
{
    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dtTable=new DataTable();
            dtTable.Columns.Add("Count");
            for (int icount = 1; icount < 5; icount++)
            {
                dtTable.Rows.Add(icount.ToString());
            }

            rptrContinent.DataSource = dtTable;
            rptrContinent.DataBind();
        }
    }
}