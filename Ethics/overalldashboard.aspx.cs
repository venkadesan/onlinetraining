﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTrainingBL.Master;
using OnlineTraining.Code;
using System.Data;
using System.Text;

namespace Ethics
{
    public partial class overalldashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if(!IsPostBack)
            {
                LoadDetails();
            }

        }

        private void LoadDetails()
        {

            DataSet ds = new BLDashboard().GetDepartmentTopics(UserSession.GroupID);
            StringBuilder litral = new StringBuilder();
            //<div class="card-container col-lg-2 col-md-4 col-sm-2" >
            //            <center>
            //                <div class="card card-green hover" onclick="divdashboardClick('3')">
            //                    <div class="front" id="Third" runat="server" style="padding-top: 40px">
            //                        <h1>
            //                            <asp:Label ID="lblThird" runat="server" Text="Online Test Cleared" ForeColor="White"></asp:Label></h1>
            //                        <p id="P2">
            //                            <asp:Label ID="lblThirdvalue" runat="server" Text="65" ForeColor="White"></asp:Label></p>
            //                    </div>
            //                    <div class="back nopadding" id="ThirdBack" runat="server">
            //                        <!-- Button trigger modal -->
            //                    </div>
            //                </div>
            //                 </center>
            //            </div>

            string[] color = { "card-cyan", "card-green", "card-red", "card-amethyst" };
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                Session["data"] = ds.Tables[0];
                this.lbltotalCount.Text = ds.Tables[0].Rows.Count.ToString();
                var ss = ds.Tables[0].AsEnumerable()
                         .Select(row => new
                         {
                             departmentID = row.Field<int>("departmentID"),
                             departmentName = row.Field<String>("DepartmentName")
                         }).Distinct();
                int count = 0;
                foreach (var row in ss)
                {

                    DataRow[] rows;
                    rows = ds.Tables[0].Select("departmentID = " + row.departmentID + "");

                    litral.Append("<div class=\"card-container col-lg-2 col-md-4 col-sm-2\" >");
                    litral.Append("<center>");
                    litral.Append("<div class=\"card " + color[count] + " hover\" onclick=\"divdashboardClick('" + row.departmentID + "')\">");
                    litral.Append("<div class=\"front\" style=\"padding-top: 40px\">");
                    litral.Append("<h1>");
                    litral.Append("<span  ForeColor=\"White\"> " + row.departmentName + " </span>");
                    litral.Append("</h1>");
                    litral.Append("<p>");
                    litral.Append("<span ForeColor=\"White\">" + rows.Length + "</span>");
                    litral.Append("</p>");
                    litral.Append("</div>");
                    litral.Append("<div class=\"back nopadding\"></div></div></center></div>");
                    count++;
                    if (count == 4) count = 0;
                }

                ltrlBox.Text = litral.ToString();

            }

        }

        protected void btnbindraw_Click(object sender, EventArgs e)
        {
            int departmentID = 0;
            int.TryParse(this.hdntypeid.Value.ToString(), out departmentID);
            if( Session["data"]== null && (DataTable)Session["data"] ==null)
            {
                return;
            }

            StringBuilder litral = new StringBuilder();
            if (departmentID == 0)
            {
                litral.Append("<div class=\"card-container col-lg-12 col-md-12 col-sm-12\" >");

                litral.Append("<div class=\"card card-amethyst hover\">");

                litral.Append("<h1>");
                litral.Append("<span  ForeColor=\"White\"> " + "All" + " </span>");
                litral.Append("</h1>");

                litral.Append("</div></div>");
                ltrlHeddign.Text = litral.ToString();
                this.rptrTotalTraining.DataSource = (DataTable)Session["data"];
                this.rptrTotalTraining.DataBind();
            }
            else
            {
                DataRow[] rows;
                rows = ((DataTable)Session["data"]).Select("departmentID = " + departmentID + "");

                litral.Append("<div class=\"card-container col-lg-12 col-md-12 col-sm-12\" >");
                
                litral.Append("<div class=\"card card-amethyst hover\">");
               
                litral.Append("<h1>");
                litral.Append("<span  ForeColor=\"White\"> " + rows[0]["DepartmentName"] + " </span>");
                litral.Append("</h1>");
                
                litral.Append("</div></div>");

                ltrlHeddign.Text = litral.ToString();
                this.rptrTotalTraining.DataSource = rows.CopyToDataTable<DataRow>();
                this.rptrTotalTraining.DataBind();
            }
        }

        
    }
}