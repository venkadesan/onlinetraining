﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using OnlineTraining.Code;
using OnlineTrainingPL;
using OnlineTrainingBL;
namespace OnlineTraining
{
    public partial class Department : PageBase
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion

        PLDepartment objDepartmentPL = new PLDepartment();
        BLDepartment objDepartmentBL = new BLDepartment();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                LoadDetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }
      

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        public void LoadDetails()
        {
            objDepartmentPL.CompanyID = UserSession.CompanyIDCurrent;
            objDepartmentPL.LocationID = UserSession.LocationIDCurrent;
            objDepartmentPL.GroupId = UserSession.GroupID;
            objDepartmentPL.DepartmentId = 0;
            DataSet Ds = new DataSet();
            Ds = objDepartmentBL.GetDepartmentDetails(objDepartmentPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrDepartment.DataSource = Ds;
                rptrDepartment.DataBind();
            }
            else
            {
                rptrDepartment.DataSource = null;
                rptrDepartment.DataBind();
            }
        }

        
        public void BindToCtrls()
        {
            DataSet Ds = new DataSet();
            objDepartmentPL.DepartmentId = this.ID.Value;

            objDepartmentPL.CompanyID = UserSession.CompanyIDCurrent;
            objDepartmentPL.LocationID = UserSession.LocationIDCurrent;
            objDepartmentPL.GroupId = UserSession.GroupID;
            Ds = objDepartmentBL.GetDepartmentDetails(objDepartmentPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.tDepartmentName.Text = Ds.Tables[0].Rows[0]["DepartmentName"].ToString();
                this.tShortName.Text = Ds.Tables[0].Rows[0]["ShortName"].ToString();
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void bCancel_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            LoadDetails();
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            if (status == Common.ButtonStatus.New || status == Common.ButtonStatus.Cancel || status == Common.ButtonStatus.Edit)
                this.tDepartmentName.Text = tShortName.Text = string.Empty;
            this.bNew.Visible = false;
            this.divmsg.Visible = false;
            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tDepartmentName.Focus();
            }
            else if (status == Common.ButtonStatus.View)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tDepartmentName.Focus();
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.tDepartmentName.Focus();
                this.pAdd.Visible = true;

                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;

                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                this.ID = 0;
            }
        }

        protected void rptrDepartment_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindToCtrls();
            }
            else if (e.CommandName == "delete")
            {
                this.ID = Convert.ToInt32(e.CommandArgument);
                objDepartmentPL.DepartmentId = this.ID.Value;
                string message = objDepartmentBL.DeleteDepartmentDetails(objDepartmentPL);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages(message, Common.ErrorType.Information);
                LoadDetails();
            }
        }

        protected void rptrDepartment_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hDepartmentID = (HiddenField)e.Item.FindControl("hDepartmentID");
                LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lnkEdit");
                LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");

                lnkEdit.CommandArgument = hDepartmentID.Value.ToString();
                lnkDelete.CommandArgument = hDepartmentID.Value.ToString();
            }
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            if (tDepartmentName.Text == string.Empty)
            {
                NotifyMessages("Please enter the Department Name", Common.ErrorType.Error);
                return;
            }
            objDepartmentPL.CompanyID = UserSession.CompanyIDCurrent;
            objDepartmentPL.LocationID = UserSession.LocationIDCurrent;
            objDepartmentPL.GroupId = UserSession.GroupID;
            objDepartmentPL.DepartmentName = this.tDepartmentName.Text.Trim();
            objDepartmentPL.ShortName = this.tShortName.Text.Trim();
            objDepartmentPL.CUID = Convert.ToInt32(UserSession.UserID);
            try
            {
                if (this.IsAdd)
                {
                    objDepartmentPL.DepartmentId = 0;
                    string[] Values = objDepartmentBL.InsertUpdateDepartment(objDepartmentPL);
                    if (Convert.ToInt32(Values[1]) == -1)
                    {
                        ButtonStatus(Common.ButtonStatus.View);
                        NotifyMessages(Values[0], Common.ErrorType.Information);
                    }
                    else
                    {
                        ButtonStatus(Common.ButtonStatus.Cancel);
                        NotifyMessages(Values[0], Common.ErrorType.Information);
                        LoadDetails();
                    }
                }
                else if (this.ID != null)
                {
                    objDepartmentPL.DepartmentId = this.ID.Value;
                    string[] Values = objDepartmentBL.InsertUpdateDepartment(objDepartmentPL);
                    if (Convert.ToInt32(Values[1]) == -1)
                    {
                        ButtonStatus(Common.ButtonStatus.View);
                        NotifyMessages(Values[0], Common.ErrorType.Information);
                    }
                    else
                    {
                        ButtonStatus(Common.ButtonStatus.Cancel);
                        NotifyMessages(Values[0], Common.ErrorType.Information);
                        LoadDetails();
                    }
                }
                else
                {
                    return;
                }
            }
            catch (ApplicationException ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }
    }
}