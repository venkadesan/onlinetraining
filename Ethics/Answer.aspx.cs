﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.HSSF.Record.Formula.Functions;
using OnlineTrainingPL;
using OnlineTrainingBL;
using System.Web.UI.HtmlControls;
using OnlineTraining.Code;
using MB = Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Web.Script.Serialization;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Data.OleDb;

namespace OnlineTraining
{
    public partial class Answer : PageBase
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableAnswer
        {
            get
            {
                if (ViewState["disableAnswer"] == null)
                    ViewState["disableAnswer"] = 0;
                return (bool)ViewState["disableAnswer"];
            }

            set
            {
                ViewState["disableAnswer"] = value;
            }
        }

        #endregion
        PLAnswer objAnswerPL = new PLAnswer();
        BLAnswer objAnswerBL = new BLAnswer();
        PLQuestion objQuestionPL = new PLQuestion();
        BLQuestion objQuestionBL = new BLQuestion();

        MB.BLCompany objCompany = new MB.BLCompany();
        MB.BLLocation objLocation = new MB.BLLocation();
        HtmlGenericControl divMessage = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (divMessage != null)
            {
                divMessage.InnerHtml = string.Empty;
                divMessage.Attributes.Clear();
            }
            if (!IsPostBack)
            {
                Setheadertextdetails();
                dTopic.Items.Clear();
                dQuestion.Items.Clear();
                LoadDepartment();
                //LoadDDlTopic();
                LoadDDlQuestion();
                LoadDetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void LoadDDlTopic()
        {
            BLTopic objBLTopic = new BLTopic();
            PLTopic objPLTopic = new PLTopic();

            int departmentid = 0;
            if (!Common.DDVal(ddldepartment, out departmentid))
            {
                NotifyMessages("Please select department", Common.ErrorType.Error);
                return;
            }

            objPLTopic.CompanyID = UserSession.CompanyIDCurrent;
            objPLTopic.Groupid = UserSession.GroupID;
            objPLTopic.DepartmentId = departmentid;
            objPLTopic.TopicID = 0;
            DataSet Ds = new DataSet();
            Ds = objBLTopic.getTopicByDepartment(objPLTopic);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.dTopic.DataSource = Ds;
                this.dTopic.DataTextField = "TopicName";
                this.dTopic.DataValueField = "TopicID";
                this.dTopic.DataBind();
            }
            else
            {
                this.dTopic.Items.Clear();
                this.dQuestion.Items.Clear();

                rptrAnswer.DataSource = null;
                rptrAnswer.DataBind();
            }

            this.dTopic_SelectedIndexChanged(null, null);
        }

        private void LoadDepartment()
        {
            BLDepartment objbldept = new BLDepartment();
            PLDepartment objpldept = new PLDepartment();

            objpldept.CompanyID = UserSession.CompanyIDCurrent;
            objpldept.GroupId = UserSession.GroupID;
            // objPLTopic.LocationID = UserSession.LocationIDCurrent;
            objpldept.DepartmentId = 0;
            DataSet Ds = new DataSet();
            Ds = objbldept.GetDepartmentDetails(objpldept);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.ddldepartment.DataSource = Ds;
                this.ddldepartment.DataTextField = "DepartmentName";
                this.ddldepartment.DataValueField = "DepartmentId";
                this.ddldepartment.DataBind();
            }
            else
            {
                this.ddldepartment.Items.Clear();
            }

            this.ddldepartment_SelectedIndexChanged(null, null);
        }

        protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDDlTopic();
        }

        protected void dTopic_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDDlQuestion();
            LoadDetails();
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        public void LoadDetails()
        {
            DataSet Ds = new DataSet();
            objAnswerPL.AnswerID = 0;
            int QuestionID = 0;

            Common.DDVal(this.dQuestion, out QuestionID);
            objQuestionPL.CompanyID = UserSession.CompanyIDCurrent;
            //  objQuestionPL.LocationID = UserSession.LocationIDCurrent;
            objQuestionPL.QuestionID = QuestionID;
            Ds = objQuestionBL.GetALLQuestionDetails(objQuestionPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrAnswer.DataSource = Ds;
                rptrAnswer.DataBind();

                DataSet Ds1 = new DataSet();
                Ds1 = objAnswerBL.GetAnswerDetails(objAnswerPL);
                if (Ds1.Tables[0].Rows.Count > 0)
                {
                    for (int item = 0; item < rptrAnswer.Items.Count; item++)
                    {
                        Repeater rptrAnswer1 = rptrAnswer.Items[item].FindControl("rptrAnswer1") as Repeater;
                        for (int items = 0; items < rptrAnswer1.Items.Count; items++)
                        {
                            CheckBox chkAnswer = (CheckBox)rptrAnswer1.Items[items].FindControl("chkAnswer");
                            HiddenField hAnswerID = (HiddenField)rptrAnswer1.Items[items].FindControl("hAnswerID");
                            DataRow[] FilterValue = Ds1.Tables[0].Select("answerid=" + hAnswerID.Value + "and IsAnswer = True");
                            if (FilterValue.Length > 0)
                                chkAnswer.Checked = true;
                        }
                    }
                }
            }
            else
            {
                EmptyGrid();
            }
        }

        private void EmptyGrid()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("QuestionName");
            dt.Columns.Add("QuestionID");
            DataRow dr;
            dr = dt.NewRow();
            dr["QuestionName"] = "";
            dr["QuestionID"] = 0;
            dt.Rows.Add(dr);
            rptrAnswer.DataSource = dt;
            rptrAnswer.DataBind();

            foreach (RepeaterItem rpt in rptrAnswer.Items)
            {
                try
                {
                    Label lbl = (Label)rpt.FindControl("Label2");
                    lbl.Visible = false;
                }
                catch { }

                //LinkButton lnkEdit = (LinkButton)rpt.FindControl("lnkEdit");
                //LinkButton lnkDelete = (LinkButton)rpt.FindControl("lnkDelete");
                //lnkEdit.Visible = false;
                //lnkDelete.Visible = false;
            }
        }

        private void LoadDDlQuestion()
        {
            int TopicID = 0;

            Common.DDVal(this.dTopic, out TopicID);

            BLQuestion objblQuestion = new BLQuestion();
            PLQuestion objplQuestion = new PLQuestion();

            objplQuestion.CompanyID = UserSession.CompanyIDCurrent;
            //objplQuestion.LocationID = UserSession.LocationIDCurrent;
            objplQuestion.QuestionID = 0;
            objplQuestion.TopicID = Convert.ToInt32(TopicID);
            DataSet Ds = new DataSet();
            Ds = objblQuestion.GetALLTopicQuestionDetails(objplQuestion);

            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.dQuestion.DataSource = Ds;
                this.dQuestion.DataTextField = "QuestionName";
                this.dQuestion.DataValueField = "QuestionID";
                this.dQuestion.DataBind();
            }
            else
            {
                this.dQuestion.Items.Clear();
            }

            //this.dQuestion_SelectedIndexChanged(null, null);
        }

        public void BindToCtrls()
        {
            DataSet Ds = new DataSet();
            objAnswerPL.AnswerID = this.ID.Value;
            int QuestionID = 0;

            Common.DDVal(this.dQuestion, out QuestionID);
            objAnswerPL.CompanyID = UserSession.CompanyIDCurrent;
            objAnswerPL.QuestionID = QuestionID;
            // objAnswerPL.LocationID = UserSession.LocationIDCurrent;
            Ds = objAnswerBL.GetAnswerDetails(objAnswerPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.tAnswerName.Text = Ds.Tables[0].Rows[0]["AnswerName"].ToString();
                this.tShortName.Text = Ds.Tables[0].Rows[0]["ShortName"].ToString();
            }
        }

        protected void rptrAnswer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindToCtrls();
            }
            else if (e.CommandName == "delete")
            {
                this.ID = Convert.ToInt32(e.CommandArgument);
                objAnswerPL.AnswerID = this.ID.Value;
                string message = objAnswerBL.DeleteAnswerDetails(objAnswerPL);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages(message, Common.ErrorType.Information);
                LoadDetails();
            }
        }

        protected void rptrAnswer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hQuestionID = (HiddenField)e.Item.FindControl("hQuestionID");
                Repeater rptranswer1 = (Repeater)e.Item.FindControl("rptrAnswer1");
                //Label Options = (Label)e.Item.FindControl("Options");

                //LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lnkEdit");
                //LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
                //CheckBoxList chkanswer = (CheckBoxList)e.Item.FindControl("chkAnswer");
                DataSet Ds = new DataSet();
                objAnswerPL.AnswerID = 0;
                int QuestionID = 0;

                Common.DDVal(this.dQuestion, out QuestionID);
                objAnswerPL.CompanyID = UserSession.CompanyIDCurrent;
                // objAnswerPL.LocationID = UserSession.LocationIDCurrent;
                objAnswerPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                objAnswerPL.AnswerID = 0;
                Ds = objAnswerBL.GetAnswerDetails(objAnswerPL);

                if (Ds.Tables[0].Rows.Count > 0)
                {
                    rptranswer1.DataSource = Ds;
                    rptranswer1.DataBind();
                }
                else
                {
                    rptranswer1.DataSource = null;
                    rptranswer1.DataBind();
                }
            }
        }

        protected void rptrAnswer1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindToCtrls();
            }
            else if (e.CommandName == "delete")
            {
                this.ID = Convert.ToInt32(e.CommandArgument);
                objAnswerPL.AnswerID = this.ID.Value;
                string message = objAnswerBL.DeleteAnswerDetails(objAnswerPL);
                ButtonStatus(Common.ButtonStatus.Cancel);
                NotifyMessages(message, Common.ErrorType.Information);
                LoadDetails();
            }
        }

        protected void rptrAnswer1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hAnswerID = (HiddenField)e.Item.FindControl("hAnswerID");
                LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lnkEdit");
                LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");

                lnkEdit.CommandArgument = hAnswerID.Value.ToString();
                lnkDelete.CommandArgument = hAnswerID.Value.ToString();
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            int QuestionID = 0;

            if (!Common.DDVal(this.dQuestion, out QuestionID))
            {
                NotifyMessages("Please select the Question", Common.ErrorType.Error);
                return;
            }

            if (tAnswerName.Text == string.Empty)
            {
                NotifyMessages("Please enter the Answer Name", Common.ErrorType.Error);
                return;
            }
            objAnswerPL.CompanyID = UserSession.CompanyIDCurrent;
            objAnswerPL.QuestionID = QuestionID;
            // objAnswerPL.LocationID = UserSession.LocationIDCurrent;
            objAnswerPL.AnswerName = this.tAnswerName.Text.Trim();
            objAnswerPL.ShortName = this.tShortName.Text.Trim();

            objAnswerPL.CUID = Convert.ToInt32(UserSession.UserID);
            try
            {
                if (this.IsAdd)
                {
                    objAnswerPL.AnswerID = 0;
                    string message = objAnswerBL.InsertUpdateAnswer(objAnswerPL);
                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(message, Common.ErrorType.Information);
                    LoadDetails();
                }
                else if (this.ID != null)
                {
                    objAnswerPL.AnswerID = this.ID.Value;
                    string message = objAnswerBL.InsertUpdateAnswer(objAnswerPL);
                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(message, Common.ErrorType.Information);
                    LoadDetails();
                }
                else
                {
                    return;
                }
            }
            catch (ApplicationException ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void Cancel1_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            LoadDetails();
        }

        private void ButtonStatus(Common.ButtonStatus Answer)
        {
            this.tAnswerName.Text = tShortName.Text = string.Empty;
            this.bNew.Visible = false;
            this.divmsg.Visible = false;
            if (Answer == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tAnswerName.Focus();
            }
            else if (Answer == Common.ButtonStatus.Edit)
            {
                this.tAnswerName.Focus();
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (Answer == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                this.ID = 0;
            }
        }

        protected void dQuestion_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDetails();
            ButtonStatus(Common.ButtonStatus.Cancel);
        }

        protected string Values;

        protected void bAnswerKeySave_Click(object sender, EventArgs e)
        {
            StringBuilder sbXML = new StringBuilder();
            //int Value1 = 0;

            for (int item = 0; item < rptrAnswer.Items.Count; item++)
            {
                sbXML.Append("<root>");
                HiddenField hQuestionID = rptrAnswer.Items[item].FindControl("hQuestionID") as HiddenField;
                Repeater rptrAnswer1 = rptrAnswer.Items[item].FindControl("rptrAnswer1") as Repeater;
                for (int items = 0; items < rptrAnswer1.Items.Count; items++)
                {
                    CheckBox chkAnswer = (CheckBox)rptrAnswer1.Items[items].FindControl("chkAnswer");
                    HiddenField hAnswerID = rptrAnswer1.Items[items].FindControl("hAnswerID") as HiddenField;
                    if (chkAnswer.Checked)
                    {
                        sbXML.Append(
                               "<rolespec locationid=\"" + UserSession.LocationIDCurrent + "\"" +
                               " questionid=\"" + Convert.ToInt32(hQuestionID.Value) + "\"" +
                               " companyid=\"" + UserSession.CompanyIDCurrent + "\"" +
                               " answerid=\"" + Convert.ToInt32(hAnswerID.Value) + "\" />");
                    }
                }
                sbXML.Append("</root>");
                objAnswerPL.XMLis = sbXML.ToString();
                objAnswerPL.CompanyID = UserSession.CompanyIDCurrent;
                //objAnswerPL.LocationID = UserSession.LocationIDCurrent;
                objAnswerPL.QuestionID = Convert.ToInt32(hQuestionID.Value);
                objAnswerPL.CUID = Convert.ToInt32(Session["UserID"]);
                string[] Valuess = objAnswerBL.InsertAnswerKey(objAnswerPL);
            }
            LoadDetails();
        }

        protected void CancelAnswerKey_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            LoadDetails();
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            var workbook = new HSSFWorkbook();
            var sheet1 = workbook.CreateSheet("Sheet1");

            int TopicID = 0;

            Common.DDVal(this.dTopic, out TopicID);
            BLQuestion objblQuestion = new BLQuestion();
            PLQuestion objplQuestion = new PLQuestion();

            objplQuestion.CompanyID = UserSession.CompanyIDCurrent;
            objplQuestion.QuestionID = 0;
            objplQuestion.TopicID = Convert.ToInt32(TopicID);
            DataSet Ds = new DataSet();
            Ds = objblQuestion.GetALLTopicQuestionDetails(objplQuestion);

            int Row = 1, col = 0;
            foreach (DataRow row in Ds.Tables[0].Rows)
            {
                sheet1.CreateRow(Row).CreateCell(col).SetCellValue(row["QuestionId"].ToString());
                Row = Row + 1;
            }

            int Row1 = 1, col1 = 1;
            foreach (DataRow row in Ds.Tables[0].Rows)
            {
                sheet1.CreateRow(Row1).CreateCell(col1).SetCellValue(row["QuestionName"].ToString());
                Row1 = Row1 + 1;
            }

            sheet1.SetColumnWidth(0, 20 * 256);

            var headerRow = sheet1.CreateRow(0);
            var boldFont = workbook.CreateFont();
            boldFont.FontHeightInPoints = 11;
            boldFont.FontName = "Calibri";
            boldFont.Boldweight = HSSFFont.BOLDWEIGHT_BOLD;

            var cell = headerRow.CreateCell(0);
            cell.SetCellValue("QuestionId");
            cell.CellStyle = workbook.CreateCellStyle();
            cell.CellStyle.SetFont(boldFont);

            cell = headerRow.CreateCell(1);
            cell.SetCellValue("Question");
            cell.CellStyle = workbook.CreateCellStyle();
            cell.CellStyle.SetFont(boldFont);

            cell = headerRow.CreateCell(2);
            cell.SetCellValue("Answer");
            cell.CellStyle = workbook.CreateCellStyle();
            cell.CellStyle.SetFont(boldFont);

            cell = headerRow.CreateCell(3);
            cell.SetCellValue("Options");
            cell.CellStyle = workbook.CreateCellStyle();
            cell.CellStyle.SetFont(boldFont);

            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            Response.Clear();
            string fileName = "Template-" + DateTime.Now.ToString() + DateTime.Now.Millisecond.ToString() + ".xls";
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(output.ToArray());
            Response.End();
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            string connString = "";
            string strFileType = Path.GetExtension(fileuploadExcel.PostedFile.FileName).ToLower();
            string fName = fileuploadExcel.PostedFile.FileName;
            string uploadFolderPath = "~/ExcelFile/";
            string filePath = HttpContext.Current.Server.MapPath(uploadFolderPath);
            fileuploadExcel.SaveAs(filePath + "\\" + fName);
            string path = filePath + fName;
            if (strFileType.Trim() == ".xls")
            {
                connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            }
            else if (strFileType.Trim() == ".xlsx")
            {
                connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }
            string query = "SELECT [QuestionId],[Question],[Answer],[Options] FROM [Sheet1$]";
            OleDbConnection conn = new OleDbConnection(connString);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            OleDbCommand cmd = new OleDbCommand(query, conn);
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            if (CheckMandatoryFields(ds.Tables[0]))
            {
                ImportExcel(ds);
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
            else
            {
                NotifyMessages("Mandatory Fields Expected", Common.ErrorType.Error);
            }

            da.Dispose();
            conn.Close();
            conn.Dispose();
            LoadDetails();
        }

        private bool CheckMandatoryFields(DataTable dtanswers)
        {
            try
            {
                bool check = true;

            
                var rows = from row in dtanswers.AsEnumerable()
                           where row.Field<string>("Answer") == null
                           select row;

                if (rows.Count() > 0)
                {
                    check = false;
                }

                return check;
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Information);
                return false;
            }
        }

        private void ImportExcel(DataSet ds)
        {
            StringBuilder sb1 = new StringBuilder();
            sb1.Append("<root>");

            string str = null;
            string[] strArr = null;
            int count = 0;
            str = "Options";
            char[] splitchar = { ',' };
            strArr = str.Split(splitchar);
            for (count = 0; count <= strArr.Length - 1; count++)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string[] ans = RemoveEscpeCharacter(row["Answer"].ToString()).Split(',');
                    string[] option = RemoveEscpeCharacter(row["Options"].ToString()).Split(',');
                    int index = 0;
                    foreach (string a in ans)
                    {
                        ans[index] = a.Trim();
                        index = index + 1;
                    }
                    index = 0;
                    foreach (string a in option)
                    {
                        option[index] = a.Trim();
                        index = index + 1;
                    }

                    foreach (string opt in option)
                    {
                        sb1.Append("<answer ");
                        sb1.Append(" questionid =\"" + RemoveEscpeCharacter(row["QuestionId"].ToString()).TrimEnd() + "\"");
                        sb1.Append(" answername =\"" + opt.Trim() + "\"");
                        if (ans.Contains(opt.Trim()))
                            sb1.Append(" isnswer =\"" + 1 + "\"");
                        else
                            sb1.Append(" isanswer =\"" + 0 + "\"");
                        sb1.Append(" />");
                    }
                }
            }

            sb1.Append("</root>");
            objAnswerPL.ExcelUploadXMLIs = sb1.ToString();

            //int questionID = 0;
            //Common.DDVal(this.dQuestion, out questionID);

            //objAnswerPL.QuestionID = questionID;
            objAnswerPL.CompanyID = UserSession.CompanyIDCurrent;
            objAnswerPL.CUID = UserSession.UserID;

            try
            {
                NotifyMessages(objAnswerBL.ImportAnswer(objAnswerPL), Common.ErrorType.Information);
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        private string RemoveEscpeCharacter(string val)
        {
            return val.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&#39;");
        }
    }
}