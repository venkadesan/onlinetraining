﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EthicsPL.Master;
using OnlineTrainingBL.Master;
using OnlineTraining.Code;

namespace Ethics
{
    public partial class dashboard : System.Web.UI.Page
    {
        PLDashboard _objPlDashboard = new PLDashboard();
        BLDashboard _objBlDashboard = new BLDashboard();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!IsPostBack)
                {
                    //DateTimeFormatInfo usDtfi = new CultureInfo("en-US", false).DateTimeFormat;
                    //DateTimeFormatInfo ukDtfi = new CultureInfo("en-GB", false).DateTimeFormat;
                    //string result = Convert.ToDateTime(DateTime.Now, usDtfi).ToString(ukDtfi.ShortDatePattern);

                    ttodate.Text = DateTime.Now.ToShortDateString();
                    tfromdate.Text = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToShortDateString();
                    btngo_Click(null, null);
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btngo_Click(object sender, EventArgs e)
        {
            try
            {
                _objPlDashboard.Startdate = DateTime.ParseExact(tfromdate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                _objPlDashboard.Enddate = DateTime.ParseExact(ttodate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                _objPlDashboard.CUID = UserSession.UserID;
                _objPlDashboard.RoleName = UserSession.RoleName;
                _objPlDashboard.CompanyID = UserSession.CompanyIDUser;
                DataSet dsDash = new DataSet();
                dsDash = _objBlDashboard.GetDepartmentDetails(_objPlDashboard);
                if (dsDash.Tables.Count > 0)
                {
                    string totaltest = "0", totaltraining = "0", pass = "0", fail = "0";

                    totaltraining = dsDash.Tables[0].Rows[0]["TotalTraining"].ToString();
                    totaltest = dsDash.Tables[1].Rows[0]["TotalTest"].ToString();
                    pass = dsDash.Tables[3].Rows[0]["Pass"].ToString();
                    fail = dsDash.Tables[2].Rows[0]["Fail"].ToString();

                    lblFirstvalue.Text = totaltraining;
                    lblsecondvalue.Text = totaltest;
                    lblThirdvalue.Text = pass;
                    lblFourthvalue.Text = fail;

                    ClearDivs();
                }

            }
            catch (Exception)
            {
                //throw;
            }
        }

        protected void btnbindraw_Click(object sender, EventArgs e)
        {
            try
            {
                string type = hdntypeid.Value.ToString();

                _objPlDashboard.Startdate = DateTime.ParseExact(tfromdate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                _objPlDashboard.Enddate = DateTime.ParseExact(ttodate.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                _objPlDashboard.CUID = UserSession.UserID;
                _objPlDashboard.RoleName = UserSession.RoleName;
                _objPlDashboard.CompanyID = UserSession.CompanyIDUser;
                _objPlDashboard.Type = Convert.ToInt32(type);
                DataSet dsDash = new DataSet();
                dsDash = _objBlDashboard.GetDashboardRawDetails(_objPlDashboard);
                ClearDivs();
                if (dsDash.Tables.Count > 0 && dsDash.Tables[0].Rows.Count > 0)
                {
                    if (type == "1")
                    {
                        divtotalTraining.Visible = true;
                        rptrTotalTraining.DataSource = dsDash.Tables[0];
                        rptrTotalTraining.DataBind();
                    }
                    if (type == "2")
                    {
                        divtotaltest.Visible = true;
                        rptrtotaltest.DataSource = dsDash.Tables[0];
                        rptrtotaltest.DataBind();
                    }
                    if (type == "3" || type == "4")
                    {
                        divpassfailDet.Visible = true;
                        rptrpassfail.DataSource = dsDash.Tables[0];
                        rptrpassfail.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        private void ClearDivs()
        {
            divpassfailDet.Visible = false;
            divtotalTraining.Visible = false;
            divtotaltest.Visible = false;

            rptrTotalTraining.DataSource = null;
            rptrTotalTraining.DataBind();

            rptrtotaltest.DataSource = null;
            rptrtotaltest.DataBind();

            rptrpassfail.DataSource = null;
            rptrpassfail.DataBind();
        }
    }
}