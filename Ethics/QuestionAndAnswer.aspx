﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="QuestionAndAnswer.aspx.cs" Inherits="Ethics.QuestionAndAnswer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="Stylesheet" />
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">

        function ConfirmDelete() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="submenu">
        <h1>
            Master</h1>
        <h1>
            Question And Answer</h1>
        <span style="padding-top: 3px;">
            <asp:Button ID="bNew" class="btn btn-info margin-bottom-20" Text="New" runat="server"
                OnClick="bNew_Click" /></span>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label></div>
    <div class="main">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <!--col-6-->
                    <div class="col-md-12">
                        <!-- tile body -->
                        <div class="tile-body">
                            <div id="divQuestion" runat="server" class="form-horizontal">
                                <div class="form-group" style="float: left; width: 149%;">
                                    <label class="col-sm-2 control-label" for="" style="float: left; margin-left: -1%;">
                                        <asp:Label Text="Topic:" runat="server" ID="Label5" CssClass="bottom-right:10%" /></label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList runat="server" class="chosen-select form-control" ID="dTopic" AutoPostBack="True"
                                            OnSelectedIndexChanged="dTopic_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /tile body -->
                    </div>
                    <!-- end col 6 -->
                </div>
                <asp:Panel ID="pAdd" runat="server" CssClass="tile-body">
                    <section class="tile cornered">
                             
         <div class="tile-body">
                    <div class="row">
                        <!--col-6-->
                         <div class="col-md-6">
                            <!-- tile body -->
                            <div class="tile-body">
                                <div id="Div1" runat="server" class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="" style="margin-left:-3%;" >
                                            <asp:Label Text="Question Name" runat="server" ID="Label6" />:</label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="tQuestionName" runat="server" Height="60" Width="300%" TextMode="MultiLine" row="20" column="10"  class="form-control parsley-validated"></asp:TextBox>
                                        </div>
                                    </div><br /> 

                                            <div class="form-group">                                            
                                             <b>Download Template: </b>
                                             <asp:Button ID="btnDownload" runat="server" Text="Download" onclick="btnDownload_Click" />                                               
                                            </div>                                

                                            <div class="form-group">
                                             <div class="col-sm-offset-5 col-sm-6" style="margin-left:45%; margin-top:-8%">
                                            <b>Please Select Excel File: </b>
                                            </div>

                                           <div class="col-sm-offset-5 col-sm-6" style="margin-left:82%; margin-top:-8%">
                                            <asp:FileUpload ID="fileuploadExcel" runat="server" />&nbsp;&nbsp; 
                                           </div>                                                                           
                                          
                                            <div class="form-group form-footer">
                                             <div class="col-sm-offset-5 col-sm-6" style="margin-left:122%; margin-top:-9%">
                                                 <asp:Button ID="btnImport" runat="server" Text="Import Data" onclick="btnImport_Click" />                                                  
                                             </div>
                                            </div>                                                

                                            <div class="form-group" style="float: left; width: 100%;">
                                            <label class="col-sm-2 control-label" for="" 
                                                style="float: left; margin-left: -1%; width: 105px;">
                                                <asp:Label Text="No of Answers:" runat="server" ID="Label1" CssClass="bottom-right:10%" /></label>
                                            <div class="col-sm-8" style="margin-left: -5%;">
                                                <asp:DropDownList runat="server" class="chosen-select form-control" 
                                                    ID="dNoofAnswers" onselectedindexchanged="dNoofAnswers_SelectedIndexChanged">
                                                    <asp:ListItem Selected="True" Text="--Select--" Value="--Select--"></asp:ListItem>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>                                          

                                    <div class="form-group">
                                      <div class="col-sm-offset-5 col-sm-6" style="margin-left:80%;margin-top:-13%;">
                                        <asp:Button ID="bSave" Text="Save" runat="server" class="btn btn-primary" OnClick="bSave_Click" />
                                        <asp:Button ID="Button1" Text="Cancel" runat="server" class="btn btn-default" OnClick="Cancel1_Click" />
                                    </div> 
                                    </div>                                                                               
                                  </div>
                               </div>
                            </div>      
                    </div>                       
                            </div>
                            <div class="cboth">
                            </div>                          
                            
                            </section>
                </asp:Panel>
                <br />
                <div class="table-responsive">
                    <asp:Repeater ID="rptrQuestion" runat="server" OnItemCommand="rptrQuestion_ItemCommand"
                        OnItemDataBound="rptrQuestion_ItemDataBound">
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr class="new-color">
                                    <th width="5%">
                                        <asp:Label Text="Q.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Question Name" runat="server" ID="lDepartmentName" />
                                    </th>
                                    <th width="15%" style="text-align: center">
                                        Actions
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lblName" runat="server" OnClick="Question_Click" Text='<%# Eval("QuestionName") %>'></asp:LinkButton>
                                </td>
                                <td class="actions text-center">
                                    <asp:LinkButton class="btn btn-primary" ID="lnkEdit" runat="server" Text="Edit" CommandName="edit" />
                                    &nbsp;
                                    <asp:LinkButton class="btn btn-danger" ID="lnkDelete" runat="server" Text="Delete"
                                        CommandName="delete" OnClientClick="return ConfirmDelete();" />
                                </td>
                            </tr>
                            <%--<tr data-ng-repeat="row in rows">
                            <td style="" colspan="" ><input class="fosrm-control" type="text" style="width:100%;outline:none;border:none;background-color:transparent;" onchange="popup(this)"/></td>
                            <td style="" colspan="" ><input class="fosrm-control text-center" type="text" style="width:100%;outline:none;border:none;background-color:transparent;" onchange="popup(this)"/></td>
                            <td style="" colspan="" ><input class="fosrm-control text-center" type="text" style="width:100%;outline:none;border:none;background-color:transparent;" onchange="popup(this)"/></td>
                        </tr>--%>
                            <asp:HiddenField runat="server" ID="hQuestionID" Value='<%# Eval("QuestionID")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <asp:Repeater ID="rptrQuestionAndAnswer" runat="server" DataSourceID="" OnItemCommand="rptrQuestionAndAnswer_ItemCommand"
                    OnItemDataBound="rptrQuestionAndAnswer_ItemDataBound">
                    <HeaderTemplate>
                        <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                            <tr>
                                <th width="5%">
                                    <asp:Label Text="Sl.No" runat="server" ID="lSNo" />
                                </th>
                                <th>
                                    <asp:Label Text="Answer" runat="server" ID="lDepartmentName" />
                                </th>
                                <th>
                                    <asp:Label Text="IsMultiple" runat="server" ID="lblIsMultiple" />
                                </th>
                                <th>
                                    <asp:Label Text="IsSingle" runat="server" ID="lblIsSingle" />
                                </th>
                                <th width="15%" style="text-align: center">
                                    Actions
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                            </td>
                            <td>
                                <asp:TextBox ID="tAnswer" runat="server" class="form-control parsley-validated"></asp:TextBox>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkAnswer" runat="server" />
                            </td>
                            <td>
                                <asp:RadioButton ID="rdButton" runat="server" />
                            </td>
                            <td class="actions text-center">
                                <asp:LinkButton class="btn btn-primary" ID="lnkSave" runat="server" Text="Save" CommandName="save" />
                                &nbsp;
                                <asp:LinkButton class="btn btn-danger" ID="lnkCancel" runat="server" Text="Cancel"
                                    CommandName="cancel" />
                            </td>
                        </tr>
                        <asp:HiddenField runat="server" ID="hQuestionID" Value='<%# Eval("QuestionID")%>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>
