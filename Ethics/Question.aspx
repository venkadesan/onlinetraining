﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true"
    CodeBehind="Question.aspx.cs" Inherits="OnlineTraining.Question" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="Stylesheet" />
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">

        function ConfirmDelete() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="submenu">
        <h1>
            <asp:Literal ID="lmainheader" runat="server" Text="Master"></asp:Literal>
        </h1>
        <h1>
            <asp:Literal ID="lsubheader" runat="server" Text="Question"></asp:Literal>
        </h1>
        <span style="padding-top: 3px;">
            <asp:Button ID="bNew" class="btn btn-info margin-bottom-20" Text="New" runat="server"
                OnClick="bNew_Click" /></span>
    </div>
    <!-- /submenu -->
    <!-- content main container -->
<%--    <div class=" col-md-5 col-xs-offset-4 alert alert-big alert-danger alert-dismissable fade in margin-top-10"
        runat="server" id="divmsg" visible="false">
        <asp:Label runat="server" ID="lblError"></asp:Label></div>--%>
        
         <div class="alert alert-danger" style="margin-top: 12px;height:40px;padding-top:10px;padding-left:420px" id="divmsg" visible="false" runat="server">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <asp:Label runat="server" ID="lblError"></asp:Label>
    </div>

    <div class="main">
        <div class="row">
            <div class="col-md-12">
               <div class="row">
                    <div class="col-md-12">
                        <!-- tile body -->
                        <div class="tile-body" style="width: 100%;">
                            <div id="divQuestion" runat="server" class="form-horizontal" style="width: 100%;">
                                <div class="form-group" style="float: left; width: 100%;">
                                   <label class="col-sm-1 control-label" for="" style="float: left; text-align: left;">
                                        <asp:Label Text="Department:" runat="server" ID="Label3" /></label>
                                    <div class="col-sm-11">
                                        <asp:DropDownList runat="server" class="chosen-select form-control" ID="ddldepartment"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddldepartment_SelectedIndexChanged" Width="103%">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group" style="float: left; width: 100%;">
                                    <label class="col-sm-1 control-label" for="" style="float: left; text-align: left;">
                                        <asp:Label Text="Topic:" runat="server" ID="Label5" /></label>
                                    <div class="col-sm-11">
                                        <asp:DropDownList runat="server" class="chosen-select form-control" ID="dTopic" AutoPostBack="True"
                                            Width="103%" OnSelectedIndexChanged="dTopic_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /tile body -->
                    </div>
                    <!-- end col 6 -->
                </div>
                <asp:Panel ID="pAdd" runat="server" CssClass="tile-body">
                    <section class="tile cornered">
                             
         <div class="tile-body">
                    <div class="row">
                        <!--col-6-->
                         <div class="col-md-6">
                            <!-- tile body -->
                            <div class="tile-body">
                                <div id="Div1" runat="server" class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" for="" style="margin-left:-3%;" >
                                            <asp:Label Text="Question Name" runat="server" ID="Label6" />:</label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="tQuestionName" runat="server" Height="60" style="width: 100%;"  TextMode="MultiLine" row="20" column="10"  class="form-control parsley-validated"></asp:TextBox>
                                        </div>
                                    </div><br /> 

                                     <div class="form-group">                                            
                                                <b>Download Template: </b>
                                                <asp:Button ID="btnDownload" runat="server" Text="Download" onclick="btnDownload_Click" />                                               
                                            </div>

                                            <div class="form-group">
                                             <div class="col-sm-offset-5 col-sm-6" style="margin-left:45%; margin-top:-8%">
                                            <b>Please Select Excel File: </b>
                                            </div>

                                           <div class="col-sm-offset-5 col-sm-6" style="margin-left:82%; margin-top:-8%">
                                            <asp:FileUpload ID="fileuploadExcel" runat="server" />&nbsp;&nbsp; 
                                           </div>                                                                           
                                          
                                            <div class="form-group form-footer">
                                             <div class="col-sm-offset-5 col-sm-6" style="margin-left:122%; margin-top:-9%">
                                                 <asp:Button ID="btnImport" runat="server" Text="Import Data" onclick="btnImport_Click" />                                                  
                                             </div>
                                            </div>      
                                             </div>
                                    <div class="form-group">
                                      <div class="col-sm-offset-5 col-sm-6" style="margin-left:70%;margin-top:-6%;">
                                        <asp:Button ID="bSave" Text="Save" runat="server" class="btn btn-primary" OnClick="bSave_Click" />
                                        <asp:Button ID="Button1" Text="Cancel" runat="server" class="btn btn-default" OnClick="Cancel1_Click" />
                                    </div> 
                                    </div>                                                                               
                                
                               </div>
                            </div>
                            <!-- /tile body -->
                        </div>
                        <%--<div class="col-md-6">
                            <!-- tile body -->
                            <div class="tile-body">
                                <div id="basicvalidations" runat="server" class="form-horizontal">                                                    
                                          <div class="form-group">
                                             <label class="col-sm-4 control-label" for="">
                                                 <asp:Label Text="Short Name" runat="server" ID="Label1" />:</label>
                                              <div class="col-sm-8">
                                                 <asp:TextBox ID="tShortName" runat="server" class="form-control parsley-validated"></asp:TextBox>
                                              </div>
                                          </div>                                            
                               </div>
                          </div>
                            <!-- /tile body -->
                        </div>          --%>             
                    </div>                       
                            </div>
                            <div class="cboth">
                            </div>                          
                            
                            </section>
                </asp:Panel>
                <br />
                <div class="table-responsive">
                    <asp:Repeater ID="rptrQuestion" runat="server" OnItemCommand="rptrQuestion_ItemCommand"
                        DataSourceID="" OnItemDataBound="rptrQuestion_ItemDataBound">
                        <HeaderTemplate>
                            <table cellpadding="0" class="table table-bordered" cellspacing="0" border="0" width="100%">
                                <tr class="new-color">
                                    <th width="5%">
                                        <asp:Label Text="S.No" runat="server" ID="lSNo" />
                                    </th>
                                    <th>
                                        <asp:Label Text="Questions" runat="server" ID="lDepartmentName" />
                                    </th>
                                    <%--<th>
                                        <asp:Label Text="Short Name" runat="server" ID="Label3" />
                                    </th>--%>
                                    <th width="15%" style="text-align: center">
                                        Actions
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%#(((RepeaterItem)Container).ItemIndex+1).ToString() %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("QuestionName") %>' />
                                </td>
                                <%--<td>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("ShortName") %>' />
                                </td>--%>
                                <td class="actions text-center">
                                    <asp:LinkButton class="btn btn-primary" ID="lnkEdit" runat="server" Text="Edit" CommandName="edit" />
                                    &nbsp;
                                    <asp:LinkButton class="btn btn-danger" ID="lnkDelete" runat="server" Text="Delete"
                                        CommandName="delete" OnClientClick="return ConfirmDelete();" />
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="hQuestionID" Value='<%# Eval("QuestionID")%>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
