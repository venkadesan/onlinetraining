﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTrainingPL;
using OnlineTrainingBL;
using System.Web.UI.HtmlControls;
using OnlineTraining.Code;
using MB = Kowni.BusinessLogic;
using MCB = Kowni.Common.BusinessLogic;
using System.Data;
using System.Text;
using System.Data.OleDb;
using System.IO;
using System.Data.SqlClient;

namespace OnlineTraining
{
    public partial class Topic : PageBase
    {
        #region Properties
        private bool IsAdd
        {
            get
            {
                if (!ID.HasValue)
                    return false;
                else if (ID.Value != 0)
                    return false;
                else
                    return true;
            }
        }

        private int? _id = 0;
        private int? ID
        {
            get
            {
                if (ViewState["id"] == null)
                    ViewState["id"] = 0;
                return (int?)ViewState["id"];
            }

            set
            {
                ViewState["id"] = value;
            }
        }

        private bool DisableStatus
        {
            get
            {
                if (ViewState["disablestatus"] == null)
                    ViewState["disablestatus"] = 0;
                return (bool)ViewState["disablestatus"];
            }

            set
            {
                ViewState["disablestatus"] = value;
            }
        }

        #endregion
        PLTopic objTopicPL = new PLTopic();
        BLTopic objTopicBL = new BLTopic();

        PLDepartment objDepartmentPL = new PLDepartment();
        BLDepartment objDepartmentBL = new BLDepartment();

        MB.BLCompany objCompany = new MB.BLCompany();
        MB.BLLocation objLocation = new MB.BLLocation();
        HtmlGenericControl divMessage = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Setheadertextdetails();
                BindDepartment();
                LoadDetails();
                LoadCommonTopicDetails();
                ButtonStatus(Common.ButtonStatus.Cancel);
            }
        }

        /// <summary>
        /// Sets the Header and Sub Header based on the Link.
        /// </summary>
        private void Setheadertextdetails()
        {
            try
            {
                string mainmenu = string.Empty;
                string submenu = string.Empty;
                string[] file = Request.CurrentExecutionFilePath.Split('/');
                string fileName = file[file.Length - 1];
                Getmainandsubform(fileName, out mainmenu, out submenu);
                if (mainmenu != string.Empty)
                    lmainheader.Text = mainmenu;
                if (submenu != string.Empty)
                    lsubheader.Text = submenu;
            }
            catch (Exception ex)
            {
                //NotifyMessages(ex.Message.ToString(), Common.ErrorType.Error);
            }
        }

        private void BindDepartment()
        {
            try
            {
                objDepartmentPL.CompanyID = UserSession.CompanyIDCurrent;
                objDepartmentPL.LocationID = UserSession.LocationIDCurrent;
                objDepartmentPL.GroupId = UserSession.GroupID;
                objDepartmentPL.DepartmentId = 0;
                DataSet Ds = new DataSet();
                Ds = objDepartmentBL.GetDepartmentDetails(objDepartmentPL);
                if (Ds.Tables[0].Rows.Count > 0)
                {
                    ddldepartment.DataSource = Ds;
                    this.ddldepartment.DataTextField = "DepartmentName";
                    this.ddldepartment.DataValueField = "DepartmentId";
                    ddldepartment.DataBind();

                    ddlDepartmentCommon.DataSource = Ds;
                    this.ddlDepartmentCommon.DataTextField = "DepartmentName";
                    this.ddlDepartmentCommon.DataValueField = "DepartmentId";
                    ddlDepartmentCommon.DataBind();
                }
            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }

        }

        public void LoadCommonTopicDetails()
        {
            objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
            //objTopicPL.LocationID = UserSession.LocationIDCurrent;
            objTopicPL.TopicID = 0;
            DataSet Ds = new DataSet();
            Ds = objTopicBL.GetCommonTopicDetails(objTopicPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrcommontopic.DataSource = Ds;
                rptrcommontopic.DataBind();
            }
            else
            {
                rptrcommontopic.DataSource = null;
                rptrcommontopic.DataBind();
            }
        }

        public void NotifyMessages(string message, Common.ErrorType et)
        {
            divmsg.Visible = true;
            lblError.Text = message;
            if (et == Common.ErrorType.Error)
            {

            }
            else if (et == Common.ErrorType.Information)
            {

            }
        }

        public void LoadDetails()
        {
            objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
            //objTopicPL.LocationID = UserSession.LocationIDCurrent;
            objTopicPL.TopicID = 0;
            objTopicPL.Groupid = UserSession.GroupID;
            DataSet Ds = new DataSet();
            Ds = objTopicBL.GetTopicDetails(objTopicPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                rptrTopic.DataSource = Ds;
                rptrTopic.DataBind();
            }
            else
            {
                EmptyGrid();
            }
        }

        private void EmptyGrid()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("TopicName");
            dt.Columns.Add("Description");
            dt.Columns.Add("PassPercentage");
            dt.Columns.Add("TopicID");
            dt.Columns.Add("IsActive");
            dt.Columns.Add("FileName");
            dt.Columns.Add("displayname");
            dt.Columns.Add("ResolutionTime");
            dt.Columns.Add("Attempts");

            DataRow dr;
            dr = dt.NewRow();
            dr["TopicName"] = "";
            dr["Description"] = "";
            dr["PassPercentage"] = "";
            dr["TopicID"] = 0;
            dr["IsActive"] = false;
            dr["Attempts"] = "";
            dr["FileName"] = "";
            dr["displayname"] = "";
            dr["ResolutionTime"] = "";
            dt.Rows.Add(dr);
            rptrTopic.DataSource = dt;
            rptrTopic.DataBind();

            foreach (RepeaterItem rpt in rptrTopic.Items)
            {
                Label lbl = (Label)rpt.FindControl("Label2");
                LinkButton lnkEdit = (LinkButton)rpt.FindControl("lnkEdit");
                LinkButton lnkDelete = (LinkButton)rpt.FindControl("lnkDelete");
                lbl.Visible = false;
                lnkEdit.Visible = false;
                lnkDelete.Visible = false;
            }
        }

        public void BindToCtrls()
        {
            DataSet Ds = new DataSet();
            DataSet Ds1 = new DataSet();
            objTopicPL.TopicID = this.ID.Value;
            objTopicPL.Groupid = UserSession.GroupID;
            objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
            //objTopicPL.LocationID = UserSession.LocationIDCurrent;
            Ds = objTopicBL.GetTopicDetails(objTopicPL);
            if (Ds.Tables[0].Rows.Count > 0)
            {
                this.tTopicName.Text = Ds.Tables[0].Rows[0]["TopicName"].ToString();
                this.tDescription.Text = Ds.Tables[0].Rows[0]["Description"].ToString();
                this.tPassPercentage.Text = Ds.Tables[0].Rows[0]["PassPercentage"].ToString();
                this.tResolutionTime.Text = Ds.Tables[0].Rows[0]["ResolutionTime"].ToString();
                this.txtAttempts.Text = Ds.Tables[0].Rows[0]["Attempts"].ToString();
                string active = Ds.Tables[0].Rows[0]["IsActive"].ToString();

                if (active.ToLower() == "true" || active.ToLower() == "1")
                {
                    ChkActive.Checked = true;
                }
                else
                {
                    ChkActive.Checked = false;
                }

                if (Ds.Tables[0].Rows[0]["departmentid"].ToString() != string.Empty)
                {
                    ddldepartment.SelectedValue = Ds.Tables[0].Rows[0]["departmentid"].ToString();
                }

                this.txttrainingdurationtime.Text = Ds.Tables[0].Rows[0]["TrainingDuration"].ToString();
                //this.ttestinstruction.Text = Ds.Tables[0].Rows[0]["testInstruction"].ToString();
                if (Ds.Tables[0].Rows[0]["isTestRequired"].ToString() != string.Empty)
                {
                    Chktestrequied.Checked = Convert.ToBoolean(Ds.Tables[0].Rows[0]["isTestRequired"].ToString());    
                }


                hdndisplyname1.Value = Ds.Tables[0].Rows[0]["displayname"].ToString();
                hdnfilename1.Value = Ds.Tables[0].Rows[0]["FileName"].ToString();
                lbldisplyname1.Text = Ds.Tables[0].Rows[0]["displayname"].ToString();

                btnDelete.Visible = lbldisplyname1.Text != string.Empty;

                try
                {
                    hdndisplyname2.Value = Ds.Tables[0].Rows[0]["displayname2"].ToString();
                    hdnfilename2.Value = Ds.Tables[0].Rows[0]["FileName2"].ToString();
                    lbldisplyname2.Text = Ds.Tables[0].Rows[0]["displayname2"].ToString();

                    btnRemove.Visible = lbldisplyname2.Text != string.Empty;
                }
                catch { }
            }
        }

        protected void rptrTopic_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                ButtonStatus(Common.ButtonStatus.Edit);
                this.ID = Convert.ToInt32(e.CommandArgument);
                BindToCtrls();
            }
            else if (e.CommandName == "delete")
            {
                CheckBox ChkRdActive = e.Item.FindControl("ChkRdActive") as CheckBox;
                if (ChkRdActive != null)
                {
                    if (ChkRdActive.Checked == true)
                    {
                        NotifyMessages("Deletion not allowed for active topic", Common.ErrorType.Error);
                        return;
                    }
                    else
                    {
                        this.ID = Convert.ToInt32(e.CommandArgument);
                        objTopicPL.TopicID = this.ID.Value;
                        string message = objTopicBL.DeleteTopicDetails(objTopicPL);
                        ButtonStatus(Common.ButtonStatus.Cancel);
                        NotifyMessages(message, Common.ErrorType.Information);
                        LoadDetails();
                    }
                }
            }
        }

        protected void rptrTopic_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField hTopicID = (HiddenField)e.Item.FindControl("hTopicID");
                LinkButton lnkEdit = (LinkButton)e.Item.FindControl("lnkEdit");
                LinkButton lnkDelete = (LinkButton)e.Item.FindControl("lnkDelete");
                Repeater rpRepeater = (Repeater)e.Item.FindControl("RptProductDetails");

                lnkEdit.CommandArgument = hTopicID.Value.ToString();
                lnkDelete.CommandArgument = hTopicID.Value.ToString();

                HiddenField HdnActive = (HiddenField)e.Item.FindControl("HdnActive");
                CheckBox ChkRdActive = (CheckBox)e.Item.FindControl("ChkRdActive");

                if (ChkRdActive != null)
                {
                    if (HdnActive != null)
                    {
                        if (HdnActive.Value.ToLower() == "true" || HdnActive.Value.ToLower() == "1")
                        {
                            ChkRdActive.Checked = true;
                        }
                        else
                        {
                            ChkRdActive.Checked = false;
                        }
                    }
                }
            }
        }

        protected void bNew_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.New);
        }

        protected void ChkRdActive_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox chk = (CheckBox)sender;
                RepeaterItem item = (RepeaterItem)chk.NamingContainer;
                HiddenField hTopicID = (HiddenField)item.FindControl("hTopicID");
                if (hTopicID != null)
                {
                    if (string.IsNullOrEmpty(hTopicID.Value) == false)
                    {
                        objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
                        objTopicPL.TopicID = Convert.ToInt32(hTopicID.Value);
                        objTopicPL.IsActive = chk.Checked;
                        int message = objTopicBL.UpdateTopicStatus(objTopicPL);
                        if (objTopicPL.ErrorMessage != string.Empty)
                        {
                            if (objTopicPL.ErrorMessage.ToString() == "Updated Successfully")
                            {
                                ButtonStatus(Common.ButtonStatus.Cancel);
                                NotifyMessages(objTopicPL.ErrorMessage, Common.ErrorType.Information);
                                LoadDetails();
                            }
                            else if (objTopicPL.ErrorMessage.ToString() == "Set atleast one Topic should be active.")
                            {
                                ButtonStatus(Common.ButtonStatus.Cancel);
                                NotifyMessages(objTopicPL.ErrorMessage, Common.ErrorType.Error);
                                LoadDetails();
                            }
                        }
                    }
                }
            }
            catch (ApplicationException ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        //protected void btnSubmit_Click(object sender, EventArgs e)
        //{           
        //    string strFileType = Path.GetExtension(FileUpload1.PostedFile.FileName).ToLower();
        //    string fName = FileUpload1.PostedFile.FileName;
        //    string uploadFolderPath = "~/File/";
        //    string filePath = HttpContext.Current.Server.MapPath(uploadFolderPath);
        //    FileUpload1.SaveAs(filePath + "\\" + fName);
        //    string path = filePath + fName;

        //    if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
        //    {
        //        string fnv = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
        //        string[] fileVExtArr = fnv.Split('.');
        //        string fileVExt = "";
        //        if (fileVExtArr.Length > 1)
        //        {
        //            fileVExt = fileVExtArr[1];
        //            fileVExt = fileVExt.ToLower();
        //        }
        //        fnv = System.IO.Path.GetFileNameWithoutExtension(FileUpload1.PostedFile.FileName);
        //        if (!fileVExt.Equals("mp4") && !fileVExt.Equals("flv") && !fileVExt.Equals("3gp") && !fileVExt.Equals("avi")) 
        //        {
        //            Page.ClientScript.RegisterStartupScript(this.GetType(), "22222", "<script> alert('File type not matched. Please upload only mp4,3gp,avi and flv file') </script>", false);                   
        //        }

        //       string SaveVideoLocation = Server.MapPath("File") + "\\" + fnv;
        //       FileUpload1.PostedFile.SaveAs(SaveVideoLocation);

        //       try
        //       {
        //           NotifyMessages("Uploaded Successfully", Common.ErrorType.Error);
        //       }
        //       catch (Exception ex)
        //       {
        //           NotifyMessages(ex.Message, Common.ErrorType.Error);
        //       }
        //    }
        //}

        //protected void btnSubmit_Click(object sender, EventArgs e)
        //{            

        //}        

        protected void bSave_Click(object sender, EventArgs e)
        {
            int departmentid = 0;
         
            if (!Common.DDVal(ddldepartment, out departmentid))
            {
                NotifyMessages("Please Select Department", Common.ErrorType.Error);
                return;
            }

            if (tTopicName.Text == string.Empty)
            {
                NotifyMessages("Please enter the Topic Name", Common.ErrorType.Error);
                tTopicName.Focus();
                return;
            }
            if (tPassPercentage.Text == string.Empty)
            {
                NotifyMessages("Please enter the Pass Percentage", Common.ErrorType.Error);
                return;
            }
            int PValue = 0;
            if (!Int32.TryParse(this.tPassPercentage.Text, out PValue))
            {
                NotifyMessages("Please enter the Pass Percentage", Common.ErrorType.Error);
                return;
            }

            if (tResolutionTime.Text == string.Empty)
            {
                NotifyMessages("Please enter the Resolution Time", Common.ErrorType.Error);
                return;
            }

            if (txttrainingdurationtime.Text == string.Empty)
            {
                NotifyMessages("Please enter the Training Duration Time", Common.ErrorType.Error);
                return;
            }

            if (txtAttempts.Text == string.Empty)
            {
                NotifyMessages("Please enter the test Attempts", Common.ErrorType.Error);
                return;
            }

            //if (ttestinstruction.Text == string.Empty)
            //{
            //    NotifyMessages("Please enter the Test Instruction", Common.ErrorType.Error);
            //    return;
            //}


          

           

            if (ChkActive.Checked == false)
            {
                if (this.IsAdd)
                {
                    objTopicPL.TopicID = 0;
                    objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
                    string message = objTopicBL.CheckActiveTopic(1, objTopicPL);
                    if (message != string.Empty)
                    {
                        if (message == "No record found")
                        {
                            NotifyMessages("Set topic as active status", Common.ErrorType.Error);
                            return;
                        }
                    }
                }
                else if (this.ID != null)
                {
                    objTopicPL.TopicID = this.ID.Value;
                    objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
                    string message = objTopicBL.CheckActiveTopic(2, objTopicPL);
                    if (message != string.Empty)
                    {
                        if (message == "No record found")
                        {
                            NotifyMessages("Set topic as active status", Common.ErrorType.Error);
                            return;
                        }
                    }
                }
            }

            objTopicPL.CompanyID = UserSession.CompanyIDCurrent;
            // objTopicPL.LocationID = UserSession.LocationIDCurrent;
            objTopicPL.TopicName = this.tTopicName.Text.Trim();

            if (FileUpload1.HasFile)
            {
                Guid g;
                g = Guid.NewGuid();
                string strFileType = Path.GetExtension(FileUpload1.PostedFile.FileName).ToLower();
                string fName = FileUpload1.PostedFile.FileName;
                string fileextension = System.IO.Path.GetExtension(FileUpload1.FileName);
                string uploadFolderPath = "~/File/";
                string filePath = HttpContext.Current.Server.MapPath(uploadFolderPath);
                FileUpload1.SaveAs(filePath + "\\" + g + fileextension);
                string path = filePath + g + fileextension;

                objTopicPL.FileName = g + fileextension;
                objTopicPL.DisplayName = fName;
            }
            else
            {
                objTopicPL.FileName = hdnfilename1.Value;
                objTopicPL.DisplayName = hdndisplyname1.Value;
            }

            if (fuupload2.HasFile)
            {
                Guid g1;
                g1 = Guid.NewGuid();

                string fName1 = fuupload2.PostedFile.FileName;
                string fileextension1 = System.IO.Path.GetExtension(fuupload2.FileName);
                string uploadFolderPath1 = "~/File/";
                string filePath1 = HttpContext.Current.Server.MapPath(uploadFolderPath1);
                fuupload2.SaveAs(filePath1 + "\\" + g1 + fileextension1);
                string path1 = filePath1 + g1 + fileextension1;

                objTopicPL.FileName2 = g1 + fileextension1;
                objTopicPL.DisplayName2 = fName1;
            }
            else
            {
                objTopicPL.FileName2 = hdnfilename2.Value;
                objTopicPL.DisplayName2 = hdndisplyname2.Value;
            }
           
            
            objTopicPL.Percentage = PValue;
            objTopicPL.Description = this.tDescription.Text.Trim();
            objTopicPL.ResolutionTime = this.tResolutionTime.Text.Trim();
            objTopicPL.CUID = Convert.ToInt32(UserSession.UserID);
            objTopicPL.IsActive = ChkActive.Checked;
            objTopicPL.Groupid = UserSession.GroupID;
            objTopicPL.DepartmentId = departmentid;
            objTopicPL.TestDuration = txttrainingdurationtime.Text;
            //objTopicPL.TestInstruction = ttestinstruction.Text;
            objTopicPL.IsTestRequired = Chktestrequied.Checked;
            objTopicPL.Attempts = txtAttempts.Text;
            try
            {
                if (this.IsAdd)
                {
                    objTopicPL.TopicID = 0;
                    int message = objTopicBL.InsertUpdateTopic(objTopicPL);
                    objTopicPL.TopicID = message;
                    string messages = string.Empty;

                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(objTopicPL.ErrorMessage, Common.ErrorType.Information);
                    LoadDetails();
                }
                else if (this.ID != null)
                {
                    objTopicPL.TopicID = this.ID.Value;
                    int messagea = objTopicBL.InsertUpdateTopic(objTopicPL);

                    ButtonStatus(Common.ButtonStatus.Cancel);
                    NotifyMessages(objTopicPL.ErrorMessage, Common.ErrorType.Information);
                    LoadDetails();
                }
                else
                {
                    return;
                }
            }
            catch (ApplicationException ex)
            {
                NotifyMessages(ex.Message, Common.ErrorType.Error);
            }
        }

        protected void dNoofAttempts_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Cancel1_Click(object sender, EventArgs e)
        {
            ButtonStatus(Common.ButtonStatus.Cancel);
            LoadDetails();
        }

        private void ButtonStatus(Common.ButtonStatus status)
        {
            this.tTopicName.Text = tDescription.Text = tPassPercentage.Text = tResolutionTime.Text = string.Empty;
            //this.txttrainingdurationtime.Text = this.ttestinstruction.Text = string.Empty;
            this.ChkActive.Checked = false;
            this.bNew.Visible = false;
            this.divmsg.Visible = false;
            Chktestrequied.Checked = false;

            hdndisplyname1.Value = string.Empty;
            hdnfilename1.Value = string.Empty;
            lbldisplyname1.Text = string.Empty;
            btnDelete.Visible = false;

            hdndisplyname2.Value = string.Empty;
            hdnfilename2.Value = string.Empty;
            lbldisplyname2.Text = string.Empty;
            btnRemove.Visible = false;


            if (status == Common.ButtonStatus.New)
            {
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
                this.tTopicName.Focus();
                ChkActive.Checked = true;
            }
            else if (status == Common.ButtonStatus.Edit)
            {
                this.tTopicName.Focus();
                this.pAdd.Visible = true;
                this.bNew.Enabled = false;
                this.bSave.Enabled = true;
            }
            else if (status == Common.ButtonStatus.Cancel)
            {
                this.pAdd.Visible = false;
                this.bNew.Visible = true;
                this.bNew.Enabled = true;
                this.bSave.Enabled = false;
                this.ID = 0;
            }
        }

        protected void btnglobalsave_Click(object sender, EventArgs e)
        {
            try
            {
                int departmentid = 0;
                if (!Common.DDVal(ddlDepartmentCommon, out departmentid))
                {
                    NotifyMessages("Select Department",Common.ErrorType.Error);
                    return;
                }
                objTopicPL.Groupid = UserSession.GroupID;
                objTopicPL.DepartmentId = departmentid;
                objTopicPL.CUID = UserSession.UserID;
                objTopicPL.IsActive = true;
                DataTable dtTopicDet=new DataTable();
                dtTopicDet.Columns.Add("TopicName", typeof(string));
                dtTopicDet.Columns.Add("PassPercentage", typeof(string));
                dtTopicDet.Columns.Add("Description", typeof(string));
                dtTopicDet.Columns.Add("filename", typeof(string));
                dtTopicDet.Columns.Add("displayname", typeof(string));
                dtTopicDet.Columns.Add("ResolutionTime", typeof(string));

                dtTopicDet.Columns.Add("TrainingDuration", typeof(string));
                dtTopicDet.Columns.Add("TestInstruction", typeof(string));

                foreach (RepeaterItem ricommontopic in rptrcommontopic.Items)
                {
                    Label lbltopicname = (Label)ricommontopic.FindControl("lbltopicname");
                    Label lblpasspercentage = (Label)ricommontopic.FindControl("lblpasspercentage");
                    Label lbldescription = (Label)ricommontopic.FindControl("lbldescription");
                    Label lblfilename = (Label)ricommontopic.FindControl("lblfilename");
                    Label lbldisplyname = (Label)ricommontopic.FindControl("lbldisplyname");
                    Label lblresolutiontime = (Label)ricommontopic.FindControl("lblresolutiontime");
                    CheckBox chkMSFVisible = (CheckBox)ricommontopic.FindControl("chkMSFVisible");

                    Label lblTrainingDuration = (Label)ricommontopic.FindControl("lblTrainingDuration");
                    Label lbltestInstruction = (Label)ricommontopic.FindControl("lbltestInstruction");

                    if (chkMSFVisible!=null && chkMSFVisible.Checked)
                    {
                        if (lbltopicname != null && lblpasspercentage != null && lbldescription != null &&
                            lblfilename != null && lbldisplyname != null && lblresolutiontime != null && lblTrainingDuration != null && lbltestInstruction!=null)
                        {
                            dtTopicDet.Rows.Add(lbltopicname.Text, lblpasspercentage.Text, lbldescription.Text,
                                lblfilename.Text, lbldisplyname.Text, lblresolutiontime.Text, lblTrainingDuration.Text, lbltestInstruction.Text);
                        }
                    }
                }

                string xmlstring = string.Empty;
                using (TextWriter writer = new StringWriter())
                {
                    dtTopicDet.TableName = "GlobalCommonTopic";
                    dtTopicDet.WriteXml(writer);
                    xmlstring = writer.ToString();
                }

                objTopicPL.TopicXml = xmlstring;
                objTopicBL.InsertUpdateTopicFromCommon(objTopicPL);
                NotifyMessages(objTopicPL.ErrorMessage, Common.ErrorType.Error);
                LoadDetails();

            }
            catch (Exception ex)
            {
                NotifyMessages(ex.Message,Common.ErrorType.Error);
            }
        }
    }
}