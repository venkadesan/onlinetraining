﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingPL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;
using OnlineTrainingPL.Master;

namespace OnlineTrainingDL.Master
{
    public class DLPlayInstruction
    {


        public DataSet GetInstructions(PLPlayInstruction objPlayInstruction)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("GetInstruction");
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objPlayInstruction.CompanyId);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }


        }
        public string[] SaveUpdateInstructions(PLPlayInstruction objPlayInstruction)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("sp_SaveUpdate");
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objPlayInstruction.CompanyId);
                db.AddInParameter(cmd, "@InstructionID", DbType.Int32, objPlayInstruction.PlayInstructionId);
                db.AddInParameter(cmd, "@PlayInstructions", DbType.String, objPlayInstruction.PlayInstructionText);
                db.ExecuteNonQuery(cmd);
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
