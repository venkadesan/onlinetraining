﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingPL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;
using OnlineTrainingPL.Master;

namespace OnlineTrainingDL.Master
{
    public class DLTest
    {       
        public string[] InsertUpdateTest(PLTest Test)
        {           
            string[] Values = new string[3];
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdate_Test");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Test.CompanyID);
            db.AddInParameter(cmd, "@TestID", DbType.Int32, Test.TestID);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Test.TopicID);
            db.AddInParameter(cmd, "@LocationID", DbType.Int32, Test.LocationID);
            db.AddInParameter(cmd, "@PassPercentage", DbType.Int32, Test.PassPercentage);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Test.CUID);
            int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
            Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Values[1] = Value.ToString();
            return Values;
        }

        public string[] InsertUpdateTestSpec(PLTest Test)
        {
            string[] Values = new string[3];
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdate_TestSpec");
            db.AddInParameter(cmd, "@TestID", DbType.Int32, Test.TestID);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Test.TopicID);
            db.AddInParameter(cmd, "@XMLis", DbType.String, Test.XMLis);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Test.CUID);
            int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
            Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Values[1] = Value.ToString();
            return Values;
        }

        public DataSet CheckResults(PLTest Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_CheckResults");
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Topic.QuestionID);
            db.AddInParameter(cmd, "@cuid", DbType.Int32, Topic.CUID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Topic.CompanyID);
            db.AddInParameter(cmd, "@LocationID", DbType.Int32, Topic.LocationID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetAnswerResults(PLTest Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetAnswerResults");
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Topic.QuestionID);
            db.AddInParameter(cmd, "@cuid", DbType.Int32, Topic.CUID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Topic.CompanyID);
            db.AddInParameter(cmd, "@LocationID", DbType.Int32, Topic.LocationID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public void UpdatePercentage(PLTest Test)
        {
            string[] Values = new string[3];
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetPercentageUpdateDetails");
            db.AddInParameter(cmd, "@TestID", DbType.Int32, Test.TestID);
            db.AddInParameter(cmd, "@UserID", DbType.Int32, Test.CUID);
            db.AddInParameter(cmd, "@PassPercentage", DbType.Int32, Test.PassPercentage);
            db.ExecuteScalar(cmd);            
        }

        public int InsertMainLogin(PLTest Test)
        {
            string[] Values = new string[3];
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdate_MainLogin");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Test.CompanyID);
            db.AddInParameter(cmd, "@LocationID", DbType.Int32, Test.LocationID);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Test.CUID);
            db.AddInParameter(cmd, "@UserName", DbType.String, Test.AnswerName);
            int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
            return Value;
        }

        public int InsertDeclaration(PLTest Test)
        {
            string[] Values = new string[3];
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdate_Declaration");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Test.CompanyID);
            db.AddInParameter(cmd, "@LocationID", DbType.Int32, Test.LocationID);
            db.AddInParameter(cmd, "@Declaration", DbType.String, Test.Declaration);
            db.AddInParameter(cmd, "@PassPercentage", DbType.Int32, Test.PassPercentage);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Test.CUID);            
            int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
            return Value;
        }

        public DataSet GetMainLogin(PLTest Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Get_MainLogin");
            db.AddInParameter(cmd, "@UID", DbType.Int32, Topic.CUID);            
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }
    }
}
