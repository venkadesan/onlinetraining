﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingPL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;

namespace OnlineTrainingDL
{
    public class DLAnswer
    {
        public DataSet GetAnswerDetails(PLAnswer Answer)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetAnswer_Details");
            db.AddInParameter(cmd, "@AnswerID", DbType.Int32, Answer.AnswerID);
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Answer.QuestionID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Answer.CompanyID);
            //db.AddInParameter(cmd, "@LocationID", DbType.Int32, Answer.LocationID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public string DeleteAnswerDetails(PLAnswer Answer)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_Delete_Answer");
            db.AddInParameter(cmd, "@AnswerID", DbType.Int32, Answer.AnswerID);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.ExecuteNonQuery(cmd);
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            return results;
        }

        public string InsertUpdateAnswer(PLAnswer Answer)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdate_Answer");
            db.AddInParameter(cmd, "@AnswerID", DbType.Int32, Answer.AnswerID);
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Answer.QuestionID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Answer.CompanyID);
            //db.AddInParameter(cmd, "@LocationID", DbType.Int32, Answer.LocationID);
            db.AddInParameter(cmd, "@AnswerName", DbType.String, Answer.AnswerName.Trim());
            db.AddInParameter(cmd, "@Options", DbType.String, Answer.AnswerName.Trim());
            db.AddInParameter(cmd, "@ShortName", DbType.String, Answer.ShortName.Trim());
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Answer.CUID);
            db.ExecuteNonQuery(cmd);
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            return results;
        }

        public string[] InsertAnswerKey(PLAnswer Answer)
        {
            string[] Values = new string[3];
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdate_AnswerKey");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Answer.CompanyID);
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Answer.QuestionID);
            //db.AddInParameter(cmd, "@LocationID", DbType.Int32, Answer.LocationID);
            db.AddInParameter(cmd, "@XMLis", DbType.String, Answer.XMLis);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Answer.CUID);
            int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
            Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Values[1] = Value.ToString();
            return Values;
        }

        public DataSet GetAnswerKeyDetails(PLAnswer Answer)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Sp_GetAnswerKey");
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Answer.QuestionID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public int ImportAnswer(PLAnswer Answer)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("dbo.Answer_Import");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Answer.CompanyID);
            db.AddInParameter(cmd, "QuestionId", DbType.Int32, Answer.QuestionID);
            db.AddInParameter(cmd, "UserId", DbType.Int32, Answer.CUID);
            db.AddInParameter(cmd, "XMLIs", DbType.Xml, Answer.ExcelUploadXMLIs);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
    }
}
