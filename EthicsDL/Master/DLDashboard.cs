﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using EthicsPL.Master;
using Microsoft.Practices.EnterpriseLibrary.Data;
using OnlineTrainingPL;

namespace OnlineTrainingDL.Master
{
    public class DLDashboard
    {

        public DataSet GetDepartmentTopics(int groupID)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("GetTotalTrainings");
                db.AddInParameter(cmd, "@groupID", DbType.Int32, groupID);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetDashboardDetails(PLDashboard objplDashboard)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetDashboardDet");
                db.AddInParameter(cmd, "@UserId", DbType.Int32, objplDashboard.CUID);
                db.AddInParameter(cmd, "@StartDate", DbType.DateTime, objplDashboard.Startdate);
                db.AddInParameter(cmd, "@EndDate", DbType.DateTime, objplDashboard.Enddate);
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objplDashboard.CompanyID);
                db.AddInParameter(cmd, "@RoleName", DbType.String, objplDashboard.RoleName);

                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetDashboardRawDetails(PLDashboard objplDashboard)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetRawDataDetails");
                db.AddInParameter(cmd, "@UserId", DbType.Int32, objplDashboard.CUID);
                db.AddInParameter(cmd, "@StartDate", DbType.DateTime, objplDashboard.Startdate);
                db.AddInParameter(cmd, "@EndDate", DbType.DateTime, objplDashboard.Enddate);
                db.AddInParameter(cmd, "@Type", DbType.Int32, objplDashboard.Type);
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objplDashboard.CompanyID);
                db.AddInParameter(cmd, "@RoleName", DbType.String, objplDashboard.RoleName);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
