﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingPL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;
using OnlineTrainingPL.Master;

namespace OnlineTrainingDL.Master
{
    public class DLEmployee
    {
        public string[] InsertUpdateEmployee(PLEmployee Employee)
        {
            string[] Values = new string[3];
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdate_Employee");
            db.AddInParameter(cmd, "@EmployeeID", DbType.Int32, Employee.EmployeeID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Employee.CompanyID);
            db.AddInParameter(cmd, "@FirstName", DbType.String, Employee.FirstName);
            db.AddInParameter(cmd, "@LastName", DbType.String, Employee.LastName);
            db.AddInParameter(cmd, "@EmailID", DbType.String, Employee.EmailID);
            db.AddInParameter(cmd, "@DepartmentName", DbType.String, Employee.DepartmentName);
            db.AddInParameter(cmd, "@Designation", DbType.String, Employee.Designation);
            db.AddInParameter(cmd, "@LocationID", DbType.Int32, Employee.LocationID);
            db.AddInParameter(cmd, "@EmployeeCode", DbType.String, Employee.EmployeeCode);
            db.AddInParameter(cmd, "@MainUserID", DbType.Int32, Employee.MainUserID);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Employee.CUID);
            int Value = Convert.ToInt32(db.ExecuteScalar(cmd));
            Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Values[1] = Value.ToString();
            return Values;
        }

        public DataSet GetPassReports(PLEmployee Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetPassReports");
            //db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Question.CompanyID);
            //db.AddInParameter(cmd, "@LocationID", DbType.Int32, Question.LocationID);
            db.AddInParameter(cmd, "@UserID", DbType.Int32, Question.UserID);
            db.AddInParameter(cmd, "@FromDate", DbType.DateTime, Question.FromDate);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            db.AddInParameter(cmd, "@ToDate", DbType.DateTime, Question.ToDate);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetSummaryReports(PLEmployee Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetSummaryReports");
            db.AddInParameter(cmd, "@FromDate", DbType.DateTime, Question.FromDate);
            db.AddInParameter(cmd, "@ToDate", DbType.DateTime, Question.ToDate);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public DataSet GetParticaptesDetails(PLEmployee Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetParticaptesLists");
            db.AddInParameter(cmd, "@FromDate", DbType.DateTime, Question.FromDate);
            db.AddInParameter(cmd, "@ToDate", DbType.DateTime, Question.ToDate);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public DataSet GetParticaptesPassDetails(PLEmployee Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetParticaptesPassLists");
            db.AddInParameter(cmd, "@FromDate", DbType.DateTime, Question.FromDate);
            db.AddInParameter(cmd, "@ToDate", DbType.DateTime, Question.ToDate);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetParticipantsList(PLEmployee Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetParticipantsList");
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetParticaptesFailDetails(PLEmployee Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetParticaptesFailLists");
            db.AddInParameter(cmd, "@FromDate", DbType.DateTime, Question.FromDate);
            db.AddInParameter(cmd, "@ToDate", DbType.DateTime, Question.ToDate);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }




        public DataSet MainLoginUserID(PLEmployee Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Get_MainLoginUserID");
            db.AddInParameter(cmd, "@EmailID", DbType.String, Question.EmailID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }
    }
}
