﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingPL.Master;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;

namespace OnlineTrainingDL.Master
{
   public class DLUser
    {
       public DataSet UserLoginGet(PLUser User)
       {
           Database db = DatabaseFactory.CreateDatabase("DBiEthics");
           DbCommand cmd = db.GetStoredProcCommand("UserLoginGet");
           db.AddInParameter(cmd, "UserName", DbType.String, User.UserName);
           db.AddInParameter(cmd, "Password", DbType.String, User.Password);
           return db.ExecuteDataSet(cmd);
       }


       public DataSet GetUserBind(PLUser User)
       {
           DataSet ds = new DataSet();
           Database db = DatabaseFactory.CreateDatabase("DBiEthics");
           DbCommand cmd = db.GetStoredProcCommand("SP_Userbind");
           db.AddInParameter(cmd, "@CompanyID", DbType.Int32, User.CompanyID);
           ds = db.ExecuteDataSet(cmd);
           return ds;
       }




    }
}
