﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingPL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;
using OnlineTrainingPL.Master;

namespace OnlineTrainingDL.Master
{
    public class DLDepartment
    {

        public DataSet GetDepartmentDetails(PLDepartment objDepartment)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetDepartmentDet");
                db.AddInParameter(cmd, "@GroupId", DbType.Int32, objDepartment.GroupId);
                db.AddInParameter(cmd, "@DepartmentID", DbType.Int32, objDepartment.DepartmentId);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }


        }

        public string[] InsertUpdateDepartment(PLDepartment objDepartment)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdateDepartment");
                db.AddInParameter(cmd, "@groupId", DbType.Int32, objDepartment.GroupId);
                db.AddInParameter(cmd, "@DepartmentID", DbType.Int32, objDepartment.DepartmentId);
                db.AddInParameter(cmd, "@DepartmentName", DbType.String, objDepartment.DepartmentName);
                db.AddInParameter(cmd, "@ShortName", DbType.String, objDepartment.ShortName.Trim());
                db.AddInParameter(cmd, "@Uid", DbType.Int32, objDepartment.CUID);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }


        }

        public string DeleteDepartmentDetails(PLDepartment objDepartment)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("SP_DeleteDepartment");
                db.AddInParameter(cmd, "@DepartmentID", DbType.Int32, objDepartment.DepartmentId);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.ExecuteNonQuery(cmd);
                string results = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                return results;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
