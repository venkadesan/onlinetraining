﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using OnlineTrainingPL;

namespace OnlineTrainingDL.Master
{
    public class DLForms
    {
        public DataSet GetMainMenus(PLForms forms)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetMainMenu");
            db.AddInParameter(cmd, "toolid", DbType.Int32, forms.ToolId);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public void InsertMainMenus(PLForms forms)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("InsertUpdateMainMenu");
            db.AddInParameter(cmd, "MainMenuID", DbType.Int32, forms.MainFormid);
            db.AddInParameter(cmd, "MenuName", DbType.String, forms.MenFormText);
            db.AddInParameter(cmd, "MenuUrl", DbType.String, forms.MenFormName);
            db.AddInParameter(cmd, "Order", DbType.Int32, forms.Order);
            db.AddInParameter(cmd, "toolid", DbType.Int32, forms.ToolId);
            db.AddInParameter(cmd, "isVisible", DbType.Boolean, forms.IsVisible);
            db.AddInParameter(cmd, "Css", DbType.String, forms.Css);
            db.ExecuteNonQuery(cmd);
        }

        public DataSet GetSubMenus(PLForms forms)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetSubMenu");
            db.AddInParameter(cmd, "toolid", DbType.Int32, forms.ToolId);
            db.AddInParameter(cmd, "mainformid", DbType.Int32, forms.MainFormid);

            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public void InsertSubMenus(PLForms forms)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("InsertUpdateSubMenu");
            db.AddInParameter(cmd, "MainMenuID", DbType.Int32, forms.MainFormid);
            db.AddInParameter(cmd, "MenuName", DbType.String, forms.SubFormText);
            db.AddInParameter(cmd, "MenuUrl", DbType.String, forms.SubFormName);
            db.AddInParameter(cmd, "Order", DbType.Int32, forms.Order);
            db.AddInParameter(cmd, "isVisible", DbType.Boolean, forms.IsVisible);
            db.AddInParameter(cmd, "SubmMenuID", DbType.Int32, forms.SubFormid);
            db.AddInParameter(cmd, "Css", DbType.String, forms.Css);
            db.ExecuteNonQuery(cmd);
        }

        public DataSet GetMainMenusDet(PLForms forms)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetMainMenuDet");
            db.AddInParameter(cmd, "MainformsID", DbType.Int32, forms.MainFormid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetSubMenusDet(PLForms forms)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetSubMenuDet");
            db.AddInParameter(cmd, "SubFormsID", DbType.Int32, forms.SubFormid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public void DeleteMainMenus(PLForms forms)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("DeleteMainmenus");
            db.AddInParameter(cmd, "MainmenusID", DbType.Int32, forms.MainFormid);
            db.ExecuteNonQuery(cmd);
        }

        public void DeleteSubMenus(PLForms forms)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("DeleteSubmenus");
            db.AddInParameter(cmd, "submenuID", DbType.Int32, forms.SubFormid);
            db.ExecuteNonQuery(cmd);
        }
    }
}
