﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingPL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;
using OnlineTrainingPL.Master;

namespace OnlineTrainingDL.Master
{
    public class DLUserDepartment
    {

        public DataSet GetDepartmentDetails(PLUserDepartment objPlUserDepartment)
        {
            try
            {


            }
            catch (Exception)
            {

                throw;
            }
            return null;
        }

        public string[] InsertUpdateUserDepartment(PLUserDepartment objUserDepartment)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdateUserDepartment");
                db.AddInParameter(cmd, "@GroupId", DbType.Int32,objUserDepartment.GroupId);
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objUserDepartment.CompanyId);
                db.AddInParameter(cmd, "@UserDepartmentID", DbType.Int32, objUserDepartment.UserDepartmentId);
                db.AddInParameter(cmd, "@Department", DbType.Int32, objUserDepartment.DepartmentId);
                db.AddInParameter(cmd, "@Topic", DbType.Int32, objUserDepartment.TopicId);
                db.AddInParameter(cmd, "@Editor", DbType.String, objUserDepartment.Editor.Trim());
                db.AddInParameter(cmd, "@StartDate", DbType.String, Convert.ToString(objUserDepartment.StartDate));
                db.AddInParameter(cmd, "@EndDate", DbType.String, Convert.ToString(objUserDepartment.EndDate));
                db.AddInParameter(cmd, "@UserID", DbType.Int32, objUserDepartment.UId);
                db.AddInParameter(cmd, "@UserXml", DbType.String, objUserDepartment.UserXml);
                db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
                db.AddOutParameter(cmd, "@Status", DbType.String, 10);
                db.ExecuteNonQuery(cmd);
                Values[0] = string.Format(CultureInfo.CurrentCulture, "{0}",
                db.GetParameterValue(cmd, "@ErrorMessage"));
                Values[1] = db.GetParameterValue(cmd, "@Status").ToString();
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetUserBind(PLUserDepartment objUserDepartment)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetUserDepartmentName");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, objUserDepartment.CompanyId);
            db.AddInParameter(cmd, "@DepartmentID", DbType.Int32, objUserDepartment.DepartmentId);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, objUserDepartment.TopicId);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

     
        
    }
}
