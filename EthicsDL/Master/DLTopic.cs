﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingPL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;

namespace OnlineTrainingDL
{
    public class DLTopic
    {
        public DataSet GetTopicDetails(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetTopic_Details");
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Topic.TopicID);
            db.AddInParameter(cmd, "@GroupID", DbType.Int32, Topic.Groupid);
            db.AddInParameter(cmd, "@Attempts", DbType.Int32, Topic.Attempts);
            //db.AddInParameter(cmd, "@LocationID", DbType.Int32, Topic.LocationID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetCurrentTopicQAReports(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetCurrentTopicQAReports");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Topic.CompanyID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetTopicByActiveStatus(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetTopicByActiveStatus");
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Topic.TopicID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Topic.CompanyID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetUserByUserId(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetUserByUserId");
            db.AddInParameter(cmd, "@locationid", DbType.Int32, Topic.LocationID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetActiveTopicDetails(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetActiveTopic_Details");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Topic.CompanyID);
            db.AddInParameter(cmd, "@topicid", DbType.Int32, Topic.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public string DeleteTopicDetails(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_Delete_Topic");
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Topic.TopicID);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.ExecuteNonQuery(cmd);
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            return results;
        }

        public int InsertUpdateTopic(PLTopic Topic)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdate_Topic");
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Topic.TopicID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Topic.CompanyID);
            // db.AddInParameter(cmd, "@LocationID", DbType.Int32, Topic.LocationID);
            db.AddInParameter(cmd, "@TopicName", DbType.String, Topic.TopicName.Trim());
            db.AddInParameter(cmd, "@Description", DbType.String, Topic.Description.Trim());
            db.AddInParameter(cmd, "@PassPercentage", DbType.Int32, Topic.Percentage);
            db.AddInParameter(cmd, "@FileName", DbType.String, Topic.FileName.Trim());
            db.AddInParameter(cmd, "@displayname", DbType.String, Topic.DisplayName.Trim());
            db.AddInParameter(cmd, "@ResolutionTime", DbType.String, Topic.ResolutionTime.Trim());
            db.AddInParameter(cmd, "@DepartmentID", DbType.Int32, Topic.DepartmentId);
            db.AddInParameter(cmd, "@GroupId", DbType.Int32, Topic.Groupid);

            db.AddInParameter(cmd, "@TrainingDuration", DbType.String, Topic.TestDuration);
            //db.AddInParameter(cmd, "@TestInstruction", DbType.String, Topic.TestInstruction);
            db.AddInParameter(cmd, "@Attempts", DbType.Int32, Topic.Attempts);
            db.AddInParameter(cmd, "@IsTestRequired", DbType.Boolean, Topic.IsTestRequired);

            db.AddInParameter(cmd, "@FileName2", DbType.String, Topic.FileName2.Trim());
            db.AddInParameter(cmd, "@displayname2", DbType.String, Topic.DisplayName2.Trim());

            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Topic.CUID);
            db.AddInParameter(cmd, "@IsActive", DbType.Boolean, Topic.IsActive);
            int IDs = Convert.ToInt32(db.ExecuteScalar(cmd));
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Topic.ErrorMessage = results;
            return IDs;
        }

        public int UpdateTopicStatus(PLTopic Topic)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_UpdateActiveStatus_Topic");
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Topic.TopicID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Topic.CompanyID);
            db.AddInParameter(cmd, "@IsActive", DbType.Boolean, Topic.IsActive);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            int IDs = Convert.ToInt32(db.ExecuteScalar(cmd));
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Topic.ErrorMessage = results;
            return IDs;
        }

        public string CheckActiveTopic(int mode, PLTopic Topic)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_CheckActive_Topic");
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Topic.TopicID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Topic.CompanyID);
            db.AddInParameter(cmd, "@Mode", DbType.Int32, mode);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            int IDs = Convert.ToInt32(db.ExecuteScalar(cmd));
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Topic.ErrorMessage = results;
            return results;
        }

        public DataSet CheckPassedstatus(string EmailID, PLTopic Topic)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("[GetisPassed]");
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Topic.TopicID);
            db.AddInParameter(cmd, "@EmailID", DbType.String, EmailID);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public int Getuser(string emailID)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Getuser");
            db.AddInParameter(cmd, "@EmailID", DbType.String, emailID);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public int GettotalAttempt(string emailID)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("totalAttempt");
            db.AddInParameter(cmd, "@EmailID", DbType.String, emailID);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }

        public DataSet GetCompany(int groupID)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetAllCompany");
            db.AddInParameter(cmd, "groupID", DbType.Int32, groupID);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetLocation(int companyID)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetAllLocation");
            db.AddInParameter(cmd, "companyID", DbType.Int32, companyID);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetAllScore(int userID, int topicID)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetAllScore");
            db.AddInParameter(cmd, "topicID", DbType.Int32, topicID);
            db.AddInParameter(cmd, "userID", DbType.Int32, userID);
            DataSet ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        #region CommonTopics

        public int InsertUpdateCommonTopic(PLTopic Topic)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdate_CommonTopic");
            db.AddInParameter(cmd, "@CommonTopicID", DbType.Int32, Topic.TopicID);
            db.AddInParameter(cmd, "@TopicName", DbType.String, Topic.TopicName.Trim());
            db.AddInParameter(cmd, "@Description", DbType.String, Topic.Description.Trim());
            db.AddInParameter(cmd, "@PassPercentage", DbType.Int32, Topic.Percentage);
            db.AddInParameter(cmd, "@FileName", DbType.String, Topic.FileName.Trim());
            db.AddInParameter(cmd, "@displayname", DbType.String, Topic.DisplayName.Trim());
            db.AddInParameter(cmd, "@ResolutionTime", DbType.String, Topic.ResolutionTime.Trim());

            db.AddInParameter(cmd, "@TrainingDuration", DbType.String, Topic.TestDuration);
            db.AddInParameter(cmd, "@TestInstruction", DbType.String, Topic.TestInstruction);
            db.AddInParameter(cmd, "@Attempts", DbType.String, Topic.Attempts);

            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Topic.CUID);
            int IDs = Convert.ToInt32(db.ExecuteScalar(cmd));
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Topic.ErrorMessage = results;
            return IDs;
        }

        public DataSet GetCommonTopicDetails(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetCommonTopic_Details");
            db.AddInParameter(cmd, "@CommonTopicID", DbType.Int32, Topic.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public string DeleteCommonTopicDetails(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_Delete_CommonTopic");
            db.AddInParameter(cmd, "@CommonTopicID", DbType.Int32, Topic.TopicID);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.ExecuteNonQuery(cmd);
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            return results;
        }


        public int InsertUpdateTopicFromCommon(PLTopic Topic)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertMultiple_Topic");
            db.AddInParameter(cmd, "@TopicXml", DbType.String, Topic.TopicXml);
            db.AddInParameter(cmd, "@DepartmentID", DbType.Int32, Topic.DepartmentId);
            db.AddInParameter(cmd, "@GroupId", DbType.Int32, Topic.Groupid);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Topic.CUID);
            db.AddInParameter(cmd, "@IsActive", DbType.Boolean, Topic.IsActive);
            int IDs = Convert.ToInt32(db.ExecuteScalar(cmd));
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Topic.ErrorMessage = results;
            return IDs;
        }

        public DataSet GetAssignedTopics(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_getAssignedTopics");
            db.AddInParameter(cmd, "@GroupId", DbType.Int32, Topic.Groupid);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Topic.CompanyID);
            db.AddInParameter(cmd, "@DepartmentID", DbType.Int32, Topic.DepartmentId);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public int AddCompanyTopicAssign(PLTopic Topic)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("AddCompanyTopicAssign");
            db.AddInParameter(cmd, "@AssignXml", DbType.String, Topic.TopicXml);
            db.AddInParameter(cmd, "@Companyid", DbType.Int32, Topic.CompanyID);
            db.AddInParameter(cmd, "@GroupId", DbType.Int32, Topic.Groupid);
            db.AddInParameter(cmd, "@departmentid", DbType.Int32, Topic.DepartmentId);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@uid", DbType.Int32, Topic.CUID);
            int IDs = Convert.ToInt32(db.ExecuteScalar(cmd));
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Topic.ErrorMessage = results;
            return IDs;
        }

        public DataSet GetTopicByCompanyID(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetTopicByCompanyID");
            db.AddInParameter(cmd, "@CompanyId", DbType.Int32, Topic.CompanyID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetTopicNameByUserID(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("sp_GetDepartmentTopicName");
            db.AddInParameter(cmd,"@CompanyId",DbType.Int32,Topic.CompanyID);
            db.AddInParameter(cmd, "@UserID", DbType.Int32, Topic.UserID);
            db.AddInParameter(cmd, "@CurrentDate", DbType.DateTime, DateTime.Now);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }
        
        public DataSet getTopicByDepartment(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_getTopicByDepartment");
            db.AddInParameter(cmd, "@DepartmentID", DbType.Int32, Topic.DepartmentId);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetActiveTopicDetailsWithGroup(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetActiveTopic_Details_WithGroup");
            db.AddInParameter(cmd, "@Groupid", DbType.Int32, Topic.Groupid);
            db.AddInParameter(cmd, "@topicid", DbType.Int32, Topic.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetCurrentTopicQAReportsWithGroup(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("GetCurrentTopicQAReportsWithGroup");
            db.AddInParameter(cmd, "@GroupId", DbType.Int32, Topic.Groupid);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }


        public void InsertTrainingDetails(PLTopic Topic)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertOnlineTrainingDet");
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Topic.TopicID);
            db.AddInParameter(cmd, "@UserId", DbType.Int32, Topic.CUID);
            db.AddInParameter(cmd, "@TrainingDate", DbType.DateTime, Topic.TrainingDate);
            db.ExecuteNonQuery(cmd);
        }

        #endregion


        #region "Check Max Attempts"

        public DataSet CheckMaximumAttempts(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_CheckMaxiAttempts");
            db.AddInParameter(cmd, "@userid", DbType.Int32, Topic.UserID);
            db.AddInParameter(cmd, "@topicid", DbType.Int32, Topic.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        #endregion

        public DataSet GetAnswerDetails(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetanswerDetails");
            db.AddInParameter(cmd, "@topicid", DbType.Int32, Topic.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetRoleName(PLTopic Topic)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("sp_GetRoleName");
            db.AddInParameter(cmd, "@roleid", DbType.Int32, Topic.RoleID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }
    }

}
