﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingPL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;

namespace OnlineTrainingDL
{
    public class DLQuestion
    {
        public DataSet GetQuestionDetails(PLQuestion Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetQuestion_Details");
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Question.QuestionID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Question.CompanyID);
            //db.AddInParameter(cmd, "@LocationID", DbType.Int32, Question.LocationID);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetQuestionAnswerReport(PLQuestion Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("Qry_GetQuestionAnswerReport");
            //db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Question.QuestionID);
            //db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Question.CompanyID);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            db.AddInParameter(cmd, "@UserId", DbType.Int32, Question.CUID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetQuestionAnswerReports(PLQuestion Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetQuestionAnswerReports");
            //db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Question.QuestionID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Question.CompanyID);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            db.AddInParameter(cmd, "@UserId", DbType.Int32, Question.CUID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetALLQuestionDetails(PLQuestion Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetAllQuestion_Details");            
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Question.CompanyID);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Question.QuestionID);            
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetALLTopicQuestionDetails(PLQuestion Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetAllTopicQuestion_Details");
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Question.CompanyID);
            //  db.AddInParameter(cmd, "@LocationID", DbType.Int32, Question.LocationID);
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Question.QuestionID);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }

        public DataSet GetProductDetails(PLQuestion Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_GetProduct_Details");            
            ds = db.ExecuteDataSet(cmd);
            return ds;
        }
       
        public string DeleteQuestionDetails(PLQuestion Question)
        {
            DataSet ds = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_Delete_Question");
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Question.QuestionID);
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.ExecuteNonQuery(cmd);
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            return results;
        }

        public int InsertUpdateQuestion(PLQuestion Question)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("SP_InsertUpdate_Question");
            db.AddInParameter(cmd, "@QuestionID", DbType.Int32, Question.QuestionID);
            db.AddInParameter(cmd, "@CompanyID", DbType.Int32, Question.CompanyID);
            //db.AddInParameter(cmd, "@LocationID", DbType.Int32, Question.LocationID);
            db.AddInParameter(cmd, "@TopicID", DbType.Int32, Question.TopicID);
            db.AddInParameter(cmd, "@QuestionName", DbType.String, Question.QuestionName.Trim());
            //db.AddInParameter(cmd, "@ShortName", DbType.String, Question.ShortName.Trim());
            db.AddOutParameter(cmd, "@ErrorMessage", DbType.String, 1000);
            db.AddInParameter(cmd, "@UID", DbType.Int32, Question.CUID);
            int IDs = Convert.ToInt32(db.ExecuteScalar(cmd));
            string results = string.Format(CultureInfo.CurrentCulture, "{0}",
            db.GetParameterValue(cmd, "@ErrorMessage"));
            Question.ErrorMessage = results;
            return IDs;            
        }

        public int ImportQuestion(PLQuestion Question)
        {
            Database db = DatabaseFactory.CreateDatabase("DBiEthics");
            DbCommand cmd = db.GetStoredProcCommand("dbo.Question_Import");
            db.AddInParameter(cmd, "CompanyId", DbType.Int32, Question.CompanyID);
            db.AddInParameter(cmd, "TopicId", DbType.Int32, Question.TopicID);
            db.AddInParameter(cmd, "UserId", DbType.Int32, Question.CUID);
            db.AddInParameter(cmd, "XMLIs", DbType.Xml, Question.ExcelUploadXMLIs);
            return Convert.ToInt32(db.ExecuteScalar(cmd));
        }
    }
}
