﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using OnlineTrainingPL;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Globalization;
using OnlineTrainingPL.Master;

namespace OnlineTrainingDL.Master
{
    public class DLTextInstruction
    {

        public DataSet GetTextInstructions(PLTextInstruction objTextInstruction)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("GetTextInstruction");
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objTextInstruction.CompanyId);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataSet GetNoofAttempts(PLTextInstruction objTextInstruction)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetTotalAttempts");
                db.AddInParameter(cmd, "@UserId", DbType.Int32, objTextInstruction.UserId);
                db.AddInParameter(cmd, "@TopicId", DbType.Int32, objTextInstruction.TopicId);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string[] SaveUpdateTextInstructions(PLTextInstruction objTextInstruction)
        {
            try
            {
                string[] Values = new string[3];
                Database db = DatabaseFactory.CreateDatabase("DBiEthics");
                DbCommand cmd = db.GetStoredProcCommand("sp_SaveUpdateTextInstruction");
                db.AddInParameter(cmd, "@CompanyId", DbType.Int32, objTextInstruction.CompanyId);
                db.AddInParameter(cmd, "@InstructionID", DbType.Int32, objTextInstruction.TextInstructionId);
                db.AddInParameter(cmd, "@TextInstructions", DbType.String, objTextInstruction.TextInstructionText);
                db.ExecuteNonQuery(cmd);
                return Values;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
