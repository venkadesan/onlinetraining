﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineTrainingPL
{
    public class PLCommon
    {
        public int CompanyID { get; set; }

        public int UserID { get; set; }

        public int DepartmentID { get; set; }

        public int TopicID { get; set; }
        public int TopicName { get; set; }

        public DateTime StartDate { get; set; } 

        public DateTime EndDate { get; set; }

        public int LocationID { get; set; }

        public int CUID { get; set; }

        public string  ShortName { get; set; }

        public DateTime CDate { get; set; }

        public int MUID { get; set; }

        public DateTime MDate { get; set; }

        public string RoleName { get; set; }
    }
}
