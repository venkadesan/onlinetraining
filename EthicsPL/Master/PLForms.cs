﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineTrainingPL
{
    public class PLForms
    {
        public int MainFormid { get; set; }

        public int SubFormid { get; set; }

        public string MenFormText { get; set; }

        public string SubFormText { get; set; }

        public string MenFormName { get; set; }

        public string SubFormName { get; set; }

        public bool IsVisible { get; set; }

        public int Order { get; set; }

        public int ToolId { get; set; }

        public string Css { get; set; }
    }
}
