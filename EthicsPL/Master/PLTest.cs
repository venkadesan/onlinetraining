﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineTrainingPL.Master
{
    public class PLTest:PLCommon
    {
        public int TestID { get; set; }

        public int AnswerID { get; set; }

        public int QuestionID { get; set; }

        public int TopicID { get; set; }

        public int PassPercentage { get; set; }

        public string AnswerName { get; set; }

        public string Declaration { get; set; }

        public string XMLis { get; set; }
    }
}
