﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineTrainingPL
{
    public class PLEmployee : PLCommon
    {
        public int EmployeeID { get; set; }

        public int MainUserID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailID { get; set; }

        public string EmployeeCode { get; set; }

        public string Designation { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public int TopicID { get; set; }

        public int ClientID { get; set; }

        public string DepartmentName { get; set; }
    }
}
