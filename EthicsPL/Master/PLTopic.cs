﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineTrainingPL
{
    public class PLTopic : PLCommon
    {
        public int TestID { get; set; }

        public int TopicID { get; set; }

        public string TopicName { get; set; }

        public string Description { get; set; }

        public int Percentage { get; set; }

        public string FileName { get; set; }

        public string DisplayName { get; set; }

        public string ResolutionTime { get; set; }

        public string ErrorMessage { get; set; }

        public bool IsActive { get; set; }        

        public int Groupid { get; set; }

        public int DepartmentId { get; set; }

        public string TopicXml { get; set; }

        public string TestDuration { get; set; }

        public string TestInstruction { get; set; }

        public string Attempts { get; set; }

        public bool IsTestRequired { get; set; }

        public DateTime TrainingDate { get; set; }

        public string DisplayName2 { get; set; }

        public string FileName2 { get; set; }

        public int RoleID{ get; set; }

    }
}
