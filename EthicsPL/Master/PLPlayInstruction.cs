﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OnlineTrainingPL;

namespace OnlineTrainingPL.Master
{
    public class PLPlayInstruction
    {
        public int CompanyId { get; set; }
        public int PlayInstructionId { get; set; }
        public string PlayInstructionText { get; set; }
    }
}
