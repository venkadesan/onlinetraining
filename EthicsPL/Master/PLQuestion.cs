﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineTrainingPL
{
    public class PLQuestion : PLCommon
    {
        public int QuestionID { get; set; }

        public int TopicID { get; set; }

        public int QuestionSpecID { get; set; }

        public string QuestionName { get; set; }

        public string ErrorMessage { get; set; }

        public int Status { get; set; }

        public string ExcelUploadXMLIs { get; set; }
    }
}
