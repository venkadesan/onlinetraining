﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OnlineTrainingPL;

namespace OnlineTrainingPL
{
    public class PLDepartment : PLCommon
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int GroupId { get; set; }
    }
}
