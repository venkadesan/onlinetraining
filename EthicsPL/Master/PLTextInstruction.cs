﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OnlineTrainingPL;


namespace OnlineTrainingPL.Master
{
    public class PLTextInstruction
    {
        public int CompanyId { get; set; }
        public int TextInstructionId { get; set; }
        public string TextInstructionText { get; set; }

        public int UserId { get; set; }
        public int TopicId { get; set; }
    }
}
