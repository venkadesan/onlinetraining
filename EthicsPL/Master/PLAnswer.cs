﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineTrainingPL
{
    public class  PLAnswer : PLCommon
    {
        public int TopicID { get; set; }

        public int AnswerID { get; set; }

        public int QuestionID { get; set; }

        public string AnswerName { get; set; }

        public string Options { get; set; }

        public string XMLis { get; set; }

        public string ExcelUploadXMLIs { get; set; }
    }
}
