﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;  
using OnlineTrainingPL;

namespace OnlineTrainingPL.Master
{
    public class PLUserDepartment
    {
        public int CompanyId { get; set; }
        public int UserDepartmentId { get; set; }
        public int DepartmentId { get; set; }
        public int GroupId { get; set; }
        public int TopicId { get; set; }
        public string Editor { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int UId { get; set; }
        public string UserXml { get; set; }
       
        
    }
}
