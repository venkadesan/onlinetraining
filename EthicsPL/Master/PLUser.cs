﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineTrainingPL.Master
{
   public class PLUser:PLCommon
    {
        public int RoleId { get; set; }

        public int UserId { get; set; }

        public int CompanyID { get; set; }

        public string UserName { get; set; }

        public string ImagePath { get; set; }

        public string ImageName { get; set; }

        public string Email { get; set; }

        public string MobileNumber { get; set; }

        public string Password { get; set; }

       public string FirstName { get; set; }

       public int Department { get; set; }

       public int Topic { get; set; }



    }
}
