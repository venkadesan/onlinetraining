﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OnlineTrainingPL;

namespace EthicsPL.Master
{
    public class PLDashboard : PLCommon
    {
        public DateTime Startdate { set; get; }
        public DateTime Enddate { set; get; }
        public int Type { set; get; }
    }
}
